// 
// emu.h
// 
// Emulator
// 
// Authors:
// - MARCONATO Maxime (aka MaaaX, aka EPOCH84 / maxime@maaax.com)
// 

#ifndef LIBRETRO_EMUCV_EMU_H
#define LIBRETRO_EMUCV_EMU_H

#include "common.h"


#define EMU_MEMORYSIZE_PROGRAM	2032	// 2032 valid program addresses 
#define EMU_MEMORYSIZE_INVALID	16		// 16 invalid program addresses 
#define EMU_MEMORYSIZE_PATTERN	896		// 128 sprites (16 invalids) x 7 lines (8 bits)
#define EMU_MEMORYSIZE_RAM		128		// 32 x 4 x 7 bits = 896 bits
#define EMU_STACK_A_SIZE		3		// Size of the A stack (store 3 register A{11:1})
#define EMU_STACK_NRM_SIZE		12		// Size of the NRM stack (store 12 register H{5:1})

#define EMU_CLOCK_CPU			4000000	// 4MHz (undefined at this time!!!)

#define EMU_OPCODE_GRANULARITY	100		// 100 = 1 OPCODE

#define EMU_NTSC_LINES			525		// 525 lines
#define EMU_NTSC_H				96		// 
#define EMU_NTSC_H_HALF			48		// 
#define EMU_NTSC_FREQUENCY		60		// 60Hz (1 frame odd + 1 frame even = 30 pictures per sec, )

#define EMU_OK							0
#define EMU_ERROR_UNKNOWN_OPCODE		1
#define EMU_ERORR_STACK_NRM_UNDERFLOW	2
#define EMU_ERORR_STACK_NRM_OVERFLOW	3
#define EMU_ERORR_STACK_A_UNDERFLOW		4
#define EMU_ERORR_STACK_A_OVERFLOW		5

#define EMU_WIDTH	91	// 15 not visible on left (H BLANK)
#define EMU_HEIGHT	60	// 238.5H, 24H not visible on top
#define EMU_RATIO	4.0f/3.0f

#define x1 254
#define x2 220
#define x3 0

static const uint32_t emu_palette[64] =
{

	// P = PRIO / R = RED / B = GREEN / B = BLUE / h = hue / b = brightness
	
	// === COLORS ===
									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 000000 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 000001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 000010 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 000011 - ???

									// PRGBhb
	f_rgb_to_color( 82, 216, 255),	// 000100 - CYAN ok
	f_rgb_to_color( x1,  x2,  x3),	// 000101 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 000110 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 000111 - ???

									// PRGBhb
	f_rgb_to_color( 57, 230, 111),	// 001000 - GREEN 
	f_rgb_to_color( x1,  x2,  x3),	// 001001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 001010 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 001011 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 001100 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 001101 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 001110 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 001111 - ???

									// PRGBhb
	f_rgb_to_color(255, 135, 234),	// 010000 - PINK ok
	f_rgb_to_color( x1,  x2,  x3),	// 010001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 010010 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 010011 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 010100 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 010101 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 010110 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 010111 - ???

									// PRGBhb
	f_rgb_to_color(255, 181,   7),	// 011000 - ORANGE ok
	f_rgb_to_color( x1,  x2,  x3),	// 011001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 011010 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 011011 - ???

									// PRGBhb
	f_rgb_to_color(213, 213, 155),	// 011100 - WHITE ok
	f_rgb_to_color( x1,  x2,  x3),	// 011101 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 011110 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 011111 - ???

	// === BLACK & WHITE ===
									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 100000 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 100001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 100010 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 100011 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 100100 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 100101 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 100110 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 100111 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 101000 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 101001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 101010 - ???
	f_rgb_to_color(108, 183,   0),	// 101011 - GREEN ok

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 101100 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 101101 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 101110 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 101111 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 110000 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 110001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 110010 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 110011 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 110100 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 110101 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 110110 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 110111 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 111000 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 111001 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 111010 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 111011 - ???

									// PRGBhb
	f_rgb_to_color( x1,  x2,  x3),	// 111100 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 111100 - ???
	f_rgb_to_color( x1,  x2,  x3),	// 111100 - ???
	f_rgb_to_color( x1,  x2,  x3)	// 111111 - ???
};

class c_emu
{
public:
	c_emu();
	~c_emu();

	void f_insert_cart(const char *file_path);
	void f_eject_cart();
	bool f_is_cart_inserted();
	void f_run(uint32_t *video_buffer, uint32_t video_stride);
	void f_reset(bool reset_reg_a = true);

private:
	uint16_t address_linear_to_polynomial[EMU_MEMORYSIZE_PROGRAM]{};						// 2032 valid addresses 
	uint16_t address_polynomial_to_linear[EMU_MEMORYSIZE_PROGRAM+EMU_MEMORYSIZE_INVALID]{};	// 2032 valid addresses + 16 invalid addresses (flagged as 0xffff)

	uint16_t program_counter_linear;

	uint16_t reg_a;			// 11 bits program address
	uint8_t  reg_hl;		// 7 bits (hhhhhll)
	uint8_t  reg_l_prime;	// 2 bits
	uint8_t  reg_a1;		// 7 bits
	uint8_t  reg_a1_prime;	// 1 bit
	uint8_t  reg_a2;		// 7 bits
	uint8_t  reg_a3;		// 7 bits
	uint8_t  reg_a4;		// 7 bits
	uint8_t  reg_fls;		// 7 bits sound control 1
	uint8_t  reg_frs;		// 7 bits sound control 2
	uint8_t  reg_mode;		// 7 bits graphics+sounds control
	uint8_t  reg_x1_prime;	// 1 bit
	uint8_t  reg_x3;		// 7 bits
	uint8_t  reg_x4;		// 7 bits

	uint16_t cnt_vc;		// 10 bits vertical counter
	bool flg_v_field;		// Vertical field
	bool flg_v_blank;		// Vertical blank
//	bool flg_v_equal;		// Vertical equal
//	bool flg_v_sync;		// Vertical sync
	uint8_t cnt_hc;			// 7 bits horizontal counter
//	bool flg_h_4t;			// Horizontal 4T (HC4T)
	bool flg_h_blank;		// Horizontal blank (HBLK)
	bool flg_h_4blank;		// Horizontal blank (H4 BLK)
//	bool flg_h_sync;		// Horizontal sync (H SYNC)
//	bool flg_h_equal;		// Horizontal equal inverted (equal H/)

	uint8_t  reg_stb;		// 4 bits key STroBe. Specify key scan signal outputs level (S[4:1]/)

	uint16_t stk_a[EMU_STACK_A_SIZE];		// 11 bits Stack of 3 addresses
	uint8_t stk_a_level;					// Stack level

	uint16_t stk_nrm[EMU_STACK_NRM_SIZE];	// 5 bits Stack of 12 values
	uint8_t stk_nrm_level;					// Stack level


	bool flg_dsp;			// Display enable: 0 = display off (blank) / 1 = display on
	bool flg_gpe;			// Gunport enable: 0 = disabled / 1 = enabled
	bool flg_kie;			// Key input enable: 0 = disabled / 1 = enabled
	bool flg_sme;			// Shot input enable: 0 = disabled / 1 = enabled

	int16_t nb_opcodes_current;	// 
	int16_t nb_opcodes_step;	// How many opcodes are runned for each line x Granularity 

/*
	bool prt_gun;	// Gun Port Latch
	bool prt_pd1;	// PD1 input
	bool prt_pd2;	// PD2 input
	bool prt_pd3;	// PD3 input
	bool prt_pd4;	// PD4 input
*/

//	uint8_t *wbank[0x200];	// 1 x 512B
//	uint8_t *rbank[0x200];	// 1 x 512B
	uint16_t memory_program[EMU_MEMORYSIZE_PROGRAM];
	uint8_t  memory_pattern[EMU_MEMORYSIZE_PATTERN];
	uint8_t  memory_ram[EMU_MEMORYSIZE_RAM];
	
	uint8_t error;
//	uint8_t wdmy[0x80];		// 1 x 128B
//	uint8_t rdmy[0x80];		// 1 x 128B

	bool cart_found;		// Is the cart file found?
	bool cart_inserted;		// Is there a cart inserted?
//	bool cart_ok;			// Is the inserted cart controlled OK? (if not correct the cart will be automatically ejected)
};


#endif // #ifndef LIBRETRO_EMUCV_EMU_H
