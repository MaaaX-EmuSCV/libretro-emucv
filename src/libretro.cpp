// 
// Libretro.cpp
// 
// Libretro wrapper
// 
// Authors:
// - MARCONATO Maxime (aka MaaaX, aka EPOCH84 / maxime@maaax.com)
// 

#include "core.h"


// Static Libretro core instance
static c_core o_core;

//
// Set the libretro environment
//
void retro_set_environment(retro_environment_t cb)
{
	o_core.f_retro_set_environment(cb);
}

//
// Set the Libretro video callback
//
void retro_set_video_refresh(retro_video_refresh_t cb)
{
	o_core.f_retro_set_video_refresh(cb);
}

//
// Set the Libretro audio sample callback
//
void retro_set_audio_sample(retro_audio_sample_t cb)
{
	o_core.f_retro_set_audio_sample(cb);
}

//
// Set the Libretro audio batch callback
//
void retro_set_audio_sample_batch(retro_audio_sample_batch_t cb)
{
	o_core.f_retro_set_audio_sample_batch(cb);
}

//
// Set the Libretro input poll callback
//
void retro_set_input_poll(retro_input_poll_t cb)
{
	o_core.f_retro_set_input_poll(cb);
}

//
// Set the Libretro input state callback
//
void retro_set_input_state(retro_input_state_t cb)
{
	o_core.f_retro_set_input_state(cb);
}

//
// Return the Libretro version used by the core for compatibility check with the frontend
//
unsigned retro_api_version()
{
	return o_core.f_retro_get_api_version();
}

//
// Return the video standard used
//
unsigned retro_get_region()
{
	return o_core.f_retro_get_video_region();
}

//
// Get the system infos
//
void retro_get_system_info(struct retro_system_info *info)
{
	o_core.f_retro_get_system_info(info);
}

//
// Get the audio/video infos
//
void retro_get_system_av_info(struct retro_system_av_info *info)
{
	o_core.f_retro_get_av_info(info);
}

//
// Libretro audio callback
//
void retro_audio_cb()
{
/*
	// Log
	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] retro_audio_cb() => UNUSED\n", CORE_NAME);
	o_core.f_retro_audio_cb();
*/
}

//
// Libretro audio set state enable/disable callback
//
void retro_audio_set_state_cb(bool enable)
{
	o_core.f_retro_audio_set_state_cb(enable);
}

//
// Libretro frame time callback
//
void retro_frame_time_cb(retro_usec_t usec)
{
/*
	// Log
	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] retro_frame_time_cb() => UNUSED\n", CORE_NAME);
	o_core.f_retro_frame_time_cb(usec);
*/
}

//
// Initialize the Libretro core
//
void retro_init()
{
	o_core.f_retro_init(retro_audio_cb, retro_audio_set_state_cb, retro_frame_time_cb);
}

//
// Deinitialize the Libretro core
//
void retro_deinit()
{
	o_core.f_retro_deinit();
}

//
// Set a controller port device
//
void retro_set_controller_port_device(unsigned port, unsigned device)
{
	o_core.f_retro_set_controller_port_device(port, device);
}

//
// Load game (or load without game)
//
bool retro_load_game(const struct retro_game_info *info)
{
	return o_core.f_retro_load_game(info);
}

//
// Unload game
//
void retro_unload_game()
{
	o_core.f_retro_unload_game();
}

//
// Unused
// Load a "special" kind of game
//
bool retro_load_game_special(unsigned type, const struct retro_game_info *info, size_t num)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] retro_load_game_special() => UNUSED\n", CORE_NAME);
	return false;
}

//
// Run for only one frame
//
void retro_run()
{
	o_core.f_retro_run();
}

//
// Libretro reset
//
void retro_reset()
{
	o_core.f_retro_reset();
}

//
// Get save state size
//
size_t retro_serialize_size()
{
	return o_core.f_retro_serialize_size();
}

//
// Serialize save state data
//
bool retro_serialize(void *data, size_t size)
{
	return o_core.f_retro_serialize((void *)data, size);
}

//
// Unserialize save state data
//
bool retro_unserialize(const void *data, size_t size)
{
	return o_core.f_retro_unserialize((void *)data, size);
}

//
// Get memory data pointer
//
void *retro_get_memory_data(unsigned id)
{
	if (id != RETRO_MEMORY_SAVE_RAM)
		return NULL;

	return o_core.f_retro_get_memory_data();
}

//
// Get memory size
//
size_t retro_get_memory_size(unsigned id)
{
	if (id != RETRO_MEMORY_SAVE_RAM)
		return 0;

	return o_core.f_retro_get_memory_size(id);
}

//
// Unused
//
void retro_cheat_reset()
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] retro_cheat_reset() => UNUSED\n", CORE_NAME);
}

//
// Unused
//
void retro_cheat_set(unsigned index, bool enabled, const char *code)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] retro_cheat_set() => UNUSED\n", CORE_NAME);
}
