#include "glsym/glsym.h"
#include <stddef.h>
#define SYM(x) { "gl" #x, &(gl##x) }
const struct rglgen_sym_map rglgen_symbol_map[] = {
    SYM(DrawRangeElements),
    SYM(TexImage3D),
    SYM(TexSubImage3D),
    SYM(CopyTexSubImage3D),
    SYM(ActiveTexture),
    SYM(SampleCoverage),
    SYM(CompressedTexImage3D),
    SYM(CompressedTexImage2D),
    SYM(CompressedTexImage1D),
    SYM(CompressedTexSubImage3D),
    SYM(CompressedTexSubImage2D),
    SYM(CompressedTexSubImage1D),
    SYM(GetCompressedTexImage),
    SYM(ClientActiveTexture),
    SYM(MultiTexCoord1d),
    SYM(MultiTexCoord1dv),
    SYM(MultiTexCoord1f),
    SYM(MultiTexCoord1fv),
    SYM(MultiTexCoord1i),
    SYM(MultiTexCoord1iv),
    SYM(MultiTexCoord1s),
    SYM(MultiTexCoord1sv),
    SYM(MultiTexCoord2d),
    SYM(MultiTexCoord2dv),
    SYM(MultiTexCoord2f),
    SYM(MultiTexCoord2fv),
    SYM(MultiTexCoord2i),
    SYM(MultiTexCoord2iv),
    SYM(MultiTexCoord2s),
    SYM(MultiTexCoord2sv),
    SYM(MultiTexCoord3d),
    SYM(MultiTexCoord3dv),
    SYM(MultiTexCoord3f),
    SYM(MultiTexCoord3fv),
    SYM(MultiTexCoord3i),
    SYM(MultiTexCoord3iv),
    SYM(MultiTexCoord3s),
    SYM(MultiTexCoord3sv),
    SYM(MultiTexCoord4d),
    SYM(MultiTexCoord4dv),
    SYM(MultiTexCoord4f),
    SYM(MultiTexCoord4fv),
    SYM(MultiTexCoord4i),
    SYM(MultiTexCoord4iv),
    SYM(MultiTexCoord4s),
    SYM(MultiTexCoord4sv),
    SYM(LoadTransposeMatrixf),
    SYM(LoadTransposeMatrixd),
    SYM(MultTransposeMatrixf),
    SYM(MultTransposeMatrixd),
    SYM(BlendFuncSeparate),
    SYM(MultiDrawArrays),
    SYM(MultiDrawElements),
    SYM(PointParameterf),
    SYM(PointParameterfv),
    SYM(PointParameteri),
    SYM(PointParameteriv),
    SYM(FogCoordf),
    SYM(FogCoordfv),
    SYM(FogCoordd),
    SYM(FogCoorddv),
    SYM(FogCoordPointer),
    SYM(SecondaryColor3b),
    SYM(SecondaryColor3bv),
    SYM(SecondaryColor3d),
    SYM(SecondaryColor3dv),
    SYM(SecondaryColor3f),
    SYM(SecondaryColor3fv),
    SYM(SecondaryColor3i),
    SYM(SecondaryColor3iv),
    SYM(SecondaryColor3s),
    SYM(SecondaryColor3sv),
    SYM(SecondaryColor3ub),
    SYM(SecondaryColor3ubv),
    SYM(SecondaryColor3ui),
    SYM(SecondaryColor3uiv),
    SYM(SecondaryColor3us),
    SYM(SecondaryColor3usv),
    SYM(SecondaryColorPointer),
    SYM(WindowPos2d),
    SYM(WindowPos2dv),
    SYM(WindowPos2f),
    SYM(WindowPos2fv),
    SYM(WindowPos2i),
    SYM(WindowPos2iv),
    SYM(WindowPos2s),
    SYM(WindowPos2sv),
    SYM(WindowPos3d),
    SYM(WindowPos3dv),
    SYM(WindowPos3f),
    SYM(WindowPos3fv),
    SYM(WindowPos3i),
    SYM(WindowPos3iv),
    SYM(WindowPos3s),
    SYM(WindowPos3sv),
    SYM(BlendColor),
    SYM(BlendEquation),
    SYM(GenQueries),
    SYM(DeleteQueries),
    SYM(IsQuery),
    SYM(BeginQuery),
    SYM(EndQuery),
    SYM(GetQueryiv),
    SYM(GetQueryObjectiv),
    SYM(GetQueryObjectuiv),
    SYM(BindBuffer),
    SYM(DeleteBuffers),
    SYM(GenBuffers),
    SYM(IsBuffer),
    SYM(BufferData),
    SYM(BufferSubData),
    SYM(GetBufferSubData),
    SYM(MapBuffer),
    SYM(UnmapBuffer),
    SYM(GetBufferParameteriv),
    SYM(GetBufferPointerv),
    SYM(BlendEquationSeparate),
    SYM(DrawBuffers),
    SYM(StencilOpSeparate),
    SYM(StencilFuncSeparate),
    SYM(StencilMaskSeparate),
    SYM(AttachShader),
    SYM(BindAttribLocation),
    SYM(CompileShader),
    SYM(CreateProgram),
    SYM(CreateShader),
    SYM(DeleteProgram),
    SYM(DeleteShader),
    SYM(DetachShader),
    SYM(DisableVertexAttribArray),
    SYM(EnableVertexAttribArray),
    SYM(GetActiveAttrib),
    SYM(GetActiveUniform),
    SYM(GetAttachedShaders),
    SYM(GetAttribLocation),
    SYM(GetProgramiv),
    SYM(GetProgramInfoLog),
    SYM(GetShaderiv),
    SYM(GetShaderInfoLog),
    SYM(GetShaderSource),
    SYM(GetUniformLocation),
    SYM(GetUniformfv),
    SYM(GetUniformiv),
    SYM(GetVertexAttribdv),
    SYM(GetVertexAttribfv),
    SYM(GetVertexAttribiv),
    SYM(GetVertexAttribPointerv),
    SYM(IsProgram),
    SYM(IsShader),
    SYM(LinkProgram),
    SYM(ShaderSource),
    SYM(UseProgram),
    SYM(Uniform1f),
    SYM(Uniform2f),
    SYM(Uniform3f),
    SYM(Uniform4f),
    SYM(Uniform1i),
    SYM(Uniform2i),
    SYM(Uniform3i),
    SYM(Uniform4i),
    SYM(Uniform1fv),
    SYM(Uniform2fv),
    SYM(Uniform3fv),
    SYM(Uniform4fv),
    SYM(Uniform1iv),
    SYM(Uniform2iv),
    SYM(Uniform3iv),
    SYM(Uniform4iv),
    SYM(UniformMatrix2fv),
    SYM(UniformMatrix3fv),
    SYM(UniformMatrix4fv),
    SYM(ValidateProgram),
    SYM(VertexAttrib1d),
    SYM(VertexAttrib1dv),
    SYM(VertexAttrib1f),
    SYM(VertexAttrib1fv),
    SYM(VertexAttrib1s),
    SYM(VertexAttrib1sv),
    SYM(VertexAttrib2d),
    SYM(VertexAttrib2dv),
    SYM(VertexAttrib2f),
    SYM(VertexAttrib2fv),
    SYM(VertexAttrib2s),
    SYM(VertexAttrib2sv),
    SYM(VertexAttrib3d),
    SYM(VertexAttrib3dv),
    SYM(VertexAttrib3f),
    SYM(VertexAttrib3fv),
    SYM(VertexAttrib3s),
    SYM(VertexAttrib3sv),
    SYM(VertexAttrib4Nbv),
    SYM(VertexAttrib4Niv),
    SYM(VertexAttrib4Nsv),
    SYM(VertexAttrib4Nub),
    SYM(VertexAttrib4Nubv),
    SYM(VertexAttrib4Nuiv),
    SYM(VertexAttrib4Nusv),
    SYM(VertexAttrib4bv),
    SYM(VertexAttrib4d),
    SYM(VertexAttrib4dv),
    SYM(VertexAttrib4f),
    SYM(VertexAttrib4fv),
    SYM(VertexAttrib4iv),
    SYM(VertexAttrib4s),
    SYM(VertexAttrib4sv),
    SYM(VertexAttrib4ubv),
    SYM(VertexAttrib4uiv),
    SYM(VertexAttrib4usv),
    SYM(VertexAttribPointer),
    SYM(UniformMatrix2x3fv),
    SYM(UniformMatrix3x2fv),
    SYM(UniformMatrix2x4fv),
    SYM(UniformMatrix4x2fv),
    SYM(UniformMatrix3x4fv),
    SYM(UniformMatrix4x3fv),
    SYM(ColorMaski),
    SYM(GetBooleani_v),
    SYM(GetIntegeri_v),
    SYM(Enablei),
    SYM(Disablei),
    SYM(IsEnabledi),
    SYM(BeginTransformFeedback),
    SYM(EndTransformFeedback),
    SYM(BindBufferRange),
    SYM(BindBufferBase),
    SYM(TransformFeedbackVaryings),
    SYM(GetTransformFeedbackVarying),
    SYM(ClampColor),
    SYM(BeginConditionalRender),
    SYM(EndConditionalRender),
    SYM(VertexAttribIPointer),
    SYM(GetVertexAttribIiv),
    SYM(GetVertexAttribIuiv),
    SYM(VertexAttribI1i),
    SYM(VertexAttribI2i),
    SYM(VertexAttribI3i),
    SYM(VertexAttribI4i),
    SYM(VertexAttribI1ui),
    SYM(VertexAttribI2ui),
    SYM(VertexAttribI3ui),
    SYM(VertexAttribI4ui),
    SYM(VertexAttribI1iv),
    SYM(VertexAttribI2iv),
    SYM(VertexAttribI3iv),
    SYM(VertexAttribI4iv),
    SYM(VertexAttribI1uiv),
    SYM(VertexAttribI2uiv),
    SYM(VertexAttribI3uiv),
    SYM(VertexAttribI4uiv),
    SYM(VertexAttribI4bv),
    SYM(VertexAttribI4sv),
    SYM(VertexAttribI4ubv),
    SYM(VertexAttribI4usv),
    SYM(GetUniformuiv),
    SYM(BindFragDataLocation),
    SYM(GetFragDataLocation),
    SYM(Uniform1ui),
    SYM(Uniform2ui),
    SYM(Uniform3ui),
    SYM(Uniform4ui),
    SYM(Uniform1uiv),
    SYM(Uniform2uiv),
    SYM(Uniform3uiv),
    SYM(Uniform4uiv),
    SYM(TexParameterIiv),
    SYM(TexParameterIuiv),
    SYM(GetTexParameterIiv),
    SYM(GetTexParameterIuiv),
    SYM(ClearBufferiv),
    SYM(ClearBufferuiv),
    SYM(ClearBufferfv),
    SYM(ClearBufferfi),
    SYM(GetStringi),
    SYM(IsRenderbuffer),
    SYM(BindRenderbuffer),
    SYM(DeleteRenderbuffers),
    SYM(GenRenderbuffers),
    SYM(RenderbufferStorage),
    SYM(GetRenderbufferParameteriv),
    SYM(IsFramebuffer),
    SYM(BindFramebuffer),
    SYM(DeleteFramebuffers),
    SYM(GenFramebuffers),
    SYM(CheckFramebufferStatus),
    SYM(FramebufferTexture1D),
    SYM(FramebufferTexture2D),
    SYM(FramebufferTexture3D),
    SYM(FramebufferRenderbuffer),
    SYM(GetFramebufferAttachmentParameteriv),
    SYM(GenerateMipmap),
    SYM(BlitFramebuffer),
    SYM(RenderbufferStorageMultisample),
    SYM(FramebufferTextureLayer),
    SYM(MapBufferRange),
    SYM(FlushMappedBufferRange),
    SYM(BindVertexArray),
    SYM(DeleteVertexArrays),
    SYM(GenVertexArrays),
    SYM(IsVertexArray),
    SYM(DrawArraysInstanced),
    SYM(DrawElementsInstanced),
    SYM(TexBuffer),
    SYM(PrimitiveRestartIndex),
    SYM(CopyBufferSubData),
    SYM(GetUniformIndices),
    SYM(GetActiveUniformsiv),
    SYM(GetActiveUniformName),
    SYM(GetUniformBlockIndex),
    SYM(GetActiveUniformBlockiv),
    SYM(GetActiveUniformBlockName),
    SYM(UniformBlockBinding),
    SYM(DrawElementsBaseVertex),
    SYM(DrawRangeElementsBaseVertex),
    SYM(DrawElementsInstancedBaseVertex),
    SYM(MultiDrawElementsBaseVertex),
    SYM(ProvokingVertex),
    SYM(FenceSync),
    SYM(IsSync),
    SYM(DeleteSync),
    SYM(ClientWaitSync),
    SYM(WaitSync),
    SYM(GetInteger64v),
    SYM(GetSynciv),
    SYM(GetInteger64i_v),
    SYM(GetBufferParameteri64v),
    SYM(FramebufferTexture),
    SYM(TexImage2DMultisample),
    SYM(TexImage3DMultisample),
    SYM(GetMultisamplefv),
    SYM(SampleMaski),
    SYM(BindFragDataLocationIndexed),
    SYM(GetFragDataIndex),
    SYM(GenSamplers),
    SYM(DeleteSamplers),
    SYM(IsSampler),
    SYM(BindSampler),
    SYM(SamplerParameteri),
    SYM(SamplerParameteriv),
    SYM(SamplerParameterf),
    SYM(SamplerParameterfv),
    SYM(SamplerParameterIiv),
    SYM(SamplerParameterIuiv),
    SYM(GetSamplerParameteriv),
    SYM(GetSamplerParameterIiv),
    SYM(GetSamplerParameterfv),
    SYM(GetSamplerParameterIuiv),
    SYM(QueryCounter),
    SYM(GetQueryObjecti64v),
    SYM(GetQueryObjectui64v),
    SYM(VertexAttribDivisor),
    SYM(VertexAttribP1ui),
    SYM(VertexAttribP1uiv),
    SYM(VertexAttribP2ui),
    SYM(VertexAttribP2uiv),
    SYM(VertexAttribP3ui),
    SYM(VertexAttribP3uiv),
    SYM(VertexAttribP4ui),
    SYM(VertexAttribP4uiv),
    SYM(VertexP2ui),
    SYM(VertexP2uiv),
    SYM(VertexP3ui),
    SYM(VertexP3uiv),
    SYM(VertexP4ui),
    SYM(VertexP4uiv),
    SYM(TexCoordP1ui),
    SYM(TexCoordP1uiv),
    SYM(TexCoordP2ui),
    SYM(TexCoordP2uiv),
    SYM(TexCoordP3ui),
    SYM(TexCoordP3uiv),
    SYM(TexCoordP4ui),
    SYM(TexCoordP4uiv),
    SYM(MultiTexCoordP1ui),
    SYM(MultiTexCoordP1uiv),
    SYM(MultiTexCoordP2ui),
    SYM(MultiTexCoordP2uiv),
    SYM(MultiTexCoordP3ui),
    SYM(MultiTexCoordP3uiv),
    SYM(MultiTexCoordP4ui),
    SYM(MultiTexCoordP4uiv),
    SYM(NormalP3ui),
    SYM(NormalP3uiv),
    SYM(ColorP3ui),
    SYM(ColorP3uiv),
    SYM(ColorP4ui),
    SYM(ColorP4uiv),
    SYM(SecondaryColorP3ui),
    SYM(SecondaryColorP3uiv),
    SYM(MinSampleShading),
    SYM(BlendEquationi),
    SYM(BlendEquationSeparatei),
    SYM(BlendFunci),
    SYM(BlendFuncSeparatei),
    SYM(DrawArraysIndirect),
    SYM(DrawElementsIndirect),
    SYM(Uniform1d),
    SYM(Uniform2d),
    SYM(Uniform3d),
    SYM(Uniform4d),
    SYM(Uniform1dv),
    SYM(Uniform2dv),
    SYM(Uniform3dv),
    SYM(Uniform4dv),
    SYM(UniformMatrix2dv),
    SYM(UniformMatrix3dv),
    SYM(UniformMatrix4dv),
    SYM(UniformMatrix2x3dv),
    SYM(UniformMatrix2x4dv),
    SYM(UniformMatrix3x2dv),
    SYM(UniformMatrix3x4dv),
    SYM(UniformMatrix4x2dv),
    SYM(UniformMatrix4x3dv),
    SYM(GetUniformdv),
    SYM(GetSubroutineUniformLocation),
    SYM(GetSubroutineIndex),
    SYM(GetActiveSubroutineUniformiv),
    SYM(GetActiveSubroutineUniformName),
    SYM(GetActiveSubroutineName),
    SYM(UniformSubroutinesuiv),
    SYM(GetUniformSubroutineuiv),
    SYM(GetProgramStageiv),
    SYM(PatchParameteri),
    SYM(PatchParameterfv),
    SYM(BindTransformFeedback),
    SYM(DeleteTransformFeedbacks),
    SYM(GenTransformFeedbacks),
    SYM(IsTransformFeedback),
    SYM(PauseTransformFeedback),
    SYM(ResumeTransformFeedback),
    SYM(DrawTransformFeedback),
    SYM(DrawTransformFeedbackStream),
    SYM(BeginQueryIndexed),
    SYM(EndQueryIndexed),
    SYM(GetQueryIndexediv),
    SYM(ReleaseShaderCompiler),
    SYM(ShaderBinary),
    SYM(GetShaderPrecisionFormat),
    SYM(DepthRangef),
    SYM(ClearDepthf),
    SYM(GetProgramBinary),
    SYM(ProgramBinary),
    SYM(ProgramParameteri),
    SYM(UseProgramStages),
    SYM(ActiveShaderProgram),
    SYM(CreateShaderProgramv),
    SYM(BindProgramPipeline),
    SYM(DeleteProgramPipelines),
    SYM(GenProgramPipelines),
    SYM(IsProgramPipeline),
    SYM(GetProgramPipelineiv),
    SYM(ProgramUniform1i),
    SYM(ProgramUniform1iv),
    SYM(ProgramUniform1f),
    SYM(ProgramUniform1fv),
    SYM(ProgramUniform1d),
    SYM(ProgramUniform1dv),
    SYM(ProgramUniform1ui),
    SYM(ProgramUniform1uiv),
    SYM(ProgramUniform2i),
    SYM(ProgramUniform2iv),
    SYM(ProgramUniform2f),
    SYM(ProgramUniform2fv),
    SYM(ProgramUniform2d),
    SYM(ProgramUniform2dv),
    SYM(ProgramUniform2ui),
    SYM(ProgramUniform2uiv),
    SYM(ProgramUniform3i),
    SYM(ProgramUniform3iv),
    SYM(ProgramUniform3f),
    SYM(ProgramUniform3fv),
    SYM(ProgramUniform3d),
    SYM(ProgramUniform3dv),
    SYM(ProgramUniform3ui),
    SYM(ProgramUniform3uiv),
    SYM(ProgramUniform4i),
    SYM(ProgramUniform4iv),
    SYM(ProgramUniform4f),
    SYM(ProgramUniform4fv),
    SYM(ProgramUniform4d),
    SYM(ProgramUniform4dv),
    SYM(ProgramUniform4ui),
    SYM(ProgramUniform4uiv),
    SYM(ProgramUniformMatrix2fv),
    SYM(ProgramUniformMatrix3fv),
    SYM(ProgramUniformMatrix4fv),
    SYM(ProgramUniformMatrix2dv),
    SYM(ProgramUniformMatrix3dv),
    SYM(ProgramUniformMatrix4dv),
    SYM(ProgramUniformMatrix2x3fv),
    SYM(ProgramUniformMatrix3x2fv),
    SYM(ProgramUniformMatrix2x4fv),
    SYM(ProgramUniformMatrix4x2fv),
    SYM(ProgramUniformMatrix3x4fv),
    SYM(ProgramUniformMatrix4x3fv),
    SYM(ProgramUniformMatrix2x3dv),
    SYM(ProgramUniformMatrix3x2dv),
    SYM(ProgramUniformMatrix2x4dv),
    SYM(ProgramUniformMatrix4x2dv),
    SYM(ProgramUniformMatrix3x4dv),
    SYM(ProgramUniformMatrix4x3dv),
    SYM(ValidateProgramPipeline),
    SYM(GetProgramPipelineInfoLog),
    SYM(VertexAttribL1d),
    SYM(VertexAttribL2d),
    SYM(VertexAttribL3d),
    SYM(VertexAttribL4d),
    SYM(VertexAttribL1dv),
    SYM(VertexAttribL2dv),
    SYM(VertexAttribL3dv),
    SYM(VertexAttribL4dv),
    SYM(VertexAttribLPointer),
    SYM(GetVertexAttribLdv),
    SYM(ViewportArrayv),
    SYM(ViewportIndexedf),
    SYM(ViewportIndexedfv),
    SYM(ScissorArrayv),
    SYM(ScissorIndexed),
    SYM(ScissorIndexedv),
    SYM(DepthRangeArrayv),
    SYM(DepthRangeIndexed),
    SYM(GetFloati_v),
    SYM(GetDoublei_v),
    SYM(DrawArraysInstancedBaseInstance),
    SYM(DrawElementsInstancedBaseInstance),
    SYM(DrawElementsInstancedBaseVertexBaseInstance),
    SYM(GetInternalformativ),
    SYM(GetActiveAtomicCounterBufferiv),
    SYM(BindImageTexture),
    SYM(MemoryBarrier),
    SYM(TexStorage1D),
    SYM(TexStorage2D),
    SYM(TexStorage3D),
    SYM(DrawTransformFeedbackInstanced),
    SYM(DrawTransformFeedbackStreamInstanced),
    SYM(ClearBufferData),
    SYM(ClearBufferSubData),
    SYM(DispatchCompute),
    SYM(DispatchComputeIndirect),
    SYM(CopyImageSubData),
    SYM(FramebufferParameteri),
    SYM(GetFramebufferParameteriv),
    SYM(GetInternalformati64v),
    SYM(InvalidateTexSubImage),
    SYM(InvalidateTexImage),
    SYM(InvalidateBufferSubData),
    SYM(InvalidateBufferData),
    SYM(InvalidateFramebuffer),
    SYM(InvalidateSubFramebuffer),
    SYM(MultiDrawArraysIndirect),
    SYM(MultiDrawElementsIndirect),
    SYM(GetProgramInterfaceiv),
    SYM(GetProgramResourceIndex),
    SYM(GetProgramResourceName),
    SYM(GetProgramResourceiv),
    SYM(GetProgramResourceLocation),
    SYM(GetProgramResourceLocationIndex),
    SYM(ShaderStorageBlockBinding),
    SYM(TexBufferRange),
    SYM(TexStorage2DMultisample),
    SYM(TexStorage3DMultisample),
    SYM(TextureView),
    SYM(BindVertexBuffer),
    SYM(VertexAttribFormat),
    SYM(VertexAttribIFormat),
    SYM(VertexAttribLFormat),
    SYM(VertexAttribBinding),
    SYM(VertexBindingDivisor),
    SYM(DebugMessageControl),
    SYM(DebugMessageInsert),
    SYM(DebugMessageCallback),
    SYM(GetDebugMessageLog),
    SYM(PushDebugGroup),
    SYM(PopDebugGroup),
    SYM(ObjectLabel),
    SYM(GetObjectLabel),
    SYM(ObjectPtrLabel),
    SYM(GetObjectPtrLabel),
    SYM(BufferStorage),
    SYM(ClearTexImage),
    SYM(ClearTexSubImage),
    SYM(BindBuffersBase),
    SYM(BindBuffersRange),
    SYM(BindTextures),
    SYM(BindSamplers),
    SYM(BindImageTextures),
    SYM(BindVertexBuffers),
    SYM(ClipControl),
    SYM(CreateTransformFeedbacks),
    SYM(TransformFeedbackBufferBase),
    SYM(TransformFeedbackBufferRange),
    SYM(GetTransformFeedbackiv),
    SYM(GetTransformFeedbacki_v),
    SYM(GetTransformFeedbacki64_v),
    SYM(CreateBuffers),
    SYM(NamedBufferStorage),
    SYM(NamedBufferData),
    SYM(NamedBufferSubData),
    SYM(CopyNamedBufferSubData),
    SYM(ClearNamedBufferData),
    SYM(ClearNamedBufferSubData),
    SYM(MapNamedBuffer),
    SYM(MapNamedBufferRange),
    SYM(UnmapNamedBuffer),
    SYM(FlushMappedNamedBufferRange),
    SYM(GetNamedBufferParameteriv),
    SYM(GetNamedBufferParameteri64v),
    SYM(GetNamedBufferPointerv),
    SYM(GetNamedBufferSubData),
    SYM(CreateFramebuffers),
    SYM(NamedFramebufferRenderbuffer),
    SYM(NamedFramebufferParameteri),
    SYM(NamedFramebufferTexture),
    SYM(NamedFramebufferTextureLayer),
    SYM(NamedFramebufferDrawBuffer),
    SYM(NamedFramebufferDrawBuffers),
    SYM(NamedFramebufferReadBuffer),
    SYM(InvalidateNamedFramebufferData),
    SYM(InvalidateNamedFramebufferSubData),
    SYM(ClearNamedFramebufferiv),
    SYM(ClearNamedFramebufferuiv),
    SYM(ClearNamedFramebufferfv),
    SYM(ClearNamedFramebufferfi),
    SYM(BlitNamedFramebuffer),
    SYM(CheckNamedFramebufferStatus),
    SYM(GetNamedFramebufferParameteriv),
    SYM(GetNamedFramebufferAttachmentParameteriv),
    SYM(CreateRenderbuffers),
    SYM(NamedRenderbufferStorage),
    SYM(NamedRenderbufferStorageMultisample),
    SYM(GetNamedRenderbufferParameteriv),
    SYM(CreateTextures),
    SYM(TextureBuffer),
    SYM(TextureBufferRange),
    SYM(TextureStorage1D),
    SYM(TextureStorage2D),
    SYM(TextureStorage3D),
    SYM(TextureStorage2DMultisample),
    SYM(TextureStorage3DMultisample),
    SYM(TextureSubImage1D),
    SYM(TextureSubImage2D),
    SYM(TextureSubImage3D),
    SYM(CompressedTextureSubImage1D),
    SYM(CompressedTextureSubImage2D),
    SYM(CompressedTextureSubImage3D),
    SYM(CopyTextureSubImage1D),
    SYM(CopyTextureSubImage2D),
    SYM(CopyTextureSubImage3D),
    SYM(TextureParameterf),
    SYM(TextureParameterfv),
    SYM(TextureParameteri),
    SYM(TextureParameterIiv),
    SYM(TextureParameterIuiv),
    SYM(TextureParameteriv),
    SYM(GenerateTextureMipmap),
    SYM(BindTextureUnit),
    SYM(GetTextureImage),
    SYM(GetCompressedTextureImage),
    SYM(GetTextureLevelParameterfv),
    SYM(GetTextureLevelParameteriv),
    SYM(GetTextureParameterfv),
    SYM(GetTextureParameterIiv),
    SYM(GetTextureParameterIuiv),
    SYM(GetTextureParameteriv),
    SYM(CreateVertexArrays),
    SYM(DisableVertexArrayAttrib),
    SYM(EnableVertexArrayAttrib),
    SYM(VertexArrayElementBuffer),
    SYM(VertexArrayVertexBuffer),
    SYM(VertexArrayVertexBuffers),
    SYM(VertexArrayAttribBinding),
    SYM(VertexArrayAttribFormat),
    SYM(VertexArrayAttribIFormat),
    SYM(VertexArrayAttribLFormat),
    SYM(VertexArrayBindingDivisor),
    SYM(GetVertexArrayiv),
    SYM(GetVertexArrayIndexediv),
    SYM(GetVertexArrayIndexed64iv),
    SYM(CreateSamplers),
    SYM(CreateProgramPipelines),
    SYM(CreateQueries),
    SYM(GetQueryBufferObjecti64v),
    SYM(GetQueryBufferObjectiv),
    SYM(GetQueryBufferObjectui64v),
    SYM(GetQueryBufferObjectuiv),
    SYM(MemoryBarrierByRegion),
    SYM(GetTextureSubImage),
    SYM(GetCompressedTextureSubImage),
    SYM(GetGraphicsResetStatus),
    SYM(GetnCompressedTexImage),
    SYM(GetnTexImage),
    SYM(GetnUniformdv),
    SYM(GetnUniformfv),
    SYM(GetnUniformiv),
    SYM(GetnUniformuiv),
    SYM(ReadnPixels),
    SYM(GetnMapdv),
    SYM(GetnMapfv),
    SYM(GetnMapiv),
    SYM(GetnPixelMapfv),
    SYM(GetnPixelMapuiv),
    SYM(GetnPixelMapusv),
    SYM(GetnPolygonStipple),
    SYM(GetnColorTable),
    SYM(GetnConvolutionFilter),
    SYM(GetnSeparableFilter),
    SYM(GetnHistogram),
    SYM(GetnMinmax),
    SYM(TextureBarrier),
    SYM(SpecializeShader),
    SYM(MultiDrawArraysIndirectCount),
    SYM(MultiDrawElementsIndirectCount),
    SYM(PolygonOffsetClamp),
    SYM(PrimitiveBoundingBoxARB),
    SYM(GetTextureHandleARB),
    SYM(GetTextureSamplerHandleARB),
    SYM(MakeTextureHandleResidentARB),
    SYM(MakeTextureHandleNonResidentARB),
    SYM(GetImageHandleARB),
    SYM(MakeImageHandleResidentARB),
    SYM(MakeImageHandleNonResidentARB),
    SYM(UniformHandleui64ARB),
    SYM(UniformHandleui64vARB),
    SYM(ProgramUniformHandleui64ARB),
    SYM(ProgramUniformHandleui64vARB),
    SYM(IsTextureHandleResidentARB),
    SYM(IsImageHandleResidentARB),
    SYM(VertexAttribL1ui64ARB),
    SYM(VertexAttribL1ui64vARB),
    SYM(GetVertexAttribLui64vARB),
    SYM(CreateSyncFromCLeventARB),
    SYM(ClampColorARB),
    SYM(DispatchComputeGroupSizeARB),
    SYM(DebugMessageControlARB),
    SYM(DebugMessageInsertARB),
    SYM(DebugMessageCallbackARB),
    SYM(GetDebugMessageLogARB),
    SYM(DrawBuffersARB),
    SYM(BlendEquationiARB),
    SYM(BlendEquationSeparateiARB),
    SYM(BlendFunciARB),
    SYM(BlendFuncSeparateiARB),
    SYM(DrawArraysInstancedARB),
    SYM(DrawElementsInstancedARB),
    SYM(ProgramStringARB),
    SYM(BindProgramARB),
    SYM(DeleteProgramsARB),
    SYM(GenProgramsARB),
    SYM(ProgramEnvParameter4dARB),
    SYM(ProgramEnvParameter4dvARB),
    SYM(ProgramEnvParameter4fARB),
    SYM(ProgramEnvParameter4fvARB),
    SYM(ProgramLocalParameter4dARB),
    SYM(ProgramLocalParameter4dvARB),
    SYM(ProgramLocalParameter4fARB),
    SYM(ProgramLocalParameter4fvARB),
    SYM(GetProgramEnvParameterdvARB),
    SYM(GetProgramEnvParameterfvARB),
    SYM(GetProgramLocalParameterdvARB),
    SYM(GetProgramLocalParameterfvARB),
    SYM(GetProgramivARB),
    SYM(GetProgramStringARB),
    SYM(IsProgramARB),
    SYM(ProgramParameteriARB),
    SYM(FramebufferTextureARB),
    SYM(FramebufferTextureLayerARB),
    SYM(FramebufferTextureFaceARB),
    SYM(SpecializeShaderARB),
    SYM(Uniform1i64ARB),
    SYM(Uniform2i64ARB),
    SYM(Uniform3i64ARB),
    SYM(Uniform4i64ARB),
    SYM(Uniform1i64vARB),
    SYM(Uniform2i64vARB),
    SYM(Uniform3i64vARB),
    SYM(Uniform4i64vARB),
    SYM(Uniform1ui64ARB),
    SYM(Uniform2ui64ARB),
    SYM(Uniform3ui64ARB),
    SYM(Uniform4ui64ARB),
    SYM(Uniform1ui64vARB),
    SYM(Uniform2ui64vARB),
    SYM(Uniform3ui64vARB),
    SYM(Uniform4ui64vARB),
    SYM(GetUniformi64vARB),
    SYM(GetUniformui64vARB),
    SYM(GetnUniformi64vARB),
    SYM(GetnUniformui64vARB),
    SYM(ProgramUniform1i64ARB),
    SYM(ProgramUniform2i64ARB),
    SYM(ProgramUniform3i64ARB),
    SYM(ProgramUniform4i64ARB),
    SYM(ProgramUniform1i64vARB),
    SYM(ProgramUniform2i64vARB),
    SYM(ProgramUniform3i64vARB),
    SYM(ProgramUniform4i64vARB),
    SYM(ProgramUniform1ui64ARB),
    SYM(ProgramUniform2ui64ARB),
    SYM(ProgramUniform3ui64ARB),
    SYM(ProgramUniform4ui64ARB),
    SYM(ProgramUniform1ui64vARB),
    SYM(ProgramUniform2ui64vARB),
    SYM(ProgramUniform3ui64vARB),
    SYM(ProgramUniform4ui64vARB),
    SYM(ColorTable),
    SYM(ColorTableParameterfv),
    SYM(ColorTableParameteriv),
    SYM(CopyColorTable),
    SYM(GetColorTable),
    SYM(GetColorTableParameterfv),
    SYM(GetColorTableParameteriv),
    SYM(ColorSubTable),
    SYM(CopyColorSubTable),
    SYM(ConvolutionFilter1D),
    SYM(ConvolutionFilter2D),
    SYM(ConvolutionParameterf),
    SYM(ConvolutionParameterfv),
    SYM(ConvolutionParameteri),
    SYM(ConvolutionParameteriv),
    SYM(CopyConvolutionFilter1D),
    SYM(CopyConvolutionFilter2D),
    SYM(GetConvolutionFilter),
    SYM(GetConvolutionParameterfv),
    SYM(GetConvolutionParameteriv),
    SYM(GetSeparableFilter),
    SYM(SeparableFilter2D),
    SYM(GetHistogram),
    SYM(GetHistogramParameterfv),
    SYM(GetHistogramParameteriv),
    SYM(GetMinmax),
    SYM(GetMinmaxParameterfv),
    SYM(GetMinmaxParameteriv),
    SYM(Histogram),
    SYM(Minmax),
    SYM(ResetHistogram),
    SYM(ResetMinmax),
    SYM(MultiDrawArraysIndirectCountARB),
    SYM(MultiDrawElementsIndirectCountARB),
    SYM(VertexAttribDivisorARB),
    SYM(CurrentPaletteMatrixARB),
    SYM(MatrixIndexubvARB),
    SYM(MatrixIndexusvARB),
    SYM(MatrixIndexuivARB),
    SYM(MatrixIndexPointerARB),
    SYM(SampleCoverageARB),
    SYM(ActiveTextureARB),
    SYM(ClientActiveTextureARB),
    SYM(MultiTexCoord1dARB),
    SYM(MultiTexCoord1dvARB),
    SYM(MultiTexCoord1fARB),
    SYM(MultiTexCoord1fvARB),
    SYM(MultiTexCoord1iARB),
    SYM(MultiTexCoord1ivARB),
    SYM(MultiTexCoord1sARB),
    SYM(MultiTexCoord1svARB),
    SYM(MultiTexCoord2dARB),
    SYM(MultiTexCoord2dvARB),
    SYM(MultiTexCoord2fARB),
    SYM(MultiTexCoord2fvARB),
    SYM(MultiTexCoord2iARB),
    SYM(MultiTexCoord2ivARB),
    SYM(MultiTexCoord2sARB),
    SYM(MultiTexCoord2svARB),
    SYM(MultiTexCoord3dARB),
    SYM(MultiTexCoord3dvARB),
    SYM(MultiTexCoord3fARB),
    SYM(MultiTexCoord3fvARB),
    SYM(MultiTexCoord3iARB),
    SYM(MultiTexCoord3ivARB),
    SYM(MultiTexCoord3sARB),
    SYM(MultiTexCoord3svARB),
    SYM(MultiTexCoord4dARB),
    SYM(MultiTexCoord4dvARB),
    SYM(MultiTexCoord4fARB),
    SYM(MultiTexCoord4fvARB),
    SYM(MultiTexCoord4iARB),
    SYM(MultiTexCoord4ivARB),
    SYM(MultiTexCoord4sARB),
    SYM(MultiTexCoord4svARB),
    SYM(GenQueriesARB),
    SYM(DeleteQueriesARB),
    SYM(IsQueryARB),
    SYM(BeginQueryARB),
    SYM(EndQueryARB),
    SYM(GetQueryivARB),
    SYM(GetQueryObjectivARB),
    SYM(GetQueryObjectuivARB),
    SYM(MaxShaderCompilerThreadsARB),
    SYM(PointParameterfARB),
    SYM(PointParameterfvARB),
    SYM(GetGraphicsResetStatusARB),
    SYM(GetnTexImageARB),
    SYM(ReadnPixelsARB),
    SYM(GetnCompressedTexImageARB),
    SYM(GetnUniformfvARB),
    SYM(GetnUniformivARB),
    SYM(GetnUniformuivARB),
    SYM(GetnUniformdvARB),
    SYM(GetnMapdvARB),
    SYM(GetnMapfvARB),
    SYM(GetnMapivARB),
    SYM(GetnPixelMapfvARB),
    SYM(GetnPixelMapuivARB),
    SYM(GetnPixelMapusvARB),
    SYM(GetnPolygonStippleARB),
    SYM(GetnColorTableARB),
    SYM(GetnConvolutionFilterARB),
    SYM(GetnSeparableFilterARB),
    SYM(GetnHistogramARB),
    SYM(GetnMinmaxARB),
    SYM(FramebufferSampleLocationsfvARB),
    SYM(NamedFramebufferSampleLocationsfvARB),
    SYM(EvaluateDepthValuesARB),
    SYM(MinSampleShadingARB),
    SYM(DeleteObjectARB),
    SYM(GetHandleARB),
    SYM(DetachObjectARB),
    SYM(CreateShaderObjectARB),
    SYM(ShaderSourceARB),
    SYM(CompileShaderARB),
    SYM(CreateProgramObjectARB),
    SYM(AttachObjectARB),
    SYM(LinkProgramARB),
    SYM(UseProgramObjectARB),
    SYM(ValidateProgramARB),
    SYM(Uniform1fARB),
    SYM(Uniform2fARB),
    SYM(Uniform3fARB),
    SYM(Uniform4fARB),
    SYM(Uniform1iARB),
    SYM(Uniform2iARB),
    SYM(Uniform3iARB),
    SYM(Uniform4iARB),
    SYM(Uniform1fvARB),
    SYM(Uniform2fvARB),
    SYM(Uniform3fvARB),
    SYM(Uniform4fvARB),
    SYM(Uniform1ivARB),
    SYM(Uniform2ivARB),
    SYM(Uniform3ivARB),
    SYM(Uniform4ivARB),
    SYM(UniformMatrix2fvARB),
    SYM(UniformMatrix3fvARB),
    SYM(UniformMatrix4fvARB),
    SYM(GetObjectParameterfvARB),
    SYM(GetObjectParameterivARB),
    SYM(GetInfoLogARB),
    SYM(GetAttachedObjectsARB),
    SYM(GetUniformLocationARB),
    SYM(GetActiveUniformARB),
    SYM(GetUniformfvARB),
    SYM(GetUniformivARB),
    SYM(GetShaderSourceARB),
    SYM(NamedStringARB),
    SYM(DeleteNamedStringARB),
    SYM(CompileShaderIncludeARB),
    SYM(IsNamedStringARB),
    SYM(GetNamedStringARB),
    SYM(GetNamedStringivARB),
    SYM(BufferPageCommitmentARB),
    SYM(NamedBufferPageCommitmentEXT),
    SYM(NamedBufferPageCommitmentARB),
    SYM(TexPageCommitmentARB),
    SYM(TexBufferARB),
    SYM(CompressedTexImage3DARB),
    SYM(CompressedTexImage2DARB),
    SYM(CompressedTexImage1DARB),
    SYM(CompressedTexSubImage3DARB),
    SYM(CompressedTexSubImage2DARB),
    SYM(CompressedTexSubImage1DARB),
    SYM(GetCompressedTexImageARB),
    SYM(LoadTransposeMatrixfARB),
    SYM(LoadTransposeMatrixdARB),
    SYM(MultTransposeMatrixfARB),
    SYM(MultTransposeMatrixdARB),
    SYM(WeightbvARB),
    SYM(WeightsvARB),
    SYM(WeightivARB),
    SYM(WeightfvARB),
    SYM(WeightdvARB),
    SYM(WeightubvARB),
    SYM(WeightusvARB),
    SYM(WeightuivARB),
    SYM(WeightPointerARB),
    SYM(VertexBlendARB),
    SYM(BindBufferARB),
    SYM(DeleteBuffersARB),
    SYM(GenBuffersARB),
    SYM(IsBufferARB),
    SYM(BufferDataARB),
    SYM(BufferSubDataARB),
    SYM(GetBufferSubDataARB),
    SYM(MapBufferARB),
    SYM(UnmapBufferARB),
    SYM(GetBufferParameterivARB),
    SYM(GetBufferPointervARB),
    SYM(VertexAttrib1dARB),
    SYM(VertexAttrib1dvARB),
    SYM(VertexAttrib1fARB),
    SYM(VertexAttrib1fvARB),
    SYM(VertexAttrib1sARB),
    SYM(VertexAttrib1svARB),
    SYM(VertexAttrib2dARB),
    SYM(VertexAttrib2dvARB),
    SYM(VertexAttrib2fARB),
    SYM(VertexAttrib2fvARB),
    SYM(VertexAttrib2sARB),
    SYM(VertexAttrib2svARB),
    SYM(VertexAttrib3dARB),
    SYM(VertexAttrib3dvARB),
    SYM(VertexAttrib3fARB),
    SYM(VertexAttrib3fvARB),
    SYM(VertexAttrib3sARB),
    SYM(VertexAttrib3svARB),
    SYM(VertexAttrib4NbvARB),
    SYM(VertexAttrib4NivARB),
    SYM(VertexAttrib4NsvARB),
    SYM(VertexAttrib4NubARB),
    SYM(VertexAttrib4NubvARB),
    SYM(VertexAttrib4NuivARB),
    SYM(VertexAttrib4NusvARB),
    SYM(VertexAttrib4bvARB),
    SYM(VertexAttrib4dARB),
    SYM(VertexAttrib4dvARB),
    SYM(VertexAttrib4fARB),
    SYM(VertexAttrib4fvARB),
    SYM(VertexAttrib4ivARB),
    SYM(VertexAttrib4sARB),
    SYM(VertexAttrib4svARB),
    SYM(VertexAttrib4ubvARB),
    SYM(VertexAttrib4uivARB),
    SYM(VertexAttrib4usvARB),
    SYM(VertexAttribPointerARB),
    SYM(EnableVertexAttribArrayARB),
    SYM(DisableVertexAttribArrayARB),
    SYM(GetVertexAttribdvARB),
    SYM(GetVertexAttribfvARB),
    SYM(GetVertexAttribivARB),
    SYM(GetVertexAttribPointervARB),
    SYM(BindAttribLocationARB),
    SYM(GetActiveAttribARB),
    SYM(GetAttribLocationARB),
    SYM(WindowPos2dARB),
    SYM(WindowPos2dvARB),
    SYM(WindowPos2fARB),
    SYM(WindowPos2fvARB),
    SYM(WindowPos2iARB),
    SYM(WindowPos2ivARB),
    SYM(WindowPos2sARB),
    SYM(WindowPos2svARB),
    SYM(WindowPos3dARB),
    SYM(WindowPos3dvARB),
    SYM(WindowPos3fARB),
    SYM(WindowPos3fvARB),
    SYM(WindowPos3iARB),
    SYM(WindowPos3ivARB),
    SYM(WindowPos3sARB),
    SYM(WindowPos3svARB),
    SYM(BlendBarrierKHR),
    SYM(MaxShaderCompilerThreadsKHR),
    SYM(MultiTexCoord1bOES),
    SYM(MultiTexCoord1bvOES),
    SYM(MultiTexCoord2bOES),
    SYM(MultiTexCoord2bvOES),
    SYM(MultiTexCoord3bOES),
    SYM(MultiTexCoord3bvOES),
    SYM(MultiTexCoord4bOES),
    SYM(MultiTexCoord4bvOES),
    SYM(TexCoord1bOES),
    SYM(TexCoord1bvOES),
    SYM(TexCoord2bOES),
    SYM(TexCoord2bvOES),
    SYM(TexCoord3bOES),
    SYM(TexCoord3bvOES),
    SYM(TexCoord4bOES),
    SYM(TexCoord4bvOES),
    SYM(Vertex2bOES),
    SYM(Vertex2bvOES),
    SYM(Vertex3bOES),
    SYM(Vertex3bvOES),
    SYM(Vertex4bOES),
    SYM(Vertex4bvOES),
    SYM(AlphaFuncxOES),
    SYM(ClearColorxOES),
    SYM(ClearDepthxOES),
    SYM(ClipPlanexOES),
    SYM(Color4xOES),
    SYM(DepthRangexOES),
    SYM(FogxOES),
    SYM(FogxvOES),
    SYM(FrustumxOES),
    SYM(GetClipPlanexOES),
    SYM(GetFixedvOES),
    SYM(GetTexEnvxvOES),
    SYM(GetTexParameterxvOES),
    SYM(LightModelxOES),
    SYM(LightModelxvOES),
    SYM(LightxOES),
    SYM(LightxvOES),
    SYM(LineWidthxOES),
    SYM(LoadMatrixxOES),
    SYM(MaterialxOES),
    SYM(MaterialxvOES),
    SYM(MultMatrixxOES),
    SYM(MultiTexCoord4xOES),
    SYM(Normal3xOES),
    SYM(OrthoxOES),
    SYM(PointParameterxvOES),
    SYM(PointSizexOES),
    SYM(PolygonOffsetxOES),
    SYM(RotatexOES),
    SYM(ScalexOES),
    SYM(TexEnvxOES),
    SYM(TexEnvxvOES),
    SYM(TexParameterxOES),
    SYM(TexParameterxvOES),
    SYM(TranslatexOES),
    SYM(AccumxOES),
    SYM(BitmapxOES),
    SYM(BlendColorxOES),
    SYM(ClearAccumxOES),
    SYM(Color3xOES),
    SYM(Color3xvOES),
    SYM(Color4xvOES),
    SYM(ConvolutionParameterxOES),
    SYM(ConvolutionParameterxvOES),
    SYM(EvalCoord1xOES),
    SYM(EvalCoord1xvOES),
    SYM(EvalCoord2xOES),
    SYM(EvalCoord2xvOES),
    SYM(FeedbackBufferxOES),
    SYM(GetConvolutionParameterxvOES),
    SYM(GetHistogramParameterxvOES),
    SYM(GetLightxOES),
    SYM(GetMapxvOES),
    SYM(GetMaterialxOES),
    SYM(GetPixelMapxv),
    SYM(GetTexGenxvOES),
    SYM(GetTexLevelParameterxvOES),
    SYM(IndexxOES),
    SYM(IndexxvOES),
    SYM(LoadTransposeMatrixxOES),
    SYM(Map1xOES),
    SYM(Map2xOES),
    SYM(MapGrid1xOES),
    SYM(MapGrid2xOES),
    SYM(MultTransposeMatrixxOES),
    SYM(MultiTexCoord1xOES),
    SYM(MultiTexCoord1xvOES),
    SYM(MultiTexCoord2xOES),
    SYM(MultiTexCoord2xvOES),
    SYM(MultiTexCoord3xOES),
    SYM(MultiTexCoord3xvOES),
    SYM(MultiTexCoord4xvOES),
    SYM(Normal3xvOES),
    SYM(PassThroughxOES),
    SYM(PixelMapx),
    SYM(PixelStorex),
    SYM(PixelTransferxOES),
    SYM(PixelZoomxOES),
    SYM(PrioritizeTexturesxOES),
    SYM(RasterPos2xOES),
    SYM(RasterPos2xvOES),
    SYM(RasterPos3xOES),
    SYM(RasterPos3xvOES),
    SYM(RasterPos4xOES),
    SYM(RasterPos4xvOES),
    SYM(RectxOES),
    SYM(RectxvOES),
    SYM(TexCoord1xOES),
    SYM(TexCoord1xvOES),
    SYM(TexCoord2xOES),
    SYM(TexCoord2xvOES),
    SYM(TexCoord3xOES),
    SYM(TexCoord3xvOES),
    SYM(TexCoord4xOES),
    SYM(TexCoord4xvOES),
    SYM(TexGenxOES),
    SYM(TexGenxvOES),
    SYM(Vertex2xOES),
    SYM(Vertex2xvOES),
    SYM(Vertex3xOES),
    SYM(Vertex3xvOES),
    SYM(Vertex4xOES),
    SYM(Vertex4xvOES),
    SYM(QueryMatrixxOES),
    SYM(ClearDepthfOES),
    SYM(ClipPlanefOES),
    SYM(DepthRangefOES),
    SYM(FrustumfOES),
    SYM(GetClipPlanefOES),
    SYM(OrthofOES),
    SYM(EGLImageTargetTexStorageEXT),
    SYM(EGLImageTargetTextureStorageEXT),
    SYM(UniformBufferEXT),
    SYM(GetUniformBufferSizeEXT),
    SYM(GetUniformOffsetEXT),
    SYM(BlendColorEXT),
    SYM(BlendEquationSeparateEXT),
    SYM(BlendFuncSeparateEXT),
    SYM(BlendEquationEXT),
    SYM(ColorSubTableEXT),
    SYM(CopyColorSubTableEXT),
    SYM(LockArraysEXT),
    SYM(UnlockArraysEXT),
    SYM(ConvolutionFilter1DEXT),
    SYM(ConvolutionFilter2DEXT),
    SYM(ConvolutionParameterfEXT),
    SYM(ConvolutionParameterfvEXT),
    SYM(ConvolutionParameteriEXT),
    SYM(ConvolutionParameterivEXT),
    SYM(CopyConvolutionFilter1DEXT),
    SYM(CopyConvolutionFilter2DEXT),
    SYM(GetConvolutionFilterEXT),
    SYM(GetConvolutionParameterfvEXT),
    SYM(GetConvolutionParameterivEXT),
    SYM(GetSeparableFilterEXT),
    SYM(SeparableFilter2DEXT),
    SYM(Tangent3bEXT),
    SYM(Tangent3bvEXT),
    SYM(Tangent3dEXT),
    SYM(Tangent3dvEXT),
    SYM(Tangent3fEXT),
    SYM(Tangent3fvEXT),
    SYM(Tangent3iEXT),
    SYM(Tangent3ivEXT),
    SYM(Tangent3sEXT),
    SYM(Tangent3svEXT),
    SYM(Binormal3bEXT),
    SYM(Binormal3bvEXT),
    SYM(Binormal3dEXT),
    SYM(Binormal3dvEXT),
    SYM(Binormal3fEXT),
    SYM(Binormal3fvEXT),
    SYM(Binormal3iEXT),
    SYM(Binormal3ivEXT),
    SYM(Binormal3sEXT),
    SYM(Binormal3svEXT),
    SYM(TangentPointerEXT),
    SYM(BinormalPointerEXT),
    SYM(CopyTexImage1DEXT),
    SYM(CopyTexImage2DEXT),
    SYM(CopyTexSubImage1DEXT),
    SYM(CopyTexSubImage2DEXT),
    SYM(CopyTexSubImage3DEXT),
    SYM(CullParameterdvEXT),
    SYM(CullParameterfvEXT),
    SYM(LabelObjectEXT),
    SYM(GetObjectLabelEXT),
    SYM(InsertEventMarkerEXT),
    SYM(PushGroupMarkerEXT),
    SYM(PopGroupMarkerEXT),
    SYM(DepthBoundsEXT),
    SYM(MatrixLoadfEXT),
    SYM(MatrixLoaddEXT),
    SYM(MatrixMultfEXT),
    SYM(MatrixMultdEXT),
    SYM(MatrixLoadIdentityEXT),
    SYM(MatrixRotatefEXT),
    SYM(MatrixRotatedEXT),
    SYM(MatrixScalefEXT),
    SYM(MatrixScaledEXT),
    SYM(MatrixTranslatefEXT),
    SYM(MatrixTranslatedEXT),
    SYM(MatrixFrustumEXT),
    SYM(MatrixOrthoEXT),
    SYM(MatrixPopEXT),
    SYM(MatrixPushEXT),
    SYM(ClientAttribDefaultEXT),
    SYM(PushClientAttribDefaultEXT),
    SYM(TextureParameterfEXT),
    SYM(TextureParameterfvEXT),
    SYM(TextureParameteriEXT),
    SYM(TextureParameterivEXT),
    SYM(TextureImage1DEXT),
    SYM(TextureImage2DEXT),
    SYM(TextureSubImage1DEXT),
    SYM(TextureSubImage2DEXT),
    SYM(CopyTextureImage1DEXT),
    SYM(CopyTextureImage2DEXT),
    SYM(CopyTextureSubImage1DEXT),
    SYM(CopyTextureSubImage2DEXT),
    SYM(GetTextureImageEXT),
    SYM(GetTextureParameterfvEXT),
    SYM(GetTextureParameterivEXT),
    SYM(GetTextureLevelParameterfvEXT),
    SYM(GetTextureLevelParameterivEXT),
    SYM(TextureImage3DEXT),
    SYM(TextureSubImage3DEXT),
    SYM(CopyTextureSubImage3DEXT),
    SYM(BindMultiTextureEXT),
    SYM(MultiTexCoordPointerEXT),
    SYM(MultiTexEnvfEXT),
    SYM(MultiTexEnvfvEXT),
    SYM(MultiTexEnviEXT),
    SYM(MultiTexEnvivEXT),
    SYM(MultiTexGendEXT),
    SYM(MultiTexGendvEXT),
    SYM(MultiTexGenfEXT),
    SYM(MultiTexGenfvEXT),
    SYM(MultiTexGeniEXT),
    SYM(MultiTexGenivEXT),
    SYM(GetMultiTexEnvfvEXT),
    SYM(GetMultiTexEnvivEXT),
    SYM(GetMultiTexGendvEXT),
    SYM(GetMultiTexGenfvEXT),
    SYM(GetMultiTexGenivEXT),
    SYM(MultiTexParameteriEXT),
    SYM(MultiTexParameterivEXT),
    SYM(MultiTexParameterfEXT),
    SYM(MultiTexParameterfvEXT),
    SYM(MultiTexImage1DEXT),
    SYM(MultiTexImage2DEXT),
    SYM(MultiTexSubImage1DEXT),
    SYM(MultiTexSubImage2DEXT),
    SYM(CopyMultiTexImage1DEXT),
    SYM(CopyMultiTexImage2DEXT),
    SYM(CopyMultiTexSubImage1DEXT),
    SYM(CopyMultiTexSubImage2DEXT),
    SYM(GetMultiTexImageEXT),
    SYM(GetMultiTexParameterfvEXT),
    SYM(GetMultiTexParameterivEXT),
    SYM(GetMultiTexLevelParameterfvEXT),
    SYM(GetMultiTexLevelParameterivEXT),
    SYM(MultiTexImage3DEXT),
    SYM(MultiTexSubImage3DEXT),
    SYM(CopyMultiTexSubImage3DEXT),
    SYM(EnableClientStateIndexedEXT),
    SYM(DisableClientStateIndexedEXT),
    SYM(GetFloatIndexedvEXT),
    SYM(GetDoubleIndexedvEXT),
    SYM(GetPointerIndexedvEXT),
    SYM(EnableIndexedEXT),
    SYM(DisableIndexedEXT),
    SYM(IsEnabledIndexedEXT),
    SYM(GetIntegerIndexedvEXT),
    SYM(GetBooleanIndexedvEXT),
    SYM(CompressedTextureImage3DEXT),
    SYM(CompressedTextureImage2DEXT),
    SYM(CompressedTextureImage1DEXT),
    SYM(CompressedTextureSubImage3DEXT),
    SYM(CompressedTextureSubImage2DEXT),
    SYM(CompressedTextureSubImage1DEXT),
    SYM(GetCompressedTextureImageEXT),
    SYM(CompressedMultiTexImage3DEXT),
    SYM(CompressedMultiTexImage2DEXT),
    SYM(CompressedMultiTexImage1DEXT),
    SYM(CompressedMultiTexSubImage3DEXT),
    SYM(CompressedMultiTexSubImage2DEXT),
    SYM(CompressedMultiTexSubImage1DEXT),
    SYM(GetCompressedMultiTexImageEXT),
    SYM(MatrixLoadTransposefEXT),
    SYM(MatrixLoadTransposedEXT),
    SYM(MatrixMultTransposefEXT),
    SYM(MatrixMultTransposedEXT),
    SYM(NamedBufferDataEXT),
    SYM(NamedBufferSubDataEXT),
    SYM(MapNamedBufferEXT),
    SYM(UnmapNamedBufferEXT),
    SYM(GetNamedBufferParameterivEXT),
    SYM(GetNamedBufferPointervEXT),
    SYM(GetNamedBufferSubDataEXT),
    SYM(ProgramUniform1fEXT),
    SYM(ProgramUniform2fEXT),
    SYM(ProgramUniform3fEXT),
    SYM(ProgramUniform4fEXT),
    SYM(ProgramUniform1iEXT),
    SYM(ProgramUniform2iEXT),
    SYM(ProgramUniform3iEXT),
    SYM(ProgramUniform4iEXT),
    SYM(ProgramUniform1fvEXT),
    SYM(ProgramUniform2fvEXT),
    SYM(ProgramUniform3fvEXT),
    SYM(ProgramUniform4fvEXT),
    SYM(ProgramUniform1ivEXT),
    SYM(ProgramUniform2ivEXT),
    SYM(ProgramUniform3ivEXT),
    SYM(ProgramUniform4ivEXT),
    SYM(ProgramUniformMatrix2fvEXT),
    SYM(ProgramUniformMatrix3fvEXT),
    SYM(ProgramUniformMatrix4fvEXT),
    SYM(ProgramUniformMatrix2x3fvEXT),
    SYM(ProgramUniformMatrix3x2fvEXT),
    SYM(ProgramUniformMatrix2x4fvEXT),
    SYM(ProgramUniformMatrix4x2fvEXT),
    SYM(ProgramUniformMatrix3x4fvEXT),
    SYM(ProgramUniformMatrix4x3fvEXT),
    SYM(TextureBufferEXT),
    SYM(MultiTexBufferEXT),
    SYM(TextureParameterIivEXT),
    SYM(TextureParameterIuivEXT),
    SYM(GetTextureParameterIivEXT),
    SYM(GetTextureParameterIuivEXT),
    SYM(MultiTexParameterIivEXT),
    SYM(MultiTexParameterIuivEXT),
    SYM(GetMultiTexParameterIivEXT),
    SYM(GetMultiTexParameterIuivEXT),
    SYM(ProgramUniform1uiEXT),
    SYM(ProgramUniform2uiEXT),
    SYM(ProgramUniform3uiEXT),
    SYM(ProgramUniform4uiEXT),
    SYM(ProgramUniform1uivEXT),
    SYM(ProgramUniform2uivEXT),
    SYM(ProgramUniform3uivEXT),
    SYM(ProgramUniform4uivEXT),
    SYM(NamedProgramLocalParameters4fvEXT),
    SYM(NamedProgramLocalParameterI4iEXT),
    SYM(NamedProgramLocalParameterI4ivEXT),
    SYM(NamedProgramLocalParametersI4ivEXT),
    SYM(NamedProgramLocalParameterI4uiEXT),
    SYM(NamedProgramLocalParameterI4uivEXT),
    SYM(NamedProgramLocalParametersI4uivEXT),
    SYM(GetNamedProgramLocalParameterIivEXT),
    SYM(GetNamedProgramLocalParameterIuivEXT),
    SYM(EnableClientStateiEXT),
    SYM(DisableClientStateiEXT),
    SYM(GetFloati_vEXT),
    SYM(GetDoublei_vEXT),
    SYM(GetPointeri_vEXT),
    SYM(NamedProgramStringEXT),
    SYM(NamedProgramLocalParameter4dEXT),
    SYM(NamedProgramLocalParameter4dvEXT),
    SYM(NamedProgramLocalParameter4fEXT),
    SYM(NamedProgramLocalParameter4fvEXT),
    SYM(GetNamedProgramLocalParameterdvEXT),
    SYM(GetNamedProgramLocalParameterfvEXT),
    SYM(GetNamedProgramivEXT),
    SYM(GetNamedProgramStringEXT),
    SYM(NamedRenderbufferStorageEXT),
    SYM(GetNamedRenderbufferParameterivEXT),
    SYM(NamedRenderbufferStorageMultisampleEXT),
    SYM(NamedRenderbufferStorageMultisampleCoverageEXT),
    SYM(CheckNamedFramebufferStatusEXT),
    SYM(NamedFramebufferTexture1DEXT),
    SYM(NamedFramebufferTexture2DEXT),
    SYM(NamedFramebufferTexture3DEXT),
    SYM(NamedFramebufferRenderbufferEXT),
    SYM(GetNamedFramebufferAttachmentParameterivEXT),
    SYM(GenerateTextureMipmapEXT),
    SYM(GenerateMultiTexMipmapEXT),
    SYM(FramebufferDrawBufferEXT),
    SYM(FramebufferDrawBuffersEXT),
    SYM(FramebufferReadBufferEXT),
    SYM(GetFramebufferParameterivEXT),
    SYM(NamedCopyBufferSubDataEXT),
    SYM(NamedFramebufferTextureEXT),
    SYM(NamedFramebufferTextureLayerEXT),
    SYM(NamedFramebufferTextureFaceEXT),
    SYM(TextureRenderbufferEXT),
    SYM(MultiTexRenderbufferEXT),
    SYM(VertexArrayVertexOffsetEXT),
    SYM(VertexArrayColorOffsetEXT),
    SYM(VertexArrayEdgeFlagOffsetEXT),
    SYM(VertexArrayIndexOffsetEXT),
    SYM(VertexArrayNormalOffsetEXT),
    SYM(VertexArrayTexCoordOffsetEXT),
    SYM(VertexArrayMultiTexCoordOffsetEXT),
    SYM(VertexArrayFogCoordOffsetEXT),
    SYM(VertexArraySecondaryColorOffsetEXT),
    SYM(VertexArrayVertexAttribOffsetEXT),
    SYM(VertexArrayVertexAttribIOffsetEXT),
    SYM(EnableVertexArrayEXT),
    SYM(DisableVertexArrayEXT),
    SYM(EnableVertexArrayAttribEXT),
    SYM(DisableVertexArrayAttribEXT),
    SYM(GetVertexArrayIntegervEXT),
    SYM(GetVertexArrayPointervEXT),
    SYM(GetVertexArrayIntegeri_vEXT),
    SYM(GetVertexArrayPointeri_vEXT),
    SYM(MapNamedBufferRangeEXT),
    SYM(FlushMappedNamedBufferRangeEXT),
    SYM(NamedBufferStorageEXT),
    SYM(ClearNamedBufferDataEXT),
    SYM(ClearNamedBufferSubDataEXT),
    SYM(NamedFramebufferParameteriEXT),
    SYM(GetNamedFramebufferParameterivEXT),
    SYM(ProgramUniform1dEXT),
    SYM(ProgramUniform2dEXT),
    SYM(ProgramUniform3dEXT),
    SYM(ProgramUniform4dEXT),
    SYM(ProgramUniform1dvEXT),
    SYM(ProgramUniform2dvEXT),
    SYM(ProgramUniform3dvEXT),
    SYM(ProgramUniform4dvEXT),
    SYM(ProgramUniformMatrix2dvEXT),
    SYM(ProgramUniformMatrix3dvEXT),
    SYM(ProgramUniformMatrix4dvEXT),
    SYM(ProgramUniformMatrix2x3dvEXT),
    SYM(ProgramUniformMatrix2x4dvEXT),
    SYM(ProgramUniformMatrix3x2dvEXT),
    SYM(ProgramUniformMatrix3x4dvEXT),
    SYM(ProgramUniformMatrix4x2dvEXT),
    SYM(ProgramUniformMatrix4x3dvEXT),
    SYM(TextureBufferRangeEXT),
    SYM(TextureStorage1DEXT),
    SYM(TextureStorage2DEXT),
    SYM(TextureStorage3DEXT),
    SYM(TextureStorage2DMultisampleEXT),
    SYM(TextureStorage3DMultisampleEXT),
    SYM(VertexArrayBindVertexBufferEXT),
    SYM(VertexArrayVertexAttribFormatEXT),
    SYM(VertexArrayVertexAttribIFormatEXT),
    SYM(VertexArrayVertexAttribLFormatEXT),
    SYM(VertexArrayVertexAttribBindingEXT),
    SYM(VertexArrayVertexBindingDivisorEXT),
    SYM(VertexArrayVertexAttribLOffsetEXT),
    SYM(TexturePageCommitmentEXT),
    SYM(VertexArrayVertexAttribDivisorEXT),
    SYM(ColorMaskIndexedEXT),
    SYM(DrawArraysInstancedEXT),
    SYM(DrawElementsInstancedEXT),
    SYM(DrawRangeElementsEXT),
    SYM(BufferStorageExternalEXT),
    SYM(NamedBufferStorageExternalEXT),
    SYM(FogCoordfEXT),
    SYM(FogCoordfvEXT),
    SYM(FogCoorddEXT),
    SYM(FogCoorddvEXT),
    SYM(FogCoordPointerEXT),
    SYM(BlitFramebufferEXT),
    SYM(RenderbufferStorageMultisampleEXT),
    SYM(IsRenderbufferEXT),
    SYM(BindRenderbufferEXT),
    SYM(DeleteRenderbuffersEXT),
    SYM(GenRenderbuffersEXT),
    SYM(RenderbufferStorageEXT),
    SYM(GetRenderbufferParameterivEXT),
    SYM(IsFramebufferEXT),
    SYM(BindFramebufferEXT),
    SYM(DeleteFramebuffersEXT),
    SYM(GenFramebuffersEXT),
    SYM(CheckFramebufferStatusEXT),
    SYM(FramebufferTexture1DEXT),
    SYM(FramebufferTexture2DEXT),
    SYM(FramebufferTexture3DEXT),
    SYM(FramebufferRenderbufferEXT),
    SYM(GetFramebufferAttachmentParameterivEXT),
    SYM(GenerateMipmapEXT),
    SYM(ProgramParameteriEXT),
    SYM(ProgramEnvParameters4fvEXT),
    SYM(ProgramLocalParameters4fvEXT),
    SYM(GetUniformuivEXT),
    SYM(BindFragDataLocationEXT),
    SYM(GetFragDataLocationEXT),
    SYM(Uniform1uiEXT),
    SYM(Uniform2uiEXT),
    SYM(Uniform3uiEXT),
    SYM(Uniform4uiEXT),
    SYM(Uniform1uivEXT),
    SYM(Uniform2uivEXT),
    SYM(Uniform3uivEXT),
    SYM(Uniform4uivEXT),
    SYM(GetHistogramEXT),
    SYM(GetHistogramParameterfvEXT),
    SYM(GetHistogramParameterivEXT),
    SYM(GetMinmaxEXT),
    SYM(GetMinmaxParameterfvEXT),
    SYM(GetMinmaxParameterivEXT),
    SYM(HistogramEXT),
    SYM(MinmaxEXT),
    SYM(ResetHistogramEXT),
    SYM(ResetMinmaxEXT),
    SYM(IndexFuncEXT),
    SYM(IndexMaterialEXT),
    SYM(ApplyTextureEXT),
    SYM(TextureLightEXT),
    SYM(TextureMaterialEXT),
    SYM(GetUnsignedBytevEXT),
    SYM(GetUnsignedBytei_vEXT),
    SYM(DeleteMemoryObjectsEXT),
    SYM(IsMemoryObjectEXT),
    SYM(CreateMemoryObjectsEXT),
    SYM(MemoryObjectParameterivEXT),
    SYM(GetMemoryObjectParameterivEXT),
    SYM(TexStorageMem2DEXT),
    SYM(TexStorageMem2DMultisampleEXT),
    SYM(TexStorageMem3DEXT),
    SYM(TexStorageMem3DMultisampleEXT),
    SYM(BufferStorageMemEXT),
    SYM(TextureStorageMem2DEXT),
    SYM(TextureStorageMem2DMultisampleEXT),
    SYM(TextureStorageMem3DEXT),
    SYM(TextureStorageMem3DMultisampleEXT),
    SYM(NamedBufferStorageMemEXT),
    SYM(TexStorageMem1DEXT),
    SYM(TextureStorageMem1DEXT),
    SYM(ImportMemoryFdEXT),
    SYM(ImportMemoryWin32HandleEXT),
    SYM(ImportMemoryWin32NameEXT),
    SYM(MultiDrawArraysEXT),
    SYM(MultiDrawElementsEXT),
    SYM(SampleMaskEXT),
    SYM(SamplePatternEXT),
    SYM(ColorTableEXT),
    SYM(GetColorTableEXT),
    SYM(GetColorTableParameterivEXT),
    SYM(GetColorTableParameterfvEXT),
    SYM(PixelTransformParameteriEXT),
    SYM(PixelTransformParameterfEXT),
    SYM(PixelTransformParameterivEXT),
    SYM(PixelTransformParameterfvEXT),
    SYM(GetPixelTransformParameterivEXT),
    SYM(GetPixelTransformParameterfvEXT),
    SYM(PointParameterfEXT),
    SYM(PointParameterfvEXT),
    SYM(PolygonOffsetEXT),
    SYM(PolygonOffsetClampEXT),
    SYM(ProvokingVertexEXT),
    SYM(RasterSamplesEXT),
    SYM(SecondaryColor3bEXT),
    SYM(SecondaryColor3bvEXT),
    SYM(SecondaryColor3dEXT),
    SYM(SecondaryColor3dvEXT),
    SYM(SecondaryColor3fEXT),
    SYM(SecondaryColor3fvEXT),
    SYM(SecondaryColor3iEXT),
    SYM(SecondaryColor3ivEXT),
    SYM(SecondaryColor3sEXT),
    SYM(SecondaryColor3svEXT),
    SYM(SecondaryColor3ubEXT),
    SYM(SecondaryColor3ubvEXT),
    SYM(SecondaryColor3uiEXT),
    SYM(SecondaryColor3uivEXT),
    SYM(SecondaryColor3usEXT),
    SYM(SecondaryColor3usvEXT),
    SYM(SecondaryColorPointerEXT),
    SYM(GenSemaphoresEXT),
    SYM(DeleteSemaphoresEXT),
    SYM(IsSemaphoreEXT),
    SYM(SemaphoreParameterui64vEXT),
    SYM(GetSemaphoreParameterui64vEXT),
    SYM(WaitSemaphoreEXT),
    SYM(SignalSemaphoreEXT),
    SYM(ImportSemaphoreFdEXT),
    SYM(ImportSemaphoreWin32HandleEXT),
    SYM(ImportSemaphoreWin32NameEXT),
    SYM(UseShaderProgramEXT),
    SYM(ActiveProgramEXT),
    SYM(CreateShaderProgramEXT),
    SYM(FramebufferFetchBarrierEXT),
    SYM(BindImageTextureEXT),
    SYM(MemoryBarrierEXT),
    SYM(StencilClearTagEXT),
    SYM(ActiveStencilFaceEXT),
    SYM(TexSubImage1DEXT),
    SYM(TexSubImage2DEXT),
    SYM(TexImage3DEXT),
    SYM(TexSubImage3DEXT),
    SYM(FramebufferTextureLayerEXT),
    SYM(TexBufferEXT),
    SYM(TexParameterIivEXT),
    SYM(TexParameterIuivEXT),
    SYM(GetTexParameterIivEXT),
    SYM(GetTexParameterIuivEXT),
    SYM(ClearColorIiEXT),
    SYM(ClearColorIuiEXT),
    SYM(AreTexturesResidentEXT),
    SYM(BindTextureEXT),
    SYM(DeleteTexturesEXT),
    SYM(GenTexturesEXT),
    SYM(IsTextureEXT),
    SYM(PrioritizeTexturesEXT),
    SYM(TextureNormalEXT),
    SYM(GetQueryObjecti64vEXT),
    SYM(GetQueryObjectui64vEXT),
    SYM(BeginTransformFeedbackEXT),
    SYM(EndTransformFeedbackEXT),
    SYM(BindBufferRangeEXT),
    SYM(BindBufferOffsetEXT),
    SYM(BindBufferBaseEXT),
    SYM(TransformFeedbackVaryingsEXT),
    SYM(GetTransformFeedbackVaryingEXT),
    SYM(ArrayElementEXT),
    SYM(ColorPointerEXT),
    SYM(DrawArraysEXT),
    SYM(EdgeFlagPointerEXT),
    SYM(GetPointervEXT),
    SYM(IndexPointerEXT),
    SYM(NormalPointerEXT),
    SYM(TexCoordPointerEXT),
    SYM(VertexPointerEXT),
    SYM(VertexAttribL1dEXT),
    SYM(VertexAttribL2dEXT),
    SYM(VertexAttribL3dEXT),
    SYM(VertexAttribL4dEXT),
    SYM(VertexAttribL1dvEXT),
    SYM(VertexAttribL2dvEXT),
    SYM(VertexAttribL3dvEXT),
    SYM(VertexAttribL4dvEXT),
    SYM(VertexAttribLPointerEXT),
    SYM(GetVertexAttribLdvEXT),
    SYM(BeginVertexShaderEXT),
    SYM(EndVertexShaderEXT),
    SYM(BindVertexShaderEXT),
    SYM(GenVertexShadersEXT),
    SYM(DeleteVertexShaderEXT),
    SYM(ShaderOp1EXT),
    SYM(ShaderOp2EXT),
    SYM(ShaderOp3EXT),
    SYM(SwizzleEXT),
    SYM(WriteMaskEXT),
    SYM(InsertComponentEXT),
    SYM(ExtractComponentEXT),
    SYM(GenSymbolsEXT),
    SYM(SetInvariantEXT),
    SYM(SetLocalConstantEXT),
    SYM(VariantbvEXT),
    SYM(VariantsvEXT),
    SYM(VariantivEXT),
    SYM(VariantfvEXT),
    SYM(VariantdvEXT),
    SYM(VariantubvEXT),
    SYM(VariantusvEXT),
    SYM(VariantuivEXT),
    SYM(VariantPointerEXT),
    SYM(EnableVariantClientStateEXT),
    SYM(DisableVariantClientStateEXT),
    SYM(BindLightParameterEXT),
    SYM(BindMaterialParameterEXT),
    SYM(BindTexGenParameterEXT),
    SYM(BindTextureUnitParameterEXT),
    SYM(BindParameterEXT),
    SYM(IsVariantEnabledEXT),
    SYM(GetVariantBooleanvEXT),
    SYM(GetVariantIntegervEXT),
    SYM(GetVariantFloatvEXT),
    SYM(GetVariantPointervEXT),
    SYM(GetInvariantBooleanvEXT),
    SYM(GetInvariantIntegervEXT),
    SYM(GetInvariantFloatvEXT),
    SYM(GetLocalConstantBooleanvEXT),
    SYM(GetLocalConstantIntegervEXT),
    SYM(GetLocalConstantFloatvEXT),
    SYM(VertexWeightfEXT),
    SYM(VertexWeightfvEXT),
    SYM(VertexWeightPointerEXT),
    SYM(AcquireKeyedMutexWin32EXT),
    SYM(ReleaseKeyedMutexWin32EXT),
    SYM(esEXT),
    SYM(ImportSyncEXT),
    SYM(ImageTransformParameteriHP),
    SYM(ImageTransformParameterfHP),
    SYM(ImageTransformParameterivHP),
    SYM(ImageTransformParameterfvHP),
    SYM(GetImageTransformParameterivHP),
    SYM(GetImageTransformParameterfvHP),
    SYM(FramebufferTextureEXT),
    SYM(FramebufferTextureFaceEXT),
    SYM(VertexAttribI1iEXT),
    SYM(VertexAttribI2iEXT),
    SYM(VertexAttribI3iEXT),
    SYM(VertexAttribI4iEXT),
    SYM(VertexAttribI1uiEXT),
    SYM(VertexAttribI2uiEXT),
    SYM(VertexAttribI3uiEXT),
    SYM(VertexAttribI4uiEXT),
    SYM(VertexAttribI1ivEXT),
    SYM(VertexAttribI2ivEXT),
    SYM(VertexAttribI3ivEXT),
    SYM(VertexAttribI4ivEXT),
    SYM(VertexAttribI1uivEXT),
    SYM(VertexAttribI2uivEXT),
    SYM(VertexAttribI3uivEXT),
    SYM(VertexAttribI4uivEXT),
    SYM(VertexAttribI4bvEXT),
    SYM(VertexAttribI4svEXT),
    SYM(VertexAttribI4ubvEXT),
    SYM(VertexAttribI4usvEXT),
    SYM(VertexAttribIPointerEXT),
    SYM(GetVertexAttribIivEXT),
    SYM(GetVertexAttribIuivEXT),
    SYM(FramebufferTextureMultiviewOVR),

    { NULL, NULL },
};
RGLSYMGLDRAWRANGEELEMENTSPROC __rglgen_glDrawRangeElements;
RGLSYMGLTEXIMAGE3DPROC __rglgen_glTexImage3D;
RGLSYMGLTEXSUBIMAGE3DPROC __rglgen_glTexSubImage3D;
RGLSYMGLCOPYTEXSUBIMAGE3DPROC __rglgen_glCopyTexSubImage3D;
RGLSYMGLACTIVETEXTUREPROC __rglgen_glActiveTexture;
RGLSYMGLSAMPLECOVERAGEPROC __rglgen_glSampleCoverage;
RGLSYMGLCOMPRESSEDTEXIMAGE3DPROC __rglgen_glCompressedTexImage3D;
RGLSYMGLCOMPRESSEDTEXIMAGE2DPROC __rglgen_glCompressedTexImage2D;
RGLSYMGLCOMPRESSEDTEXIMAGE1DPROC __rglgen_glCompressedTexImage1D;
RGLSYMGLCOMPRESSEDTEXSUBIMAGE3DPROC __rglgen_glCompressedTexSubImage3D;
RGLSYMGLCOMPRESSEDTEXSUBIMAGE2DPROC __rglgen_glCompressedTexSubImage2D;
RGLSYMGLCOMPRESSEDTEXSUBIMAGE1DPROC __rglgen_glCompressedTexSubImage1D;
RGLSYMGLGETCOMPRESSEDTEXIMAGEPROC __rglgen_glGetCompressedTexImage;
RGLSYMGLCLIENTACTIVETEXTUREPROC __rglgen_glClientActiveTexture;
RGLSYMGLMULTITEXCOORD1DPROC __rglgen_glMultiTexCoord1d;
RGLSYMGLMULTITEXCOORD1DVPROC __rglgen_glMultiTexCoord1dv;
RGLSYMGLMULTITEXCOORD1FPROC __rglgen_glMultiTexCoord1f;
RGLSYMGLMULTITEXCOORD1FVPROC __rglgen_glMultiTexCoord1fv;
RGLSYMGLMULTITEXCOORD1IPROC __rglgen_glMultiTexCoord1i;
RGLSYMGLMULTITEXCOORD1IVPROC __rglgen_glMultiTexCoord1iv;
RGLSYMGLMULTITEXCOORD1SPROC __rglgen_glMultiTexCoord1s;
RGLSYMGLMULTITEXCOORD1SVPROC __rglgen_glMultiTexCoord1sv;
RGLSYMGLMULTITEXCOORD2DPROC __rglgen_glMultiTexCoord2d;
RGLSYMGLMULTITEXCOORD2DVPROC __rglgen_glMultiTexCoord2dv;
RGLSYMGLMULTITEXCOORD2FPROC __rglgen_glMultiTexCoord2f;
RGLSYMGLMULTITEXCOORD2FVPROC __rglgen_glMultiTexCoord2fv;
RGLSYMGLMULTITEXCOORD2IPROC __rglgen_glMultiTexCoord2i;
RGLSYMGLMULTITEXCOORD2IVPROC __rglgen_glMultiTexCoord2iv;
RGLSYMGLMULTITEXCOORD2SPROC __rglgen_glMultiTexCoord2s;
RGLSYMGLMULTITEXCOORD2SVPROC __rglgen_glMultiTexCoord2sv;
RGLSYMGLMULTITEXCOORD3DPROC __rglgen_glMultiTexCoord3d;
RGLSYMGLMULTITEXCOORD3DVPROC __rglgen_glMultiTexCoord3dv;
RGLSYMGLMULTITEXCOORD3FPROC __rglgen_glMultiTexCoord3f;
RGLSYMGLMULTITEXCOORD3FVPROC __rglgen_glMultiTexCoord3fv;
RGLSYMGLMULTITEXCOORD3IPROC __rglgen_glMultiTexCoord3i;
RGLSYMGLMULTITEXCOORD3IVPROC __rglgen_glMultiTexCoord3iv;
RGLSYMGLMULTITEXCOORD3SPROC __rglgen_glMultiTexCoord3s;
RGLSYMGLMULTITEXCOORD3SVPROC __rglgen_glMultiTexCoord3sv;
RGLSYMGLMULTITEXCOORD4DPROC __rglgen_glMultiTexCoord4d;
RGLSYMGLMULTITEXCOORD4DVPROC __rglgen_glMultiTexCoord4dv;
RGLSYMGLMULTITEXCOORD4FPROC __rglgen_glMultiTexCoord4f;
RGLSYMGLMULTITEXCOORD4FVPROC __rglgen_glMultiTexCoord4fv;
RGLSYMGLMULTITEXCOORD4IPROC __rglgen_glMultiTexCoord4i;
RGLSYMGLMULTITEXCOORD4IVPROC __rglgen_glMultiTexCoord4iv;
RGLSYMGLMULTITEXCOORD4SPROC __rglgen_glMultiTexCoord4s;
RGLSYMGLMULTITEXCOORD4SVPROC __rglgen_glMultiTexCoord4sv;
RGLSYMGLLOADTRANSPOSEMATRIXFPROC __rglgen_glLoadTransposeMatrixf;
RGLSYMGLLOADTRANSPOSEMATRIXDPROC __rglgen_glLoadTransposeMatrixd;
RGLSYMGLMULTTRANSPOSEMATRIXFPROC __rglgen_glMultTransposeMatrixf;
RGLSYMGLMULTTRANSPOSEMATRIXDPROC __rglgen_glMultTransposeMatrixd;
RGLSYMGLBLENDFUNCSEPARATEPROC __rglgen_glBlendFuncSeparate;
RGLSYMGLMULTIDRAWARRAYSPROC __rglgen_glMultiDrawArrays;
RGLSYMGLMULTIDRAWELEMENTSPROC __rglgen_glMultiDrawElements;
RGLSYMGLPOINTPARAMETERFPROC __rglgen_glPointParameterf;
RGLSYMGLPOINTPARAMETERFVPROC __rglgen_glPointParameterfv;
RGLSYMGLPOINTPARAMETERIPROC __rglgen_glPointParameteri;
RGLSYMGLPOINTPARAMETERIVPROC __rglgen_glPointParameteriv;
RGLSYMGLFOGCOORDFPROC __rglgen_glFogCoordf;
RGLSYMGLFOGCOORDFVPROC __rglgen_glFogCoordfv;
RGLSYMGLFOGCOORDDPROC __rglgen_glFogCoordd;
RGLSYMGLFOGCOORDDVPROC __rglgen_glFogCoorddv;
RGLSYMGLFOGCOORDPOINTERPROC __rglgen_glFogCoordPointer;
RGLSYMGLSECONDARYCOLOR3BPROC __rglgen_glSecondaryColor3b;
RGLSYMGLSECONDARYCOLOR3BVPROC __rglgen_glSecondaryColor3bv;
RGLSYMGLSECONDARYCOLOR3DPROC __rglgen_glSecondaryColor3d;
RGLSYMGLSECONDARYCOLOR3DVPROC __rglgen_glSecondaryColor3dv;
RGLSYMGLSECONDARYCOLOR3FPROC __rglgen_glSecondaryColor3f;
RGLSYMGLSECONDARYCOLOR3FVPROC __rglgen_glSecondaryColor3fv;
RGLSYMGLSECONDARYCOLOR3IPROC __rglgen_glSecondaryColor3i;
RGLSYMGLSECONDARYCOLOR3IVPROC __rglgen_glSecondaryColor3iv;
RGLSYMGLSECONDARYCOLOR3SPROC __rglgen_glSecondaryColor3s;
RGLSYMGLSECONDARYCOLOR3SVPROC __rglgen_glSecondaryColor3sv;
RGLSYMGLSECONDARYCOLOR3UBPROC __rglgen_glSecondaryColor3ub;
RGLSYMGLSECONDARYCOLOR3UBVPROC __rglgen_glSecondaryColor3ubv;
RGLSYMGLSECONDARYCOLOR3UIPROC __rglgen_glSecondaryColor3ui;
RGLSYMGLSECONDARYCOLOR3UIVPROC __rglgen_glSecondaryColor3uiv;
RGLSYMGLSECONDARYCOLOR3USPROC __rglgen_glSecondaryColor3us;
RGLSYMGLSECONDARYCOLOR3USVPROC __rglgen_glSecondaryColor3usv;
RGLSYMGLSECONDARYCOLORPOINTERPROC __rglgen_glSecondaryColorPointer;
RGLSYMGLWINDOWPOS2DPROC __rglgen_glWindowPos2d;
RGLSYMGLWINDOWPOS2DVPROC __rglgen_glWindowPos2dv;
RGLSYMGLWINDOWPOS2FPROC __rglgen_glWindowPos2f;
RGLSYMGLWINDOWPOS2FVPROC __rglgen_glWindowPos2fv;
RGLSYMGLWINDOWPOS2IPROC __rglgen_glWindowPos2i;
RGLSYMGLWINDOWPOS2IVPROC __rglgen_glWindowPos2iv;
RGLSYMGLWINDOWPOS2SPROC __rglgen_glWindowPos2s;
RGLSYMGLWINDOWPOS2SVPROC __rglgen_glWindowPos2sv;
RGLSYMGLWINDOWPOS3DPROC __rglgen_glWindowPos3d;
RGLSYMGLWINDOWPOS3DVPROC __rglgen_glWindowPos3dv;
RGLSYMGLWINDOWPOS3FPROC __rglgen_glWindowPos3f;
RGLSYMGLWINDOWPOS3FVPROC __rglgen_glWindowPos3fv;
RGLSYMGLWINDOWPOS3IPROC __rglgen_glWindowPos3i;
RGLSYMGLWINDOWPOS3IVPROC __rglgen_glWindowPos3iv;
RGLSYMGLWINDOWPOS3SPROC __rglgen_glWindowPos3s;
RGLSYMGLWINDOWPOS3SVPROC __rglgen_glWindowPos3sv;
RGLSYMGLBLENDCOLORPROC __rglgen_glBlendColor;
RGLSYMGLBLENDEQUATIONPROC __rglgen_glBlendEquation;
RGLSYMGLGENQUERIESPROC __rglgen_glGenQueries;
RGLSYMGLDELETEQUERIESPROC __rglgen_glDeleteQueries;
RGLSYMGLISQUERYPROC __rglgen_glIsQuery;
RGLSYMGLBEGINQUERYPROC __rglgen_glBeginQuery;
RGLSYMGLENDQUERYPROC __rglgen_glEndQuery;
RGLSYMGLGETQUERYIVPROC __rglgen_glGetQueryiv;
RGLSYMGLGETQUERYOBJECTIVPROC __rglgen_glGetQueryObjectiv;
RGLSYMGLGETQUERYOBJECTUIVPROC __rglgen_glGetQueryObjectuiv;
RGLSYMGLBINDBUFFERPROC __rglgen_glBindBuffer;
RGLSYMGLDELETEBUFFERSPROC __rglgen_glDeleteBuffers;
RGLSYMGLGENBUFFERSPROC __rglgen_glGenBuffers;
RGLSYMGLISBUFFERPROC __rglgen_glIsBuffer;
RGLSYMGLBUFFERDATAPROC __rglgen_glBufferData;
RGLSYMGLBUFFERSUBDATAPROC __rglgen_glBufferSubData;
RGLSYMGLGETBUFFERSUBDATAPROC __rglgen_glGetBufferSubData;
RGLSYMGLMAPBUFFERPROC __rglgen_glMapBuffer;
RGLSYMGLUNMAPBUFFERPROC __rglgen_glUnmapBuffer;
RGLSYMGLGETBUFFERPARAMETERIVPROC __rglgen_glGetBufferParameteriv;
RGLSYMGLGETBUFFERPOINTERVPROC __rglgen_glGetBufferPointerv;
RGLSYMGLBLENDEQUATIONSEPARATEPROC __rglgen_glBlendEquationSeparate;
RGLSYMGLDRAWBUFFERSPROC __rglgen_glDrawBuffers;
RGLSYMGLSTENCILOPSEPARATEPROC __rglgen_glStencilOpSeparate;
RGLSYMGLSTENCILFUNCSEPARATEPROC __rglgen_glStencilFuncSeparate;
RGLSYMGLSTENCILMASKSEPARATEPROC __rglgen_glStencilMaskSeparate;
RGLSYMGLATTACHSHADERPROC __rglgen_glAttachShader;
RGLSYMGLBINDATTRIBLOCATIONPROC __rglgen_glBindAttribLocation;
RGLSYMGLCOMPILESHADERPROC __rglgen_glCompileShader;
RGLSYMGLCREATEPROGRAMPROC __rglgen_glCreateProgram;
RGLSYMGLCREATESHADERPROC __rglgen_glCreateShader;
RGLSYMGLDELETEPROGRAMPROC __rglgen_glDeleteProgram;
RGLSYMGLDELETESHADERPROC __rglgen_glDeleteShader;
RGLSYMGLDETACHSHADERPROC __rglgen_glDetachShader;
RGLSYMGLDISABLEVERTEXATTRIBARRAYPROC __rglgen_glDisableVertexAttribArray;
RGLSYMGLENABLEVERTEXATTRIBARRAYPROC __rglgen_glEnableVertexAttribArray;
RGLSYMGLGETACTIVEATTRIBPROC __rglgen_glGetActiveAttrib;
RGLSYMGLGETACTIVEUNIFORMPROC __rglgen_glGetActiveUniform;
RGLSYMGLGETATTACHEDSHADERSPROC __rglgen_glGetAttachedShaders;
RGLSYMGLGETATTRIBLOCATIONPROC __rglgen_glGetAttribLocation;
RGLSYMGLGETPROGRAMIVPROC __rglgen_glGetProgramiv;
RGLSYMGLGETPROGRAMINFOLOGPROC __rglgen_glGetProgramInfoLog;
RGLSYMGLGETSHADERIVPROC __rglgen_glGetShaderiv;
RGLSYMGLGETSHADERINFOLOGPROC __rglgen_glGetShaderInfoLog;
RGLSYMGLGETSHADERSOURCEPROC __rglgen_glGetShaderSource;
RGLSYMGLGETUNIFORMLOCATIONPROC __rglgen_glGetUniformLocation;
RGLSYMGLGETUNIFORMFVPROC __rglgen_glGetUniformfv;
RGLSYMGLGETUNIFORMIVPROC __rglgen_glGetUniformiv;
RGLSYMGLGETVERTEXATTRIBDVPROC __rglgen_glGetVertexAttribdv;
RGLSYMGLGETVERTEXATTRIBFVPROC __rglgen_glGetVertexAttribfv;
RGLSYMGLGETVERTEXATTRIBIVPROC __rglgen_glGetVertexAttribiv;
RGLSYMGLGETVERTEXATTRIBPOINTERVPROC __rglgen_glGetVertexAttribPointerv;
RGLSYMGLISPROGRAMPROC __rglgen_glIsProgram;
RGLSYMGLISSHADERPROC __rglgen_glIsShader;
RGLSYMGLLINKPROGRAMPROC __rglgen_glLinkProgram;
RGLSYMGLSHADERSOURCEPROC __rglgen_glShaderSource;
RGLSYMGLUSEPROGRAMPROC __rglgen_glUseProgram;
RGLSYMGLUNIFORM1FPROC __rglgen_glUniform1f;
RGLSYMGLUNIFORM2FPROC __rglgen_glUniform2f;
RGLSYMGLUNIFORM3FPROC __rglgen_glUniform3f;
RGLSYMGLUNIFORM4FPROC __rglgen_glUniform4f;
RGLSYMGLUNIFORM1IPROC __rglgen_glUniform1i;
RGLSYMGLUNIFORM2IPROC __rglgen_glUniform2i;
RGLSYMGLUNIFORM3IPROC __rglgen_glUniform3i;
RGLSYMGLUNIFORM4IPROC __rglgen_glUniform4i;
RGLSYMGLUNIFORM1FVPROC __rglgen_glUniform1fv;
RGLSYMGLUNIFORM2FVPROC __rglgen_glUniform2fv;
RGLSYMGLUNIFORM3FVPROC __rglgen_glUniform3fv;
RGLSYMGLUNIFORM4FVPROC __rglgen_glUniform4fv;
RGLSYMGLUNIFORM1IVPROC __rglgen_glUniform1iv;
RGLSYMGLUNIFORM2IVPROC __rglgen_glUniform2iv;
RGLSYMGLUNIFORM3IVPROC __rglgen_glUniform3iv;
RGLSYMGLUNIFORM4IVPROC __rglgen_glUniform4iv;
RGLSYMGLUNIFORMMATRIX2FVPROC __rglgen_glUniformMatrix2fv;
RGLSYMGLUNIFORMMATRIX3FVPROC __rglgen_glUniformMatrix3fv;
RGLSYMGLUNIFORMMATRIX4FVPROC __rglgen_glUniformMatrix4fv;
RGLSYMGLVALIDATEPROGRAMPROC __rglgen_glValidateProgram;
RGLSYMGLVERTEXATTRIB1DPROC __rglgen_glVertexAttrib1d;
RGLSYMGLVERTEXATTRIB1DVPROC __rglgen_glVertexAttrib1dv;
RGLSYMGLVERTEXATTRIB1FPROC __rglgen_glVertexAttrib1f;
RGLSYMGLVERTEXATTRIB1FVPROC __rglgen_glVertexAttrib1fv;
RGLSYMGLVERTEXATTRIB1SPROC __rglgen_glVertexAttrib1s;
RGLSYMGLVERTEXATTRIB1SVPROC __rglgen_glVertexAttrib1sv;
RGLSYMGLVERTEXATTRIB2DPROC __rglgen_glVertexAttrib2d;
RGLSYMGLVERTEXATTRIB2DVPROC __rglgen_glVertexAttrib2dv;
RGLSYMGLVERTEXATTRIB2FPROC __rglgen_glVertexAttrib2f;
RGLSYMGLVERTEXATTRIB2FVPROC __rglgen_glVertexAttrib2fv;
RGLSYMGLVERTEXATTRIB2SPROC __rglgen_glVertexAttrib2s;
RGLSYMGLVERTEXATTRIB2SVPROC __rglgen_glVertexAttrib2sv;
RGLSYMGLVERTEXATTRIB3DPROC __rglgen_glVertexAttrib3d;
RGLSYMGLVERTEXATTRIB3DVPROC __rglgen_glVertexAttrib3dv;
RGLSYMGLVERTEXATTRIB3FPROC __rglgen_glVertexAttrib3f;
RGLSYMGLVERTEXATTRIB3FVPROC __rglgen_glVertexAttrib3fv;
RGLSYMGLVERTEXATTRIB3SPROC __rglgen_glVertexAttrib3s;
RGLSYMGLVERTEXATTRIB3SVPROC __rglgen_glVertexAttrib3sv;
RGLSYMGLVERTEXATTRIB4NBVPROC __rglgen_glVertexAttrib4Nbv;
RGLSYMGLVERTEXATTRIB4NIVPROC __rglgen_glVertexAttrib4Niv;
RGLSYMGLVERTEXATTRIB4NSVPROC __rglgen_glVertexAttrib4Nsv;
RGLSYMGLVERTEXATTRIB4NUBPROC __rglgen_glVertexAttrib4Nub;
RGLSYMGLVERTEXATTRIB4NUBVPROC __rglgen_glVertexAttrib4Nubv;
RGLSYMGLVERTEXATTRIB4NUIVPROC __rglgen_glVertexAttrib4Nuiv;
RGLSYMGLVERTEXATTRIB4NUSVPROC __rglgen_glVertexAttrib4Nusv;
RGLSYMGLVERTEXATTRIB4BVPROC __rglgen_glVertexAttrib4bv;
RGLSYMGLVERTEXATTRIB4DPROC __rglgen_glVertexAttrib4d;
RGLSYMGLVERTEXATTRIB4DVPROC __rglgen_glVertexAttrib4dv;
RGLSYMGLVERTEXATTRIB4FPROC __rglgen_glVertexAttrib4f;
RGLSYMGLVERTEXATTRIB4FVPROC __rglgen_glVertexAttrib4fv;
RGLSYMGLVERTEXATTRIB4IVPROC __rglgen_glVertexAttrib4iv;
RGLSYMGLVERTEXATTRIB4SPROC __rglgen_glVertexAttrib4s;
RGLSYMGLVERTEXATTRIB4SVPROC __rglgen_glVertexAttrib4sv;
RGLSYMGLVERTEXATTRIB4UBVPROC __rglgen_glVertexAttrib4ubv;
RGLSYMGLVERTEXATTRIB4UIVPROC __rglgen_glVertexAttrib4uiv;
RGLSYMGLVERTEXATTRIB4USVPROC __rglgen_glVertexAttrib4usv;
RGLSYMGLVERTEXATTRIBPOINTERPROC __rglgen_glVertexAttribPointer;
RGLSYMGLUNIFORMMATRIX2X3FVPROC __rglgen_glUniformMatrix2x3fv;
RGLSYMGLUNIFORMMATRIX3X2FVPROC __rglgen_glUniformMatrix3x2fv;
RGLSYMGLUNIFORMMATRIX2X4FVPROC __rglgen_glUniformMatrix2x4fv;
RGLSYMGLUNIFORMMATRIX4X2FVPROC __rglgen_glUniformMatrix4x2fv;
RGLSYMGLUNIFORMMATRIX3X4FVPROC __rglgen_glUniformMatrix3x4fv;
RGLSYMGLUNIFORMMATRIX4X3FVPROC __rglgen_glUniformMatrix4x3fv;
RGLSYMGLCOLORMASKIPROC __rglgen_glColorMaski;
RGLSYMGLGETBOOLEANI_VPROC __rglgen_glGetBooleani_v;
RGLSYMGLGETINTEGERI_VPROC __rglgen_glGetIntegeri_v;
RGLSYMGLENABLEIPROC __rglgen_glEnablei;
RGLSYMGLDISABLEIPROC __rglgen_glDisablei;
RGLSYMGLISENABLEDIPROC __rglgen_glIsEnabledi;
RGLSYMGLBEGINTRANSFORMFEEDBACKPROC __rglgen_glBeginTransformFeedback;
RGLSYMGLENDTRANSFORMFEEDBACKPROC __rglgen_glEndTransformFeedback;
RGLSYMGLBINDBUFFERRANGEPROC __rglgen_glBindBufferRange;
RGLSYMGLBINDBUFFERBASEPROC __rglgen_glBindBufferBase;
RGLSYMGLTRANSFORMFEEDBACKVARYINGSPROC __rglgen_glTransformFeedbackVaryings;
RGLSYMGLGETTRANSFORMFEEDBACKVARYINGPROC __rglgen_glGetTransformFeedbackVarying;
RGLSYMGLCLAMPCOLORPROC __rglgen_glClampColor;
RGLSYMGLBEGINCONDITIONALRENDERPROC __rglgen_glBeginConditionalRender;
RGLSYMGLENDCONDITIONALRENDERPROC __rglgen_glEndConditionalRender;
RGLSYMGLVERTEXATTRIBIPOINTERPROC __rglgen_glVertexAttribIPointer;
RGLSYMGLGETVERTEXATTRIBIIVPROC __rglgen_glGetVertexAttribIiv;
RGLSYMGLGETVERTEXATTRIBIUIVPROC __rglgen_glGetVertexAttribIuiv;
RGLSYMGLVERTEXATTRIBI1IPROC __rglgen_glVertexAttribI1i;
RGLSYMGLVERTEXATTRIBI2IPROC __rglgen_glVertexAttribI2i;
RGLSYMGLVERTEXATTRIBI3IPROC __rglgen_glVertexAttribI3i;
RGLSYMGLVERTEXATTRIBI4IPROC __rglgen_glVertexAttribI4i;
RGLSYMGLVERTEXATTRIBI1UIPROC __rglgen_glVertexAttribI1ui;
RGLSYMGLVERTEXATTRIBI2UIPROC __rglgen_glVertexAttribI2ui;
RGLSYMGLVERTEXATTRIBI3UIPROC __rglgen_glVertexAttribI3ui;
RGLSYMGLVERTEXATTRIBI4UIPROC __rglgen_glVertexAttribI4ui;
RGLSYMGLVERTEXATTRIBI1IVPROC __rglgen_glVertexAttribI1iv;
RGLSYMGLVERTEXATTRIBI2IVPROC __rglgen_glVertexAttribI2iv;
RGLSYMGLVERTEXATTRIBI3IVPROC __rglgen_glVertexAttribI3iv;
RGLSYMGLVERTEXATTRIBI4IVPROC __rglgen_glVertexAttribI4iv;
RGLSYMGLVERTEXATTRIBI1UIVPROC __rglgen_glVertexAttribI1uiv;
RGLSYMGLVERTEXATTRIBI2UIVPROC __rglgen_glVertexAttribI2uiv;
RGLSYMGLVERTEXATTRIBI3UIVPROC __rglgen_glVertexAttribI3uiv;
RGLSYMGLVERTEXATTRIBI4UIVPROC __rglgen_glVertexAttribI4uiv;
RGLSYMGLVERTEXATTRIBI4BVPROC __rglgen_glVertexAttribI4bv;
RGLSYMGLVERTEXATTRIBI4SVPROC __rglgen_glVertexAttribI4sv;
RGLSYMGLVERTEXATTRIBI4UBVPROC __rglgen_glVertexAttribI4ubv;
RGLSYMGLVERTEXATTRIBI4USVPROC __rglgen_glVertexAttribI4usv;
RGLSYMGLGETUNIFORMUIVPROC __rglgen_glGetUniformuiv;
RGLSYMGLBINDFRAGDATALOCATIONPROC __rglgen_glBindFragDataLocation;
RGLSYMGLGETFRAGDATALOCATIONPROC __rglgen_glGetFragDataLocation;
RGLSYMGLUNIFORM1UIPROC __rglgen_glUniform1ui;
RGLSYMGLUNIFORM2UIPROC __rglgen_glUniform2ui;
RGLSYMGLUNIFORM3UIPROC __rglgen_glUniform3ui;
RGLSYMGLUNIFORM4UIPROC __rglgen_glUniform4ui;
RGLSYMGLUNIFORM1UIVPROC __rglgen_glUniform1uiv;
RGLSYMGLUNIFORM2UIVPROC __rglgen_glUniform2uiv;
RGLSYMGLUNIFORM3UIVPROC __rglgen_glUniform3uiv;
RGLSYMGLUNIFORM4UIVPROC __rglgen_glUniform4uiv;
RGLSYMGLTEXPARAMETERIIVPROC __rglgen_glTexParameterIiv;
RGLSYMGLTEXPARAMETERIUIVPROC __rglgen_glTexParameterIuiv;
RGLSYMGLGETTEXPARAMETERIIVPROC __rglgen_glGetTexParameterIiv;
RGLSYMGLGETTEXPARAMETERIUIVPROC __rglgen_glGetTexParameterIuiv;
RGLSYMGLCLEARBUFFERIVPROC __rglgen_glClearBufferiv;
RGLSYMGLCLEARBUFFERUIVPROC __rglgen_glClearBufferuiv;
RGLSYMGLCLEARBUFFERFVPROC __rglgen_glClearBufferfv;
RGLSYMGLCLEARBUFFERFIPROC __rglgen_glClearBufferfi;
RGLSYMGLGETSTRINGIPROC __rglgen_glGetStringi;
RGLSYMGLISRENDERBUFFERPROC __rglgen_glIsRenderbuffer;
RGLSYMGLBINDRENDERBUFFERPROC __rglgen_glBindRenderbuffer;
RGLSYMGLDELETERENDERBUFFERSPROC __rglgen_glDeleteRenderbuffers;
RGLSYMGLGENRENDERBUFFERSPROC __rglgen_glGenRenderbuffers;
RGLSYMGLRENDERBUFFERSTORAGEPROC __rglgen_glRenderbufferStorage;
RGLSYMGLGETRENDERBUFFERPARAMETERIVPROC __rglgen_glGetRenderbufferParameteriv;
RGLSYMGLISFRAMEBUFFERPROC __rglgen_glIsFramebuffer;
RGLSYMGLBINDFRAMEBUFFERPROC __rglgen_glBindFramebuffer;
RGLSYMGLDELETEFRAMEBUFFERSPROC __rglgen_glDeleteFramebuffers;
RGLSYMGLGENFRAMEBUFFERSPROC __rglgen_glGenFramebuffers;
RGLSYMGLCHECKFRAMEBUFFERSTATUSPROC __rglgen_glCheckFramebufferStatus;
RGLSYMGLFRAMEBUFFERTEXTURE1DPROC __rglgen_glFramebufferTexture1D;
RGLSYMGLFRAMEBUFFERTEXTURE2DPROC __rglgen_glFramebufferTexture2D;
RGLSYMGLFRAMEBUFFERTEXTURE3DPROC __rglgen_glFramebufferTexture3D;
RGLSYMGLFRAMEBUFFERRENDERBUFFERPROC __rglgen_glFramebufferRenderbuffer;
RGLSYMGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC __rglgen_glGetFramebufferAttachmentParameteriv;
RGLSYMGLGENERATEMIPMAPPROC __rglgen_glGenerateMipmap;
RGLSYMGLBLITFRAMEBUFFERPROC __rglgen_glBlitFramebuffer;
RGLSYMGLRENDERBUFFERSTORAGEMULTISAMPLEPROC __rglgen_glRenderbufferStorageMultisample;
RGLSYMGLFRAMEBUFFERTEXTURELAYERPROC __rglgen_glFramebufferTextureLayer;
RGLSYMGLMAPBUFFERRANGEPROC __rglgen_glMapBufferRange;
RGLSYMGLFLUSHMAPPEDBUFFERRANGEPROC __rglgen_glFlushMappedBufferRange;
RGLSYMGLBINDVERTEXARRAYPROC __rglgen_glBindVertexArray;
RGLSYMGLDELETEVERTEXARRAYSPROC __rglgen_glDeleteVertexArrays;
RGLSYMGLGENVERTEXARRAYSPROC __rglgen_glGenVertexArrays;
RGLSYMGLISVERTEXARRAYPROC __rglgen_glIsVertexArray;
RGLSYMGLDRAWARRAYSINSTANCEDPROC __rglgen_glDrawArraysInstanced;
RGLSYMGLDRAWELEMENTSINSTANCEDPROC __rglgen_glDrawElementsInstanced;
RGLSYMGLTEXBUFFERPROC __rglgen_glTexBuffer;
RGLSYMGLPRIMITIVERESTARTINDEXPROC __rglgen_glPrimitiveRestartIndex;
RGLSYMGLCOPYBUFFERSUBDATAPROC __rglgen_glCopyBufferSubData;
RGLSYMGLGETUNIFORMINDICESPROC __rglgen_glGetUniformIndices;
RGLSYMGLGETACTIVEUNIFORMSIVPROC __rglgen_glGetActiveUniformsiv;
RGLSYMGLGETACTIVEUNIFORMNAMEPROC __rglgen_glGetActiveUniformName;
RGLSYMGLGETUNIFORMBLOCKINDEXPROC __rglgen_glGetUniformBlockIndex;
RGLSYMGLGETACTIVEUNIFORMBLOCKIVPROC __rglgen_glGetActiveUniformBlockiv;
RGLSYMGLGETACTIVEUNIFORMBLOCKNAMEPROC __rglgen_glGetActiveUniformBlockName;
RGLSYMGLUNIFORMBLOCKBINDINGPROC __rglgen_glUniformBlockBinding;
RGLSYMGLDRAWELEMENTSBASEVERTEXPROC __rglgen_glDrawElementsBaseVertex;
RGLSYMGLDRAWRANGEELEMENTSBASEVERTEXPROC __rglgen_glDrawRangeElementsBaseVertex;
RGLSYMGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC __rglgen_glDrawElementsInstancedBaseVertex;
RGLSYMGLMULTIDRAWELEMENTSBASEVERTEXPROC __rglgen_glMultiDrawElementsBaseVertex;
RGLSYMGLPROVOKINGVERTEXPROC __rglgen_glProvokingVertex;
RGLSYMGLFENCESYNCPROC __rglgen_glFenceSync;
RGLSYMGLISSYNCPROC __rglgen_glIsSync;
RGLSYMGLDELETESYNCPROC __rglgen_glDeleteSync;
RGLSYMGLCLIENTWAITSYNCPROC __rglgen_glClientWaitSync;
RGLSYMGLWAITSYNCPROC __rglgen_glWaitSync;
RGLSYMGLGETINTEGER64VPROC __rglgen_glGetInteger64v;
RGLSYMGLGETSYNCIVPROC __rglgen_glGetSynciv;
RGLSYMGLGETINTEGER64I_VPROC __rglgen_glGetInteger64i_v;
RGLSYMGLGETBUFFERPARAMETERI64VPROC __rglgen_glGetBufferParameteri64v;
RGLSYMGLFRAMEBUFFERTEXTUREPROC __rglgen_glFramebufferTexture;
RGLSYMGLTEXIMAGE2DMULTISAMPLEPROC __rglgen_glTexImage2DMultisample;
RGLSYMGLTEXIMAGE3DMULTISAMPLEPROC __rglgen_glTexImage3DMultisample;
RGLSYMGLGETMULTISAMPLEFVPROC __rglgen_glGetMultisamplefv;
RGLSYMGLSAMPLEMASKIPROC __rglgen_glSampleMaski;
RGLSYMGLBINDFRAGDATALOCATIONINDEXEDPROC __rglgen_glBindFragDataLocationIndexed;
RGLSYMGLGETFRAGDATAINDEXPROC __rglgen_glGetFragDataIndex;
RGLSYMGLGENSAMPLERSPROC __rglgen_glGenSamplers;
RGLSYMGLDELETESAMPLERSPROC __rglgen_glDeleteSamplers;
RGLSYMGLISSAMPLERPROC __rglgen_glIsSampler;
RGLSYMGLBINDSAMPLERPROC __rglgen_glBindSampler;
RGLSYMGLSAMPLERPARAMETERIPROC __rglgen_glSamplerParameteri;
RGLSYMGLSAMPLERPARAMETERIVPROC __rglgen_glSamplerParameteriv;
RGLSYMGLSAMPLERPARAMETERFPROC __rglgen_glSamplerParameterf;
RGLSYMGLSAMPLERPARAMETERFVPROC __rglgen_glSamplerParameterfv;
RGLSYMGLSAMPLERPARAMETERIIVPROC __rglgen_glSamplerParameterIiv;
RGLSYMGLSAMPLERPARAMETERIUIVPROC __rglgen_glSamplerParameterIuiv;
RGLSYMGLGETSAMPLERPARAMETERIVPROC __rglgen_glGetSamplerParameteriv;
RGLSYMGLGETSAMPLERPARAMETERIIVPROC __rglgen_glGetSamplerParameterIiv;
RGLSYMGLGETSAMPLERPARAMETERFVPROC __rglgen_glGetSamplerParameterfv;
RGLSYMGLGETSAMPLERPARAMETERIUIVPROC __rglgen_glGetSamplerParameterIuiv;
RGLSYMGLQUERYCOUNTERPROC __rglgen_glQueryCounter;
RGLSYMGLGETQUERYOBJECTI64VPROC __rglgen_glGetQueryObjecti64v;
RGLSYMGLGETQUERYOBJECTUI64VPROC __rglgen_glGetQueryObjectui64v;
RGLSYMGLVERTEXATTRIBDIVISORPROC __rglgen_glVertexAttribDivisor;
RGLSYMGLVERTEXATTRIBP1UIPROC __rglgen_glVertexAttribP1ui;
RGLSYMGLVERTEXATTRIBP1UIVPROC __rglgen_glVertexAttribP1uiv;
RGLSYMGLVERTEXATTRIBP2UIPROC __rglgen_glVertexAttribP2ui;
RGLSYMGLVERTEXATTRIBP2UIVPROC __rglgen_glVertexAttribP2uiv;
RGLSYMGLVERTEXATTRIBP3UIPROC __rglgen_glVertexAttribP3ui;
RGLSYMGLVERTEXATTRIBP3UIVPROC __rglgen_glVertexAttribP3uiv;
RGLSYMGLVERTEXATTRIBP4UIPROC __rglgen_glVertexAttribP4ui;
RGLSYMGLVERTEXATTRIBP4UIVPROC __rglgen_glVertexAttribP4uiv;
RGLSYMGLVERTEXP2UIPROC __rglgen_glVertexP2ui;
RGLSYMGLVERTEXP2UIVPROC __rglgen_glVertexP2uiv;
RGLSYMGLVERTEXP3UIPROC __rglgen_glVertexP3ui;
RGLSYMGLVERTEXP3UIVPROC __rglgen_glVertexP3uiv;
RGLSYMGLVERTEXP4UIPROC __rglgen_glVertexP4ui;
RGLSYMGLVERTEXP4UIVPROC __rglgen_glVertexP4uiv;
RGLSYMGLTEXCOORDP1UIPROC __rglgen_glTexCoordP1ui;
RGLSYMGLTEXCOORDP1UIVPROC __rglgen_glTexCoordP1uiv;
RGLSYMGLTEXCOORDP2UIPROC __rglgen_glTexCoordP2ui;
RGLSYMGLTEXCOORDP2UIVPROC __rglgen_glTexCoordP2uiv;
RGLSYMGLTEXCOORDP3UIPROC __rglgen_glTexCoordP3ui;
RGLSYMGLTEXCOORDP3UIVPROC __rglgen_glTexCoordP3uiv;
RGLSYMGLTEXCOORDP4UIPROC __rglgen_glTexCoordP4ui;
RGLSYMGLTEXCOORDP4UIVPROC __rglgen_glTexCoordP4uiv;
RGLSYMGLMULTITEXCOORDP1UIPROC __rglgen_glMultiTexCoordP1ui;
RGLSYMGLMULTITEXCOORDP1UIVPROC __rglgen_glMultiTexCoordP1uiv;
RGLSYMGLMULTITEXCOORDP2UIPROC __rglgen_glMultiTexCoordP2ui;
RGLSYMGLMULTITEXCOORDP2UIVPROC __rglgen_glMultiTexCoordP2uiv;
RGLSYMGLMULTITEXCOORDP3UIPROC __rglgen_glMultiTexCoordP3ui;
RGLSYMGLMULTITEXCOORDP3UIVPROC __rglgen_glMultiTexCoordP3uiv;
RGLSYMGLMULTITEXCOORDP4UIPROC __rglgen_glMultiTexCoordP4ui;
RGLSYMGLMULTITEXCOORDP4UIVPROC __rglgen_glMultiTexCoordP4uiv;
RGLSYMGLNORMALP3UIPROC __rglgen_glNormalP3ui;
RGLSYMGLNORMALP3UIVPROC __rglgen_glNormalP3uiv;
RGLSYMGLCOLORP3UIPROC __rglgen_glColorP3ui;
RGLSYMGLCOLORP3UIVPROC __rglgen_glColorP3uiv;
RGLSYMGLCOLORP4UIPROC __rglgen_glColorP4ui;
RGLSYMGLCOLORP4UIVPROC __rglgen_glColorP4uiv;
RGLSYMGLSECONDARYCOLORP3UIPROC __rglgen_glSecondaryColorP3ui;
RGLSYMGLSECONDARYCOLORP3UIVPROC __rglgen_glSecondaryColorP3uiv;
RGLSYMGLMINSAMPLESHADINGPROC __rglgen_glMinSampleShading;
RGLSYMGLBLENDEQUATIONIPROC __rglgen_glBlendEquationi;
RGLSYMGLBLENDEQUATIONSEPARATEIPROC __rglgen_glBlendEquationSeparatei;
RGLSYMGLBLENDFUNCIPROC __rglgen_glBlendFunci;
RGLSYMGLBLENDFUNCSEPARATEIPROC __rglgen_glBlendFuncSeparatei;
RGLSYMGLDRAWARRAYSINDIRECTPROC __rglgen_glDrawArraysIndirect;
RGLSYMGLDRAWELEMENTSINDIRECTPROC __rglgen_glDrawElementsIndirect;
RGLSYMGLUNIFORM1DPROC __rglgen_glUniform1d;
RGLSYMGLUNIFORM2DPROC __rglgen_glUniform2d;
RGLSYMGLUNIFORM3DPROC __rglgen_glUniform3d;
RGLSYMGLUNIFORM4DPROC __rglgen_glUniform4d;
RGLSYMGLUNIFORM1DVPROC __rglgen_glUniform1dv;
RGLSYMGLUNIFORM2DVPROC __rglgen_glUniform2dv;
RGLSYMGLUNIFORM3DVPROC __rglgen_glUniform3dv;
RGLSYMGLUNIFORM4DVPROC __rglgen_glUniform4dv;
RGLSYMGLUNIFORMMATRIX2DVPROC __rglgen_glUniformMatrix2dv;
RGLSYMGLUNIFORMMATRIX3DVPROC __rglgen_glUniformMatrix3dv;
RGLSYMGLUNIFORMMATRIX4DVPROC __rglgen_glUniformMatrix4dv;
RGLSYMGLUNIFORMMATRIX2X3DVPROC __rglgen_glUniformMatrix2x3dv;
RGLSYMGLUNIFORMMATRIX2X4DVPROC __rglgen_glUniformMatrix2x4dv;
RGLSYMGLUNIFORMMATRIX3X2DVPROC __rglgen_glUniformMatrix3x2dv;
RGLSYMGLUNIFORMMATRIX3X4DVPROC __rglgen_glUniformMatrix3x4dv;
RGLSYMGLUNIFORMMATRIX4X2DVPROC __rglgen_glUniformMatrix4x2dv;
RGLSYMGLUNIFORMMATRIX4X3DVPROC __rglgen_glUniformMatrix4x3dv;
RGLSYMGLGETUNIFORMDVPROC __rglgen_glGetUniformdv;
RGLSYMGLGETSUBROUTINEUNIFORMLOCATIONPROC __rglgen_glGetSubroutineUniformLocation;
RGLSYMGLGETSUBROUTINEINDEXPROC __rglgen_glGetSubroutineIndex;
RGLSYMGLGETACTIVESUBROUTINEUNIFORMIVPROC __rglgen_glGetActiveSubroutineUniformiv;
RGLSYMGLGETACTIVESUBROUTINEUNIFORMNAMEPROC __rglgen_glGetActiveSubroutineUniformName;
RGLSYMGLGETACTIVESUBROUTINENAMEPROC __rglgen_glGetActiveSubroutineName;
RGLSYMGLUNIFORMSUBROUTINESUIVPROC __rglgen_glUniformSubroutinesuiv;
RGLSYMGLGETUNIFORMSUBROUTINEUIVPROC __rglgen_glGetUniformSubroutineuiv;
RGLSYMGLGETPROGRAMSTAGEIVPROC __rglgen_glGetProgramStageiv;
RGLSYMGLPATCHPARAMETERIPROC __rglgen_glPatchParameteri;
RGLSYMGLPATCHPARAMETERFVPROC __rglgen_glPatchParameterfv;
RGLSYMGLBINDTRANSFORMFEEDBACKPROC __rglgen_glBindTransformFeedback;
RGLSYMGLDELETETRANSFORMFEEDBACKSPROC __rglgen_glDeleteTransformFeedbacks;
RGLSYMGLGENTRANSFORMFEEDBACKSPROC __rglgen_glGenTransformFeedbacks;
RGLSYMGLISTRANSFORMFEEDBACKPROC __rglgen_glIsTransformFeedback;
RGLSYMGLPAUSETRANSFORMFEEDBACKPROC __rglgen_glPauseTransformFeedback;
RGLSYMGLRESUMETRANSFORMFEEDBACKPROC __rglgen_glResumeTransformFeedback;
RGLSYMGLDRAWTRANSFORMFEEDBACKPROC __rglgen_glDrawTransformFeedback;
RGLSYMGLDRAWTRANSFORMFEEDBACKSTREAMPROC __rglgen_glDrawTransformFeedbackStream;
RGLSYMGLBEGINQUERYINDEXEDPROC __rglgen_glBeginQueryIndexed;
RGLSYMGLENDQUERYINDEXEDPROC __rglgen_glEndQueryIndexed;
RGLSYMGLGETQUERYINDEXEDIVPROC __rglgen_glGetQueryIndexediv;
RGLSYMGLRELEASESHADERCOMPILERPROC __rglgen_glReleaseShaderCompiler;
RGLSYMGLSHADERBINARYPROC __rglgen_glShaderBinary;
RGLSYMGLGETSHADERPRECISIONFORMATPROC __rglgen_glGetShaderPrecisionFormat;
RGLSYMGLDEPTHRANGEFPROC __rglgen_glDepthRangef;
RGLSYMGLCLEARDEPTHFPROC __rglgen_glClearDepthf;
RGLSYMGLGETPROGRAMBINARYPROC __rglgen_glGetProgramBinary;
RGLSYMGLPROGRAMBINARYPROC __rglgen_glProgramBinary;
RGLSYMGLPROGRAMPARAMETERIPROC __rglgen_glProgramParameteri;
RGLSYMGLUSEPROGRAMSTAGESPROC __rglgen_glUseProgramStages;
RGLSYMGLACTIVESHADERPROGRAMPROC __rglgen_glActiveShaderProgram;
RGLSYMGLCREATESHADERPROGRAMVPROC __rglgen_glCreateShaderProgramv;
RGLSYMGLBINDPROGRAMPIPELINEPROC __rglgen_glBindProgramPipeline;
RGLSYMGLDELETEPROGRAMPIPELINESPROC __rglgen_glDeleteProgramPipelines;
RGLSYMGLGENPROGRAMPIPELINESPROC __rglgen_glGenProgramPipelines;
RGLSYMGLISPROGRAMPIPELINEPROC __rglgen_glIsProgramPipeline;
RGLSYMGLGETPROGRAMPIPELINEIVPROC __rglgen_glGetProgramPipelineiv;
RGLSYMGLPROGRAMUNIFORM1IPROC __rglgen_glProgramUniform1i;
RGLSYMGLPROGRAMUNIFORM1IVPROC __rglgen_glProgramUniform1iv;
RGLSYMGLPROGRAMUNIFORM1FPROC __rglgen_glProgramUniform1f;
RGLSYMGLPROGRAMUNIFORM1FVPROC __rglgen_glProgramUniform1fv;
RGLSYMGLPROGRAMUNIFORM1DPROC __rglgen_glProgramUniform1d;
RGLSYMGLPROGRAMUNIFORM1DVPROC __rglgen_glProgramUniform1dv;
RGLSYMGLPROGRAMUNIFORM1UIPROC __rglgen_glProgramUniform1ui;
RGLSYMGLPROGRAMUNIFORM1UIVPROC __rglgen_glProgramUniform1uiv;
RGLSYMGLPROGRAMUNIFORM2IPROC __rglgen_glProgramUniform2i;
RGLSYMGLPROGRAMUNIFORM2IVPROC __rglgen_glProgramUniform2iv;
RGLSYMGLPROGRAMUNIFORM2FPROC __rglgen_glProgramUniform2f;
RGLSYMGLPROGRAMUNIFORM2FVPROC __rglgen_glProgramUniform2fv;
RGLSYMGLPROGRAMUNIFORM2DPROC __rglgen_glProgramUniform2d;
RGLSYMGLPROGRAMUNIFORM2DVPROC __rglgen_glProgramUniform2dv;
RGLSYMGLPROGRAMUNIFORM2UIPROC __rglgen_glProgramUniform2ui;
RGLSYMGLPROGRAMUNIFORM2UIVPROC __rglgen_glProgramUniform2uiv;
RGLSYMGLPROGRAMUNIFORM3IPROC __rglgen_glProgramUniform3i;
RGLSYMGLPROGRAMUNIFORM3IVPROC __rglgen_glProgramUniform3iv;
RGLSYMGLPROGRAMUNIFORM3FPROC __rglgen_glProgramUniform3f;
RGLSYMGLPROGRAMUNIFORM3FVPROC __rglgen_glProgramUniform3fv;
RGLSYMGLPROGRAMUNIFORM3DPROC __rglgen_glProgramUniform3d;
RGLSYMGLPROGRAMUNIFORM3DVPROC __rglgen_glProgramUniform3dv;
RGLSYMGLPROGRAMUNIFORM3UIPROC __rglgen_glProgramUniform3ui;
RGLSYMGLPROGRAMUNIFORM3UIVPROC __rglgen_glProgramUniform3uiv;
RGLSYMGLPROGRAMUNIFORM4IPROC __rglgen_glProgramUniform4i;
RGLSYMGLPROGRAMUNIFORM4IVPROC __rglgen_glProgramUniform4iv;
RGLSYMGLPROGRAMUNIFORM4FPROC __rglgen_glProgramUniform4f;
RGLSYMGLPROGRAMUNIFORM4FVPROC __rglgen_glProgramUniform4fv;
RGLSYMGLPROGRAMUNIFORM4DPROC __rglgen_glProgramUniform4d;
RGLSYMGLPROGRAMUNIFORM4DVPROC __rglgen_glProgramUniform4dv;
RGLSYMGLPROGRAMUNIFORM4UIPROC __rglgen_glProgramUniform4ui;
RGLSYMGLPROGRAMUNIFORM4UIVPROC __rglgen_glProgramUniform4uiv;
RGLSYMGLPROGRAMUNIFORMMATRIX2FVPROC __rglgen_glProgramUniformMatrix2fv;
RGLSYMGLPROGRAMUNIFORMMATRIX3FVPROC __rglgen_glProgramUniformMatrix3fv;
RGLSYMGLPROGRAMUNIFORMMATRIX4FVPROC __rglgen_glProgramUniformMatrix4fv;
RGLSYMGLPROGRAMUNIFORMMATRIX2DVPROC __rglgen_glProgramUniformMatrix2dv;
RGLSYMGLPROGRAMUNIFORMMATRIX3DVPROC __rglgen_glProgramUniformMatrix3dv;
RGLSYMGLPROGRAMUNIFORMMATRIX4DVPROC __rglgen_glProgramUniformMatrix4dv;
RGLSYMGLPROGRAMUNIFORMMATRIX2X3FVPROC __rglgen_glProgramUniformMatrix2x3fv;
RGLSYMGLPROGRAMUNIFORMMATRIX3X2FVPROC __rglgen_glProgramUniformMatrix3x2fv;
RGLSYMGLPROGRAMUNIFORMMATRIX2X4FVPROC __rglgen_glProgramUniformMatrix2x4fv;
RGLSYMGLPROGRAMUNIFORMMATRIX4X2FVPROC __rglgen_glProgramUniformMatrix4x2fv;
RGLSYMGLPROGRAMUNIFORMMATRIX3X4FVPROC __rglgen_glProgramUniformMatrix3x4fv;
RGLSYMGLPROGRAMUNIFORMMATRIX4X3FVPROC __rglgen_glProgramUniformMatrix4x3fv;
RGLSYMGLPROGRAMUNIFORMMATRIX2X3DVPROC __rglgen_glProgramUniformMatrix2x3dv;
RGLSYMGLPROGRAMUNIFORMMATRIX3X2DVPROC __rglgen_glProgramUniformMatrix3x2dv;
RGLSYMGLPROGRAMUNIFORMMATRIX2X4DVPROC __rglgen_glProgramUniformMatrix2x4dv;
RGLSYMGLPROGRAMUNIFORMMATRIX4X2DVPROC __rglgen_glProgramUniformMatrix4x2dv;
RGLSYMGLPROGRAMUNIFORMMATRIX3X4DVPROC __rglgen_glProgramUniformMatrix3x4dv;
RGLSYMGLPROGRAMUNIFORMMATRIX4X3DVPROC __rglgen_glProgramUniformMatrix4x3dv;
RGLSYMGLVALIDATEPROGRAMPIPELINEPROC __rglgen_glValidateProgramPipeline;
RGLSYMGLGETPROGRAMPIPELINEINFOLOGPROC __rglgen_glGetProgramPipelineInfoLog;
RGLSYMGLVERTEXATTRIBL1DPROC __rglgen_glVertexAttribL1d;
RGLSYMGLVERTEXATTRIBL2DPROC __rglgen_glVertexAttribL2d;
RGLSYMGLVERTEXATTRIBL3DPROC __rglgen_glVertexAttribL3d;
RGLSYMGLVERTEXATTRIBL4DPROC __rglgen_glVertexAttribL4d;
RGLSYMGLVERTEXATTRIBL1DVPROC __rglgen_glVertexAttribL1dv;
RGLSYMGLVERTEXATTRIBL2DVPROC __rglgen_glVertexAttribL2dv;
RGLSYMGLVERTEXATTRIBL3DVPROC __rglgen_glVertexAttribL3dv;
RGLSYMGLVERTEXATTRIBL4DVPROC __rglgen_glVertexAttribL4dv;
RGLSYMGLVERTEXATTRIBLPOINTERPROC __rglgen_glVertexAttribLPointer;
RGLSYMGLGETVERTEXATTRIBLDVPROC __rglgen_glGetVertexAttribLdv;
RGLSYMGLVIEWPORTARRAYVPROC __rglgen_glViewportArrayv;
RGLSYMGLVIEWPORTINDEXEDFPROC __rglgen_glViewportIndexedf;
RGLSYMGLVIEWPORTINDEXEDFVPROC __rglgen_glViewportIndexedfv;
RGLSYMGLSCISSORARRAYVPROC __rglgen_glScissorArrayv;
RGLSYMGLSCISSORINDEXEDPROC __rglgen_glScissorIndexed;
RGLSYMGLSCISSORINDEXEDVPROC __rglgen_glScissorIndexedv;
RGLSYMGLDEPTHRANGEARRAYVPROC __rglgen_glDepthRangeArrayv;
RGLSYMGLDEPTHRANGEINDEXEDPROC __rglgen_glDepthRangeIndexed;
RGLSYMGLGETFLOATI_VPROC __rglgen_glGetFloati_v;
RGLSYMGLGETDOUBLEI_VPROC __rglgen_glGetDoublei_v;
RGLSYMGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC __rglgen_glDrawArraysInstancedBaseInstance;
RGLSYMGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC __rglgen_glDrawElementsInstancedBaseInstance;
RGLSYMGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC __rglgen_glDrawElementsInstancedBaseVertexBaseInstance;
RGLSYMGLGETINTERNALFORMATIVPROC __rglgen_glGetInternalformativ;
RGLSYMGLGETACTIVEATOMICCOUNTERBUFFERIVPROC __rglgen_glGetActiveAtomicCounterBufferiv;
RGLSYMGLBINDIMAGETEXTUREPROC __rglgen_glBindImageTexture;
RGLSYMGLMEMORYBARRIERPROC __rglgen_glMemoryBarrier;
RGLSYMGLTEXSTORAGE1DPROC __rglgen_glTexStorage1D;
RGLSYMGLTEXSTORAGE2DPROC __rglgen_glTexStorage2D;
RGLSYMGLTEXSTORAGE3DPROC __rglgen_glTexStorage3D;
RGLSYMGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC __rglgen_glDrawTransformFeedbackInstanced;
RGLSYMGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC __rglgen_glDrawTransformFeedbackStreamInstanced;
RGLSYMGLCLEARBUFFERDATAPROC __rglgen_glClearBufferData;
RGLSYMGLCLEARBUFFERSUBDATAPROC __rglgen_glClearBufferSubData;
RGLSYMGLDISPATCHCOMPUTEPROC __rglgen_glDispatchCompute;
RGLSYMGLDISPATCHCOMPUTEINDIRECTPROC __rglgen_glDispatchComputeIndirect;
RGLSYMGLCOPYIMAGESUBDATAPROC __rglgen_glCopyImageSubData;
RGLSYMGLFRAMEBUFFERPARAMETERIPROC __rglgen_glFramebufferParameteri;
RGLSYMGLGETFRAMEBUFFERPARAMETERIVPROC __rglgen_glGetFramebufferParameteriv;
RGLSYMGLGETINTERNALFORMATI64VPROC __rglgen_glGetInternalformati64v;
RGLSYMGLINVALIDATETEXSUBIMAGEPROC __rglgen_glInvalidateTexSubImage;
RGLSYMGLINVALIDATETEXIMAGEPROC __rglgen_glInvalidateTexImage;
RGLSYMGLINVALIDATEBUFFERSUBDATAPROC __rglgen_glInvalidateBufferSubData;
RGLSYMGLINVALIDATEBUFFERDATAPROC __rglgen_glInvalidateBufferData;
RGLSYMGLINVALIDATEFRAMEBUFFERPROC __rglgen_glInvalidateFramebuffer;
RGLSYMGLINVALIDATESUBFRAMEBUFFERPROC __rglgen_glInvalidateSubFramebuffer;
RGLSYMGLMULTIDRAWARRAYSINDIRECTPROC __rglgen_glMultiDrawArraysIndirect;
RGLSYMGLMULTIDRAWELEMENTSINDIRECTPROC __rglgen_glMultiDrawElementsIndirect;
RGLSYMGLGETPROGRAMINTERFACEIVPROC __rglgen_glGetProgramInterfaceiv;
RGLSYMGLGETPROGRAMRESOURCEINDEXPROC __rglgen_glGetProgramResourceIndex;
RGLSYMGLGETPROGRAMRESOURCENAMEPROC __rglgen_glGetProgramResourceName;
RGLSYMGLGETPROGRAMRESOURCEIVPROC __rglgen_glGetProgramResourceiv;
RGLSYMGLGETPROGRAMRESOURCELOCATIONPROC __rglgen_glGetProgramResourceLocation;
RGLSYMGLGETPROGRAMRESOURCELOCATIONINDEXPROC __rglgen_glGetProgramResourceLocationIndex;
RGLSYMGLSHADERSTORAGEBLOCKBINDINGPROC __rglgen_glShaderStorageBlockBinding;
RGLSYMGLTEXBUFFERRANGEPROC __rglgen_glTexBufferRange;
RGLSYMGLTEXSTORAGE2DMULTISAMPLEPROC __rglgen_glTexStorage2DMultisample;
RGLSYMGLTEXSTORAGE3DMULTISAMPLEPROC __rglgen_glTexStorage3DMultisample;
RGLSYMGLTEXTUREVIEWPROC __rglgen_glTextureView;
RGLSYMGLBINDVERTEXBUFFERPROC __rglgen_glBindVertexBuffer;
RGLSYMGLVERTEXATTRIBFORMATPROC __rglgen_glVertexAttribFormat;
RGLSYMGLVERTEXATTRIBIFORMATPROC __rglgen_glVertexAttribIFormat;
RGLSYMGLVERTEXATTRIBLFORMATPROC __rglgen_glVertexAttribLFormat;
RGLSYMGLVERTEXATTRIBBINDINGPROC __rglgen_glVertexAttribBinding;
RGLSYMGLVERTEXBINDINGDIVISORPROC __rglgen_glVertexBindingDivisor;
RGLSYMGLDEBUGMESSAGECONTROLPROC __rglgen_glDebugMessageControl;
RGLSYMGLDEBUGMESSAGEINSERTPROC __rglgen_glDebugMessageInsert;
RGLSYMGLDEBUGMESSAGECALLBACKPROC __rglgen_glDebugMessageCallback;
RGLSYMGLGETDEBUGMESSAGELOGPROC __rglgen_glGetDebugMessageLog;
RGLSYMGLPUSHDEBUGGROUPPROC __rglgen_glPushDebugGroup;
RGLSYMGLPOPDEBUGGROUPPROC __rglgen_glPopDebugGroup;
RGLSYMGLOBJECTLABELPROC __rglgen_glObjectLabel;
RGLSYMGLGETOBJECTLABELPROC __rglgen_glGetObjectLabel;
RGLSYMGLOBJECTPTRLABELPROC __rglgen_glObjectPtrLabel;
RGLSYMGLGETOBJECTPTRLABELPROC __rglgen_glGetObjectPtrLabel;
RGLSYMGLBUFFERSTORAGEPROC __rglgen_glBufferStorage;
RGLSYMGLCLEARTEXIMAGEPROC __rglgen_glClearTexImage;
RGLSYMGLCLEARTEXSUBIMAGEPROC __rglgen_glClearTexSubImage;
RGLSYMGLBINDBUFFERSBASEPROC __rglgen_glBindBuffersBase;
RGLSYMGLBINDBUFFERSRANGEPROC __rglgen_glBindBuffersRange;
RGLSYMGLBINDTEXTURESPROC __rglgen_glBindTextures;
RGLSYMGLBINDSAMPLERSPROC __rglgen_glBindSamplers;
RGLSYMGLBINDIMAGETEXTURESPROC __rglgen_glBindImageTextures;
RGLSYMGLBINDVERTEXBUFFERSPROC __rglgen_glBindVertexBuffers;
RGLSYMGLCLIPCONTROLPROC __rglgen_glClipControl;
RGLSYMGLCREATETRANSFORMFEEDBACKSPROC __rglgen_glCreateTransformFeedbacks;
RGLSYMGLTRANSFORMFEEDBACKBUFFERBASEPROC __rglgen_glTransformFeedbackBufferBase;
RGLSYMGLTRANSFORMFEEDBACKBUFFERRANGEPROC __rglgen_glTransformFeedbackBufferRange;
RGLSYMGLGETTRANSFORMFEEDBACKIVPROC __rglgen_glGetTransformFeedbackiv;
RGLSYMGLGETTRANSFORMFEEDBACKI_VPROC __rglgen_glGetTransformFeedbacki_v;
RGLSYMGLGETTRANSFORMFEEDBACKI64_VPROC __rglgen_glGetTransformFeedbacki64_v;
RGLSYMGLCREATEBUFFERSPROC __rglgen_glCreateBuffers;
RGLSYMGLNAMEDBUFFERSTORAGEPROC __rglgen_glNamedBufferStorage;
RGLSYMGLNAMEDBUFFERDATAPROC __rglgen_glNamedBufferData;
RGLSYMGLNAMEDBUFFERSUBDATAPROC __rglgen_glNamedBufferSubData;
RGLSYMGLCOPYNAMEDBUFFERSUBDATAPROC __rglgen_glCopyNamedBufferSubData;
RGLSYMGLCLEARNAMEDBUFFERDATAPROC __rglgen_glClearNamedBufferData;
RGLSYMGLCLEARNAMEDBUFFERSUBDATAPROC __rglgen_glClearNamedBufferSubData;
RGLSYMGLMAPNAMEDBUFFERPROC __rglgen_glMapNamedBuffer;
RGLSYMGLMAPNAMEDBUFFERRANGEPROC __rglgen_glMapNamedBufferRange;
RGLSYMGLUNMAPNAMEDBUFFERPROC __rglgen_glUnmapNamedBuffer;
RGLSYMGLFLUSHMAPPEDNAMEDBUFFERRANGEPROC __rglgen_glFlushMappedNamedBufferRange;
RGLSYMGLGETNAMEDBUFFERPARAMETERIVPROC __rglgen_glGetNamedBufferParameteriv;
RGLSYMGLGETNAMEDBUFFERPARAMETERI64VPROC __rglgen_glGetNamedBufferParameteri64v;
RGLSYMGLGETNAMEDBUFFERPOINTERVPROC __rglgen_glGetNamedBufferPointerv;
RGLSYMGLGETNAMEDBUFFERSUBDATAPROC __rglgen_glGetNamedBufferSubData;
RGLSYMGLCREATEFRAMEBUFFERSPROC __rglgen_glCreateFramebuffers;
RGLSYMGLNAMEDFRAMEBUFFERRENDERBUFFERPROC __rglgen_glNamedFramebufferRenderbuffer;
RGLSYMGLNAMEDFRAMEBUFFERPARAMETERIPROC __rglgen_glNamedFramebufferParameteri;
RGLSYMGLNAMEDFRAMEBUFFERTEXTUREPROC __rglgen_glNamedFramebufferTexture;
RGLSYMGLNAMEDFRAMEBUFFERTEXTURELAYERPROC __rglgen_glNamedFramebufferTextureLayer;
RGLSYMGLNAMEDFRAMEBUFFERDRAWBUFFERPROC __rglgen_glNamedFramebufferDrawBuffer;
RGLSYMGLNAMEDFRAMEBUFFERDRAWBUFFERSPROC __rglgen_glNamedFramebufferDrawBuffers;
RGLSYMGLNAMEDFRAMEBUFFERREADBUFFERPROC __rglgen_glNamedFramebufferReadBuffer;
RGLSYMGLINVALIDATENAMEDFRAMEBUFFERDATAPROC __rglgen_glInvalidateNamedFramebufferData;
RGLSYMGLINVALIDATENAMEDFRAMEBUFFERSUBDATAPROC __rglgen_glInvalidateNamedFramebufferSubData;
RGLSYMGLCLEARNAMEDFRAMEBUFFERIVPROC __rglgen_glClearNamedFramebufferiv;
RGLSYMGLCLEARNAMEDFRAMEBUFFERUIVPROC __rglgen_glClearNamedFramebufferuiv;
RGLSYMGLCLEARNAMEDFRAMEBUFFERFVPROC __rglgen_glClearNamedFramebufferfv;
RGLSYMGLCLEARNAMEDFRAMEBUFFERFIPROC __rglgen_glClearNamedFramebufferfi;
RGLSYMGLBLITNAMEDFRAMEBUFFERPROC __rglgen_glBlitNamedFramebuffer;
RGLSYMGLCHECKNAMEDFRAMEBUFFERSTATUSPROC __rglgen_glCheckNamedFramebufferStatus;
RGLSYMGLGETNAMEDFRAMEBUFFERPARAMETERIVPROC __rglgen_glGetNamedFramebufferParameteriv;
RGLSYMGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVPROC __rglgen_glGetNamedFramebufferAttachmentParameteriv;
RGLSYMGLCREATERENDERBUFFERSPROC __rglgen_glCreateRenderbuffers;
RGLSYMGLNAMEDRENDERBUFFERSTORAGEPROC __rglgen_glNamedRenderbufferStorage;
RGLSYMGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEPROC __rglgen_glNamedRenderbufferStorageMultisample;
RGLSYMGLGETNAMEDRENDERBUFFERPARAMETERIVPROC __rglgen_glGetNamedRenderbufferParameteriv;
RGLSYMGLCREATETEXTURESPROC __rglgen_glCreateTextures;
RGLSYMGLTEXTUREBUFFERPROC __rglgen_glTextureBuffer;
RGLSYMGLTEXTUREBUFFERRANGEPROC __rglgen_glTextureBufferRange;
RGLSYMGLTEXTURESTORAGE1DPROC __rglgen_glTextureStorage1D;
RGLSYMGLTEXTURESTORAGE2DPROC __rglgen_glTextureStorage2D;
RGLSYMGLTEXTURESTORAGE3DPROC __rglgen_glTextureStorage3D;
RGLSYMGLTEXTURESTORAGE2DMULTISAMPLEPROC __rglgen_glTextureStorage2DMultisample;
RGLSYMGLTEXTURESTORAGE3DMULTISAMPLEPROC __rglgen_glTextureStorage3DMultisample;
RGLSYMGLTEXTURESUBIMAGE1DPROC __rglgen_glTextureSubImage1D;
RGLSYMGLTEXTURESUBIMAGE2DPROC __rglgen_glTextureSubImage2D;
RGLSYMGLTEXTURESUBIMAGE3DPROC __rglgen_glTextureSubImage3D;
RGLSYMGLCOMPRESSEDTEXTURESUBIMAGE1DPROC __rglgen_glCompressedTextureSubImage1D;
RGLSYMGLCOMPRESSEDTEXTURESUBIMAGE2DPROC __rglgen_glCompressedTextureSubImage2D;
RGLSYMGLCOMPRESSEDTEXTURESUBIMAGE3DPROC __rglgen_glCompressedTextureSubImage3D;
RGLSYMGLCOPYTEXTURESUBIMAGE1DPROC __rglgen_glCopyTextureSubImage1D;
RGLSYMGLCOPYTEXTURESUBIMAGE2DPROC __rglgen_glCopyTextureSubImage2D;
RGLSYMGLCOPYTEXTURESUBIMAGE3DPROC __rglgen_glCopyTextureSubImage3D;
RGLSYMGLTEXTUREPARAMETERFPROC __rglgen_glTextureParameterf;
RGLSYMGLTEXTUREPARAMETERFVPROC __rglgen_glTextureParameterfv;
RGLSYMGLTEXTUREPARAMETERIPROC __rglgen_glTextureParameteri;
RGLSYMGLTEXTUREPARAMETERIIVPROC __rglgen_glTextureParameterIiv;
RGLSYMGLTEXTUREPARAMETERIUIVPROC __rglgen_glTextureParameterIuiv;
RGLSYMGLTEXTUREPARAMETERIVPROC __rglgen_glTextureParameteriv;
RGLSYMGLGENERATETEXTUREMIPMAPPROC __rglgen_glGenerateTextureMipmap;
RGLSYMGLBINDTEXTUREUNITPROC __rglgen_glBindTextureUnit;
RGLSYMGLGETTEXTUREIMAGEPROC __rglgen_glGetTextureImage;
RGLSYMGLGETCOMPRESSEDTEXTUREIMAGEPROC __rglgen_glGetCompressedTextureImage;
RGLSYMGLGETTEXTURELEVELPARAMETERFVPROC __rglgen_glGetTextureLevelParameterfv;
RGLSYMGLGETTEXTURELEVELPARAMETERIVPROC __rglgen_glGetTextureLevelParameteriv;
RGLSYMGLGETTEXTUREPARAMETERFVPROC __rglgen_glGetTextureParameterfv;
RGLSYMGLGETTEXTUREPARAMETERIIVPROC __rglgen_glGetTextureParameterIiv;
RGLSYMGLGETTEXTUREPARAMETERIUIVPROC __rglgen_glGetTextureParameterIuiv;
RGLSYMGLGETTEXTUREPARAMETERIVPROC __rglgen_glGetTextureParameteriv;
RGLSYMGLCREATEVERTEXARRAYSPROC __rglgen_glCreateVertexArrays;
RGLSYMGLDISABLEVERTEXARRAYATTRIBPROC __rglgen_glDisableVertexArrayAttrib;
RGLSYMGLENABLEVERTEXARRAYATTRIBPROC __rglgen_glEnableVertexArrayAttrib;
RGLSYMGLVERTEXARRAYELEMENTBUFFERPROC __rglgen_glVertexArrayElementBuffer;
RGLSYMGLVERTEXARRAYVERTEXBUFFERPROC __rglgen_glVertexArrayVertexBuffer;
RGLSYMGLVERTEXARRAYVERTEXBUFFERSPROC __rglgen_glVertexArrayVertexBuffers;
RGLSYMGLVERTEXARRAYATTRIBBINDINGPROC __rglgen_glVertexArrayAttribBinding;
RGLSYMGLVERTEXARRAYATTRIBFORMATPROC __rglgen_glVertexArrayAttribFormat;
RGLSYMGLVERTEXARRAYATTRIBIFORMATPROC __rglgen_glVertexArrayAttribIFormat;
RGLSYMGLVERTEXARRAYATTRIBLFORMATPROC __rglgen_glVertexArrayAttribLFormat;
RGLSYMGLVERTEXARRAYBINDINGDIVISORPROC __rglgen_glVertexArrayBindingDivisor;
RGLSYMGLGETVERTEXARRAYIVPROC __rglgen_glGetVertexArrayiv;
RGLSYMGLGETVERTEXARRAYINDEXEDIVPROC __rglgen_glGetVertexArrayIndexediv;
RGLSYMGLGETVERTEXARRAYINDEXED64IVPROC __rglgen_glGetVertexArrayIndexed64iv;
RGLSYMGLCREATESAMPLERSPROC __rglgen_glCreateSamplers;
RGLSYMGLCREATEPROGRAMPIPELINESPROC __rglgen_glCreateProgramPipelines;
RGLSYMGLCREATEQUERIESPROC __rglgen_glCreateQueries;
RGLSYMGLGETQUERYBUFFEROBJECTI64VPROC __rglgen_glGetQueryBufferObjecti64v;
RGLSYMGLGETQUERYBUFFEROBJECTIVPROC __rglgen_glGetQueryBufferObjectiv;
RGLSYMGLGETQUERYBUFFEROBJECTUI64VPROC __rglgen_glGetQueryBufferObjectui64v;
RGLSYMGLGETQUERYBUFFEROBJECTUIVPROC __rglgen_glGetQueryBufferObjectuiv;
RGLSYMGLMEMORYBARRIERBYREGIONPROC __rglgen_glMemoryBarrierByRegion;
RGLSYMGLGETTEXTURESUBIMAGEPROC __rglgen_glGetTextureSubImage;
RGLSYMGLGETCOMPRESSEDTEXTURESUBIMAGEPROC __rglgen_glGetCompressedTextureSubImage;
RGLSYMGLGETGRAPHICSRESETSTATUSPROC __rglgen_glGetGraphicsResetStatus;
RGLSYMGLGETNCOMPRESSEDTEXIMAGEPROC __rglgen_glGetnCompressedTexImage;
RGLSYMGLGETNTEXIMAGEPROC __rglgen_glGetnTexImage;
RGLSYMGLGETNUNIFORMDVPROC __rglgen_glGetnUniformdv;
RGLSYMGLGETNUNIFORMFVPROC __rglgen_glGetnUniformfv;
RGLSYMGLGETNUNIFORMIVPROC __rglgen_glGetnUniformiv;
RGLSYMGLGETNUNIFORMUIVPROC __rglgen_glGetnUniformuiv;
RGLSYMGLREADNPIXELSPROC __rglgen_glReadnPixels;
RGLSYMGLGETNMAPDVPROC __rglgen_glGetnMapdv;
RGLSYMGLGETNMAPFVPROC __rglgen_glGetnMapfv;
RGLSYMGLGETNMAPIVPROC __rglgen_glGetnMapiv;
RGLSYMGLGETNPIXELMAPFVPROC __rglgen_glGetnPixelMapfv;
RGLSYMGLGETNPIXELMAPUIVPROC __rglgen_glGetnPixelMapuiv;
RGLSYMGLGETNPIXELMAPUSVPROC __rglgen_glGetnPixelMapusv;
RGLSYMGLGETNPOLYGONSTIPPLEPROC __rglgen_glGetnPolygonStipple;
RGLSYMGLGETNCOLORTABLEPROC __rglgen_glGetnColorTable;
RGLSYMGLGETNCONVOLUTIONFILTERPROC __rglgen_glGetnConvolutionFilter;
RGLSYMGLGETNSEPARABLEFILTERPROC __rglgen_glGetnSeparableFilter;
RGLSYMGLGETNHISTOGRAMPROC __rglgen_glGetnHistogram;
RGLSYMGLGETNMINMAXPROC __rglgen_glGetnMinmax;
RGLSYMGLTEXTUREBARRIERPROC __rglgen_glTextureBarrier;
RGLSYMGLSPECIALIZESHADERPROC __rglgen_glSpecializeShader;
RGLSYMGLMULTIDRAWARRAYSINDIRECTCOUNTPROC __rglgen_glMultiDrawArraysIndirectCount;
RGLSYMGLMULTIDRAWELEMENTSINDIRECTCOUNTPROC __rglgen_glMultiDrawElementsIndirectCount;
RGLSYMGLPOLYGONOFFSETCLAMPPROC __rglgen_glPolygonOffsetClamp;
RGLSYMGLPRIMITIVEBOUNDINGBOXARBPROC __rglgen_glPrimitiveBoundingBoxARB;
RGLSYMGLGETTEXTUREHANDLEARBPROC __rglgen_glGetTextureHandleARB;
RGLSYMGLGETTEXTURESAMPLERHANDLEARBPROC __rglgen_glGetTextureSamplerHandleARB;
RGLSYMGLMAKETEXTUREHANDLERESIDENTARBPROC __rglgen_glMakeTextureHandleResidentARB;
RGLSYMGLMAKETEXTUREHANDLENONRESIDENTARBPROC __rglgen_glMakeTextureHandleNonResidentARB;
RGLSYMGLGETIMAGEHANDLEARBPROC __rglgen_glGetImageHandleARB;
RGLSYMGLMAKEIMAGEHANDLERESIDENTARBPROC __rglgen_glMakeImageHandleResidentARB;
RGLSYMGLMAKEIMAGEHANDLENONRESIDENTARBPROC __rglgen_glMakeImageHandleNonResidentARB;
RGLSYMGLUNIFORMHANDLEUI64ARBPROC __rglgen_glUniformHandleui64ARB;
RGLSYMGLUNIFORMHANDLEUI64VARBPROC __rglgen_glUniformHandleui64vARB;
RGLSYMGLPROGRAMUNIFORMHANDLEUI64ARBPROC __rglgen_glProgramUniformHandleui64ARB;
RGLSYMGLPROGRAMUNIFORMHANDLEUI64VARBPROC __rglgen_glProgramUniformHandleui64vARB;
RGLSYMGLISTEXTUREHANDLERESIDENTARBPROC __rglgen_glIsTextureHandleResidentARB;
RGLSYMGLISIMAGEHANDLERESIDENTARBPROC __rglgen_glIsImageHandleResidentARB;
RGLSYMGLVERTEXATTRIBL1UI64ARBPROC __rglgen_glVertexAttribL1ui64ARB;
RGLSYMGLVERTEXATTRIBL1UI64VARBPROC __rglgen_glVertexAttribL1ui64vARB;
RGLSYMGLGETVERTEXATTRIBLUI64VARBPROC __rglgen_glGetVertexAttribLui64vARB;
RGLSYMGLCREATESYNCFROMCLEVENTARBPROC __rglgen_glCreateSyncFromCLeventARB;
RGLSYMGLCLAMPCOLORARBPROC __rglgen_glClampColorARB;
RGLSYMGLDISPATCHCOMPUTEGROUPSIZEARBPROC __rglgen_glDispatchComputeGroupSizeARB;
RGLSYMGLDEBUGMESSAGECONTROLARBPROC __rglgen_glDebugMessageControlARB;
RGLSYMGLDEBUGMESSAGEINSERTARBPROC __rglgen_glDebugMessageInsertARB;
RGLSYMGLDEBUGMESSAGECALLBACKARBPROC __rglgen_glDebugMessageCallbackARB;
RGLSYMGLGETDEBUGMESSAGELOGARBPROC __rglgen_glGetDebugMessageLogARB;
RGLSYMGLDRAWBUFFERSARBPROC __rglgen_glDrawBuffersARB;
RGLSYMGLBLENDEQUATIONIARBPROC __rglgen_glBlendEquationiARB;
RGLSYMGLBLENDEQUATIONSEPARATEIARBPROC __rglgen_glBlendEquationSeparateiARB;
RGLSYMGLBLENDFUNCIARBPROC __rglgen_glBlendFunciARB;
RGLSYMGLBLENDFUNCSEPARATEIARBPROC __rglgen_glBlendFuncSeparateiARB;
RGLSYMGLDRAWARRAYSINSTANCEDARBPROC __rglgen_glDrawArraysInstancedARB;
RGLSYMGLDRAWELEMENTSINSTANCEDARBPROC __rglgen_glDrawElementsInstancedARB;
RGLSYMGLPROGRAMSTRINGARBPROC __rglgen_glProgramStringARB;
RGLSYMGLBINDPROGRAMARBPROC __rglgen_glBindProgramARB;
RGLSYMGLDELETEPROGRAMSARBPROC __rglgen_glDeleteProgramsARB;
RGLSYMGLGENPROGRAMSARBPROC __rglgen_glGenProgramsARB;
RGLSYMGLPROGRAMENVPARAMETER4DARBPROC __rglgen_glProgramEnvParameter4dARB;
RGLSYMGLPROGRAMENVPARAMETER4DVARBPROC __rglgen_glProgramEnvParameter4dvARB;
RGLSYMGLPROGRAMENVPARAMETER4FARBPROC __rglgen_glProgramEnvParameter4fARB;
RGLSYMGLPROGRAMENVPARAMETER4FVARBPROC __rglgen_glProgramEnvParameter4fvARB;
RGLSYMGLPROGRAMLOCALPARAMETER4DARBPROC __rglgen_glProgramLocalParameter4dARB;
RGLSYMGLPROGRAMLOCALPARAMETER4DVARBPROC __rglgen_glProgramLocalParameter4dvARB;
RGLSYMGLPROGRAMLOCALPARAMETER4FARBPROC __rglgen_glProgramLocalParameter4fARB;
RGLSYMGLPROGRAMLOCALPARAMETER4FVARBPROC __rglgen_glProgramLocalParameter4fvARB;
RGLSYMGLGETPROGRAMENVPARAMETERDVARBPROC __rglgen_glGetProgramEnvParameterdvARB;
RGLSYMGLGETPROGRAMENVPARAMETERFVARBPROC __rglgen_glGetProgramEnvParameterfvARB;
RGLSYMGLGETPROGRAMLOCALPARAMETERDVARBPROC __rglgen_glGetProgramLocalParameterdvARB;
RGLSYMGLGETPROGRAMLOCALPARAMETERFVARBPROC __rglgen_glGetProgramLocalParameterfvARB;
RGLSYMGLGETPROGRAMIVARBPROC __rglgen_glGetProgramivARB;
RGLSYMGLGETPROGRAMSTRINGARBPROC __rglgen_glGetProgramStringARB;
RGLSYMGLISPROGRAMARBPROC __rglgen_glIsProgramARB;
RGLSYMGLPROGRAMPARAMETERIARBPROC __rglgen_glProgramParameteriARB;
RGLSYMGLFRAMEBUFFERTEXTUREARBPROC __rglgen_glFramebufferTextureARB;
RGLSYMGLFRAMEBUFFERTEXTURELAYERARBPROC __rglgen_glFramebufferTextureLayerARB;
RGLSYMGLFRAMEBUFFERTEXTUREFACEARBPROC __rglgen_glFramebufferTextureFaceARB;
RGLSYMGLSPECIALIZESHADERARBPROC __rglgen_glSpecializeShaderARB;
RGLSYMGLUNIFORM1I64ARBPROC __rglgen_glUniform1i64ARB;
RGLSYMGLUNIFORM2I64ARBPROC __rglgen_glUniform2i64ARB;
RGLSYMGLUNIFORM3I64ARBPROC __rglgen_glUniform3i64ARB;
RGLSYMGLUNIFORM4I64ARBPROC __rglgen_glUniform4i64ARB;
RGLSYMGLUNIFORM1I64VARBPROC __rglgen_glUniform1i64vARB;
RGLSYMGLUNIFORM2I64VARBPROC __rglgen_glUniform2i64vARB;
RGLSYMGLUNIFORM3I64VARBPROC __rglgen_glUniform3i64vARB;
RGLSYMGLUNIFORM4I64VARBPROC __rglgen_glUniform4i64vARB;
RGLSYMGLUNIFORM1UI64ARBPROC __rglgen_glUniform1ui64ARB;
RGLSYMGLUNIFORM2UI64ARBPROC __rglgen_glUniform2ui64ARB;
RGLSYMGLUNIFORM3UI64ARBPROC __rglgen_glUniform3ui64ARB;
RGLSYMGLUNIFORM4UI64ARBPROC __rglgen_glUniform4ui64ARB;
RGLSYMGLUNIFORM1UI64VARBPROC __rglgen_glUniform1ui64vARB;
RGLSYMGLUNIFORM2UI64VARBPROC __rglgen_glUniform2ui64vARB;
RGLSYMGLUNIFORM3UI64VARBPROC __rglgen_glUniform3ui64vARB;
RGLSYMGLUNIFORM4UI64VARBPROC __rglgen_glUniform4ui64vARB;
RGLSYMGLGETUNIFORMI64VARBPROC __rglgen_glGetUniformi64vARB;
RGLSYMGLGETUNIFORMUI64VARBPROC __rglgen_glGetUniformui64vARB;
RGLSYMGLGETNUNIFORMI64VARBPROC __rglgen_glGetnUniformi64vARB;
RGLSYMGLGETNUNIFORMUI64VARBPROC __rglgen_glGetnUniformui64vARB;
RGLSYMGLPROGRAMUNIFORM1I64ARBPROC __rglgen_glProgramUniform1i64ARB;
RGLSYMGLPROGRAMUNIFORM2I64ARBPROC __rglgen_glProgramUniform2i64ARB;
RGLSYMGLPROGRAMUNIFORM3I64ARBPROC __rglgen_glProgramUniform3i64ARB;
RGLSYMGLPROGRAMUNIFORM4I64ARBPROC __rglgen_glProgramUniform4i64ARB;
RGLSYMGLPROGRAMUNIFORM1I64VARBPROC __rglgen_glProgramUniform1i64vARB;
RGLSYMGLPROGRAMUNIFORM2I64VARBPROC __rglgen_glProgramUniform2i64vARB;
RGLSYMGLPROGRAMUNIFORM3I64VARBPROC __rglgen_glProgramUniform3i64vARB;
RGLSYMGLPROGRAMUNIFORM4I64VARBPROC __rglgen_glProgramUniform4i64vARB;
RGLSYMGLPROGRAMUNIFORM1UI64ARBPROC __rglgen_glProgramUniform1ui64ARB;
RGLSYMGLPROGRAMUNIFORM2UI64ARBPROC __rglgen_glProgramUniform2ui64ARB;
RGLSYMGLPROGRAMUNIFORM3UI64ARBPROC __rglgen_glProgramUniform3ui64ARB;
RGLSYMGLPROGRAMUNIFORM4UI64ARBPROC __rglgen_glProgramUniform4ui64ARB;
RGLSYMGLPROGRAMUNIFORM1UI64VARBPROC __rglgen_glProgramUniform1ui64vARB;
RGLSYMGLPROGRAMUNIFORM2UI64VARBPROC __rglgen_glProgramUniform2ui64vARB;
RGLSYMGLPROGRAMUNIFORM3UI64VARBPROC __rglgen_glProgramUniform3ui64vARB;
RGLSYMGLPROGRAMUNIFORM4UI64VARBPROC __rglgen_glProgramUniform4ui64vARB;
RGLSYMGLCOLORTABLEPROC __rglgen_glColorTable;
RGLSYMGLCOLORTABLEPARAMETERFVPROC __rglgen_glColorTableParameterfv;
RGLSYMGLCOLORTABLEPARAMETERIVPROC __rglgen_glColorTableParameteriv;
RGLSYMGLCOPYCOLORTABLEPROC __rglgen_glCopyColorTable;
RGLSYMGLGETCOLORTABLEPROC __rglgen_glGetColorTable;
RGLSYMGLGETCOLORTABLEPARAMETERFVPROC __rglgen_glGetColorTableParameterfv;
RGLSYMGLGETCOLORTABLEPARAMETERIVPROC __rglgen_glGetColorTableParameteriv;
RGLSYMGLCOLORSUBTABLEPROC __rglgen_glColorSubTable;
RGLSYMGLCOPYCOLORSUBTABLEPROC __rglgen_glCopyColorSubTable;
RGLSYMGLCONVOLUTIONFILTER1DPROC __rglgen_glConvolutionFilter1D;
RGLSYMGLCONVOLUTIONFILTER2DPROC __rglgen_glConvolutionFilter2D;
RGLSYMGLCONVOLUTIONPARAMETERFPROC __rglgen_glConvolutionParameterf;
RGLSYMGLCONVOLUTIONPARAMETERFVPROC __rglgen_glConvolutionParameterfv;
RGLSYMGLCONVOLUTIONPARAMETERIPROC __rglgen_glConvolutionParameteri;
RGLSYMGLCONVOLUTIONPARAMETERIVPROC __rglgen_glConvolutionParameteriv;
RGLSYMGLCOPYCONVOLUTIONFILTER1DPROC __rglgen_glCopyConvolutionFilter1D;
RGLSYMGLCOPYCONVOLUTIONFILTER2DPROC __rglgen_glCopyConvolutionFilter2D;
RGLSYMGLGETCONVOLUTIONFILTERPROC __rglgen_glGetConvolutionFilter;
RGLSYMGLGETCONVOLUTIONPARAMETERFVPROC __rglgen_glGetConvolutionParameterfv;
RGLSYMGLGETCONVOLUTIONPARAMETERIVPROC __rglgen_glGetConvolutionParameteriv;
RGLSYMGLGETSEPARABLEFILTERPROC __rglgen_glGetSeparableFilter;
RGLSYMGLSEPARABLEFILTER2DPROC __rglgen_glSeparableFilter2D;
RGLSYMGLGETHISTOGRAMPROC __rglgen_glGetHistogram;
RGLSYMGLGETHISTOGRAMPARAMETERFVPROC __rglgen_glGetHistogramParameterfv;
RGLSYMGLGETHISTOGRAMPARAMETERIVPROC __rglgen_glGetHistogramParameteriv;
RGLSYMGLGETMINMAXPROC __rglgen_glGetMinmax;
RGLSYMGLGETMINMAXPARAMETERFVPROC __rglgen_glGetMinmaxParameterfv;
RGLSYMGLGETMINMAXPARAMETERIVPROC __rglgen_glGetMinmaxParameteriv;
RGLSYMGLHISTOGRAMPROC __rglgen_glHistogram;
RGLSYMGLMINMAXPROC __rglgen_glMinmax;
RGLSYMGLRESETHISTOGRAMPROC __rglgen_glResetHistogram;
RGLSYMGLRESETMINMAXPROC __rglgen_glResetMinmax;
RGLSYMGLMULTIDRAWARRAYSINDIRECTCOUNTARBPROC __rglgen_glMultiDrawArraysIndirectCountARB;
RGLSYMGLMULTIDRAWELEMENTSINDIRECTCOUNTARBPROC __rglgen_glMultiDrawElementsIndirectCountARB;
RGLSYMGLVERTEXATTRIBDIVISORARBPROC __rglgen_glVertexAttribDivisorARB;
RGLSYMGLCURRENTPALETTEMATRIXARBPROC __rglgen_glCurrentPaletteMatrixARB;
RGLSYMGLMATRIXINDEXUBVARBPROC __rglgen_glMatrixIndexubvARB;
RGLSYMGLMATRIXINDEXUSVARBPROC __rglgen_glMatrixIndexusvARB;
RGLSYMGLMATRIXINDEXUIVARBPROC __rglgen_glMatrixIndexuivARB;
RGLSYMGLMATRIXINDEXPOINTERARBPROC __rglgen_glMatrixIndexPointerARB;
RGLSYMGLSAMPLECOVERAGEARBPROC __rglgen_glSampleCoverageARB;
RGLSYMGLACTIVETEXTUREARBPROC __rglgen_glActiveTextureARB;
RGLSYMGLCLIENTACTIVETEXTUREARBPROC __rglgen_glClientActiveTextureARB;
RGLSYMGLMULTITEXCOORD1DARBPROC __rglgen_glMultiTexCoord1dARB;
RGLSYMGLMULTITEXCOORD1DVARBPROC __rglgen_glMultiTexCoord1dvARB;
RGLSYMGLMULTITEXCOORD1FARBPROC __rglgen_glMultiTexCoord1fARB;
RGLSYMGLMULTITEXCOORD1FVARBPROC __rglgen_glMultiTexCoord1fvARB;
RGLSYMGLMULTITEXCOORD1IARBPROC __rglgen_glMultiTexCoord1iARB;
RGLSYMGLMULTITEXCOORD1IVARBPROC __rglgen_glMultiTexCoord1ivARB;
RGLSYMGLMULTITEXCOORD1SARBPROC __rglgen_glMultiTexCoord1sARB;
RGLSYMGLMULTITEXCOORD1SVARBPROC __rglgen_glMultiTexCoord1svARB;
RGLSYMGLMULTITEXCOORD2DARBPROC __rglgen_glMultiTexCoord2dARB;
RGLSYMGLMULTITEXCOORD2DVARBPROC __rglgen_glMultiTexCoord2dvARB;
RGLSYMGLMULTITEXCOORD2FARBPROC __rglgen_glMultiTexCoord2fARB;
RGLSYMGLMULTITEXCOORD2FVARBPROC __rglgen_glMultiTexCoord2fvARB;
RGLSYMGLMULTITEXCOORD2IARBPROC __rglgen_glMultiTexCoord2iARB;
RGLSYMGLMULTITEXCOORD2IVARBPROC __rglgen_glMultiTexCoord2ivARB;
RGLSYMGLMULTITEXCOORD2SARBPROC __rglgen_glMultiTexCoord2sARB;
RGLSYMGLMULTITEXCOORD2SVARBPROC __rglgen_glMultiTexCoord2svARB;
RGLSYMGLMULTITEXCOORD3DARBPROC __rglgen_glMultiTexCoord3dARB;
RGLSYMGLMULTITEXCOORD3DVARBPROC __rglgen_glMultiTexCoord3dvARB;
RGLSYMGLMULTITEXCOORD3FARBPROC __rglgen_glMultiTexCoord3fARB;
RGLSYMGLMULTITEXCOORD3FVARBPROC __rglgen_glMultiTexCoord3fvARB;
RGLSYMGLMULTITEXCOORD3IARBPROC __rglgen_glMultiTexCoord3iARB;
RGLSYMGLMULTITEXCOORD3IVARBPROC __rglgen_glMultiTexCoord3ivARB;
RGLSYMGLMULTITEXCOORD3SARBPROC __rglgen_glMultiTexCoord3sARB;
RGLSYMGLMULTITEXCOORD3SVARBPROC __rglgen_glMultiTexCoord3svARB;
RGLSYMGLMULTITEXCOORD4DARBPROC __rglgen_glMultiTexCoord4dARB;
RGLSYMGLMULTITEXCOORD4DVARBPROC __rglgen_glMultiTexCoord4dvARB;
RGLSYMGLMULTITEXCOORD4FARBPROC __rglgen_glMultiTexCoord4fARB;
RGLSYMGLMULTITEXCOORD4FVARBPROC __rglgen_glMultiTexCoord4fvARB;
RGLSYMGLMULTITEXCOORD4IARBPROC __rglgen_glMultiTexCoord4iARB;
RGLSYMGLMULTITEXCOORD4IVARBPROC __rglgen_glMultiTexCoord4ivARB;
RGLSYMGLMULTITEXCOORD4SARBPROC __rglgen_glMultiTexCoord4sARB;
RGLSYMGLMULTITEXCOORD4SVARBPROC __rglgen_glMultiTexCoord4svARB;
RGLSYMGLGENQUERIESARBPROC __rglgen_glGenQueriesARB;
RGLSYMGLDELETEQUERIESARBPROC __rglgen_glDeleteQueriesARB;
RGLSYMGLISQUERYARBPROC __rglgen_glIsQueryARB;
RGLSYMGLBEGINQUERYARBPROC __rglgen_glBeginQueryARB;
RGLSYMGLENDQUERYARBPROC __rglgen_glEndQueryARB;
RGLSYMGLGETQUERYIVARBPROC __rglgen_glGetQueryivARB;
RGLSYMGLGETQUERYOBJECTIVARBPROC __rglgen_glGetQueryObjectivARB;
RGLSYMGLGETQUERYOBJECTUIVARBPROC __rglgen_glGetQueryObjectuivARB;
RGLSYMGLMAXSHADERCOMPILERTHREADSARBPROC __rglgen_glMaxShaderCompilerThreadsARB;
RGLSYMGLPOINTPARAMETERFARBPROC __rglgen_glPointParameterfARB;
RGLSYMGLPOINTPARAMETERFVARBPROC __rglgen_glPointParameterfvARB;
RGLSYMGLGETGRAPHICSRESETSTATUSARBPROC __rglgen_glGetGraphicsResetStatusARB;
RGLSYMGLGETNTEXIMAGEARBPROC __rglgen_glGetnTexImageARB;
RGLSYMGLREADNPIXELSARBPROC __rglgen_glReadnPixelsARB;
RGLSYMGLGETNCOMPRESSEDTEXIMAGEARBPROC __rglgen_glGetnCompressedTexImageARB;
RGLSYMGLGETNUNIFORMFVARBPROC __rglgen_glGetnUniformfvARB;
RGLSYMGLGETNUNIFORMIVARBPROC __rglgen_glGetnUniformivARB;
RGLSYMGLGETNUNIFORMUIVARBPROC __rglgen_glGetnUniformuivARB;
RGLSYMGLGETNUNIFORMDVARBPROC __rglgen_glGetnUniformdvARB;
RGLSYMGLGETNMAPDVARBPROC __rglgen_glGetnMapdvARB;
RGLSYMGLGETNMAPFVARBPROC __rglgen_glGetnMapfvARB;
RGLSYMGLGETNMAPIVARBPROC __rglgen_glGetnMapivARB;
RGLSYMGLGETNPIXELMAPFVARBPROC __rglgen_glGetnPixelMapfvARB;
RGLSYMGLGETNPIXELMAPUIVARBPROC __rglgen_glGetnPixelMapuivARB;
RGLSYMGLGETNPIXELMAPUSVARBPROC __rglgen_glGetnPixelMapusvARB;
RGLSYMGLGETNPOLYGONSTIPPLEARBPROC __rglgen_glGetnPolygonStippleARB;
RGLSYMGLGETNCOLORTABLEARBPROC __rglgen_glGetnColorTableARB;
RGLSYMGLGETNCONVOLUTIONFILTERARBPROC __rglgen_glGetnConvolutionFilterARB;
RGLSYMGLGETNSEPARABLEFILTERARBPROC __rglgen_glGetnSeparableFilterARB;
RGLSYMGLGETNHISTOGRAMARBPROC __rglgen_glGetnHistogramARB;
RGLSYMGLGETNMINMAXARBPROC __rglgen_glGetnMinmaxARB;
RGLSYMGLFRAMEBUFFERSAMPLELOCATIONSFVARBPROC __rglgen_glFramebufferSampleLocationsfvARB;
RGLSYMGLNAMEDFRAMEBUFFERSAMPLELOCATIONSFVARBPROC __rglgen_glNamedFramebufferSampleLocationsfvARB;
RGLSYMGLEVALUATEDEPTHVALUESARBPROC __rglgen_glEvaluateDepthValuesARB;
RGLSYMGLMINSAMPLESHADINGARBPROC __rglgen_glMinSampleShadingARB;
RGLSYMGLDELETEOBJECTARBPROC __rglgen_glDeleteObjectARB;
RGLSYMGLGETHANDLEARBPROC __rglgen_glGetHandleARB;
RGLSYMGLDETACHOBJECTARBPROC __rglgen_glDetachObjectARB;
RGLSYMGLCREATESHADEROBJECTARBPROC __rglgen_glCreateShaderObjectARB;
RGLSYMGLSHADERSOURCEARBPROC __rglgen_glShaderSourceARB;
RGLSYMGLCOMPILESHADERARBPROC __rglgen_glCompileShaderARB;
RGLSYMGLCREATEPROGRAMOBJECTARBPROC __rglgen_glCreateProgramObjectARB;
RGLSYMGLATTACHOBJECTARBPROC __rglgen_glAttachObjectARB;
RGLSYMGLLINKPROGRAMARBPROC __rglgen_glLinkProgramARB;
RGLSYMGLUSEPROGRAMOBJECTARBPROC __rglgen_glUseProgramObjectARB;
RGLSYMGLVALIDATEPROGRAMARBPROC __rglgen_glValidateProgramARB;
RGLSYMGLUNIFORM1FARBPROC __rglgen_glUniform1fARB;
RGLSYMGLUNIFORM2FARBPROC __rglgen_glUniform2fARB;
RGLSYMGLUNIFORM3FARBPROC __rglgen_glUniform3fARB;
RGLSYMGLUNIFORM4FARBPROC __rglgen_glUniform4fARB;
RGLSYMGLUNIFORM1IARBPROC __rglgen_glUniform1iARB;
RGLSYMGLUNIFORM2IARBPROC __rglgen_glUniform2iARB;
RGLSYMGLUNIFORM3IARBPROC __rglgen_glUniform3iARB;
RGLSYMGLUNIFORM4IARBPROC __rglgen_glUniform4iARB;
RGLSYMGLUNIFORM1FVARBPROC __rglgen_glUniform1fvARB;
RGLSYMGLUNIFORM2FVARBPROC __rglgen_glUniform2fvARB;
RGLSYMGLUNIFORM3FVARBPROC __rglgen_glUniform3fvARB;
RGLSYMGLUNIFORM4FVARBPROC __rglgen_glUniform4fvARB;
RGLSYMGLUNIFORM1IVARBPROC __rglgen_glUniform1ivARB;
RGLSYMGLUNIFORM2IVARBPROC __rglgen_glUniform2ivARB;
RGLSYMGLUNIFORM3IVARBPROC __rglgen_glUniform3ivARB;
RGLSYMGLUNIFORM4IVARBPROC __rglgen_glUniform4ivARB;
RGLSYMGLUNIFORMMATRIX2FVARBPROC __rglgen_glUniformMatrix2fvARB;
RGLSYMGLUNIFORMMATRIX3FVARBPROC __rglgen_glUniformMatrix3fvARB;
RGLSYMGLUNIFORMMATRIX4FVARBPROC __rglgen_glUniformMatrix4fvARB;
RGLSYMGLGETOBJECTPARAMETERFVARBPROC __rglgen_glGetObjectParameterfvARB;
RGLSYMGLGETOBJECTPARAMETERIVARBPROC __rglgen_glGetObjectParameterivARB;
RGLSYMGLGETINFOLOGARBPROC __rglgen_glGetInfoLogARB;
RGLSYMGLGETATTACHEDOBJECTSARBPROC __rglgen_glGetAttachedObjectsARB;
RGLSYMGLGETUNIFORMLOCATIONARBPROC __rglgen_glGetUniformLocationARB;
RGLSYMGLGETACTIVEUNIFORMARBPROC __rglgen_glGetActiveUniformARB;
RGLSYMGLGETUNIFORMFVARBPROC __rglgen_glGetUniformfvARB;
RGLSYMGLGETUNIFORMIVARBPROC __rglgen_glGetUniformivARB;
RGLSYMGLGETSHADERSOURCEARBPROC __rglgen_glGetShaderSourceARB;
RGLSYMGLNAMEDSTRINGARBPROC __rglgen_glNamedStringARB;
RGLSYMGLDELETENAMEDSTRINGARBPROC __rglgen_glDeleteNamedStringARB;
RGLSYMGLCOMPILESHADERINCLUDEARBPROC __rglgen_glCompileShaderIncludeARB;
RGLSYMGLISNAMEDSTRINGARBPROC __rglgen_glIsNamedStringARB;
RGLSYMGLGETNAMEDSTRINGARBPROC __rglgen_glGetNamedStringARB;
RGLSYMGLGETNAMEDSTRINGIVARBPROC __rglgen_glGetNamedStringivARB;
RGLSYMGLBUFFERPAGECOMMITMENTARBPROC __rglgen_glBufferPageCommitmentARB;
RGLSYMGLNAMEDBUFFERPAGECOMMITMENTEXTPROC __rglgen_glNamedBufferPageCommitmentEXT;
RGLSYMGLNAMEDBUFFERPAGECOMMITMENTARBPROC __rglgen_glNamedBufferPageCommitmentARB;
RGLSYMGLTEXPAGECOMMITMENTARBPROC __rglgen_glTexPageCommitmentARB;
RGLSYMGLTEXBUFFERARBPROC __rglgen_glTexBufferARB;
RGLSYMGLCOMPRESSEDTEXIMAGE3DARBPROC __rglgen_glCompressedTexImage3DARB;
RGLSYMGLCOMPRESSEDTEXIMAGE2DARBPROC __rglgen_glCompressedTexImage2DARB;
RGLSYMGLCOMPRESSEDTEXIMAGE1DARBPROC __rglgen_glCompressedTexImage1DARB;
RGLSYMGLCOMPRESSEDTEXSUBIMAGE3DARBPROC __rglgen_glCompressedTexSubImage3DARB;
RGLSYMGLCOMPRESSEDTEXSUBIMAGE2DARBPROC __rglgen_glCompressedTexSubImage2DARB;
RGLSYMGLCOMPRESSEDTEXSUBIMAGE1DARBPROC __rglgen_glCompressedTexSubImage1DARB;
RGLSYMGLGETCOMPRESSEDTEXIMAGEARBPROC __rglgen_glGetCompressedTexImageARB;
RGLSYMGLLOADTRANSPOSEMATRIXFARBPROC __rglgen_glLoadTransposeMatrixfARB;
RGLSYMGLLOADTRANSPOSEMATRIXDARBPROC __rglgen_glLoadTransposeMatrixdARB;
RGLSYMGLMULTTRANSPOSEMATRIXFARBPROC __rglgen_glMultTransposeMatrixfARB;
RGLSYMGLMULTTRANSPOSEMATRIXDARBPROC __rglgen_glMultTransposeMatrixdARB;
RGLSYMGLWEIGHTBVARBPROC __rglgen_glWeightbvARB;
RGLSYMGLWEIGHTSVARBPROC __rglgen_glWeightsvARB;
RGLSYMGLWEIGHTIVARBPROC __rglgen_glWeightivARB;
RGLSYMGLWEIGHTFVARBPROC __rglgen_glWeightfvARB;
RGLSYMGLWEIGHTDVARBPROC __rglgen_glWeightdvARB;
RGLSYMGLWEIGHTUBVARBPROC __rglgen_glWeightubvARB;
RGLSYMGLWEIGHTUSVARBPROC __rglgen_glWeightusvARB;
RGLSYMGLWEIGHTUIVARBPROC __rglgen_glWeightuivARB;
RGLSYMGLWEIGHTPOINTERARBPROC __rglgen_glWeightPointerARB;
RGLSYMGLVERTEXBLENDARBPROC __rglgen_glVertexBlendARB;
RGLSYMGLBINDBUFFERARBPROC __rglgen_glBindBufferARB;
RGLSYMGLDELETEBUFFERSARBPROC __rglgen_glDeleteBuffersARB;
RGLSYMGLGENBUFFERSARBPROC __rglgen_glGenBuffersARB;
RGLSYMGLISBUFFERARBPROC __rglgen_glIsBufferARB;
RGLSYMGLBUFFERDATAARBPROC __rglgen_glBufferDataARB;
RGLSYMGLBUFFERSUBDATAARBPROC __rglgen_glBufferSubDataARB;
RGLSYMGLGETBUFFERSUBDATAARBPROC __rglgen_glGetBufferSubDataARB;
RGLSYMGLMAPBUFFERARBPROC __rglgen_glMapBufferARB;
RGLSYMGLUNMAPBUFFERARBPROC __rglgen_glUnmapBufferARB;
RGLSYMGLGETBUFFERPARAMETERIVARBPROC __rglgen_glGetBufferParameterivARB;
RGLSYMGLGETBUFFERPOINTERVARBPROC __rglgen_glGetBufferPointervARB;
RGLSYMGLVERTEXATTRIB1DARBPROC __rglgen_glVertexAttrib1dARB;
RGLSYMGLVERTEXATTRIB1DVARBPROC __rglgen_glVertexAttrib1dvARB;
RGLSYMGLVERTEXATTRIB1FARBPROC __rglgen_glVertexAttrib1fARB;
RGLSYMGLVERTEXATTRIB1FVARBPROC __rglgen_glVertexAttrib1fvARB;
RGLSYMGLVERTEXATTRIB1SARBPROC __rglgen_glVertexAttrib1sARB;
RGLSYMGLVERTEXATTRIB1SVARBPROC __rglgen_glVertexAttrib1svARB;
RGLSYMGLVERTEXATTRIB2DARBPROC __rglgen_glVertexAttrib2dARB;
RGLSYMGLVERTEXATTRIB2DVARBPROC __rglgen_glVertexAttrib2dvARB;
RGLSYMGLVERTEXATTRIB2FARBPROC __rglgen_glVertexAttrib2fARB;
RGLSYMGLVERTEXATTRIB2FVARBPROC __rglgen_glVertexAttrib2fvARB;
RGLSYMGLVERTEXATTRIB2SARBPROC __rglgen_glVertexAttrib2sARB;
RGLSYMGLVERTEXATTRIB2SVARBPROC __rglgen_glVertexAttrib2svARB;
RGLSYMGLVERTEXATTRIB3DARBPROC __rglgen_glVertexAttrib3dARB;
RGLSYMGLVERTEXATTRIB3DVARBPROC __rglgen_glVertexAttrib3dvARB;
RGLSYMGLVERTEXATTRIB3FARBPROC __rglgen_glVertexAttrib3fARB;
RGLSYMGLVERTEXATTRIB3FVARBPROC __rglgen_glVertexAttrib3fvARB;
RGLSYMGLVERTEXATTRIB3SARBPROC __rglgen_glVertexAttrib3sARB;
RGLSYMGLVERTEXATTRIB3SVARBPROC __rglgen_glVertexAttrib3svARB;
RGLSYMGLVERTEXATTRIB4NBVARBPROC __rglgen_glVertexAttrib4NbvARB;
RGLSYMGLVERTEXATTRIB4NIVARBPROC __rglgen_glVertexAttrib4NivARB;
RGLSYMGLVERTEXATTRIB4NSVARBPROC __rglgen_glVertexAttrib4NsvARB;
RGLSYMGLVERTEXATTRIB4NUBARBPROC __rglgen_glVertexAttrib4NubARB;
RGLSYMGLVERTEXATTRIB4NUBVARBPROC __rglgen_glVertexAttrib4NubvARB;
RGLSYMGLVERTEXATTRIB4NUIVARBPROC __rglgen_glVertexAttrib4NuivARB;
RGLSYMGLVERTEXATTRIB4NUSVARBPROC __rglgen_glVertexAttrib4NusvARB;
RGLSYMGLVERTEXATTRIB4BVARBPROC __rglgen_glVertexAttrib4bvARB;
RGLSYMGLVERTEXATTRIB4DARBPROC __rglgen_glVertexAttrib4dARB;
RGLSYMGLVERTEXATTRIB4DVARBPROC __rglgen_glVertexAttrib4dvARB;
RGLSYMGLVERTEXATTRIB4FARBPROC __rglgen_glVertexAttrib4fARB;
RGLSYMGLVERTEXATTRIB4FVARBPROC __rglgen_glVertexAttrib4fvARB;
RGLSYMGLVERTEXATTRIB4IVARBPROC __rglgen_glVertexAttrib4ivARB;
RGLSYMGLVERTEXATTRIB4SARBPROC __rglgen_glVertexAttrib4sARB;
RGLSYMGLVERTEXATTRIB4SVARBPROC __rglgen_glVertexAttrib4svARB;
RGLSYMGLVERTEXATTRIB4UBVARBPROC __rglgen_glVertexAttrib4ubvARB;
RGLSYMGLVERTEXATTRIB4UIVARBPROC __rglgen_glVertexAttrib4uivARB;
RGLSYMGLVERTEXATTRIB4USVARBPROC __rglgen_glVertexAttrib4usvARB;
RGLSYMGLVERTEXATTRIBPOINTERARBPROC __rglgen_glVertexAttribPointerARB;
RGLSYMGLENABLEVERTEXATTRIBARRAYARBPROC __rglgen_glEnableVertexAttribArrayARB;
RGLSYMGLDISABLEVERTEXATTRIBARRAYARBPROC __rglgen_glDisableVertexAttribArrayARB;
RGLSYMGLGETVERTEXATTRIBDVARBPROC __rglgen_glGetVertexAttribdvARB;
RGLSYMGLGETVERTEXATTRIBFVARBPROC __rglgen_glGetVertexAttribfvARB;
RGLSYMGLGETVERTEXATTRIBIVARBPROC __rglgen_glGetVertexAttribivARB;
RGLSYMGLGETVERTEXATTRIBPOINTERVARBPROC __rglgen_glGetVertexAttribPointervARB;
RGLSYMGLBINDATTRIBLOCATIONARBPROC __rglgen_glBindAttribLocationARB;
RGLSYMGLGETACTIVEATTRIBARBPROC __rglgen_glGetActiveAttribARB;
RGLSYMGLGETATTRIBLOCATIONARBPROC __rglgen_glGetAttribLocationARB;
RGLSYMGLWINDOWPOS2DARBPROC __rglgen_glWindowPos2dARB;
RGLSYMGLWINDOWPOS2DVARBPROC __rglgen_glWindowPos2dvARB;
RGLSYMGLWINDOWPOS2FARBPROC __rglgen_glWindowPos2fARB;
RGLSYMGLWINDOWPOS2FVARBPROC __rglgen_glWindowPos2fvARB;
RGLSYMGLWINDOWPOS2IARBPROC __rglgen_glWindowPos2iARB;
RGLSYMGLWINDOWPOS2IVARBPROC __rglgen_glWindowPos2ivARB;
RGLSYMGLWINDOWPOS2SARBPROC __rglgen_glWindowPos2sARB;
RGLSYMGLWINDOWPOS2SVARBPROC __rglgen_glWindowPos2svARB;
RGLSYMGLWINDOWPOS3DARBPROC __rglgen_glWindowPos3dARB;
RGLSYMGLWINDOWPOS3DVARBPROC __rglgen_glWindowPos3dvARB;
RGLSYMGLWINDOWPOS3FARBPROC __rglgen_glWindowPos3fARB;
RGLSYMGLWINDOWPOS3FVARBPROC __rglgen_glWindowPos3fvARB;
RGLSYMGLWINDOWPOS3IARBPROC __rglgen_glWindowPos3iARB;
RGLSYMGLWINDOWPOS3IVARBPROC __rglgen_glWindowPos3ivARB;
RGLSYMGLWINDOWPOS3SARBPROC __rglgen_glWindowPos3sARB;
RGLSYMGLWINDOWPOS3SVARBPROC __rglgen_glWindowPos3svARB;
RGLSYMGLBLENDBARRIERKHRPROC __rglgen_glBlendBarrierKHR;
RGLSYMGLMAXSHADERCOMPILERTHREADSKHRPROC __rglgen_glMaxShaderCompilerThreadsKHR;
RGLSYMGLMULTITEXCOORD1BOESPROC __rglgen_glMultiTexCoord1bOES;
RGLSYMGLMULTITEXCOORD1BVOESPROC __rglgen_glMultiTexCoord1bvOES;
RGLSYMGLMULTITEXCOORD2BOESPROC __rglgen_glMultiTexCoord2bOES;
RGLSYMGLMULTITEXCOORD2BVOESPROC __rglgen_glMultiTexCoord2bvOES;
RGLSYMGLMULTITEXCOORD3BOESPROC __rglgen_glMultiTexCoord3bOES;
RGLSYMGLMULTITEXCOORD3BVOESPROC __rglgen_glMultiTexCoord3bvOES;
RGLSYMGLMULTITEXCOORD4BOESPROC __rglgen_glMultiTexCoord4bOES;
RGLSYMGLMULTITEXCOORD4BVOESPROC __rglgen_glMultiTexCoord4bvOES;
RGLSYMGLTEXCOORD1BOESPROC __rglgen_glTexCoord1bOES;
RGLSYMGLTEXCOORD1BVOESPROC __rglgen_glTexCoord1bvOES;
RGLSYMGLTEXCOORD2BOESPROC __rglgen_glTexCoord2bOES;
RGLSYMGLTEXCOORD2BVOESPROC __rglgen_glTexCoord2bvOES;
RGLSYMGLTEXCOORD3BOESPROC __rglgen_glTexCoord3bOES;
RGLSYMGLTEXCOORD3BVOESPROC __rglgen_glTexCoord3bvOES;
RGLSYMGLTEXCOORD4BOESPROC __rglgen_glTexCoord4bOES;
RGLSYMGLTEXCOORD4BVOESPROC __rglgen_glTexCoord4bvOES;
RGLSYMGLVERTEX2BOESPROC __rglgen_glVertex2bOES;
RGLSYMGLVERTEX2BVOESPROC __rglgen_glVertex2bvOES;
RGLSYMGLVERTEX3BOESPROC __rglgen_glVertex3bOES;
RGLSYMGLVERTEX3BVOESPROC __rglgen_glVertex3bvOES;
RGLSYMGLVERTEX4BOESPROC __rglgen_glVertex4bOES;
RGLSYMGLVERTEX4BVOESPROC __rglgen_glVertex4bvOES;
RGLSYMGLALPHAFUNCXOESPROC __rglgen_glAlphaFuncxOES;
RGLSYMGLCLEARCOLORXOESPROC __rglgen_glClearColorxOES;
RGLSYMGLCLEARDEPTHXOESPROC __rglgen_glClearDepthxOES;
RGLSYMGLCLIPPLANEXOESPROC __rglgen_glClipPlanexOES;
RGLSYMGLCOLOR4XOESPROC __rglgen_glColor4xOES;
RGLSYMGLDEPTHRANGEXOESPROC __rglgen_glDepthRangexOES;
RGLSYMGLFOGXOESPROC __rglgen_glFogxOES;
RGLSYMGLFOGXVOESPROC __rglgen_glFogxvOES;
RGLSYMGLFRUSTUMXOESPROC __rglgen_glFrustumxOES;
RGLSYMGLGETCLIPPLANEXOESPROC __rglgen_glGetClipPlanexOES;
RGLSYMGLGETFIXEDVOESPROC __rglgen_glGetFixedvOES;
RGLSYMGLGETTEXENVXVOESPROC __rglgen_glGetTexEnvxvOES;
RGLSYMGLGETTEXPARAMETERXVOESPROC __rglgen_glGetTexParameterxvOES;
RGLSYMGLLIGHTMODELXOESPROC __rglgen_glLightModelxOES;
RGLSYMGLLIGHTMODELXVOESPROC __rglgen_glLightModelxvOES;
RGLSYMGLLIGHTXOESPROC __rglgen_glLightxOES;
RGLSYMGLLIGHTXVOESPROC __rglgen_glLightxvOES;
RGLSYMGLLINEWIDTHXOESPROC __rglgen_glLineWidthxOES;
RGLSYMGLLOADMATRIXXOESPROC __rglgen_glLoadMatrixxOES;
RGLSYMGLMATERIALXOESPROC __rglgen_glMaterialxOES;
RGLSYMGLMATERIALXVOESPROC __rglgen_glMaterialxvOES;
RGLSYMGLMULTMATRIXXOESPROC __rglgen_glMultMatrixxOES;
RGLSYMGLMULTITEXCOORD4XOESPROC __rglgen_glMultiTexCoord4xOES;
RGLSYMGLNORMAL3XOESPROC __rglgen_glNormal3xOES;
RGLSYMGLORTHOXOESPROC __rglgen_glOrthoxOES;
RGLSYMGLPOINTPARAMETERXVOESPROC __rglgen_glPointParameterxvOES;
RGLSYMGLPOINTSIZEXOESPROC __rglgen_glPointSizexOES;
RGLSYMGLPOLYGONOFFSETXOESPROC __rglgen_glPolygonOffsetxOES;
RGLSYMGLROTATEXOESPROC __rglgen_glRotatexOES;
RGLSYMGLSCALEXOESPROC __rglgen_glScalexOES;
RGLSYMGLTEXENVXOESPROC __rglgen_glTexEnvxOES;
RGLSYMGLTEXENVXVOESPROC __rglgen_glTexEnvxvOES;
RGLSYMGLTEXPARAMETERXOESPROC __rglgen_glTexParameterxOES;
RGLSYMGLTEXPARAMETERXVOESPROC __rglgen_glTexParameterxvOES;
RGLSYMGLTRANSLATEXOESPROC __rglgen_glTranslatexOES;
RGLSYMGLACCUMXOESPROC __rglgen_glAccumxOES;
RGLSYMGLBITMAPXOESPROC __rglgen_glBitmapxOES;
RGLSYMGLBLENDCOLORXOESPROC __rglgen_glBlendColorxOES;
RGLSYMGLCLEARACCUMXOESPROC __rglgen_glClearAccumxOES;
RGLSYMGLCOLOR3XOESPROC __rglgen_glColor3xOES;
RGLSYMGLCOLOR3XVOESPROC __rglgen_glColor3xvOES;
RGLSYMGLCOLOR4XVOESPROC __rglgen_glColor4xvOES;
RGLSYMGLCONVOLUTIONPARAMETERXOESPROC __rglgen_glConvolutionParameterxOES;
RGLSYMGLCONVOLUTIONPARAMETERXVOESPROC __rglgen_glConvolutionParameterxvOES;
RGLSYMGLEVALCOORD1XOESPROC __rglgen_glEvalCoord1xOES;
RGLSYMGLEVALCOORD1XVOESPROC __rglgen_glEvalCoord1xvOES;
RGLSYMGLEVALCOORD2XOESPROC __rglgen_glEvalCoord2xOES;
RGLSYMGLEVALCOORD2XVOESPROC __rglgen_glEvalCoord2xvOES;
RGLSYMGLFEEDBACKBUFFERXOESPROC __rglgen_glFeedbackBufferxOES;
RGLSYMGLGETCONVOLUTIONPARAMETERXVOESPROC __rglgen_glGetConvolutionParameterxvOES;
RGLSYMGLGETHISTOGRAMPARAMETERXVOESPROC __rglgen_glGetHistogramParameterxvOES;
RGLSYMGLGETLIGHTXOESPROC __rglgen_glGetLightxOES;
RGLSYMGLGETMAPXVOESPROC __rglgen_glGetMapxvOES;
RGLSYMGLGETMATERIALXOESPROC __rglgen_glGetMaterialxOES;
RGLSYMGLGETPIXELMAPXVPROC __rglgen_glGetPixelMapxv;
RGLSYMGLGETTEXGENXVOESPROC __rglgen_glGetTexGenxvOES;
RGLSYMGLGETTEXLEVELPARAMETERXVOESPROC __rglgen_glGetTexLevelParameterxvOES;
RGLSYMGLINDEXXOESPROC __rglgen_glIndexxOES;
RGLSYMGLINDEXXVOESPROC __rglgen_glIndexxvOES;
RGLSYMGLLOADTRANSPOSEMATRIXXOESPROC __rglgen_glLoadTransposeMatrixxOES;
RGLSYMGLMAP1XOESPROC __rglgen_glMap1xOES;
RGLSYMGLMAP2XOESPROC __rglgen_glMap2xOES;
RGLSYMGLMAPGRID1XOESPROC __rglgen_glMapGrid1xOES;
RGLSYMGLMAPGRID2XOESPROC __rglgen_glMapGrid2xOES;
RGLSYMGLMULTTRANSPOSEMATRIXXOESPROC __rglgen_glMultTransposeMatrixxOES;
RGLSYMGLMULTITEXCOORD1XOESPROC __rglgen_glMultiTexCoord1xOES;
RGLSYMGLMULTITEXCOORD1XVOESPROC __rglgen_glMultiTexCoord1xvOES;
RGLSYMGLMULTITEXCOORD2XOESPROC __rglgen_glMultiTexCoord2xOES;
RGLSYMGLMULTITEXCOORD2XVOESPROC __rglgen_glMultiTexCoord2xvOES;
RGLSYMGLMULTITEXCOORD3XOESPROC __rglgen_glMultiTexCoord3xOES;
RGLSYMGLMULTITEXCOORD3XVOESPROC __rglgen_glMultiTexCoord3xvOES;
RGLSYMGLMULTITEXCOORD4XVOESPROC __rglgen_glMultiTexCoord4xvOES;
RGLSYMGLNORMAL3XVOESPROC __rglgen_glNormal3xvOES;
RGLSYMGLPASSTHROUGHXOESPROC __rglgen_glPassThroughxOES;
RGLSYMGLPIXELMAPXPROC __rglgen_glPixelMapx;
RGLSYMGLPIXELSTOREXPROC __rglgen_glPixelStorex;
RGLSYMGLPIXELTRANSFERXOESPROC __rglgen_glPixelTransferxOES;
RGLSYMGLPIXELZOOMXOESPROC __rglgen_glPixelZoomxOES;
RGLSYMGLPRIORITIZETEXTURESXOESPROC __rglgen_glPrioritizeTexturesxOES;
RGLSYMGLRASTERPOS2XOESPROC __rglgen_glRasterPos2xOES;
RGLSYMGLRASTERPOS2XVOESPROC __rglgen_glRasterPos2xvOES;
RGLSYMGLRASTERPOS3XOESPROC __rglgen_glRasterPos3xOES;
RGLSYMGLRASTERPOS3XVOESPROC __rglgen_glRasterPos3xvOES;
RGLSYMGLRASTERPOS4XOESPROC __rglgen_glRasterPos4xOES;
RGLSYMGLRASTERPOS4XVOESPROC __rglgen_glRasterPos4xvOES;
RGLSYMGLRECTXOESPROC __rglgen_glRectxOES;
RGLSYMGLRECTXVOESPROC __rglgen_glRectxvOES;
RGLSYMGLTEXCOORD1XOESPROC __rglgen_glTexCoord1xOES;
RGLSYMGLTEXCOORD1XVOESPROC __rglgen_glTexCoord1xvOES;
RGLSYMGLTEXCOORD2XOESPROC __rglgen_glTexCoord2xOES;
RGLSYMGLTEXCOORD2XVOESPROC __rglgen_glTexCoord2xvOES;
RGLSYMGLTEXCOORD3XOESPROC __rglgen_glTexCoord3xOES;
RGLSYMGLTEXCOORD3XVOESPROC __rglgen_glTexCoord3xvOES;
RGLSYMGLTEXCOORD4XOESPROC __rglgen_glTexCoord4xOES;
RGLSYMGLTEXCOORD4XVOESPROC __rglgen_glTexCoord4xvOES;
RGLSYMGLTEXGENXOESPROC __rglgen_glTexGenxOES;
RGLSYMGLTEXGENXVOESPROC __rglgen_glTexGenxvOES;
RGLSYMGLVERTEX2XOESPROC __rglgen_glVertex2xOES;
RGLSYMGLVERTEX2XVOESPROC __rglgen_glVertex2xvOES;
RGLSYMGLVERTEX3XOESPROC __rglgen_glVertex3xOES;
RGLSYMGLVERTEX3XVOESPROC __rglgen_glVertex3xvOES;
RGLSYMGLVERTEX4XOESPROC __rglgen_glVertex4xOES;
RGLSYMGLVERTEX4XVOESPROC __rglgen_glVertex4xvOES;
RGLSYMGLQUERYMATRIXXOESPROC __rglgen_glQueryMatrixxOES;
RGLSYMGLCLEARDEPTHFOESPROC __rglgen_glClearDepthfOES;
RGLSYMGLCLIPPLANEFOESPROC __rglgen_glClipPlanefOES;
RGLSYMGLDEPTHRANGEFOESPROC __rglgen_glDepthRangefOES;
RGLSYMGLFRUSTUMFOESPROC __rglgen_glFrustumfOES;
RGLSYMGLGETCLIPPLANEFOESPROC __rglgen_glGetClipPlanefOES;
RGLSYMGLORTHOFOESPROC __rglgen_glOrthofOES;
RGLSYMGLEGLIMAGETARGETTEXSTORAGEEXTPROC __rglgen_glEGLImageTargetTexStorageEXT;
RGLSYMGLEGLIMAGETARGETTEXTURESTORAGEEXTPROC __rglgen_glEGLImageTargetTextureStorageEXT;
RGLSYMGLUNIFORMBUFFEREXTPROC __rglgen_glUniformBufferEXT;
RGLSYMGLGETUNIFORMBUFFERSIZEEXTPROC __rglgen_glGetUniformBufferSizeEXT;
RGLSYMGLGETUNIFORMOFFSETEXTPROC __rglgen_glGetUniformOffsetEXT;
RGLSYMGLBLENDCOLOREXTPROC __rglgen_glBlendColorEXT;
RGLSYMGLBLENDEQUATIONSEPARATEEXTPROC __rglgen_glBlendEquationSeparateEXT;
RGLSYMGLBLENDFUNCSEPARATEEXTPROC __rglgen_glBlendFuncSeparateEXT;
RGLSYMGLBLENDEQUATIONEXTPROC __rglgen_glBlendEquationEXT;
RGLSYMGLCOLORSUBTABLEEXTPROC __rglgen_glColorSubTableEXT;
RGLSYMGLCOPYCOLORSUBTABLEEXTPROC __rglgen_glCopyColorSubTableEXT;
RGLSYMGLLOCKARRAYSEXTPROC __rglgen_glLockArraysEXT;
RGLSYMGLUNLOCKARRAYSEXTPROC __rglgen_glUnlockArraysEXT;
RGLSYMGLCONVOLUTIONFILTER1DEXTPROC __rglgen_glConvolutionFilter1DEXT;
RGLSYMGLCONVOLUTIONFILTER2DEXTPROC __rglgen_glConvolutionFilter2DEXT;
RGLSYMGLCONVOLUTIONPARAMETERFEXTPROC __rglgen_glConvolutionParameterfEXT;
RGLSYMGLCONVOLUTIONPARAMETERFVEXTPROC __rglgen_glConvolutionParameterfvEXT;
RGLSYMGLCONVOLUTIONPARAMETERIEXTPROC __rglgen_glConvolutionParameteriEXT;
RGLSYMGLCONVOLUTIONPARAMETERIVEXTPROC __rglgen_glConvolutionParameterivEXT;
RGLSYMGLCOPYCONVOLUTIONFILTER1DEXTPROC __rglgen_glCopyConvolutionFilter1DEXT;
RGLSYMGLCOPYCONVOLUTIONFILTER2DEXTPROC __rglgen_glCopyConvolutionFilter2DEXT;
RGLSYMGLGETCONVOLUTIONFILTEREXTPROC __rglgen_glGetConvolutionFilterEXT;
RGLSYMGLGETCONVOLUTIONPARAMETERFVEXTPROC __rglgen_glGetConvolutionParameterfvEXT;
RGLSYMGLGETCONVOLUTIONPARAMETERIVEXTPROC __rglgen_glGetConvolutionParameterivEXT;
RGLSYMGLGETSEPARABLEFILTEREXTPROC __rglgen_glGetSeparableFilterEXT;
RGLSYMGLSEPARABLEFILTER2DEXTPROC __rglgen_glSeparableFilter2DEXT;
RGLSYMGLTANGENT3BEXTPROC __rglgen_glTangent3bEXT;
RGLSYMGLTANGENT3BVEXTPROC __rglgen_glTangent3bvEXT;
RGLSYMGLTANGENT3DEXTPROC __rglgen_glTangent3dEXT;
RGLSYMGLTANGENT3DVEXTPROC __rglgen_glTangent3dvEXT;
RGLSYMGLTANGENT3FEXTPROC __rglgen_glTangent3fEXT;
RGLSYMGLTANGENT3FVEXTPROC __rglgen_glTangent3fvEXT;
RGLSYMGLTANGENT3IEXTPROC __rglgen_glTangent3iEXT;
RGLSYMGLTANGENT3IVEXTPROC __rglgen_glTangent3ivEXT;
RGLSYMGLTANGENT3SEXTPROC __rglgen_glTangent3sEXT;
RGLSYMGLTANGENT3SVEXTPROC __rglgen_glTangent3svEXT;
RGLSYMGLBINORMAL3BEXTPROC __rglgen_glBinormal3bEXT;
RGLSYMGLBINORMAL3BVEXTPROC __rglgen_glBinormal3bvEXT;
RGLSYMGLBINORMAL3DEXTPROC __rglgen_glBinormal3dEXT;
RGLSYMGLBINORMAL3DVEXTPROC __rglgen_glBinormal3dvEXT;
RGLSYMGLBINORMAL3FEXTPROC __rglgen_glBinormal3fEXT;
RGLSYMGLBINORMAL3FVEXTPROC __rglgen_glBinormal3fvEXT;
RGLSYMGLBINORMAL3IEXTPROC __rglgen_glBinormal3iEXT;
RGLSYMGLBINORMAL3IVEXTPROC __rglgen_glBinormal3ivEXT;
RGLSYMGLBINORMAL3SEXTPROC __rglgen_glBinormal3sEXT;
RGLSYMGLBINORMAL3SVEXTPROC __rglgen_glBinormal3svEXT;
RGLSYMGLTANGENTPOINTEREXTPROC __rglgen_glTangentPointerEXT;
RGLSYMGLBINORMALPOINTEREXTPROC __rglgen_glBinormalPointerEXT;
RGLSYMGLCOPYTEXIMAGE1DEXTPROC __rglgen_glCopyTexImage1DEXT;
RGLSYMGLCOPYTEXIMAGE2DEXTPROC __rglgen_glCopyTexImage2DEXT;
RGLSYMGLCOPYTEXSUBIMAGE1DEXTPROC __rglgen_glCopyTexSubImage1DEXT;
RGLSYMGLCOPYTEXSUBIMAGE2DEXTPROC __rglgen_glCopyTexSubImage2DEXT;
RGLSYMGLCOPYTEXSUBIMAGE3DEXTPROC __rglgen_glCopyTexSubImage3DEXT;
RGLSYMGLCULLPARAMETERDVEXTPROC __rglgen_glCullParameterdvEXT;
RGLSYMGLCULLPARAMETERFVEXTPROC __rglgen_glCullParameterfvEXT;
RGLSYMGLLABELOBJECTEXTPROC __rglgen_glLabelObjectEXT;
RGLSYMGLGETOBJECTLABELEXTPROC __rglgen_glGetObjectLabelEXT;
RGLSYMGLINSERTEVENTMARKEREXTPROC __rglgen_glInsertEventMarkerEXT;
RGLSYMGLPUSHGROUPMARKEREXTPROC __rglgen_glPushGroupMarkerEXT;
RGLSYMGLPOPGROUPMARKEREXTPROC __rglgen_glPopGroupMarkerEXT;
RGLSYMGLDEPTHBOUNDSEXTPROC __rglgen_glDepthBoundsEXT;
RGLSYMGLMATRIXLOADFEXTPROC __rglgen_glMatrixLoadfEXT;
RGLSYMGLMATRIXLOADDEXTPROC __rglgen_glMatrixLoaddEXT;
RGLSYMGLMATRIXMULTFEXTPROC __rglgen_glMatrixMultfEXT;
RGLSYMGLMATRIXMULTDEXTPROC __rglgen_glMatrixMultdEXT;
RGLSYMGLMATRIXLOADIDENTITYEXTPROC __rglgen_glMatrixLoadIdentityEXT;
RGLSYMGLMATRIXROTATEFEXTPROC __rglgen_glMatrixRotatefEXT;
RGLSYMGLMATRIXROTATEDEXTPROC __rglgen_glMatrixRotatedEXT;
RGLSYMGLMATRIXSCALEFEXTPROC __rglgen_glMatrixScalefEXT;
RGLSYMGLMATRIXSCALEDEXTPROC __rglgen_glMatrixScaledEXT;
RGLSYMGLMATRIXTRANSLATEFEXTPROC __rglgen_glMatrixTranslatefEXT;
RGLSYMGLMATRIXTRANSLATEDEXTPROC __rglgen_glMatrixTranslatedEXT;
RGLSYMGLMATRIXFRUSTUMEXTPROC __rglgen_glMatrixFrustumEXT;
RGLSYMGLMATRIXORTHOEXTPROC __rglgen_glMatrixOrthoEXT;
RGLSYMGLMATRIXPOPEXTPROC __rglgen_glMatrixPopEXT;
RGLSYMGLMATRIXPUSHEXTPROC __rglgen_glMatrixPushEXT;
RGLSYMGLCLIENTATTRIBDEFAULTEXTPROC __rglgen_glClientAttribDefaultEXT;
RGLSYMGLPUSHCLIENTATTRIBDEFAULTEXTPROC __rglgen_glPushClientAttribDefaultEXT;
RGLSYMGLTEXTUREPARAMETERFEXTPROC __rglgen_glTextureParameterfEXT;
RGLSYMGLTEXTUREPARAMETERFVEXTPROC __rglgen_glTextureParameterfvEXT;
RGLSYMGLTEXTUREPARAMETERIEXTPROC __rglgen_glTextureParameteriEXT;
RGLSYMGLTEXTUREPARAMETERIVEXTPROC __rglgen_glTextureParameterivEXT;
RGLSYMGLTEXTUREIMAGE1DEXTPROC __rglgen_glTextureImage1DEXT;
RGLSYMGLTEXTUREIMAGE2DEXTPROC __rglgen_glTextureImage2DEXT;
RGLSYMGLTEXTURESUBIMAGE1DEXTPROC __rglgen_glTextureSubImage1DEXT;
RGLSYMGLTEXTURESUBIMAGE2DEXTPROC __rglgen_glTextureSubImage2DEXT;
RGLSYMGLCOPYTEXTUREIMAGE1DEXTPROC __rglgen_glCopyTextureImage1DEXT;
RGLSYMGLCOPYTEXTUREIMAGE2DEXTPROC __rglgen_glCopyTextureImage2DEXT;
RGLSYMGLCOPYTEXTURESUBIMAGE1DEXTPROC __rglgen_glCopyTextureSubImage1DEXT;
RGLSYMGLCOPYTEXTURESUBIMAGE2DEXTPROC __rglgen_glCopyTextureSubImage2DEXT;
RGLSYMGLGETTEXTUREIMAGEEXTPROC __rglgen_glGetTextureImageEXT;
RGLSYMGLGETTEXTUREPARAMETERFVEXTPROC __rglgen_glGetTextureParameterfvEXT;
RGLSYMGLGETTEXTUREPARAMETERIVEXTPROC __rglgen_glGetTextureParameterivEXT;
RGLSYMGLGETTEXTURELEVELPARAMETERFVEXTPROC __rglgen_glGetTextureLevelParameterfvEXT;
RGLSYMGLGETTEXTURELEVELPARAMETERIVEXTPROC __rglgen_glGetTextureLevelParameterivEXT;
RGLSYMGLTEXTUREIMAGE3DEXTPROC __rglgen_glTextureImage3DEXT;
RGLSYMGLTEXTURESUBIMAGE3DEXTPROC __rglgen_glTextureSubImage3DEXT;
RGLSYMGLCOPYTEXTURESUBIMAGE3DEXTPROC __rglgen_glCopyTextureSubImage3DEXT;
RGLSYMGLBINDMULTITEXTUREEXTPROC __rglgen_glBindMultiTextureEXT;
RGLSYMGLMULTITEXCOORDPOINTEREXTPROC __rglgen_glMultiTexCoordPointerEXT;
RGLSYMGLMULTITEXENVFEXTPROC __rglgen_glMultiTexEnvfEXT;
RGLSYMGLMULTITEXENVFVEXTPROC __rglgen_glMultiTexEnvfvEXT;
RGLSYMGLMULTITEXENVIEXTPROC __rglgen_glMultiTexEnviEXT;
RGLSYMGLMULTITEXENVIVEXTPROC __rglgen_glMultiTexEnvivEXT;
RGLSYMGLMULTITEXGENDEXTPROC __rglgen_glMultiTexGendEXT;
RGLSYMGLMULTITEXGENDVEXTPROC __rglgen_glMultiTexGendvEXT;
RGLSYMGLMULTITEXGENFEXTPROC __rglgen_glMultiTexGenfEXT;
RGLSYMGLMULTITEXGENFVEXTPROC __rglgen_glMultiTexGenfvEXT;
RGLSYMGLMULTITEXGENIEXTPROC __rglgen_glMultiTexGeniEXT;
RGLSYMGLMULTITEXGENIVEXTPROC __rglgen_glMultiTexGenivEXT;
RGLSYMGLGETMULTITEXENVFVEXTPROC __rglgen_glGetMultiTexEnvfvEXT;
RGLSYMGLGETMULTITEXENVIVEXTPROC __rglgen_glGetMultiTexEnvivEXT;
RGLSYMGLGETMULTITEXGENDVEXTPROC __rglgen_glGetMultiTexGendvEXT;
RGLSYMGLGETMULTITEXGENFVEXTPROC __rglgen_glGetMultiTexGenfvEXT;
RGLSYMGLGETMULTITEXGENIVEXTPROC __rglgen_glGetMultiTexGenivEXT;
RGLSYMGLMULTITEXPARAMETERIEXTPROC __rglgen_glMultiTexParameteriEXT;
RGLSYMGLMULTITEXPARAMETERIVEXTPROC __rglgen_glMultiTexParameterivEXT;
RGLSYMGLMULTITEXPARAMETERFEXTPROC __rglgen_glMultiTexParameterfEXT;
RGLSYMGLMULTITEXPARAMETERFVEXTPROC __rglgen_glMultiTexParameterfvEXT;
RGLSYMGLMULTITEXIMAGE1DEXTPROC __rglgen_glMultiTexImage1DEXT;
RGLSYMGLMULTITEXIMAGE2DEXTPROC __rglgen_glMultiTexImage2DEXT;
RGLSYMGLMULTITEXSUBIMAGE1DEXTPROC __rglgen_glMultiTexSubImage1DEXT;
RGLSYMGLMULTITEXSUBIMAGE2DEXTPROC __rglgen_glMultiTexSubImage2DEXT;
RGLSYMGLCOPYMULTITEXIMAGE1DEXTPROC __rglgen_glCopyMultiTexImage1DEXT;
RGLSYMGLCOPYMULTITEXIMAGE2DEXTPROC __rglgen_glCopyMultiTexImage2DEXT;
RGLSYMGLCOPYMULTITEXSUBIMAGE1DEXTPROC __rglgen_glCopyMultiTexSubImage1DEXT;
RGLSYMGLCOPYMULTITEXSUBIMAGE2DEXTPROC __rglgen_glCopyMultiTexSubImage2DEXT;
RGLSYMGLGETMULTITEXIMAGEEXTPROC __rglgen_glGetMultiTexImageEXT;
RGLSYMGLGETMULTITEXPARAMETERFVEXTPROC __rglgen_glGetMultiTexParameterfvEXT;
RGLSYMGLGETMULTITEXPARAMETERIVEXTPROC __rglgen_glGetMultiTexParameterivEXT;
RGLSYMGLGETMULTITEXLEVELPARAMETERFVEXTPROC __rglgen_glGetMultiTexLevelParameterfvEXT;
RGLSYMGLGETMULTITEXLEVELPARAMETERIVEXTPROC __rglgen_glGetMultiTexLevelParameterivEXT;
RGLSYMGLMULTITEXIMAGE3DEXTPROC __rglgen_glMultiTexImage3DEXT;
RGLSYMGLMULTITEXSUBIMAGE3DEXTPROC __rglgen_glMultiTexSubImage3DEXT;
RGLSYMGLCOPYMULTITEXSUBIMAGE3DEXTPROC __rglgen_glCopyMultiTexSubImage3DEXT;
RGLSYMGLENABLECLIENTSTATEINDEXEDEXTPROC __rglgen_glEnableClientStateIndexedEXT;
RGLSYMGLDISABLECLIENTSTATEINDEXEDEXTPROC __rglgen_glDisableClientStateIndexedEXT;
RGLSYMGLGETFLOATINDEXEDVEXTPROC __rglgen_glGetFloatIndexedvEXT;
RGLSYMGLGETDOUBLEINDEXEDVEXTPROC __rglgen_glGetDoubleIndexedvEXT;
RGLSYMGLGETPOINTERINDEXEDVEXTPROC __rglgen_glGetPointerIndexedvEXT;
RGLSYMGLENABLEINDEXEDEXTPROC __rglgen_glEnableIndexedEXT;
RGLSYMGLDISABLEINDEXEDEXTPROC __rglgen_glDisableIndexedEXT;
RGLSYMGLISENABLEDINDEXEDEXTPROC __rglgen_glIsEnabledIndexedEXT;
RGLSYMGLGETINTEGERINDEXEDVEXTPROC __rglgen_glGetIntegerIndexedvEXT;
RGLSYMGLGETBOOLEANINDEXEDVEXTPROC __rglgen_glGetBooleanIndexedvEXT;
RGLSYMGLCOMPRESSEDTEXTUREIMAGE3DEXTPROC __rglgen_glCompressedTextureImage3DEXT;
RGLSYMGLCOMPRESSEDTEXTUREIMAGE2DEXTPROC __rglgen_glCompressedTextureImage2DEXT;
RGLSYMGLCOMPRESSEDTEXTUREIMAGE1DEXTPROC __rglgen_glCompressedTextureImage1DEXT;
RGLSYMGLCOMPRESSEDTEXTURESUBIMAGE3DEXTPROC __rglgen_glCompressedTextureSubImage3DEXT;
RGLSYMGLCOMPRESSEDTEXTURESUBIMAGE2DEXTPROC __rglgen_glCompressedTextureSubImage2DEXT;
RGLSYMGLCOMPRESSEDTEXTURESUBIMAGE1DEXTPROC __rglgen_glCompressedTextureSubImage1DEXT;
RGLSYMGLGETCOMPRESSEDTEXTUREIMAGEEXTPROC __rglgen_glGetCompressedTextureImageEXT;
RGLSYMGLCOMPRESSEDMULTITEXIMAGE3DEXTPROC __rglgen_glCompressedMultiTexImage3DEXT;
RGLSYMGLCOMPRESSEDMULTITEXIMAGE2DEXTPROC __rglgen_glCompressedMultiTexImage2DEXT;
RGLSYMGLCOMPRESSEDMULTITEXIMAGE1DEXTPROC __rglgen_glCompressedMultiTexImage1DEXT;
RGLSYMGLCOMPRESSEDMULTITEXSUBIMAGE3DEXTPROC __rglgen_glCompressedMultiTexSubImage3DEXT;
RGLSYMGLCOMPRESSEDMULTITEXSUBIMAGE2DEXTPROC __rglgen_glCompressedMultiTexSubImage2DEXT;
RGLSYMGLCOMPRESSEDMULTITEXSUBIMAGE1DEXTPROC __rglgen_glCompressedMultiTexSubImage1DEXT;
RGLSYMGLGETCOMPRESSEDMULTITEXIMAGEEXTPROC __rglgen_glGetCompressedMultiTexImageEXT;
RGLSYMGLMATRIXLOADTRANSPOSEFEXTPROC __rglgen_glMatrixLoadTransposefEXT;
RGLSYMGLMATRIXLOADTRANSPOSEDEXTPROC __rglgen_glMatrixLoadTransposedEXT;
RGLSYMGLMATRIXMULTTRANSPOSEFEXTPROC __rglgen_glMatrixMultTransposefEXT;
RGLSYMGLMATRIXMULTTRANSPOSEDEXTPROC __rglgen_glMatrixMultTransposedEXT;
RGLSYMGLNAMEDBUFFERDATAEXTPROC __rglgen_glNamedBufferDataEXT;
RGLSYMGLNAMEDBUFFERSUBDATAEXTPROC __rglgen_glNamedBufferSubDataEXT;
RGLSYMGLMAPNAMEDBUFFEREXTPROC __rglgen_glMapNamedBufferEXT;
RGLSYMGLUNMAPNAMEDBUFFEREXTPROC __rglgen_glUnmapNamedBufferEXT;
RGLSYMGLGETNAMEDBUFFERPARAMETERIVEXTPROC __rglgen_glGetNamedBufferParameterivEXT;
RGLSYMGLGETNAMEDBUFFERPOINTERVEXTPROC __rglgen_glGetNamedBufferPointervEXT;
RGLSYMGLGETNAMEDBUFFERSUBDATAEXTPROC __rglgen_glGetNamedBufferSubDataEXT;
RGLSYMGLPROGRAMUNIFORM1FEXTPROC __rglgen_glProgramUniform1fEXT;
RGLSYMGLPROGRAMUNIFORM2FEXTPROC __rglgen_glProgramUniform2fEXT;
RGLSYMGLPROGRAMUNIFORM3FEXTPROC __rglgen_glProgramUniform3fEXT;
RGLSYMGLPROGRAMUNIFORM4FEXTPROC __rglgen_glProgramUniform4fEXT;
RGLSYMGLPROGRAMUNIFORM1IEXTPROC __rglgen_glProgramUniform1iEXT;
RGLSYMGLPROGRAMUNIFORM2IEXTPROC __rglgen_glProgramUniform2iEXT;
RGLSYMGLPROGRAMUNIFORM3IEXTPROC __rglgen_glProgramUniform3iEXT;
RGLSYMGLPROGRAMUNIFORM4IEXTPROC __rglgen_glProgramUniform4iEXT;
RGLSYMGLPROGRAMUNIFORM1FVEXTPROC __rglgen_glProgramUniform1fvEXT;
RGLSYMGLPROGRAMUNIFORM2FVEXTPROC __rglgen_glProgramUniform2fvEXT;
RGLSYMGLPROGRAMUNIFORM3FVEXTPROC __rglgen_glProgramUniform3fvEXT;
RGLSYMGLPROGRAMUNIFORM4FVEXTPROC __rglgen_glProgramUniform4fvEXT;
RGLSYMGLPROGRAMUNIFORM1IVEXTPROC __rglgen_glProgramUniform1ivEXT;
RGLSYMGLPROGRAMUNIFORM2IVEXTPROC __rglgen_glProgramUniform2ivEXT;
RGLSYMGLPROGRAMUNIFORM3IVEXTPROC __rglgen_glProgramUniform3ivEXT;
RGLSYMGLPROGRAMUNIFORM4IVEXTPROC __rglgen_glProgramUniform4ivEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX2FVEXTPROC __rglgen_glProgramUniformMatrix2fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX3FVEXTPROC __rglgen_glProgramUniformMatrix3fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX4FVEXTPROC __rglgen_glProgramUniformMatrix4fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX2X3FVEXTPROC __rglgen_glProgramUniformMatrix2x3fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX3X2FVEXTPROC __rglgen_glProgramUniformMatrix3x2fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX2X4FVEXTPROC __rglgen_glProgramUniformMatrix2x4fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX4X2FVEXTPROC __rglgen_glProgramUniformMatrix4x2fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX3X4FVEXTPROC __rglgen_glProgramUniformMatrix3x4fvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX4X3FVEXTPROC __rglgen_glProgramUniformMatrix4x3fvEXT;
RGLSYMGLTEXTUREBUFFEREXTPROC __rglgen_glTextureBufferEXT;
RGLSYMGLMULTITEXBUFFEREXTPROC __rglgen_glMultiTexBufferEXT;
RGLSYMGLTEXTUREPARAMETERIIVEXTPROC __rglgen_glTextureParameterIivEXT;
RGLSYMGLTEXTUREPARAMETERIUIVEXTPROC __rglgen_glTextureParameterIuivEXT;
RGLSYMGLGETTEXTUREPARAMETERIIVEXTPROC __rglgen_glGetTextureParameterIivEXT;
RGLSYMGLGETTEXTUREPARAMETERIUIVEXTPROC __rglgen_glGetTextureParameterIuivEXT;
RGLSYMGLMULTITEXPARAMETERIIVEXTPROC __rglgen_glMultiTexParameterIivEXT;
RGLSYMGLMULTITEXPARAMETERIUIVEXTPROC __rglgen_glMultiTexParameterIuivEXT;
RGLSYMGLGETMULTITEXPARAMETERIIVEXTPROC __rglgen_glGetMultiTexParameterIivEXT;
RGLSYMGLGETMULTITEXPARAMETERIUIVEXTPROC __rglgen_glGetMultiTexParameterIuivEXT;
RGLSYMGLPROGRAMUNIFORM1UIEXTPROC __rglgen_glProgramUniform1uiEXT;
RGLSYMGLPROGRAMUNIFORM2UIEXTPROC __rglgen_glProgramUniform2uiEXT;
RGLSYMGLPROGRAMUNIFORM3UIEXTPROC __rglgen_glProgramUniform3uiEXT;
RGLSYMGLPROGRAMUNIFORM4UIEXTPROC __rglgen_glProgramUniform4uiEXT;
RGLSYMGLPROGRAMUNIFORM1UIVEXTPROC __rglgen_glProgramUniform1uivEXT;
RGLSYMGLPROGRAMUNIFORM2UIVEXTPROC __rglgen_glProgramUniform2uivEXT;
RGLSYMGLPROGRAMUNIFORM3UIVEXTPROC __rglgen_glProgramUniform3uivEXT;
RGLSYMGLPROGRAMUNIFORM4UIVEXTPROC __rglgen_glProgramUniform4uivEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETERS4FVEXTPROC __rglgen_glNamedProgramLocalParameters4fvEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETERI4IEXTPROC __rglgen_glNamedProgramLocalParameterI4iEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETERI4IVEXTPROC __rglgen_glNamedProgramLocalParameterI4ivEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETERSI4IVEXTPROC __rglgen_glNamedProgramLocalParametersI4ivEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETERI4UIEXTPROC __rglgen_glNamedProgramLocalParameterI4uiEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETERI4UIVEXTPROC __rglgen_glNamedProgramLocalParameterI4uivEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETERSI4UIVEXTPROC __rglgen_glNamedProgramLocalParametersI4uivEXT;
RGLSYMGLGETNAMEDPROGRAMLOCALPARAMETERIIVEXTPROC __rglgen_glGetNamedProgramLocalParameterIivEXT;
RGLSYMGLGETNAMEDPROGRAMLOCALPARAMETERIUIVEXTPROC __rglgen_glGetNamedProgramLocalParameterIuivEXT;
RGLSYMGLENABLECLIENTSTATEIEXTPROC __rglgen_glEnableClientStateiEXT;
RGLSYMGLDISABLECLIENTSTATEIEXTPROC __rglgen_glDisableClientStateiEXT;
RGLSYMGLGETFLOATI_VEXTPROC __rglgen_glGetFloati_vEXT;
RGLSYMGLGETDOUBLEI_VEXTPROC __rglgen_glGetDoublei_vEXT;
RGLSYMGLGETPOINTERI_VEXTPROC __rglgen_glGetPointeri_vEXT;
RGLSYMGLNAMEDPROGRAMSTRINGEXTPROC __rglgen_glNamedProgramStringEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETER4DEXTPROC __rglgen_glNamedProgramLocalParameter4dEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETER4DVEXTPROC __rglgen_glNamedProgramLocalParameter4dvEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETER4FEXTPROC __rglgen_glNamedProgramLocalParameter4fEXT;
RGLSYMGLNAMEDPROGRAMLOCALPARAMETER4FVEXTPROC __rglgen_glNamedProgramLocalParameter4fvEXT;
RGLSYMGLGETNAMEDPROGRAMLOCALPARAMETERDVEXTPROC __rglgen_glGetNamedProgramLocalParameterdvEXT;
RGLSYMGLGETNAMEDPROGRAMLOCALPARAMETERFVEXTPROC __rglgen_glGetNamedProgramLocalParameterfvEXT;
RGLSYMGLGETNAMEDPROGRAMIVEXTPROC __rglgen_glGetNamedProgramivEXT;
RGLSYMGLGETNAMEDPROGRAMSTRINGEXTPROC __rglgen_glGetNamedProgramStringEXT;
RGLSYMGLNAMEDRENDERBUFFERSTORAGEEXTPROC __rglgen_glNamedRenderbufferStorageEXT;
RGLSYMGLGETNAMEDRENDERBUFFERPARAMETERIVEXTPROC __rglgen_glGetNamedRenderbufferParameterivEXT;
RGLSYMGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEEXTPROC __rglgen_glNamedRenderbufferStorageMultisampleEXT;
RGLSYMGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLECOVERAGEEXTPROC __rglgen_glNamedRenderbufferStorageMultisampleCoverageEXT;
RGLSYMGLCHECKNAMEDFRAMEBUFFERSTATUSEXTPROC __rglgen_glCheckNamedFramebufferStatusEXT;
RGLSYMGLNAMEDFRAMEBUFFERTEXTURE1DEXTPROC __rglgen_glNamedFramebufferTexture1DEXT;
RGLSYMGLNAMEDFRAMEBUFFERTEXTURE2DEXTPROC __rglgen_glNamedFramebufferTexture2DEXT;
RGLSYMGLNAMEDFRAMEBUFFERTEXTURE3DEXTPROC __rglgen_glNamedFramebufferTexture3DEXT;
RGLSYMGLNAMEDFRAMEBUFFERRENDERBUFFEREXTPROC __rglgen_glNamedFramebufferRenderbufferEXT;
RGLSYMGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC __rglgen_glGetNamedFramebufferAttachmentParameterivEXT;
RGLSYMGLGENERATETEXTUREMIPMAPEXTPROC __rglgen_glGenerateTextureMipmapEXT;
RGLSYMGLGENERATEMULTITEXMIPMAPEXTPROC __rglgen_glGenerateMultiTexMipmapEXT;
RGLSYMGLFRAMEBUFFERDRAWBUFFEREXTPROC __rglgen_glFramebufferDrawBufferEXT;
RGLSYMGLFRAMEBUFFERDRAWBUFFERSEXTPROC __rglgen_glFramebufferDrawBuffersEXT;
RGLSYMGLFRAMEBUFFERREADBUFFEREXTPROC __rglgen_glFramebufferReadBufferEXT;
RGLSYMGLGETFRAMEBUFFERPARAMETERIVEXTPROC __rglgen_glGetFramebufferParameterivEXT;
RGLSYMGLNAMEDCOPYBUFFERSUBDATAEXTPROC __rglgen_glNamedCopyBufferSubDataEXT;
RGLSYMGLNAMEDFRAMEBUFFERTEXTUREEXTPROC __rglgen_glNamedFramebufferTextureEXT;
RGLSYMGLNAMEDFRAMEBUFFERTEXTURELAYEREXTPROC __rglgen_glNamedFramebufferTextureLayerEXT;
RGLSYMGLNAMEDFRAMEBUFFERTEXTUREFACEEXTPROC __rglgen_glNamedFramebufferTextureFaceEXT;
RGLSYMGLTEXTURERENDERBUFFEREXTPROC __rglgen_glTextureRenderbufferEXT;
RGLSYMGLMULTITEXRENDERBUFFEREXTPROC __rglgen_glMultiTexRenderbufferEXT;
RGLSYMGLVERTEXARRAYVERTEXOFFSETEXTPROC __rglgen_glVertexArrayVertexOffsetEXT;
RGLSYMGLVERTEXARRAYCOLOROFFSETEXTPROC __rglgen_glVertexArrayColorOffsetEXT;
RGLSYMGLVERTEXARRAYEDGEFLAGOFFSETEXTPROC __rglgen_glVertexArrayEdgeFlagOffsetEXT;
RGLSYMGLVERTEXARRAYINDEXOFFSETEXTPROC __rglgen_glVertexArrayIndexOffsetEXT;
RGLSYMGLVERTEXARRAYNORMALOFFSETEXTPROC __rglgen_glVertexArrayNormalOffsetEXT;
RGLSYMGLVERTEXARRAYTEXCOORDOFFSETEXTPROC __rglgen_glVertexArrayTexCoordOffsetEXT;
RGLSYMGLVERTEXARRAYMULTITEXCOORDOFFSETEXTPROC __rglgen_glVertexArrayMultiTexCoordOffsetEXT;
RGLSYMGLVERTEXARRAYFOGCOORDOFFSETEXTPROC __rglgen_glVertexArrayFogCoordOffsetEXT;
RGLSYMGLVERTEXARRAYSECONDARYCOLOROFFSETEXTPROC __rglgen_glVertexArraySecondaryColorOffsetEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBOFFSETEXTPROC __rglgen_glVertexArrayVertexAttribOffsetEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBIOFFSETEXTPROC __rglgen_glVertexArrayVertexAttribIOffsetEXT;
RGLSYMGLENABLEVERTEXARRAYEXTPROC __rglgen_glEnableVertexArrayEXT;
RGLSYMGLDISABLEVERTEXARRAYEXTPROC __rglgen_glDisableVertexArrayEXT;
RGLSYMGLENABLEVERTEXARRAYATTRIBEXTPROC __rglgen_glEnableVertexArrayAttribEXT;
RGLSYMGLDISABLEVERTEXARRAYATTRIBEXTPROC __rglgen_glDisableVertexArrayAttribEXT;
RGLSYMGLGETVERTEXARRAYINTEGERVEXTPROC __rglgen_glGetVertexArrayIntegervEXT;
RGLSYMGLGETVERTEXARRAYPOINTERVEXTPROC __rglgen_glGetVertexArrayPointervEXT;
RGLSYMGLGETVERTEXARRAYINTEGERI_VEXTPROC __rglgen_glGetVertexArrayIntegeri_vEXT;
RGLSYMGLGETVERTEXARRAYPOINTERI_VEXTPROC __rglgen_glGetVertexArrayPointeri_vEXT;
RGLSYMGLMAPNAMEDBUFFERRANGEEXTPROC __rglgen_glMapNamedBufferRangeEXT;
RGLSYMGLFLUSHMAPPEDNAMEDBUFFERRANGEEXTPROC __rglgen_glFlushMappedNamedBufferRangeEXT;
RGLSYMGLNAMEDBUFFERSTORAGEEXTPROC __rglgen_glNamedBufferStorageEXT;
RGLSYMGLCLEARNAMEDBUFFERDATAEXTPROC __rglgen_glClearNamedBufferDataEXT;
RGLSYMGLCLEARNAMEDBUFFERSUBDATAEXTPROC __rglgen_glClearNamedBufferSubDataEXT;
RGLSYMGLNAMEDFRAMEBUFFERPARAMETERIEXTPROC __rglgen_glNamedFramebufferParameteriEXT;
RGLSYMGLGETNAMEDFRAMEBUFFERPARAMETERIVEXTPROC __rglgen_glGetNamedFramebufferParameterivEXT;
RGLSYMGLPROGRAMUNIFORM1DEXTPROC __rglgen_glProgramUniform1dEXT;
RGLSYMGLPROGRAMUNIFORM2DEXTPROC __rglgen_glProgramUniform2dEXT;
RGLSYMGLPROGRAMUNIFORM3DEXTPROC __rglgen_glProgramUniform3dEXT;
RGLSYMGLPROGRAMUNIFORM4DEXTPROC __rglgen_glProgramUniform4dEXT;
RGLSYMGLPROGRAMUNIFORM1DVEXTPROC __rglgen_glProgramUniform1dvEXT;
RGLSYMGLPROGRAMUNIFORM2DVEXTPROC __rglgen_glProgramUniform2dvEXT;
RGLSYMGLPROGRAMUNIFORM3DVEXTPROC __rglgen_glProgramUniform3dvEXT;
RGLSYMGLPROGRAMUNIFORM4DVEXTPROC __rglgen_glProgramUniform4dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX2DVEXTPROC __rglgen_glProgramUniformMatrix2dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX3DVEXTPROC __rglgen_glProgramUniformMatrix3dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX4DVEXTPROC __rglgen_glProgramUniformMatrix4dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX2X3DVEXTPROC __rglgen_glProgramUniformMatrix2x3dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX2X4DVEXTPROC __rglgen_glProgramUniformMatrix2x4dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX3X2DVEXTPROC __rglgen_glProgramUniformMatrix3x2dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX3X4DVEXTPROC __rglgen_glProgramUniformMatrix3x4dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX4X2DVEXTPROC __rglgen_glProgramUniformMatrix4x2dvEXT;
RGLSYMGLPROGRAMUNIFORMMATRIX4X3DVEXTPROC __rglgen_glProgramUniformMatrix4x3dvEXT;
RGLSYMGLTEXTUREBUFFERRANGEEXTPROC __rglgen_glTextureBufferRangeEXT;
RGLSYMGLTEXTURESTORAGE1DEXTPROC __rglgen_glTextureStorage1DEXT;
RGLSYMGLTEXTURESTORAGE2DEXTPROC __rglgen_glTextureStorage2DEXT;
RGLSYMGLTEXTURESTORAGE3DEXTPROC __rglgen_glTextureStorage3DEXT;
RGLSYMGLTEXTURESTORAGE2DMULTISAMPLEEXTPROC __rglgen_glTextureStorage2DMultisampleEXT;
RGLSYMGLTEXTURESTORAGE3DMULTISAMPLEEXTPROC __rglgen_glTextureStorage3DMultisampleEXT;
RGLSYMGLVERTEXARRAYBINDVERTEXBUFFEREXTPROC __rglgen_glVertexArrayBindVertexBufferEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBFORMATEXTPROC __rglgen_glVertexArrayVertexAttribFormatEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBIFORMATEXTPROC __rglgen_glVertexArrayVertexAttribIFormatEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBLFORMATEXTPROC __rglgen_glVertexArrayVertexAttribLFormatEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBBINDINGEXTPROC __rglgen_glVertexArrayVertexAttribBindingEXT;
RGLSYMGLVERTEXARRAYVERTEXBINDINGDIVISOREXTPROC __rglgen_glVertexArrayVertexBindingDivisorEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBLOFFSETEXTPROC __rglgen_glVertexArrayVertexAttribLOffsetEXT;
RGLSYMGLTEXTUREPAGECOMMITMENTEXTPROC __rglgen_glTexturePageCommitmentEXT;
RGLSYMGLVERTEXARRAYVERTEXATTRIBDIVISOREXTPROC __rglgen_glVertexArrayVertexAttribDivisorEXT;
RGLSYMGLCOLORMASKINDEXEDEXTPROC __rglgen_glColorMaskIndexedEXT;
RGLSYMGLDRAWARRAYSINSTANCEDEXTPROC __rglgen_glDrawArraysInstancedEXT;
RGLSYMGLDRAWELEMENTSINSTANCEDEXTPROC __rglgen_glDrawElementsInstancedEXT;
RGLSYMGLDRAWRANGEELEMENTSEXTPROC __rglgen_glDrawRangeElementsEXT;
RGLSYMGLBUFFERSTORAGEEXTERNALEXTPROC __rglgen_glBufferStorageExternalEXT;
RGLSYMGLNAMEDBUFFERSTORAGEEXTERNALEXTPROC __rglgen_glNamedBufferStorageExternalEXT;
RGLSYMGLFOGCOORDFEXTPROC __rglgen_glFogCoordfEXT;
RGLSYMGLFOGCOORDFVEXTPROC __rglgen_glFogCoordfvEXT;
RGLSYMGLFOGCOORDDEXTPROC __rglgen_glFogCoorddEXT;
RGLSYMGLFOGCOORDDVEXTPROC __rglgen_glFogCoorddvEXT;
RGLSYMGLFOGCOORDPOINTEREXTPROC __rglgen_glFogCoordPointerEXT;
RGLSYMGLBLITFRAMEBUFFEREXTPROC __rglgen_glBlitFramebufferEXT;
RGLSYMGLRENDERBUFFERSTORAGEMULTISAMPLEEXTPROC __rglgen_glRenderbufferStorageMultisampleEXT;
RGLSYMGLISRENDERBUFFEREXTPROC __rglgen_glIsRenderbufferEXT;
RGLSYMGLBINDRENDERBUFFEREXTPROC __rglgen_glBindRenderbufferEXT;
RGLSYMGLDELETERENDERBUFFERSEXTPROC __rglgen_glDeleteRenderbuffersEXT;
RGLSYMGLGENRENDERBUFFERSEXTPROC __rglgen_glGenRenderbuffersEXT;
RGLSYMGLRENDERBUFFERSTORAGEEXTPROC __rglgen_glRenderbufferStorageEXT;
RGLSYMGLGETRENDERBUFFERPARAMETERIVEXTPROC __rglgen_glGetRenderbufferParameterivEXT;
RGLSYMGLISFRAMEBUFFEREXTPROC __rglgen_glIsFramebufferEXT;
RGLSYMGLBINDFRAMEBUFFEREXTPROC __rglgen_glBindFramebufferEXT;
RGLSYMGLDELETEFRAMEBUFFERSEXTPROC __rglgen_glDeleteFramebuffersEXT;
RGLSYMGLGENFRAMEBUFFERSEXTPROC __rglgen_glGenFramebuffersEXT;
RGLSYMGLCHECKFRAMEBUFFERSTATUSEXTPROC __rglgen_glCheckFramebufferStatusEXT;
RGLSYMGLFRAMEBUFFERTEXTURE1DEXTPROC __rglgen_glFramebufferTexture1DEXT;
RGLSYMGLFRAMEBUFFERTEXTURE2DEXTPROC __rglgen_glFramebufferTexture2DEXT;
RGLSYMGLFRAMEBUFFERTEXTURE3DEXTPROC __rglgen_glFramebufferTexture3DEXT;
RGLSYMGLFRAMEBUFFERRENDERBUFFEREXTPROC __rglgen_glFramebufferRenderbufferEXT;
RGLSYMGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC __rglgen_glGetFramebufferAttachmentParameterivEXT;
RGLSYMGLGENERATEMIPMAPEXTPROC __rglgen_glGenerateMipmapEXT;
RGLSYMGLPROGRAMPARAMETERIEXTPROC __rglgen_glProgramParameteriEXT;
RGLSYMGLPROGRAMENVPARAMETERS4FVEXTPROC __rglgen_glProgramEnvParameters4fvEXT;
RGLSYMGLPROGRAMLOCALPARAMETERS4FVEXTPROC __rglgen_glProgramLocalParameters4fvEXT;
RGLSYMGLGETUNIFORMUIVEXTPROC __rglgen_glGetUniformuivEXT;
RGLSYMGLBINDFRAGDATALOCATIONEXTPROC __rglgen_glBindFragDataLocationEXT;
RGLSYMGLGETFRAGDATALOCATIONEXTPROC __rglgen_glGetFragDataLocationEXT;
RGLSYMGLUNIFORM1UIEXTPROC __rglgen_glUniform1uiEXT;
RGLSYMGLUNIFORM2UIEXTPROC __rglgen_glUniform2uiEXT;
RGLSYMGLUNIFORM3UIEXTPROC __rglgen_glUniform3uiEXT;
RGLSYMGLUNIFORM4UIEXTPROC __rglgen_glUniform4uiEXT;
RGLSYMGLUNIFORM1UIVEXTPROC __rglgen_glUniform1uivEXT;
RGLSYMGLUNIFORM2UIVEXTPROC __rglgen_glUniform2uivEXT;
RGLSYMGLUNIFORM3UIVEXTPROC __rglgen_glUniform3uivEXT;
RGLSYMGLUNIFORM4UIVEXTPROC __rglgen_glUniform4uivEXT;
RGLSYMGLGETHISTOGRAMEXTPROC __rglgen_glGetHistogramEXT;
RGLSYMGLGETHISTOGRAMPARAMETERFVEXTPROC __rglgen_glGetHistogramParameterfvEXT;
RGLSYMGLGETHISTOGRAMPARAMETERIVEXTPROC __rglgen_glGetHistogramParameterivEXT;
RGLSYMGLGETMINMAXEXTPROC __rglgen_glGetMinmaxEXT;
RGLSYMGLGETMINMAXPARAMETERFVEXTPROC __rglgen_glGetMinmaxParameterfvEXT;
RGLSYMGLGETMINMAXPARAMETERIVEXTPROC __rglgen_glGetMinmaxParameterivEXT;
RGLSYMGLHISTOGRAMEXTPROC __rglgen_glHistogramEXT;
RGLSYMGLMINMAXEXTPROC __rglgen_glMinmaxEXT;
RGLSYMGLRESETHISTOGRAMEXTPROC __rglgen_glResetHistogramEXT;
RGLSYMGLRESETMINMAXEXTPROC __rglgen_glResetMinmaxEXT;
RGLSYMGLINDEXFUNCEXTPROC __rglgen_glIndexFuncEXT;
RGLSYMGLINDEXMATERIALEXTPROC __rglgen_glIndexMaterialEXT;
RGLSYMGLAPPLYTEXTUREEXTPROC __rglgen_glApplyTextureEXT;
RGLSYMGLTEXTURELIGHTEXTPROC __rglgen_glTextureLightEXT;
RGLSYMGLTEXTUREMATERIALEXTPROC __rglgen_glTextureMaterialEXT;
RGLSYMGLGETUNSIGNEDBYTEVEXTPROC __rglgen_glGetUnsignedBytevEXT;
RGLSYMGLGETUNSIGNEDBYTEI_VEXTPROC __rglgen_glGetUnsignedBytei_vEXT;
RGLSYMGLDELETEMEMORYOBJECTSEXTPROC __rglgen_glDeleteMemoryObjectsEXT;
RGLSYMGLISMEMORYOBJECTEXTPROC __rglgen_glIsMemoryObjectEXT;
RGLSYMGLCREATEMEMORYOBJECTSEXTPROC __rglgen_glCreateMemoryObjectsEXT;
RGLSYMGLMEMORYOBJECTPARAMETERIVEXTPROC __rglgen_glMemoryObjectParameterivEXT;
RGLSYMGLGETMEMORYOBJECTPARAMETERIVEXTPROC __rglgen_glGetMemoryObjectParameterivEXT;
RGLSYMGLTEXSTORAGEMEM2DEXTPROC __rglgen_glTexStorageMem2DEXT;
RGLSYMGLTEXSTORAGEMEM2DMULTISAMPLEEXTPROC __rglgen_glTexStorageMem2DMultisampleEXT;
RGLSYMGLTEXSTORAGEMEM3DEXTPROC __rglgen_glTexStorageMem3DEXT;
RGLSYMGLTEXSTORAGEMEM3DMULTISAMPLEEXTPROC __rglgen_glTexStorageMem3DMultisampleEXT;
RGLSYMGLBUFFERSTORAGEMEMEXTPROC __rglgen_glBufferStorageMemEXT;
RGLSYMGLTEXTURESTORAGEMEM2DEXTPROC __rglgen_glTextureStorageMem2DEXT;
RGLSYMGLTEXTURESTORAGEMEM2DMULTISAMPLEEXTPROC __rglgen_glTextureStorageMem2DMultisampleEXT;
RGLSYMGLTEXTURESTORAGEMEM3DEXTPROC __rglgen_glTextureStorageMem3DEXT;
RGLSYMGLTEXTURESTORAGEMEM3DMULTISAMPLEEXTPROC __rglgen_glTextureStorageMem3DMultisampleEXT;
RGLSYMGLNAMEDBUFFERSTORAGEMEMEXTPROC __rglgen_glNamedBufferStorageMemEXT;
RGLSYMGLTEXSTORAGEMEM1DEXTPROC __rglgen_glTexStorageMem1DEXT;
RGLSYMGLTEXTURESTORAGEMEM1DEXTPROC __rglgen_glTextureStorageMem1DEXT;
RGLSYMGLIMPORTMEMORYFDEXTPROC __rglgen_glImportMemoryFdEXT;
RGLSYMGLIMPORTMEMORYWIN32HANDLEEXTPROC __rglgen_glImportMemoryWin32HandleEXT;
RGLSYMGLIMPORTMEMORYWIN32NAMEEXTPROC __rglgen_glImportMemoryWin32NameEXT;
RGLSYMGLMULTIDRAWARRAYSEXTPROC __rglgen_glMultiDrawArraysEXT;
RGLSYMGLMULTIDRAWELEMENTSEXTPROC __rglgen_glMultiDrawElementsEXT;
RGLSYMGLSAMPLEMASKEXTPROC __rglgen_glSampleMaskEXT;
RGLSYMGLSAMPLEPATTERNEXTPROC __rglgen_glSamplePatternEXT;
RGLSYMGLCOLORTABLEEXTPROC __rglgen_glColorTableEXT;
RGLSYMGLGETCOLORTABLEEXTPROC __rglgen_glGetColorTableEXT;
RGLSYMGLGETCOLORTABLEPARAMETERIVEXTPROC __rglgen_glGetColorTableParameterivEXT;
RGLSYMGLGETCOLORTABLEPARAMETERFVEXTPROC __rglgen_glGetColorTableParameterfvEXT;
RGLSYMGLPIXELTRANSFORMPARAMETERIEXTPROC __rglgen_glPixelTransformParameteriEXT;
RGLSYMGLPIXELTRANSFORMPARAMETERFEXTPROC __rglgen_glPixelTransformParameterfEXT;
RGLSYMGLPIXELTRANSFORMPARAMETERIVEXTPROC __rglgen_glPixelTransformParameterivEXT;
RGLSYMGLPIXELTRANSFORMPARAMETERFVEXTPROC __rglgen_glPixelTransformParameterfvEXT;
RGLSYMGLGETPIXELTRANSFORMPARAMETERIVEXTPROC __rglgen_glGetPixelTransformParameterivEXT;
RGLSYMGLGETPIXELTRANSFORMPARAMETERFVEXTPROC __rglgen_glGetPixelTransformParameterfvEXT;
RGLSYMGLPOINTPARAMETERFEXTPROC __rglgen_glPointParameterfEXT;
RGLSYMGLPOINTPARAMETERFVEXTPROC __rglgen_glPointParameterfvEXT;
RGLSYMGLPOLYGONOFFSETEXTPROC __rglgen_glPolygonOffsetEXT;
RGLSYMGLPOLYGONOFFSETCLAMPEXTPROC __rglgen_glPolygonOffsetClampEXT;
RGLSYMGLPROVOKINGVERTEXEXTPROC __rglgen_glProvokingVertexEXT;
RGLSYMGLRASTERSAMPLESEXTPROC __rglgen_glRasterSamplesEXT;
RGLSYMGLSECONDARYCOLOR3BEXTPROC __rglgen_glSecondaryColor3bEXT;
RGLSYMGLSECONDARYCOLOR3BVEXTPROC __rglgen_glSecondaryColor3bvEXT;
RGLSYMGLSECONDARYCOLOR3DEXTPROC __rglgen_glSecondaryColor3dEXT;
RGLSYMGLSECONDARYCOLOR3DVEXTPROC __rglgen_glSecondaryColor3dvEXT;
RGLSYMGLSECONDARYCOLOR3FEXTPROC __rglgen_glSecondaryColor3fEXT;
RGLSYMGLSECONDARYCOLOR3FVEXTPROC __rglgen_glSecondaryColor3fvEXT;
RGLSYMGLSECONDARYCOLOR3IEXTPROC __rglgen_glSecondaryColor3iEXT;
RGLSYMGLSECONDARYCOLOR3IVEXTPROC __rglgen_glSecondaryColor3ivEXT;
RGLSYMGLSECONDARYCOLOR3SEXTPROC __rglgen_glSecondaryColor3sEXT;
RGLSYMGLSECONDARYCOLOR3SVEXTPROC __rglgen_glSecondaryColor3svEXT;
RGLSYMGLSECONDARYCOLOR3UBEXTPROC __rglgen_glSecondaryColor3ubEXT;
RGLSYMGLSECONDARYCOLOR3UBVEXTPROC __rglgen_glSecondaryColor3ubvEXT;
RGLSYMGLSECONDARYCOLOR3UIEXTPROC __rglgen_glSecondaryColor3uiEXT;
RGLSYMGLSECONDARYCOLOR3UIVEXTPROC __rglgen_glSecondaryColor3uivEXT;
RGLSYMGLSECONDARYCOLOR3USEXTPROC __rglgen_glSecondaryColor3usEXT;
RGLSYMGLSECONDARYCOLOR3USVEXTPROC __rglgen_glSecondaryColor3usvEXT;
RGLSYMGLSECONDARYCOLORPOINTEREXTPROC __rglgen_glSecondaryColorPointerEXT;
RGLSYMGLGENSEMAPHORESEXTPROC __rglgen_glGenSemaphoresEXT;
RGLSYMGLDELETESEMAPHORESEXTPROC __rglgen_glDeleteSemaphoresEXT;
RGLSYMGLISSEMAPHOREEXTPROC __rglgen_glIsSemaphoreEXT;
RGLSYMGLSEMAPHOREPARAMETERUI64VEXTPROC __rglgen_glSemaphoreParameterui64vEXT;
RGLSYMGLGETSEMAPHOREPARAMETERUI64VEXTPROC __rglgen_glGetSemaphoreParameterui64vEXT;
RGLSYMGLWAITSEMAPHOREEXTPROC __rglgen_glWaitSemaphoreEXT;
RGLSYMGLSIGNALSEMAPHOREEXTPROC __rglgen_glSignalSemaphoreEXT;
RGLSYMGLIMPORTSEMAPHOREFDEXTPROC __rglgen_glImportSemaphoreFdEXT;
RGLSYMGLIMPORTSEMAPHOREWIN32HANDLEEXTPROC __rglgen_glImportSemaphoreWin32HandleEXT;
RGLSYMGLIMPORTSEMAPHOREWIN32NAMEEXTPROC __rglgen_glImportSemaphoreWin32NameEXT;
RGLSYMGLUSESHADERPROGRAMEXTPROC __rglgen_glUseShaderProgramEXT;
RGLSYMGLACTIVEPROGRAMEXTPROC __rglgen_glActiveProgramEXT;
RGLSYMGLCREATESHADERPROGRAMEXTPROC __rglgen_glCreateShaderProgramEXT;
RGLSYMGLFRAMEBUFFERFETCHBARRIEREXTPROC __rglgen_glFramebufferFetchBarrierEXT;
RGLSYMGLBINDIMAGETEXTUREEXTPROC __rglgen_glBindImageTextureEXT;
RGLSYMGLMEMORYBARRIEREXTPROC __rglgen_glMemoryBarrierEXT;
RGLSYMGLSTENCILCLEARTAGEXTPROC __rglgen_glStencilClearTagEXT;
RGLSYMGLACTIVESTENCILFACEEXTPROC __rglgen_glActiveStencilFaceEXT;
RGLSYMGLTEXSUBIMAGE1DEXTPROC __rglgen_glTexSubImage1DEXT;
RGLSYMGLTEXSUBIMAGE2DEXTPROC __rglgen_glTexSubImage2DEXT;
RGLSYMGLTEXIMAGE3DEXTPROC __rglgen_glTexImage3DEXT;
RGLSYMGLTEXSUBIMAGE3DEXTPROC __rglgen_glTexSubImage3DEXT;
RGLSYMGLFRAMEBUFFERTEXTURELAYEREXTPROC __rglgen_glFramebufferTextureLayerEXT;
RGLSYMGLTEXBUFFEREXTPROC __rglgen_glTexBufferEXT;
RGLSYMGLTEXPARAMETERIIVEXTPROC __rglgen_glTexParameterIivEXT;
RGLSYMGLTEXPARAMETERIUIVEXTPROC __rglgen_glTexParameterIuivEXT;
RGLSYMGLGETTEXPARAMETERIIVEXTPROC __rglgen_glGetTexParameterIivEXT;
RGLSYMGLGETTEXPARAMETERIUIVEXTPROC __rglgen_glGetTexParameterIuivEXT;
RGLSYMGLCLEARCOLORIIEXTPROC __rglgen_glClearColorIiEXT;
RGLSYMGLCLEARCOLORIUIEXTPROC __rglgen_glClearColorIuiEXT;
RGLSYMGLARETEXTURESRESIDENTEXTPROC __rglgen_glAreTexturesResidentEXT;
RGLSYMGLBINDTEXTUREEXTPROC __rglgen_glBindTextureEXT;
RGLSYMGLDELETETEXTURESEXTPROC __rglgen_glDeleteTexturesEXT;
RGLSYMGLGENTEXTURESEXTPROC __rglgen_glGenTexturesEXT;
RGLSYMGLISTEXTUREEXTPROC __rglgen_glIsTextureEXT;
RGLSYMGLPRIORITIZETEXTURESEXTPROC __rglgen_glPrioritizeTexturesEXT;
RGLSYMGLTEXTURENORMALEXTPROC __rglgen_glTextureNormalEXT;
RGLSYMGLGETQUERYOBJECTI64VEXTPROC __rglgen_glGetQueryObjecti64vEXT;
RGLSYMGLGETQUERYOBJECTUI64VEXTPROC __rglgen_glGetQueryObjectui64vEXT;
RGLSYMGLBEGINTRANSFORMFEEDBACKEXTPROC __rglgen_glBeginTransformFeedbackEXT;
RGLSYMGLENDTRANSFORMFEEDBACKEXTPROC __rglgen_glEndTransformFeedbackEXT;
RGLSYMGLBINDBUFFERRANGEEXTPROC __rglgen_glBindBufferRangeEXT;
RGLSYMGLBINDBUFFEROFFSETEXTPROC __rglgen_glBindBufferOffsetEXT;
RGLSYMGLBINDBUFFERBASEEXTPROC __rglgen_glBindBufferBaseEXT;
RGLSYMGLTRANSFORMFEEDBACKVARYINGSEXTPROC __rglgen_glTransformFeedbackVaryingsEXT;
RGLSYMGLGETTRANSFORMFEEDBACKVARYINGEXTPROC __rglgen_glGetTransformFeedbackVaryingEXT;
RGLSYMGLARRAYELEMENTEXTPROC __rglgen_glArrayElementEXT;
RGLSYMGLCOLORPOINTEREXTPROC __rglgen_glColorPointerEXT;
RGLSYMGLDRAWARRAYSEXTPROC __rglgen_glDrawArraysEXT;
RGLSYMGLEDGEFLAGPOINTEREXTPROC __rglgen_glEdgeFlagPointerEXT;
RGLSYMGLGETPOINTERVEXTPROC __rglgen_glGetPointervEXT;
RGLSYMGLINDEXPOINTEREXTPROC __rglgen_glIndexPointerEXT;
RGLSYMGLNORMALPOINTEREXTPROC __rglgen_glNormalPointerEXT;
RGLSYMGLTEXCOORDPOINTEREXTPROC __rglgen_glTexCoordPointerEXT;
RGLSYMGLVERTEXPOINTEREXTPROC __rglgen_glVertexPointerEXT;
RGLSYMGLVERTEXATTRIBL1DEXTPROC __rglgen_glVertexAttribL1dEXT;
RGLSYMGLVERTEXATTRIBL2DEXTPROC __rglgen_glVertexAttribL2dEXT;
RGLSYMGLVERTEXATTRIBL3DEXTPROC __rglgen_glVertexAttribL3dEXT;
RGLSYMGLVERTEXATTRIBL4DEXTPROC __rglgen_glVertexAttribL4dEXT;
RGLSYMGLVERTEXATTRIBL1DVEXTPROC __rglgen_glVertexAttribL1dvEXT;
RGLSYMGLVERTEXATTRIBL2DVEXTPROC __rglgen_glVertexAttribL2dvEXT;
RGLSYMGLVERTEXATTRIBL3DVEXTPROC __rglgen_glVertexAttribL3dvEXT;
RGLSYMGLVERTEXATTRIBL4DVEXTPROC __rglgen_glVertexAttribL4dvEXT;
RGLSYMGLVERTEXATTRIBLPOINTEREXTPROC __rglgen_glVertexAttribLPointerEXT;
RGLSYMGLGETVERTEXATTRIBLDVEXTPROC __rglgen_glGetVertexAttribLdvEXT;
RGLSYMGLBEGINVERTEXSHADEREXTPROC __rglgen_glBeginVertexShaderEXT;
RGLSYMGLENDVERTEXSHADEREXTPROC __rglgen_glEndVertexShaderEXT;
RGLSYMGLBINDVERTEXSHADEREXTPROC __rglgen_glBindVertexShaderEXT;
RGLSYMGLGENVERTEXSHADERSEXTPROC __rglgen_glGenVertexShadersEXT;
RGLSYMGLDELETEVERTEXSHADEREXTPROC __rglgen_glDeleteVertexShaderEXT;
RGLSYMGLSHADEROP1EXTPROC __rglgen_glShaderOp1EXT;
RGLSYMGLSHADEROP2EXTPROC __rglgen_glShaderOp2EXT;
RGLSYMGLSHADEROP3EXTPROC __rglgen_glShaderOp3EXT;
RGLSYMGLSWIZZLEEXTPROC __rglgen_glSwizzleEXT;
RGLSYMGLWRITEMASKEXTPROC __rglgen_glWriteMaskEXT;
RGLSYMGLINSERTCOMPONENTEXTPROC __rglgen_glInsertComponentEXT;
RGLSYMGLEXTRACTCOMPONENTEXTPROC __rglgen_glExtractComponentEXT;
RGLSYMGLGENSYMBOLSEXTPROC __rglgen_glGenSymbolsEXT;
RGLSYMGLSETINVARIANTEXTPROC __rglgen_glSetInvariantEXT;
RGLSYMGLSETLOCALCONSTANTEXTPROC __rglgen_glSetLocalConstantEXT;
RGLSYMGLVARIANTBVEXTPROC __rglgen_glVariantbvEXT;
RGLSYMGLVARIANTSVEXTPROC __rglgen_glVariantsvEXT;
RGLSYMGLVARIANTIVEXTPROC __rglgen_glVariantivEXT;
RGLSYMGLVARIANTFVEXTPROC __rglgen_glVariantfvEXT;
RGLSYMGLVARIANTDVEXTPROC __rglgen_glVariantdvEXT;
RGLSYMGLVARIANTUBVEXTPROC __rglgen_glVariantubvEXT;
RGLSYMGLVARIANTUSVEXTPROC __rglgen_glVariantusvEXT;
RGLSYMGLVARIANTUIVEXTPROC __rglgen_glVariantuivEXT;
RGLSYMGLVARIANTPOINTEREXTPROC __rglgen_glVariantPointerEXT;
RGLSYMGLENABLEVARIANTCLIENTSTATEEXTPROC __rglgen_glEnableVariantClientStateEXT;
RGLSYMGLDISABLEVARIANTCLIENTSTATEEXTPROC __rglgen_glDisableVariantClientStateEXT;
RGLSYMGLBINDLIGHTPARAMETEREXTPROC __rglgen_glBindLightParameterEXT;
RGLSYMGLBINDMATERIALPARAMETEREXTPROC __rglgen_glBindMaterialParameterEXT;
RGLSYMGLBINDTEXGENPARAMETEREXTPROC __rglgen_glBindTexGenParameterEXT;
RGLSYMGLBINDTEXTUREUNITPARAMETEREXTPROC __rglgen_glBindTextureUnitParameterEXT;
RGLSYMGLBINDPARAMETEREXTPROC __rglgen_glBindParameterEXT;
RGLSYMGLISVARIANTENABLEDEXTPROC __rglgen_glIsVariantEnabledEXT;
RGLSYMGLGETVARIANTBOOLEANVEXTPROC __rglgen_glGetVariantBooleanvEXT;
RGLSYMGLGETVARIANTINTEGERVEXTPROC __rglgen_glGetVariantIntegervEXT;
RGLSYMGLGETVARIANTFLOATVEXTPROC __rglgen_glGetVariantFloatvEXT;
RGLSYMGLGETVARIANTPOINTERVEXTPROC __rglgen_glGetVariantPointervEXT;
RGLSYMGLGETINVARIANTBOOLEANVEXTPROC __rglgen_glGetInvariantBooleanvEXT;
RGLSYMGLGETINVARIANTINTEGERVEXTPROC __rglgen_glGetInvariantIntegervEXT;
RGLSYMGLGETINVARIANTFLOATVEXTPROC __rglgen_glGetInvariantFloatvEXT;
RGLSYMGLGETLOCALCONSTANTBOOLEANVEXTPROC __rglgen_glGetLocalConstantBooleanvEXT;
RGLSYMGLGETLOCALCONSTANTINTEGERVEXTPROC __rglgen_glGetLocalConstantIntegervEXT;
RGLSYMGLGETLOCALCONSTANTFLOATVEXTPROC __rglgen_glGetLocalConstantFloatvEXT;
RGLSYMGLVERTEXWEIGHTFEXTPROC __rglgen_glVertexWeightfEXT;
RGLSYMGLVERTEXWEIGHTFVEXTPROC __rglgen_glVertexWeightfvEXT;
RGLSYMGLVERTEXWEIGHTPOINTEREXTPROC __rglgen_glVertexWeightPointerEXT;
RGLSYMGLACQUIREKEYEDMUTEXWIN32EXTPROC __rglgen_glAcquireKeyedMutexWin32EXT;
RGLSYMGLRELEASEKEYEDMUTEXWIN32EXTPROC __rglgen_glReleaseKeyedMutexWin32EXT;
RGLSYMGLESEXTPROC __rglgen_glesEXT;
RGLSYMGLIMPORTSYNCEXTPROC __rglgen_glImportSyncEXT;
RGLSYMGLIMAGETRANSFORMPARAMETERIHPPROC __rglgen_glImageTransformParameteriHP;
RGLSYMGLIMAGETRANSFORMPARAMETERFHPPROC __rglgen_glImageTransformParameterfHP;
RGLSYMGLIMAGETRANSFORMPARAMETERIVHPPROC __rglgen_glImageTransformParameterivHP;
RGLSYMGLIMAGETRANSFORMPARAMETERFVHPPROC __rglgen_glImageTransformParameterfvHP;
RGLSYMGLGETIMAGETRANSFORMPARAMETERIVHPPROC __rglgen_glGetImageTransformParameterivHP;
RGLSYMGLGETIMAGETRANSFORMPARAMETERFVHPPROC __rglgen_glGetImageTransformParameterfvHP;
RGLSYMGLFRAMEBUFFERTEXTUREEXTPROC __rglgen_glFramebufferTextureEXT;
RGLSYMGLFRAMEBUFFERTEXTUREFACEEXTPROC __rglgen_glFramebufferTextureFaceEXT;
RGLSYMGLVERTEXATTRIBI1IEXTPROC __rglgen_glVertexAttribI1iEXT;
RGLSYMGLVERTEXATTRIBI2IEXTPROC __rglgen_glVertexAttribI2iEXT;
RGLSYMGLVERTEXATTRIBI3IEXTPROC __rglgen_glVertexAttribI3iEXT;
RGLSYMGLVERTEXATTRIBI4IEXTPROC __rglgen_glVertexAttribI4iEXT;
RGLSYMGLVERTEXATTRIBI1UIEXTPROC __rglgen_glVertexAttribI1uiEXT;
RGLSYMGLVERTEXATTRIBI2UIEXTPROC __rglgen_glVertexAttribI2uiEXT;
RGLSYMGLVERTEXATTRIBI3UIEXTPROC __rglgen_glVertexAttribI3uiEXT;
RGLSYMGLVERTEXATTRIBI4UIEXTPROC __rglgen_glVertexAttribI4uiEXT;
RGLSYMGLVERTEXATTRIBI1IVEXTPROC __rglgen_glVertexAttribI1ivEXT;
RGLSYMGLVERTEXATTRIBI2IVEXTPROC __rglgen_glVertexAttribI2ivEXT;
RGLSYMGLVERTEXATTRIBI3IVEXTPROC __rglgen_glVertexAttribI3ivEXT;
RGLSYMGLVERTEXATTRIBI4IVEXTPROC __rglgen_glVertexAttribI4ivEXT;
RGLSYMGLVERTEXATTRIBI1UIVEXTPROC __rglgen_glVertexAttribI1uivEXT;
RGLSYMGLVERTEXATTRIBI2UIVEXTPROC __rglgen_glVertexAttribI2uivEXT;
RGLSYMGLVERTEXATTRIBI3UIVEXTPROC __rglgen_glVertexAttribI3uivEXT;
RGLSYMGLVERTEXATTRIBI4UIVEXTPROC __rglgen_glVertexAttribI4uivEXT;
RGLSYMGLVERTEXATTRIBI4BVEXTPROC __rglgen_glVertexAttribI4bvEXT;
RGLSYMGLVERTEXATTRIBI4SVEXTPROC __rglgen_glVertexAttribI4svEXT;
RGLSYMGLVERTEXATTRIBI4UBVEXTPROC __rglgen_glVertexAttribI4ubvEXT;
RGLSYMGLVERTEXATTRIBI4USVEXTPROC __rglgen_glVertexAttribI4usvEXT;
RGLSYMGLVERTEXATTRIBIPOINTEREXTPROC __rglgen_glVertexAttribIPointerEXT;
RGLSYMGLGETVERTEXATTRIBIIVEXTPROC __rglgen_glGetVertexAttribIivEXT;
RGLSYMGLGETVERTEXATTRIBIUIVEXTPROC __rglgen_glGetVertexAttribIuivEXT;
RGLSYMGLFRAMEBUFFERTEXTUREMULTIVIEWOVRPROC __rglgen_glFramebufferTextureMultiviewOVR;

