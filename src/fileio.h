// 
// fileio.h
// 
// File input/output
// 
// Authors:
// - TAKEDA Toshiya
// - MARCONATO Maxime (aka MaaaX, aka EPOCH84 / maxime@maaax.com)
// 

#ifndef LIBRETRO_EMUCV_FILEIO_H
#define LIBRETRO_EMUCV_FILEIO_H

#include "common.h"

#define FILEIO_MODE_READ_BINARY				 1
#define FILEIO_MODE_WRITE_BINARY			 2
#define FILEIO_MODE_READ_WRITE_BINARY		 3
#define FILEIO_MODE_READ_WRITE_NEW_BINARY	 4
#define FILEIO_MODE_READ_ASCII				 5
#define FILEIO_MODE_WRITE_ASCII				 6
#define FILEIO_MODE_READ_WRITE_ASCII		 7
#define FILEIO_MODE_READ_WRITE_NEW_ASCII	 8
#define FILEIO_MODE_WRITE_APPEND_ASCII		 9
#define FILEIO_MODE_READ_WRITE_APPEND_ASCII	10

#define FILEIO_SEEK_SET		0
#define FILEIO_SEEK_CUR		1
#define FILEIO_SEEK_END		2

class c_fileio
{
private:
	FILE* fp;
	char path[_MAX_PATH];
	uint8_t open_mode;

public:
	c_fileio();
	~c_fileio();

	static bool f_is_existing(const _TCHAR *file_path);
	static bool f_is_protected(const _TCHAR *file_path);
	static bool f_remove(const _TCHAR *file_path);
	static bool f_rename(const _TCHAR *existing_file_path, const _TCHAR *new_file_path);

	bool f_open(const _TCHAR *file_path, int mode);
	void f_close();
	bool f_is_opened(){ return (fp != NULL); }
	const char *f_path(){ return path; }
	long f_size();

	int f_get_c();
	int f_put_c(int c);
	char *f_get_s(char *str, int n);
	_TCHAR *f_get_ts(_TCHAR *str, int n);
	int f_printf(const char* format, ...);
	int f_tprintf(const _TCHAR* format, ...);

	size_t f_read(void* buffer, size_t size, size_t count);
	size_t f_write(const void* buffer, size_t size, size_t count);
	int f_seek(long offset, int origin);
	long f_tell();
/*
	bool f_get_bool();
	void f_put_bool(bool val);

	uint8_t f_get_uint8();
	void f_put_uint8(uint8_t val);
	uint16_t f_get_uint16();
	void f_put_uint16(uint16_t val);
	uint32_t f_get_uint32();
	void f_put_uint32(uint32_t val);
	uint64_t f_get_uint64();
	void f_put_uint64(uint64_t val);
	int8_t f_get_int8();
	void f_put_int8(int8_t val);
	int16_t f_get_int16();
	void f_put_int16(int16_t val);
	int32_t f_get_int32();
	void f_put_int32(int32_t val);
	int64_t f_get_int64();
	void f_put_int64(int64_t val);
	float f_get_float();
	void f_put_float(float val);
	double f_get_double();
	void f_put_double(double val);

	uint16_t f_get_uint16_LE();
	void f_put_uint16_LE(uint16_t val);
	uint32_t f_get_uint32_LE();
	void f_put_uint32_LE(uint32_t val);
	uint64_t f_get_uint64_LE();
	void f_put_uint64_LE(uint64_t val);
	int16_t f_get_int16_LE();
	void f_put_int16_LE(int16_t val);
	int32_t f_get_int32_LE();
	void f_put_int32_LE(int32_t val);
	int64_t f_get_int64_LE();
	void f_put_int64_LE(int64_t val);
	float f_get_float_LE();
	void f_put_float_LE(float val);
	double f_get_double_LE();
	void f_put_double_LE(double val);
	char f_get_char_LE();
	void f_put_char_LE(char val);

	uint16_t f_get_uint16_BE();
	void f_put_uint16_BE(uint16_t val);
	uint32_t f_get_uint32_BE();
	void f_put_uint32_BE(uint32_t val);
	uint64_t f_get_uint64_BE();
	void f_put_uint64_BE(uint64_t val);
	int16_t f_get_int16_BE();
	void f_put_int16_BE(int16_t val);
	int32_t f_get_int32_BE();
	void f_put_int32_BE(int32_t val);
	int64_t f_get_int64_BE();
	void f_put_int64_BE(int64_t val);
	float f_get_float_BE();
	void f_put_float_BE(float val);
	double f_get_double_BE();
	void f_put_double_BE(double val);
	char f_get_char_BE();
	void f_put_char_BE(char val);

	bool fStateCheckUint32(uint32_t val);
	bool fStateCheckInt32(int32_t val);
	bool fStateCheckBuffer(const char *buffer, size_t size, size_t count);

	void fStateValue(bool &val);
	void fStateValue(uint8_t &val);
	void fStateValue(uint16_t &val);
	void fStateValue(uint32_t &val);
	void fStateValue(uint64_t &val);
	void fStateValue(int8_t &val);
	void fStateValue(int16_t &val);
	void fStateValue(int32_t &val);
	void fStateValue(int64_t &val);
//	void fStateValue(pair16_t &val);
//	void fStateValue(pair32_t &val);
//	void fStateValue(pair64_t &val);
	void fStateValue(float &val);
	void fStateValue(double &val);
	void fStateValue(char &val);

	void fStateArray(bool *buffer, size_t size, size_t count);
	void fStateArray(uint8_t *buffer, size_t size, size_t count);
	void fStateArray(uint16_t *buffer, size_t size, size_t count);
	void fStateArray(uint32_t *buffer, size_t size, size_t count);
	void fStateArray(uint64_t *buffer, size_t size, size_t count);
	void fStateArray(int8_t *buffer, size_t size, size_t count);
	void fStateArray(int16_t *buffer, size_t size, size_t count);
	void fStateArray(int32_t *buffer, size_t size, size_t count);
	void fStateArray(int64_t *buffer, size_t size, size_t count);
//	void fStateArray(pair16_t *buffer, size_t size, size_t count);
//	void fStateArray(pair32_t *buffer, size_t size, size_t count);
//	void fStateArray(pair64_t *buffer, size_t size, size_t count);
	void fStateArray(float *buffer, size_t size, size_t count);
	void fStateArray(double *buffer, size_t size, size_t count);
	void fStateArray(char *buffer, size_t size, size_t count);
*/
};

#endif	// #ifndef LIBRETRO_EMUCV_FILEIO_H
