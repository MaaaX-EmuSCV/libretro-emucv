// 
// core.h
// 
// Libretro core
// 
// Authors:
// - MARCONATO Maxime (aka MaaaX, aka EPOCH84 / maxime@maaax.com)
// 

#ifndef LIBRETRO_EMUCV_CORE_H
#define LIBRETRO_EMUCV_CORE_H

#include <libretro.h>
#include "common.h"
#include "emu.h"


#define CORE_NAME				"EmuCV"
#define CORE_DESCRIPTION		"EmuCV is an open source emulator for the EPOCH Casette Vision under General Public Licence (GPL)."
#ifndef CORE_VERSION_MAJOR
#define CORE_VERSION_MAJOR		"0"
#endif
#ifndef CORE_VERSION_MINOR
#define CORE_VERSION_MINOR		"00"
#endif
#ifndef CORE_VERSION_DATETIME
#define CORE_VERSION_DATETIME	"00000000000000"
#endif
#define CORE_VERSION			CORE_VERSION_MAJOR "." CORE_VERSION_MINOR "." CORE_VERSION_DATETIME
#define CORE_AUTHOR				"Maxime MARCONATO (aka MaaaX, aka EPOCH84 / maxime@maaax.com)"
#define CORE_EXTENSIONS			"bin"//|rom"
#define CORE_SUPPORT_NO_GAME	false
#define CORE_REGION				RETRO_REGION_NTSC
#define CORE_PERFORMANCE_LEVEL	4

// Input descriptions
#define CORE_INPUTDESC_BUTTON_SELECT	"BUTTON SELECT"
#define CORE_INPUTDESC_BUTTON_START		"BUTTON START"
#define CORE_INPUTDESC_BUTTON_LEFT		"BUTTON LEFT"
#define CORE_INPUTDESC_BUTTON_RIGHT		"BUTTON RIGHT"	// ┌───────┬──────┬─────────────┬──────┐
#define CORE_INPUTDESC_BUTTON_UP		"BUTTON UP"		// │ POS.  │ SNES │ PLAYSTATION │ XBOX │
#define CORE_INPUTDESC_BUTTON_DOWN		"BUTTON DOWN"	// ├───────┼──────┼─────────────┼──────┤
#define CORE_INPUTDESC_BUTTON_1			"BUTTON 1"		// │ RIGHT │  A   │  CIRCLE     │  B   │
#define CORE_INPUTDESC_BUTTON_2			"BUTTON 2"		// │ DOWN  │  B   │  CROSS      │  A   │
#define CORE_INPUTDESC_BUTTON_3			"BUTTON 3"		// │ TOP   │  X   │  TRIANGLE   │  Y   │
#define CORE_INPUTDESC_BUTTON_4			"BUTTON 4"		// │ LEFT  │  Y   │  SQUARE     │  X   │
#define CORE_INPUTDESC_BUTTON_L1		"BUTTON L1"		// └───────┴──────┴─────────────┴──────┘
#define CORE_INPUTDESC_BUTTON_R1		"BUTTON R1"
#define CORE_INPUTDESC_BUTTON_L2		"BUTTON L2"
#define CORE_INPUTDESC_BUTTON_R2		"BUTTON R2"
#define CORE_INPUTDESC_BUTTON_L3		"BUTTON L3"
#define CORE_INPUTDESC_BUTTON_R3		"BUTTON R3"
#define CORE_INPUTDESC_ANALOG_LEFT_X	"ANALOG LEFT X"
#define CORE_INPUTDESC_ANALOG_LEFT_Y	"ANALOG LEFT Y"
#define CORE_INPUTDESC_ANALOG_RIGHT_X	"ANALOG RIGHT X"
#define CORE_INPUTDESC_ANALOG_RIGHT_Y	"ANALOG RIGHT Y"

#define CORE_AXIS_NEUTRAL_MAX			8192
#define CORE_AXIS_NEUTRAL_MIN			-CORE_AXIS_NEUTRAL_MAX


//
// Libretro: Default log handler, to use if any log callback handler is defined by the frontend
//
static void f_core_retro_log_fallback(enum retro_log_level level, const char *fmt, ...)
{
	(void)level;

	va_list va;

	va_start(va, fmt);
	vfprintf(stderr, fmt, va);
	va_end(va);
}

static retro_log_printf_t f_core_retro_log_printf = f_core_retro_log_fallback;

class c_core
{
public:
	c_core();	// Constructor
	~c_core();	// Destructor

	void f_retro_set_environment(retro_environment_t f_retro_environment_cb);						// Libretro: set the environment
	void f_retro_set_video_refresh(retro_video_refresh_t f_retro_video_refresh_cb);					// Libretro: set the video refresh callback
	void f_retro_set_audio_sample(retro_audio_sample_t f_retro_audio_sample_cb);					// Libretro: set the audio sample callback
	void f_retro_set_audio_sample_batch(retro_audio_sample_batch_t f_retro_audio_sample_batch_cb);	// Libretro: set the audio batch callback
	void f_retro_set_input_poll(retro_input_poll_t f_retro_input_poll_cb);							// Libretro: set the input poll callback
	void f_retro_set_input_state(retro_input_state_t f_retro_input_state_cb);						// Libretro: set the input state callback
	unsigned f_retro_get_api_version();																// Libretro: return the version used by the core for compatibility check with the frontend
	unsigned f_retro_get_video_region();															// Libretro: return the video standard used
	void f_retro_get_system_info(struct retro_system_info *system_info);							// Libretro: get the system infos
	void f_retro_get_av_info(struct retro_system_av_info *system_av_info);							// Libretro: get the audio/video infos
	void f_retro_init(retro_audio_callback_t f_retro_audio_cb, retro_audio_set_state_callback_t f_retro_audio_set_state_cb, retro_frame_time_callback_t f_Sretro_frame_time_cb);
																				// Libretro: initialize the core
	void f_retro_deinit();														// Libretro: deinitialize the core
	void f_retro_set_controller_port_device(unsigned port, unsigned device);	// Libretro: set controller port device
	void f_retro_audio_set_state_cb(bool enable);								// Libretro: audio set state enable/disable callback
	void f_retro_frame_time_cb(retro_usec_t usec);								// Libretro: retro frame time callback
	bool f_retro_load_game(const struct retro_game_info *game_info);			// Libretro: load game
	void f_retro_unload_game();													// Libretro: unload game
	void f_retro_run();															// Libretro: run for only one frame
	void f_retro_reset();														// Libretro: reset the Libretro core
	void f_retro_load_settings();												// Libretro: load core settings
	void f_retro_save_settings();												// Libretro: save core settings
	size_t f_retro_serialize_size();											// Libretro: return save state size
	bool f_retro_serialize(void *data, size_t size);							// Libretro: save current state
	bool f_retro_unserialize(void *data, size_t size);							// Libretro: load a saved state
	void *f_retro_get_memory_data();											// Libretro: return memory data pointer
	size_t f_retro_get_memory_size(unsigned id);								// Libretro: return memory size
	
	retro_environment_t 		f_retro_environment;
	retro_video_refresh_t		f_retro_video_refresh;
	retro_audio_sample_t		f_retro_audio_sample;
	retro_audio_sample_batch_t	f_retro_audio_sample_batch;
	retro_input_poll_t			f_retro_input_poll;
	retro_input_state_t			f_retro_input_state;

private:
	bool retro_core_initialized;		// Is the libretro core initialized ?
	bool retro_audio_enable;			// Is the libretro audio enabled ?
	uint64_t retro_frame_counter;		// Total number of frames drawn from startup
	retro_usec_t retro_frame_time;		// Frame time of last frame
	bool retro_use_audio_cb;			// Is the audio callback used by Libretro Frontend ?

	bool retro_input_support_bitmask;
	bool retro_game_loaded;

	char retro_base_directory[_MAX_PATH];
	char retro_save_directory[_MAX_PATH];
	char retro_game_path[_MAX_PATH];

	uint32_t *video_buffer;
	uint32_t video_width;
	uint32_t video_height;
	uint32_t video_stride;

	bool power_on;

	c_emu* o_emu;
};

#endif // #ifndef LIBRETRO_EMUCV_CORE_H
