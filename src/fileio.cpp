// 
// c_fileio.h
// 
// File input/output
// 
// Authors:
// - TAKEDA Toshiya
// - MARCONATO Maxime (aka MaaaX, aka EPOCH84 / maxime@maaax.com)
// 

/*
#if defined(_USE_QT) || defined(_USE_SDL)
	#include <stdarg.h>
	#include <fcntl.h>
	#include <stdio.h>
	#include <iostream>
	#include <fstream>
	#include <cstdio>
	#if defined(_USE_QT)
		#include <sys/types.h>
		#include <sys/stat.h>
		#if !defined(Q_OS_WIN)
			#include <unistd.h>
		#endif
	#endif
#elif defined(_WIN32)
	#include <windows.h>
#endif
*/
#include "fileio.h"


c_fileio::c_fileio()
{
	fp = NULL;
	memset(path, 0, sizeof(path));
}

c_fileio::~c_fileio()
{
	f_close();
}

bool c_fileio::f_is_existing(const _TCHAR *file_path)
{
#ifdef _WIN32
	DWORD attr = GetFileAttributes(file_path);
	if(attr == -1)
		return false;
	return ((attr & FILE_ATTRIBUTE_DIRECTORY) == 0);
#else	// #ifdef _WIN32
	return (_taccess(file_path, 0) == 0);
#endif	// #ifdef _WIN32
	return false;
}

bool c_fileio::f_is_protected(const _TCHAR *file_path)
{
#ifdef _WIN32
	return ((GetFileAttributes(file_path) & FILE_ATTRIBUTE_READONLY) != 0);
#else	// #ifdef _WIN32
	return (_taccess(file_path, 2) != 0);
#endif	// #ifdef _WIN32
}

bool c_fileio::f_remove(const _TCHAR *file_path)
{
#ifdef _WIN32
	return (DeleteFile(file_path) != 0);
#else	// #ifdef _WIN32
	return (_tremove(file_path) == 0);
#endif	// #ifdef _WIN32
}

bool c_fileio::f_rename(const _TCHAR *existing_file_path, const _TCHAR *new_file_path)
{
#ifdef _WIN32
	return (MoveFile(existing_file_path, new_file_path) != 0);
#else	// #ifdef _WIN32
	return (_trename(existing_file_path, new_file_path) == 0);
#endif	// #ifdef _WIN32
}

bool c_fileio::f_open(const _TCHAR *file_path, int mode)
{
	f_close();
	memset(path, 0, sizeof(path));
	_tcscpy_s(path, _MAX_PATH, file_path);
	open_mode = mode;
	switch(mode)
	{
		case FILEIO_MODE_READ_BINARY:
			return ((fp = _tfopen(file_path, _T("rb"))) != NULL);
		case FILEIO_MODE_WRITE_BINARY:
			return ((fp = _tfopen(file_path, _T("wb"))) != NULL);
		case FILEIO_MODE_READ_WRITE_BINARY:
			return ((fp = _tfopen(file_path, _T("r+b"))) != NULL);
		case FILEIO_MODE_READ_WRITE_NEW_BINARY:
			return ((fp = _tfopen(file_path, _T("w+b"))) != NULL);
		case FILEIO_MODE_READ_ASCII:
			return ((fp = _tfopen(file_path, _T("r"))) != NULL);
		case FILEIO_MODE_WRITE_ASCII:
			return ((fp = _tfopen(file_path, _T("w"))) != NULL);
		case FILEIO_MODE_WRITE_APPEND_ASCII:
			return ((fp = _tfopen(file_path, _T("a"))) != NULL);
		case FILEIO_MODE_READ_WRITE_ASCII:
			return ((fp = _tfopen(file_path, _T("r+"))) != NULL);
		case FILEIO_MODE_READ_WRITE_NEW_ASCII:
			return ((fp = _tfopen(file_path, _T("w+"))) != NULL);
		case FILEIO_MODE_READ_WRITE_APPEND_ASCII:
			return ((fp = _tfopen(file_path, _T("a+"))) != NULL);
	}
	return false;
}

void c_fileio::f_close()
{
	if(fp != NULL)
	{
		fclose(fp);
		fp = NULL;
	}
	memset(path, 0, sizeof(path));
}

long c_fileio::f_size()
{
	long pos = f_tell();
	f_seek(0, FILEIO_SEEK_END);
	long len = f_tell();
	f_seek(pos, FILEIO_SEEK_SET);
	return len;
}

int c_fileio::f_get_c()
{
	if(fp != NULL)
		return fgetc(fp);
	return 0;
}

int c_fileio::f_put_c(int c)
{
	if(fp != NULL)
		return fputc(c, fp);
	return 0;
}

char *c_fileio::f_get_s(char *str, int n)
{
	if(fp != NULL)
		return fgets(str, n, fp);
	return 0;
}

_TCHAR *c_fileio::f_get_ts(_TCHAR *str, int n)
{
	if(fp != NULL)
		return _fgetts(str, n, fp);
	return 0;
}

int c_fileio::f_printf(const char* format, ...)
{
	va_list ap;
	char buffer[1024];

	va_start(ap, format);
	_vsprintf_s(buffer, 1024, format, ap);
	va_end(ap);

	if(fp != NULL)
		return _fprintf_s(fp, "%s", buffer);
	return 0;
}

int c_fileio::f_tprintf(const _TCHAR* format, ...)
{
 	va_list ap;
 	_TCHAR buffer[1024];

 	va_start(ap, format);
 	_vstprintf_s(buffer, 1024, format, ap);
 	va_end(ap);

 	if(fp != NULL)
		return _ftprintf_s((const char *)fp, _T("%s"), buffer);
	return 0;
}

size_t c_fileio::f_read(void* buffer, size_t size, size_t count)
{
	if(fp != NULL)
		return fread(buffer, size, count, fp);
	return 0;
}

size_t c_fileio::f_write(const void* buffer, size_t size, size_t count)
{
	if(fp != NULL)
		return fwrite(buffer, size, count, fp);
	return 0;
}

int c_fileio::f_seek(long offset, int origin)
{
	if(fp != NULL)
	{
		switch(origin)
		{
			case FILEIO_SEEK_CUR:
				return fseek(fp, offset, SEEK_CUR);
			case FILEIO_SEEK_END:
				return fseek(fp, offset, SEEK_END);
			case FILEIO_SEEK_SET:
				return fseek(fp, offset, SEEK_SET);
		}
	}
	return -1;
}

long c_fileio::f_tell()
{
	if(fp != NULL)
		return ftell(fp);
	return 0;
}


/*
#define GET_VALUE(type) \
	uint8_t buffer[sizeof(type)] = {0}; \
	Fread(buffer, sizeof(buffer), 1); \
	return *(type *)buffer

#define PUT_VALUE(type, v) \
	Fwrite(&v, sizeof(type), 1)

bool c_fileio::f_get_bool()
{
	GET_VALUE(bool);
}

void c_fileio::f_put_bool(bool val)
{
	PUT_VALUE(bool, val);
}

uint8_t c_fileio::FgetUint8()
{
	GET_VALUE(uint8_t);
}

void c_fileio::FputUint8(uint8_t val)
{
	PUT_VALUE(uint8_t, val);
}

uint16_t c_fileio::FgetUint16()
{
	GET_VALUE(uint16_t);
}

void c_fileio::FputUint16(uint16_t val)
{
	PUT_VALUE(uint16_t, val);
}

uint32_t c_fileio::FgetUint32()
{
	GET_VALUE(uint32_t);
}

void c_fileio::FputUint32(uint32_t val)
{
	PUT_VALUE(uint32_t, val);
}

uint64_t c_fileio::FgetUint64()
{
	GET_VALUE(uint64_t);
}

void c_fileio::FputUint64(uint64_t val)
{
	PUT_VALUE(uint64_t, val);
}

int8_t c_fileio::FgetInt8()
{
	GET_VALUE(int8_t);
}

void c_fileio::FputInt8(int8_t val)
{
	PUT_VALUE(int8_t, val);
}

int16_t c_fileio::FgetInt16()
{
	GET_VALUE(int16_t);
}

void c_fileio::FputInt16(int16_t val)
{
	PUT_VALUE(int16_t, val);
}

int32_t c_fileio::FgetInt32()
{
	GET_VALUE(int32_t);
}

void c_fileio::FputInt32(int32_t val)
{
	PUT_VALUE(int32_t, val);
}

int64_t c_fileio::FgetInt64()
{
	GET_VALUE(int64_t);
}

void c_fileio::FputInt64(int64_t val)
{
	PUT_VALUE(int64_t, val);
}

float c_fileio::FgetFloat()
{
	GET_VALUE(float);
}

void c_fileio::FputFloat(float val)
{
	PUT_VALUE(float, val);
}

double c_fileio::FgetDouble()
{
	GET_VALUE(double);
}

void c_fileio::FputDouble(double val)
{
	PUT_VALUE(double, val);
}

uint16_t c_fileio::FgetUint16_LE()
{
	pair16_t tmp;
	tmp.b.l = FgetUint8();
	tmp.b.h = FgetUint8();
	return tmp.w;
}

void c_fileio::FputUint16_LE(uint16_t val)
{
	pair16_t tmp;
	tmp.w = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
}

uint32_t c_fileio::FgetUint32_LE()
{
	pair32_t tmp;
	tmp.b.l  = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	return tmp.d;
}

void c_fileio::FputUint32_LE(uint32_t val)
{
	pair32_t tmp;
	tmp.d = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h3);
}

uint64_t c_fileio::FgetUint64_LE()
{
	pair64_t tmp;
	tmp.b.l  = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	tmp.b.h4 = FgetUint8();
	tmp.b.h5 = FgetUint8();
	tmp.b.h6 = FgetUint8();
	tmp.b.h7 = FgetUint8();
	return tmp.q;
}

void c_fileio::FputUint64_LE(uint64_t val)
{
	pair64_t tmp;
	tmp.q = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h4);
	FputUint8(tmp.b.h5);
	FputUint8(tmp.b.h6);
	FputUint8(tmp.b.h7);
}

int16_t c_fileio::FgetInt16_LE()
{
	pair16_t tmp;
	tmp.b.l = FgetUint8();
	tmp.b.h = FgetUint8();
	return tmp.sw;
}

void c_fileio::FputInt16_LE(int16_t val)
{
	pair16_t tmp;
	tmp.sw = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
}

int32_t c_fileio::FgetInt32_LE()
{
	pair32_t tmp;
	tmp.b.l  = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	return tmp.sd;
}

void c_fileio::FputInt32_LE(int32_t val)
{
	pair32_t tmp;
	tmp.sd = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h3);
}

int64_t c_fileio::FgetInt64_LE()
{
	pair64_t tmp;
	tmp.b.l  = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	tmp.b.h4 = FgetUint8();
	tmp.b.h5 = FgetUint8();
	tmp.b.h6 = FgetUint8();
	tmp.b.h7 = FgetUint8();
	return tmp.sq;
}

void c_fileio::FputInt64_LE(int64_t val)
{
	pair64_t tmp;
	tmp.sq = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h4);
	FputUint8(tmp.b.h5);
	FputUint8(tmp.b.h6);
	FputUint8(tmp.b.h7);
}

float c_fileio::FgetFloat_LE()
{
	pair32_t tmp;
	tmp.b.l  = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	return tmp.f;
}

void c_fileio::FputFloat_LE(float val)
{
	pair32_t tmp;
	tmp.f = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h3);
}

double c_fileio::FgetDouble_LE()
{
	pair64_t tmp;
	tmp.b.l  = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	tmp.b.h4 = FgetUint8();
	tmp.b.h5 = FgetUint8();
	tmp.b.h6 = FgetUint8();
	tmp.b.h7 = FgetUint8();
	return tmp.df;
}

void c_fileio::FputDouble_LE(double val)
{
	pair64_t tmp;
	tmp.df = val;
	FputUint8(tmp.b.l);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h4);
	FputUint8(tmp.b.h5);
	FputUint8(tmp.b.h6);
	FputUint8(tmp.b.h7);
}

_TCHAR c_fileio::FgetTCHAR_LE()
{
	switch(sizeof(_TCHAR)) {
	case 1: return (_TCHAR)FgetUint8();
	case 2: return (_TCHAR)FgetUint16_LE();
	case 4: return (_TCHAR)FgetUint32_LE();
	case 8: return (_TCHAR)FgetUint64_LE();
	}
	return (_TCHAR)FgetUint8();
}

void c_fileio::FputTCHAR_LE(_TCHAR val)
{
	switch(sizeof(_TCHAR)) {
	case 1: FputUint8((uint8_t )val); return;
	case 2: FputUint16_LE((uint16_t)val); return;
	case 4: FputUint32_LE((uint32_t)val); return;
	case 8: FputUint32_LE((uint64_t)val); return;
	}
	FputUint8((uint8_t )val);
}

uint16_t c_fileio::FgetUint16_BE()
{
	pair16_t tmp;
	tmp.b.h = FgetUint8();
	tmp.b.l = FgetUint8();
	return tmp.w;
}

void c_fileio::FputUint16_BE(uint16_t val)
{
	pair16_t tmp;
	tmp.w = val;
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

uint32_t c_fileio::FgetUint32_BE()
{
	pair32_t tmp;
	tmp.b.h3 = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.l  = FgetUint8();
	return tmp.d;
}

void c_fileio::FputUint32_BE(uint32_t val)
{
	pair32_t tmp;
	tmp.d = val;
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

uint64_t c_fileio::FgetUint64_BE()
{
	pair64_t tmp;
	tmp.b.h7 = FgetUint8();
	tmp.b.h6 = FgetUint8();
	tmp.b.h5 = FgetUint8();
	tmp.b.h4 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.l  = FgetUint8();
	return tmp.q;
}

void c_fileio::FputUint64_BE(uint64_t val)
{
	pair64_t tmp;
	tmp.q = val;
	FputUint8(tmp.b.h7);
	FputUint8(tmp.b.h6);
	FputUint8(tmp.b.h5);
	FputUint8(tmp.b.h4);
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

int16_t c_fileio::FgetInt16_BE()
{
	pair16_t tmp;
	tmp.b.h = FgetUint8();
	tmp.b.l = FgetUint8();
	return tmp.sw;
}

void c_fileio::FputInt16_BE(int16_t val)
{
	pair16_t tmp;
	tmp.sw = val;
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

int32_t c_fileio::FgetInt32_BE()
{
	pair32_t tmp;
	tmp.b.h3 = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.l  = FgetUint8();
	return tmp.sd;
}

void c_fileio::FputInt32_BE(int32_t val)
{
	pair32_t tmp;
	tmp.sd = val;
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

int64_t c_fileio::FgetInt64_BE()
{
	pair64_t tmp;
	tmp.b.h7 = FgetUint8();
	tmp.b.h6 = FgetUint8();
	tmp.b.h5 = FgetUint8();
	tmp.b.h4 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.l  = FgetUint8();
	return tmp.sq;
}

void c_fileio::FputInt64_BE(int64_t val)
{
	pair64_t tmp;
	tmp.sq = val;
	FputUint8(tmp.b.h7);
	FputUint8(tmp.b.h6);
	FputUint8(tmp.b.h5);
	FputUint8(tmp.b.h4);
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

float c_fileio::FgetFloat_BE()
{
	pair32_t tmp;
	tmp.b.h3 = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.l  = FgetUint8();
	return tmp.f;
}

void c_fileio::FputFloat_BE(float val)
{
	pair32_t tmp;
	tmp.f = val;
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

double c_fileio::FgetDouble_BE()
{
	pair64_t tmp;
	tmp.b.h7 = FgetUint8();
	tmp.b.h6 = FgetUint8();
	tmp.b.h5 = FgetUint8();
	tmp.b.h4 = FgetUint8();
	tmp.b.h3 = FgetUint8();
	tmp.b.h2 = FgetUint8();
	tmp.b.h  = FgetUint8();
	tmp.b.l  = FgetUint8();
	return tmp.df;
}

void c_fileio::FputDouble_BE(double val)
{
	pair64_t tmp;
	tmp.df = val;
	FputUint8(tmp.b.h7);
	FputUint8(tmp.b.h6);
	FputUint8(tmp.b.h5);
	FputUint8(tmp.b.h4);
	FputUint8(tmp.b.h3);
	FputUint8(tmp.b.h2);
	FputUint8(tmp.b.h);
	FputUint8(tmp.b.l);
}

_TCHAR c_fileio::FgetTCHAR_BE()
{
	switch(sizeof(_TCHAR)) {
	case 1: return (_TCHAR)FgetUint8();
	case 2: return (_TCHAR)FgetUint16_BE();
	case 4: return (_TCHAR)FgetUint32_BE();
	case 8: return (_TCHAR)FgetUint64_BE();
	}
	return (_TCHAR)FgetUint8();
}

void c_fileio::FputTCHAR_BE(_TCHAR val)
{
	switch(sizeof(_TCHAR)) {
	case 1: FputUint8((uint8_t )val); return;
	case 2: FputUint16_BE((uint16_t)val); return;
	case 4: FputUint32_BE((uint32_t)val); return;
	case 8: FputUint32_BE((uint64_t)val); return;
	}
	FputUint8((uint8_t )val);
}

bool c_fileio::StateCheckUint32(uint32_t val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		return (val == FgetUint32_LE());
	} else {
		FputUint32_LE(val);
		return true;
	}
}

bool c_fileio::StateCheckInt32(int32_t val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		return (val == FgetInt32_LE());
	} else {
		FputInt32_LE(val);
		return true;
	}
}

bool c_fileio::StateCheckBuffer(const _TCHAR *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		if(open_mode == c_fileio_READ_BINARY) {
			if(buffer[i] != FgetTCHAR_LE()) {
				return false;
			}
		} else {
			FputTCHAR_LE(buffer[i]);
		}
	}
	return true;
}

void c_fileio::StateValue(bool &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetBool();
	} else {
		FputBool(val);
	}
}

void c_fileio::StateValue(uint8_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetUint8();
	} else {
		FputUint8(val);
	}
}

void c_fileio::StateValue(uint16_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetUint16_LE();
	} else {
		FputUint16_LE(val);
	}
}

void c_fileio::StateValue(uint32_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetUint32_LE();
	} else {
		FputUint32_LE(val);
	}
}

void c_fileio::StateValue(uint64_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetUint64_LE();
	} else {
		FputUint64_LE(val);
	}
}

void c_fileio::StateValue(int8_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetInt8();
	} else {
		FputInt8(val);
	}
}

void c_fileio::StateValue(int16_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetInt16_LE();
	} else {
		FputInt16_LE(val);
	}
}

void c_fileio::StateValue(int32_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetInt32_LE();
	} else {
		FputInt32_LE(val);
	}
}

void c_fileio::StateValue(int64_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetInt64_LE();
	} else {
		FputInt64_LE(val);
	}
}

void c_fileio::StateValue(pair16_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val.w = FgetUint16_LE();
	} else {
		FputUint16_LE(val.w);
	}
}

void c_fileio::StateValue(pair32_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val.d = FgetUint32_LE();
	} else {
		FputUint32_LE(val.d);
	}
}

void c_fileio::StateValue(pair64_t &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val.q = FgetUint64_LE();
	} else {
		FputUint64_LE(val.q);
	}
}

void c_fileio::StateValue(float &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetFloat_LE();
	} else {
		FputFloat_LE(val);
	}
}

void c_fileio::StateValue(double &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetDouble_LE();
	} else {
		FputDouble_LE(val);
	}
}

void c_fileio::StateValue(_TCHAR &val)
{
	if(open_mode == c_fileio_READ_BINARY) {
		val = FgetTCHAR_LE();
	} else {
		FputTCHAR_LE(val);
	}
}

void c_fileio::StateArray(bool *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(uint8_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(uint16_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(uint32_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(uint64_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(int8_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(int16_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(int32_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(int64_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(pair16_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(pair32_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(pair64_t *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(float *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(double *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateArray(_TCHAR *buffer, size_t size, size_t count)
{
	for(unsigned int i = 0; i < size / sizeof(buffer[0]) * count; i++) {
		StateValue(buffer[i]);
	}
}

void c_fileio::StateBuffer(void *buffer, size_t size, size_t count)
{
	if(open_mode == c_fileio_READ_BINARY) {
		Fread(buffer, size, count);
	} else {
		Fwrite(buffer, size, count);
	}
}
*/
