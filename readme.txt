===============================================
Libretro-EmuCV (EPOCH Cassette Vision Emulator)
===============================================

-----
About
-----
Libretro-EmuCV emulte the "EPOCH Cassette Vision", a home video game console
made by EPOCH CO. and released only in Japan on July 30th. 1981.
A redesigned model called the EPOCH Cassette Vision Jr. was released afterwards.
The Cassette Vision was the best selling video game console in Japan before Nintendo's Family Computer.

This emulator is purposed as a crossplatform core for Libretro compatible
fondends as Retroarch, Recalbox, RetroPie, etc.
It is written by:
- MARCONATO Maxime (aka MaaaX, aka EPOCH84)
with help and informations of:
- Tetsuji OGUCHI (chip upD777 and upD778 creator)
- James BROLLY
- Sean RIDDLE


---------------
For developpers
---------------
Please use Visual Studio Code as editor on Windows, Mac and Linux, 
On Windows, install Mingw64 as described on the Libretro dev page.
On Linux install build-essential and retroarch (not from flatpak).
On Mac please install "xcode-select --install" (debug with lldb on OS X 10.9+). Unable to debug or compile on recent OS X versions.
...work in progress (cross-platform compil/debug toolchain)...

-------
Authors
-------

Libretro-EmuSCV:
- MARCONATO Maxime (aka MaaaX, aka EPOCH84) | maxime@maaax.com | http://www.maaax.com/
