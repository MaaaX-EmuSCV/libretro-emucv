// 
// core.cpp
// 
// Libretro core
// 
// Authors:
// - MARCONATO Maxime (aka MaaaX, aka EPOCH84 / maxime@maaax.com)
// 

#include "core.h"


static uint32_t *retro_frame_buffer;

//
// Libretro core class constructor.
//
c_core::c_core()
{
	// Log
	f_core_retro_log_printf = f_core_retro_log_fallback;
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::cCore()\n", CORE_NAME);

	// Init variables
	retro_core_initialized		= false;
	retro_audio_enable			= false;
	retro_frame_counter			= 0;
	retro_frame_time			= 0;
	retro_input_support_bitmask	= false;
	retro_game_loaded			= false;
	retro_use_audio_cb			= false;

	retro_frame_buffer			= nullptr;

	power_on					= true;

	memset(retro_base_directory, 0, sizeof(retro_base_directory));
	memset(retro_save_directory, 0, sizeof(retro_save_directory));
	memset(retro_game_path, 0, sizeof(retro_game_path));

	// Init functions
	f_retro_environment			= nullptr;
	f_retro_video_refresh		= nullptr;
	f_retro_audio_sample		= nullptr;
	f_retro_audio_sample_batch	= nullptr;
	f_retro_input_poll			= nullptr;
	f_retro_input_state			= nullptr;


	o_emu = nullptr;
}

//
// Libretro core class destructor.
//
c_core::~c_core()
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::~cCore()\n", CORE_NAME);

	if (retro_game_loaded)
	{
		f_retro_unload_game();
		retro_game_loaded = true;
	}
	if (retro_core_initialized)
	{
		f_retro_deinit();
		retro_core_initialized = false;
	}
	if(retro_frame_buffer)
	{
		free(retro_frame_buffer);
		retro_frame_buffer = nullptr;
	}
}

//
// Libretro: set the environment
//
void c_core::f_retro_set_environment(retro_environment_t f_retro_environment_cb)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroSetEnvironment()\n", CORE_NAME);

	// Set the Libretro environment callback
	f_retro_environment = f_retro_environment_cb;
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Environment callback set\n", CORE_NAME);

	// Set the log callback
	static struct retro_log_callback log_callback;
	if (f_retro_environment(RETRO_ENVIRONMENT_GET_LOG_INTERFACE, &log_callback))
	{
		f_core_retro_log_printf = log_callback.log;
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Log callback set to LibRetro\n", CORE_NAME);
	}
	else
	{
		f_core_retro_log_printf = f_core_retro_log_fallback;
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Log callback set to EmuSCV\n", CORE_NAME);
	}

	// The core can be run without ROM (to show display test if no game loaded)
	static bool support_no_game = CORE_SUPPORT_NO_GAME;
	f_retro_environment(RETRO_ENVIRONMENT_SET_SUPPORT_NO_GAME, &support_no_game);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] support_no_game set\n", CORE_NAME);

	// Set the controllers description
	static const struct retro_controller_description controller_joypad[] =
	{
		{ "Controller", RETRO_DEVICE_JOYPAD },	// controllers
		{ nullptr }
	};
	static const struct retro_controller_description controller_keyboard[] =
	{
		{ "Keyboard", RETRO_DEVICE_KEYBOARD },	// keyboard on the console
		{ nullptr }
	};
	// 2 controllers and 1 keyboard
	static const struct retro_controller_info controller_info[] =
	{
		{ controller_joypad,   1 },	// port 0
		{ controller_joypad,   1 },	// port 1
		{ controller_keyboard, 1 },	// port 2
		{ nullptr },
	};
	f_retro_environment(RETRO_ENVIRONMENT_SET_CONTROLLER_INFO, (void*)controller_info);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Controller infos set\n", CORE_NAME);

	// Set the controller descriptor
	struct retro_input_descriptor input_descriptor[] =
	{
		// Joypad 1: left side orange controller
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_SELECT, CORE_INPUTDESC_BUTTON_SELECT },	// Display console options, keyboard, manuals, etc.
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_START,  CORE_INPUTDESC_BUTTON_START },	// Pause
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_LEFT,   CORE_INPUTDESC_BUTTON_LEFT },	// Controller joystick
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_RIGHT,  CORE_INPUTDESC_BUTTON_RIGHT },	// Controller joystick
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_UP,     CORE_INPUTDESC_BUTTON_UP },		// Controller joystick
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_DOWN,   CORE_INPUTDESC_BUTTON_DOWN },	// Controller joystick
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_A,      CORE_INPUTDESC_BUTTON_1 },		// Right controller button
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_B,      CORE_INPUTDESC_BUTTON_2 },		// Left controller button
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_X,      CORE_INPUTDESC_BUTTON_3},
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_Y,      CORE_INPUTDESC_BUTTON_4 },
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_L,      CORE_INPUTDESC_BUTTON_L1 },
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_R,      CORE_INPUTDESC_BUTTON_R1 },
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_L2,     CORE_INPUTDESC_BUTTON_L2 },
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_R2,     CORE_INPUTDESC_BUTTON_R2 },
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_L3,     CORE_INPUTDESC_BUTTON_L3 },
		{ 0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_R3,     CORE_INPUTDESC_BUTTON_R3 },
		{ 0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT,  RETRO_DEVICE_ID_ANALOG_X, CORE_INPUTDESC_ANALOG_LEFT_X },	// Controller joystick
		{ 0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT,  RETRO_DEVICE_ID_ANALOG_Y, CORE_INPUTDESC_ANALOG_LEFT_Y },	// Controller joystick
		{ 0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_X, CORE_INPUTDESC_ANALOG_RIGHT_X },
		{ 0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_Y, CORE_INPUTDESC_ANALOG_RIGHT_Y },

		// Joypad 2: right side blue controller
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_SELECT, CORE_INPUTDESC_BUTTON_SELECT },	// Display console options, keyboard, manuals, etc.
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_START,  CORE_INPUTDESC_BUTTON_START },	// Pause
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_LEFT,   CORE_INPUTDESC_BUTTON_LEFT },	// Controller joystick
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_RIGHT,  CORE_INPUTDESC_BUTTON_RIGHT },	// Controller joystick
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_UP,     CORE_INPUTDESC_BUTTON_UP },		// Controller joystick
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_DOWN,   CORE_INPUTDESC_BUTTON_DOWN },	// Controller joystick
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_A,      CORE_INPUTDESC_BUTTON_1 },		// Right controller button
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_B,      CORE_INPUTDESC_BUTTON_2 },		// Left controller button
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_X,      CORE_INPUTDESC_BUTTON_3},
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_Y,      CORE_INPUTDESC_BUTTON_4 },
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_L,      CORE_INPUTDESC_BUTTON_L1 },
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_R,      CORE_INPUTDESC_BUTTON_R1 },
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_L2,     CORE_INPUTDESC_BUTTON_L2 },
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_R2,     CORE_INPUTDESC_BUTTON_R2 },
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_L3,     CORE_INPUTDESC_BUTTON_L3 },
		{ 1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_R3,     CORE_INPUTDESC_BUTTON_R3 },
		{ 1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT,  RETRO_DEVICE_ID_ANALOG_X, CORE_INPUTDESC_ANALOG_LEFT_X },	// Controller joystick
		{ 1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT,  RETRO_DEVICE_ID_ANALOG_Y, CORE_INPUTDESC_ANALOG_LEFT_Y },	// Controller joystick
		{ 1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_X, CORE_INPUTDESC_ANALOG_RIGHT_X },
		{ 1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_Y, CORE_INPUTDESC_ANALOG_RIGHT_Y },

		{ 0 }
	};
	f_retro_environment(RETRO_ENVIRONMENT_SET_INPUT_DESCRIPTORS, input_descriptor);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Input descriptors set\n", CORE_NAME);

//	// Set config variables
//	struct retro_variable variable[] =
//			{
//					{ SETTING_CONSOLE_KEY, "CONSOLE; AUTO|EPOCH|YENO|EPOCHLADY" },
//					{ SETTING_DISPLAY_KEY, "DISPLAY; AUTO|EMUSCV|EPOCH|YENO" },
//					{ SETTING_PIXELASPECT_KEY, "PIXELASPECT; AUTO|RECTANGULAR|SQUARE" },
//					{ SETTING_RESOLUTION_KEY, "RESOLUTION; AUTO|LOW|MEDIUM|HIGH" },
//					{ SETTING_PALETTE_KEY, "PALETTE; AUTO|STANDARD|OLDNTSC" },
//					{ SETTING_FPS_KEY, "FPS; AUTO|EPOCH60|YENO50" },
//					{ SETTING_DISPLAYFULLMEMORY_KEY, "DISPLAYFULLMEMORY; AUTO|YES|NO" },
//					{ SETTING_DISPLAYINPUTS_KEY, "DISPLAYINPUTS; AUTO|YES|NO" },
//					{ SETTING_LANGAGE_KEY, "LANGAGE; AUTO|JP|FR|EN" },
//					{ SETTING_CHECKBIOS_KEY, "CHECKBIOS; AUTO|YES|NO" },
//					{ NULL, NULL }
//			};
//	f_retro_environment(RETRO_ENVIRONMENT_SET_VARIABLES, variable);
//	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Variables set\n", CORE_NAME);
}

//
// Libretro: set the video refresh callback
//
void c_core::f_retro_set_video_refresh(retro_video_refresh_t f_retro_video_refresh_cb)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroSetVideoRefresh()\n", CORE_NAME);

	// Set the Libretro video callback
	f_retro_video_refresh = f_retro_video_refresh_cb;
}

//
// Libretro: set the audio sample callback
//
void c_core::f_retro_set_audio_sample(retro_audio_sample_t f_retro_audio_sample_cb)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroSetAudioSample()\n", CORE_NAME);

	// Set the Libretro audio sample callback
	f_retro_audio_sample = f_retro_audio_sample_cb;
}

//
// Libretro: set the audio batch callback
//
void c_core::f_retro_set_audio_sample_batch(retro_audio_sample_batch_t f_retro_audio_sample_batch_cb)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroSetAudioSampleBatch()\n", CORE_NAME);

	// Set the Libretro audio sample batch callback
	f_retro_audio_sample_batch = f_retro_audio_sample_batch_cb;
}

//
// Libretro: set the input poll callback
//
void c_core::f_retro_set_input_poll(retro_input_poll_t f_retro_input_poll_cb)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroSetInputPoll()\n", CORE_NAME);

	f_retro_input_poll = f_retro_input_poll_cb;
}

//
// Libretro: set the input state callback
//
void c_core::f_retro_set_input_state(retro_input_state_t f_retro_input_state_cb)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroSetInputState()\n", CORE_NAME);

	f_retro_input_state = f_retro_input_state_cb;
}

//
// Libretro: return the version used by the core for compatibility check with the frontend
//
unsigned c_core::f_retro_get_api_version()
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroApiVersion() => %d\n", CORE_NAME, RETRO_API_VERSION);

	return RETRO_API_VERSION;
}

//
// Libretro: return the video standard used
//
unsigned c_core::f_retro_get_video_region()
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	if(CORE_REGION == RETRO_REGION_NTSC)
		f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroGetVideoRegion() => RETRO_REGION_PAL\n", CORE_NAME);
	else
		f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroGetVideoRegion() => RETRO_REGION_NTSC\n", CORE_NAME);

	return CORE_REGION;
}

//
// Libretro: get the system infos
//
void c_core::f_retro_get_system_info(struct retro_system_info *system_info)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroGetSystemInfo()\n     library_name     = %s\n     library_version  = %s\n     valid_extensions = %s\n     need_fullpath    = false\n     block_extract    = false\n", CORE_NAME, CORE_NAME, CORE_VERSION, CORE_EXTENSIONS);

	memset(system_info, 0, sizeof(*system_info));
	system_info->library_name		= CORE_NAME;
	system_info->library_version	= CORE_VERSION;
	system_info->valid_extensions	= CORE_EXTENSIONS;
	system_info->need_fullpath		= true;
	system_info->block_extract		= true;
}

//
// Libretro: get the audio/video infos
//
void c_core::f_retro_get_av_info(struct retro_system_av_info *system_av_info)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore:RetroGetAudioVideoInfo()\n", CORE_NAME);

	memset(system_av_info, 0, sizeof(*system_av_info));
	system_av_info->timing.fps				= 60.0;//config.window_fps;
	system_av_info->timing.sample_rate		= 0;//AUDIO_SAMPLING_RATE;
	system_av_info->geometry.base_width		= 320;//EMU_WIDTH;//config.window_width;
	system_av_info->geometry.base_height	= 240;//EMU_HEIGHT;//config.window_height;
	system_av_info->geometry.max_width		= 320;//EMU_WIDTH;//config.window_width * WINDOW_ZOOM_MAX;
	system_av_info->geometry.max_height		= 240;//EMU_HEIGHT;//config.window_height * WINDOW_ZOOM_MAX;
	system_av_info->geometry.aspect_ratio	= EMU_RATIO;//config.window_aspect_ratio;
	f_core_retro_log_printf(RETRO_LOG_INFO, "     timing.fps            = %f\n     timing.sample_rate    = AUDIO_SAMPLING_RATE\n     geometry.base_width   = %d\n     geometry.base_height  = %d\n     geometry.max_width    = %d\n     geometry.max_height   = %d\n     geometry.aspect_ratio = %f\n", system_av_info->timing.fps, system_av_info->geometry.base_width, system_av_info->geometry.base_height, system_av_info->geometry.max_width, system_av_info->geometry.max_height, system_av_info->geometry.aspect_ratio);
}

//
// Libretro: initialize the core
//
void c_core::f_retro_init(retro_audio_callback_t f_retro_audio_cb, retro_audio_set_state_callback_t f_retro_audio_set_state_cb, retro_frame_time_callback_t f_retro_frame_time_cb)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroInit()\n", CORE_NAME);

	if (retro_core_initialized)
	{
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] All initializations already done\n", CORE_NAME);
		return;
	}

	// Set audio callbacks
	struct retro_audio_callback audio_callback = { f_retro_audio_cb, f_retro_audio_set_state_cb };
	retro_use_audio_cb = f_retro_environment(RETRO_ENVIRONMENT_SET_AUDIO_CALLBACK, &audio_callback);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Audio callback set\n", CORE_NAME);

	// Set frame time callback
	struct retro_frame_time_callback frame_time_callback{};
	frame_time_callback.callback  = f_retro_frame_time_cb;
	frame_time_callback.reference = 1000000 / 60;//FRAMES_PER_SEC;
	frame_time_callback.callback(frame_time_callback.reference);
	f_retro_environment(RETRO_ENVIRONMENT_SET_FRAME_TIME_CALLBACK, &frame_time_callback);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Frame time callback set\n", CORE_NAME);

	// Retrieve base directory
	const char *dir = nullptr;
	memset(retro_base_directory, 0, sizeof(retro_base_directory));
	if (f_retro_environment(RETRO_ENVIRONMENT_GET_SYSTEM_DIRECTORY, &dir) && dir)
	{
		snprintf(retro_base_directory, sizeof(retro_base_directory), "%s", dir);
		set_libretro_system_directory(dir);
		f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] Base directory: %s\n", CORE_NAME, retro_base_directory);
	}
	else
	{
		f_core_retro_log_printf(RETRO_LOG_ERROR, "[%s] Base directory not set\n", CORE_NAME);
		return;
	}

	// Retrieve save directory	retro_base_path
	dir = nullptr;
	memset(retro_save_directory, 0, sizeof(retro_save_directory));
	if (f_retro_environment(RETRO_ENVIRONMENT_GET_SAVE_DIRECTORY, &dir) && dir)
	{
		snprintf(retro_save_directory, sizeof(retro_save_directory), "%s", dir);
		set_libretro_save_directory(dir);
		f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] Save directory: %s\n", CORE_NAME, retro_save_directory);
	}
	else
	{
		f_core_retro_log_printf(RETRO_LOG_ERROR, "[%s] Save directory not set\n", CORE_NAME);
		return;
	}

	// Init core variables
//	is_power_on				= false;
	retro_core_initialized 	= false;
	retro_frame_time		= 0;
	retro_game_loaded		= false;
	retro_audio_enable		= false;

	retro_input_support_bitmask	= f_retro_environment(RETRO_ENVIRONMENT_GET_INPUT_BITMASKS, nullptr);
	if(retro_input_support_bitmask)
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Input supports bitmask\n", CORE_NAME);
	else
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] retro_input_support_bitmask set (false)\n", CORE_NAME);

	// Initialize emulator settings
//	f_initialize_config();
//	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] config initialized\n", CORE_NAME);

	// Load emulator settings
	f_retro_load_settings();
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] settings (variables) loaded\n", CORE_NAME);

	// initialize the eSCV emulation core
	o_emu = new c_emu();
	retro_core_initialized	= true;
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] All initialisations done\n", CORE_NAME);
}

//
// Libretro: deinitialize the core
//
void c_core::f_retro_deinit()
{
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroDeinit()\n", CORE_NAME);

	if (!retro_core_initialized)
	{
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] All initializations already done\n", CORE_NAME);
		return;
	}

	if (retro_game_loaded)
	{
		f_retro_unload_game();
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Game unloaded\n", CORE_NAME);
	}
	else
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Game not loaded, nothing to unload\n", CORE_NAME);

	// Destroy emulator
	if (o_emu)
	{
		delete(o_emu);
		o_emu = nullptr;
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] emu deleted\n", CORE_NAME);
	}
	else
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] emu don't exists, nothing to delete\n", CORE_NAME);

	// Reinit core variables
	retro_frame_time			= 0;
	retro_game_loaded			= false;
	retro_input_support_bitmask	= false;
	retro_audio_enable			= false;
	retro_core_initialized 		= false;

	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] All deinitializations done\n", CORE_NAME);
}

//
// Libretro: set controller port device
//
void c_core::f_retro_set_controller_port_device(unsigned port, unsigned device)
{
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroSetControllerPortDevice()\n          Set device %d into port %d\n", CORE_NAME, device, port);
}

/*
//
// Libretro: audio callback
//
void cCore::RetroAudioCb(void)
{
	// Log
//	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
//	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroAudioCb()\n", CORE_NAME);
	int16_t val = 0x0000;
//	if (retro_audio_enable == TRUE)// && button_pressed == TRUE)
//	{
//		// Play A 440Hz
//		for (unsigned i = 0; i < AUDIO_SAMPLES_PER_FRAME; i++, phase++)
//		{
//			int16_t val = INT16_MAX * sinf(2.0f * M_PI * phase * 440.0f / AUDIO_SAMPLING_RATE);
//			audio_cb(val, val);
//		}
//	}
//	else
//	{
		// Mute
//		for (unsigned i = 0; i < AUDIO_SAMPLES_PER_FRAME; i++)

		for (uint16_t i = 0; i < AUDIO_SAMPLING_RATE; i++)
			RetroAudioSample(val, val);

//	}
	retro_audio_phase %= AUDIO_SAMPLING_RATE;
//Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] cCore::RetroAudioCb() => RetroAudioSample()\n", CORE_NAME);
//int16_t val = 0;
//for (uint32_t i = 0; i < AUDIO_SAMPLING_RATE; i++)
//	RetroAudioSample(val, val);
}
*/

//
// Libretro: audio set state enable/disable callback
//
void c_core::f_retro_audio_set_state_cb(bool enable)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroAudioSetStateCb()\n          Set audio state = %s\n", CORE_NAME, (enable ? "ON" : "OFF"));

	// Set audio state
	retro_audio_enable = enable;
}

//
// Libretro: frame time callback
//
void c_core::f_retro_frame_time_cb(retro_usec_t usec)
{
	int64_t usec_corrected = usec;//*FRAMES_PER_SEC/config.window_fps;

	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] cCore::RetroFrameTimeCb()\n          Frame time = %d microseconds\n", CORE_NAME, usec_corrected);

	// Memorise current frame time
	retro_frame_time = usec_corrected;
}

//
// Libretro: load game
//
bool c_core::f_retro_load_game(const struct retro_game_info *game_info)
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroLoadGame()\n", CORE_NAME);

	if(retro_game_loaded)
	{
		f_retro_unload_game();
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Previous game unloded\n", CORE_NAME);
	}
	else
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] No previous game, nothing to unload\n", CORE_NAME);

	retro_game_loaded = false;

	// Set the performance level is guide to Libretro to give an idea of how intensive this core is to run for the game
	// It should be set in retro_load_game()
	static unsigned performance_level = CORE_PERFORMANCE_LEVEL;
	f_retro_environment(RETRO_ENVIRONMENT_SET_PERFORMANCE_LEVEL, &performance_level);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Performance level set (4)\n", CORE_NAME);

	enum retro_pixel_format pixel_format = RETRO_PIXEL_FORMAT_XRGB8888;
	if (!f_retro_environment(RETRO_ENVIRONMENT_SET_PIXEL_FORMAT, &pixel_format))
	{
		f_core_retro_log_printf(RETRO_LOG_ERROR, "[%s] Pixel format not supported! (XRGB8888)\n", CORE_NAME);
		return false;
	}
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Pixel format set (XRGB8888)\n", CORE_NAME);

	bool updated = false;
	if (f_retro_environment(RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE, &updated) && updated)
	{
		f_retro_load_settings();
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Settings (variable) loaded\n", CORE_NAME);
	}
	else
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Settings not updated, already loaded\n", CORE_NAME);

	// reset the frame counter
	retro_frame_counter = 0;

	// Get game rom path
	memset(retro_game_path, 0, sizeof(retro_game_path));
	if(o_emu)
	{
		if (game_info && strcmp(game_info->path, "") != 0)
		{
			snprintf(retro_game_path, sizeof(retro_game_path), "%s", game_info->path);
			f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] Try to load game: %s\n", CORE_NAME, retro_game_path);

			// Insert cart
			o_emu->f_insert_cart(retro_game_path);
			if(o_emu->f_is_cart_inserted())
			{
				retro_game_loaded = true;
				f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Game loaded\n", CORE_NAME);
			}
			else
				f_core_retro_log_printf(RETRO_LOG_ERROR, "[%s] Game not loaded\n", CORE_NAME);
		}
		else
			f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] No game to load\n", CORE_NAME);
	}
	else
		f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] No emulator instance -> game not loaded\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Game loading done\n", CORE_NAME);

//	sound_buffer_samples = (AUDIO_SAMPLING_RATE / config.window_fps + 0.5);

	return true;
}

//
// Libretro: unload game
//
void c_core::f_retro_unload_game()
{
	// Log
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
	f_core_retro_log_printf(RETRO_LOG_INFO, "[%s] cCore::RetroUnloadGame()\n", CORE_NAME);

	// Free emulator
	if(o_emu && o_emu->f_is_cart_inserted())
	{
		o_emu->f_eject_cart();
			f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Game unloaded\n", CORE_NAME);
	}
	else
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] No game loaded, nothing to unload\n", CORE_NAME);

	retro_game_loaded = false;
	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Game unloading done\n", CORE_NAME);
}

//
// Libretro: run for only one frame
//
void c_core::f_retro_run()
{
//	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
//	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] cCore::RetroRun()\n", CORE_NAME);

	unsigned port0			= 0;
	unsigned port1			= 1;
	int16_t ret0			= 0;
	int16_t ret1			= 0;
//	int16_t key0			= 0;
//	int16_t key1			= 0;
//	int16_t key2			= 0;
//	int16_t key3			= 0;
//	int16_t key4			= 0;
//	int16_t key5			= 0;
//	int16_t key6			= 0;
//	int16_t key7			= 0;
//	int16_t key8			= 0;
//	int16_t key9			= 0;
//	int16_t keyEnter		= 0;
//	int16_t keyClear		= 0;
//	int16_t keyPower		= 0;
//	int16_t keyReset		= 0;
//	int16_t keyPause		= 0;
	int16_t analogleftx0	= 0;
	int16_t analoglefty0	= 0;
	int16_t analogrightx0	= 0;
	int16_t analogrighty0	= 0;
	int16_t analogleftx1	= 0;
	int16_t analoglefty1	= 0;
	int16_t analogrightx1	= 0;
	int16_t analogrighty1	= 0;

//	uint32_t posx			= 0;
//	uint32_t posy			= 0;
//	uint8_t  alpha			= 127;
//	uint32_t sizx			= 0;
//	uint32_t sizy			= 0;
//	uint32_t space			= 4;
//
//	uint8_t spacex			= 0;
//	uint8_t spacey			= 0;
//	uint8_t sizex			= 0;
//	uint8_t sizey			= 0;
//
//	int16_t joyposx			= 0;
//	int16_t joyposy			= 0;
//	uint32_t color			= 0xFF000000;

//	bool config_changed = false;
//
//	// Load previous save state
//	if(state.IsReadyToLoad())
//	{
//		if(emu)
//			emu->load_state(&state);
//		RetroLoadSettings();
//		state.SetReadyToLoad(false);
//	}

	bool updated = false;
	if (f_retro_environment(RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE, &updated) && updated)
	{
		f_retro_load_settings();
		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Settings (variables) loaded\n", CORE_NAME);
	}
//	else
//		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Settings (variables) unchanged, already loaded\n", CORE_NAME);

	// Get inputs
	f_retro_input_poll();
//	f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] Inputs polled\n", CORE_NAME);

	// Controllers buttons
	if (retro_input_support_bitmask)
	{
		ret0 = f_retro_input_state(port0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_MASK);
		ret1 = f_retro_input_state(port1, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_MASK);
	}
	else
	{
		unsigned i;
		for (i = RETRO_DEVICE_ID_JOYPAD_B; i < RETRO_DEVICE_ID_JOYPAD_R3+1; i++)
		{
			if (f_retro_input_state(port0, RETRO_DEVICE_JOYPAD, 0, i))
				ret0 |= (1 << i);
		}
		for (i = RETRO_DEVICE_ID_JOYPAD_B; i < RETRO_DEVICE_ID_JOYPAD_R3+1; i++)
		{
			if (f_retro_input_state(port1, RETRO_DEVICE_JOYPAD, 0, i))
				ret1 |= (1 << i);
		}
	}
	// Controllers analog sticks
	analogleftx0	= f_retro_input_state(port0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT, RETRO_DEVICE_ID_ANALOG_X);
	analoglefty0	= f_retro_input_state(port0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT, RETRO_DEVICE_ID_ANALOG_Y);
	analogrightx0	= f_retro_input_state(port0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_X);
	analogrighty0	= f_retro_input_state(port0, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_Y);
	analogleftx1	= f_retro_input_state(port1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT, RETRO_DEVICE_ID_ANALOG_X);
	analoglefty1	= f_retro_input_state(port1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_LEFT, RETRO_DEVICE_ID_ANALOG_Y);
	analogrightx1	= f_retro_input_state(port1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_X);
	analogrighty1	= f_retro_input_state(port1, RETRO_DEVICE_ANALOG, RETRO_DEVICE_INDEX_ANALOG_RIGHT, RETRO_DEVICE_ID_ANALOG_Y);
//	// Keyboard
//	key1 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_1)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP1);
//	key2 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_2)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP2);
//	key3 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_3)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP3);
//	key4 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_4)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP4);
//	key5 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_5)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP5);
//	key6 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_6)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP6);
//	key7 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_7)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP7);
//	key8 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_8)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP8);
//	key9 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_9)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP9);
//	key0 = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_0)
//		   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP0);
//	keyEnter = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_RETURN)
//			   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_KP_ENTER);
//	keyClear = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_BACKSPACE)
//			   || RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_DELETE);
//	keyPower = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_F12);
//	keyReset = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_F11);
//	keyPause = RetroInputState(port0, RETRO_DEVICE_KEYBOARD, 0, RETROK_F9);
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] Imputs parsed\n", CORE_NAME);

	// Button SELECT
	// open configuration
/*
	if ((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_SELECT)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_SELECT)))
	{
		if(!button_menu_pressed)
		{
			button_menu_pressed = true;
			is_menu_displayed = !is_menu_displayed
			if(is_menu_displayed)
				is_keyboard_displayed = false;
		}
	}
	else
		button_menu_pressed = false;
*/

//
//	// Other buttons
//	// open the console keyboard overlay
//	if ((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_SELECT)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_SELECT))
//		|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R2))/* || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L3)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R3))*/
//		|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R2))/* || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L3)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R3))*/)
//	{
//		if(!button_keyboard_pressed)
//		{
//			button_keyboard_pressed = true;
//			is_keyboard_displayed = !is_keyboard_displayed;
//			if(is_keyboard_displayed)
//				is_menu_displayed = false;
//		}
//	}
//	else
//		button_keyboard_pressed = false;
//
//	// Open or close fading the keyboard
//	if(is_keyboard_displayed)
//	{
//		if(keyboard_counter < EMUSCV_KEYBOARD_DELAY)
//		{
//			keyboard_counter += 300/config.window_fps;
//			if(keyboard_counter >= EMUSCV_KEYBOARD_DELAY)
//				keyboard_counter = EMUSCV_KEYBOARD_DELAY;
//		}
//	}
//	else
//	{
//		if(keyboard_counter > 0)
//		{
//			keyboard_counter -= 300/config.window_fps;
//			if(keyboard_counter < 0)
//				keyboard_counter = 0;
//		}
//	}
//
//	// Key UP with controller
//	if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_UP)) || (analoglefty0 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrighty0 <= EMUSCV_AXIS_NEUTRAL_MIN)
//	   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_UP)) || (analoglefty1 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrighty1 <= EMUSCV_AXIS_NEUTRAL_MIN))
//	{
//		if(!key_pressed_up)
//		{
//			key_pressed_up = true;
//			if(is_keyboard_displayed && keyboard_y > 0)
//			{
//				if(keyboard_x == 0 && (keyboard_y == 1 || keyboard_y == 2))
//					keyboard_y = 0;
//				else
//					keyboard_y--;
//			}
//		}
//	}
//	else
//		key_pressed_up = false;
//
//	// Key DOWN with controller
//	if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_DOWN)) || (analoglefty0 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrighty0 >= EMUSCV_AXIS_NEUTRAL_MAX)
//	   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_DOWN)) || (analoglefty1 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrighty1 >= EMUSCV_AXIS_NEUTRAL_MAX))
//	{
//		if(!key_pressed_down)
//		{
//			key_pressed_down = true;
//			if(is_keyboard_displayed && keyboard_y < 3)
//			{
//				if(keyboard_x == 0 && (keyboard_y == 1 || keyboard_y == 2))
//					keyboard_y = 3;
//				else
//					keyboard_y++;
//			}
//		}
//	}
//	else
//		key_pressed_down = false;
//
//	// Key LEFT with controller
//	if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_LEFT)) || (analogleftx0 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrightx0 <= EMUSCV_AXIS_NEUTRAL_MIN)
//	   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_LEFT)) || (analogleftx1 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrightx1 <= EMUSCV_AXIS_NEUTRAL_MIN))
//	{
//		if(!key_pressed_left)
//		{
//			key_pressed_left = true;
//			if(is_keyboard_displayed && keyboard_x > 0)
//				keyboard_x--;
//		}
//	}
//	else
//		key_pressed_left = false;
//
//	// Key RIGHT with controller
//	if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_RIGHT)) || (analogleftx0 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrightx0 >= EMUSCV_AXIS_NEUTRAL_MAX)
//	   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_RIGHT)) || (analogleftx1 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrightx1 >= EMUSCV_AXIS_NEUTRAL_MAX))
//	{
//		if(!key_pressed_right)
//		{
//			key_pressed_right = true;
//			if(is_keyboard_displayed && keyboard_x < 3)
//				keyboard_x++;
//		}
//	}
//	else
//		key_pressed_right = false;
//
//	key_pressed_keyboard_stay_opened = false;
//	key_pressed_keyboard_close = false;
//
//	// Controllers
//	if(emu)
//	{
//		if(!is_keyboard_displayed && keyboard_counter == 0 && !is_menu_displayed
//		   && !key_pressed_0 && !key_pressed_cl && !key_pressed_en
//		   && !key_pressed_1 && !key_pressed_2 && !key_pressed_3
//		   && !key_pressed_4 && !key_pressed_5 && !key_pressed_6
//		   && !key_pressed_7 && !key_pressed_8 && !key_pressed_9
//		   && !key_pressed_power && !key_pressed_reset && !key_pressed_pause)
//		{
//			// LEFT CONTROLLER 1, orange
//			// Directionnal cross, analog stick left or analog stick right
//			emu->get_osd()->set_joy_status(	0,
//													(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_UP)) || (analoglefty0 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrighty0 <= EMUSCV_AXIS_NEUTRAL_MIN),
//													(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_DOWN)) || (analoglefty0 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrighty0 >= EMUSCV_AXIS_NEUTRAL_MAX),
//													(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_LEFT)) || (analogleftx0 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrightx0 <= EMUSCV_AXIS_NEUTRAL_MIN),
//													(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_RIGHT)) || (analogleftx0 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrightx0 >= EMUSCV_AXIS_NEUTRAL_MAX),
//													(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)),
//													(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)),
//													false, false, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0);
//
//			// LEFT CONTROLLER 2, blue
//			// Directionnal cross, analog stick left or analog stick right
//			emu->get_osd()->set_joy_status(	1,
//													(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_UP)) || (analoglefty1 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrighty1 <= EMUSCV_AXIS_NEUTRAL_MIN),
//													(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_DOWN)) || (analoglefty1 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrighty1 >= EMUSCV_AXIS_NEUTRAL_MAX),
//													(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_LEFT)) || (analogleftx1 <= EMUSCV_AXIS_NEUTRAL_MIN) || (analogrightx1 <= EMUSCV_AXIS_NEUTRAL_MIN),
//													(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_RIGHT)) || (analogleftx1 >= EMUSCV_AXIS_NEUTRAL_MAX) || (analogrightx1 >= EMUSCV_AXIS_NEUTRAL_MAX),
//													(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START)),
//													(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START)),
//													false, false, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0);
//		}
//		else if ((is_keyboard_displayed || is_menu_displayed) && ((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START))))
//		{
//			// LEFT CONTROLLER 1, orange
//			// Buttons A & B
//			emu->get_osd()->set_joy_status(0, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0);
//
//			// LEFT CONTROLLER 2, blue
//			// Buttons A & B
//			emu->get_osd()->set_joy_status(1, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0);
//
//			key_pressed_keyboard_close = true;
//		}
//	}
//
//	if(start_up_counter_power < 150)
//	{
//		// Auto power ON after 0,5 seconds
//		// Power button inactive
//		start_up_counter_power += 300/config.window_fps;
//		is_power_on = (start_up_counter_power >= 150);
//	}
//	else
//	{
//		// Key POWER ON/OFF
//		if(keyPower != 0
//		   || (is_keyboard_displayed && keyboard_x == 0 && keyboard_y == 0
//			   && (
//					   (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			   )))
//		{
//			if (!key_pressed_power)
//			{
//				is_power_on = !is_power_on;
//				if(is_power_on)
//					RetroReset(false);
//				else if(emu)
//					emu->save_cart(0);
//			}
//			key_pressed_power = true;
//			keyboard_x = 0;
//			keyboard_y = 0;
//			if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//				key_pressed_keyboard_close = true;
//			else
//				key_pressed_keyboard_stay_opened = true;
//		}
//		else
//			key_pressed_power = false;
//	}
//
////	if (!is_power_on)
////	{
////#ifdef USE_NOTIFY_POWER_OFF
////		// notify power off
////		if(emu)
////		{
////			emu->notify_power_off();
////		}
////#endif
////		RetroEnvironment(RETRO_ENVIRONMENT_SHUTDOWN, NULL);
////	}
//
//	// Key RESET
//	if (keyReset != 0
//		|| (is_keyboard_displayed && keyboard_x == 0 && (keyboard_y == 1 || keyboard_y == 2)
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
//		RetroReset(true);
//		key_pressed_reset = true;
//		keyboard_x = 0;
//		if(keyboard_y < 2)
//			keyboard_y = 1;
//		else
//			keyboard_y = 2;
//		if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else
//		key_pressed_reset = false;
//
//	// Key PAUSE
//	if (keyPause != 0
//		|| (is_keyboard_displayed && keyboard_x == 0 && keyboard_y == 3
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
//		if(emu)
//			emu->key_down(VK_SPACE, false, key_pressed_pause);
//		key_pressed_pause = true;
//		keyboard_x = 0;
//		keyboard_y = 3;
//		if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_pause)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_SPACE, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_SPACE, false);
//			key_pressed_pause = false;
//		}
//	}
//
//	// Key 0
//	if (key0 != 0
//		|| (is_keyboard_displayed && keyboard_x == 1 && keyboard_y == 3
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
//		if(emu)
//			emu->key_down(VK_NUMPAD0, false, key_pressed_0);
//		key_pressed_0 = true;
//		keyboard_x = 1;
//		keyboard_y = 3;
//		if(key0 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_0)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD0, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD0, false);
//			key_pressed_0 = false;
//		}
//	}
//
//	// Key 1
//	if (key1 != 0
//		|| (is_keyboard_displayed && keyboard_x == 1 && keyboard_y == 2
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
////		if(!key_pressed_1)
////		{
////			config.scv_display = SETTING_DISPLAY_EMUSCV_VAL;
////			config_changed = true;
////		}
//		if(emu)
//			emu->key_down(VK_NUMPAD1, false, key_pressed_1);
//		key_pressed_1 = true;
//		keyboard_x = 1;
//		keyboard_y = 2;
//		if(key1 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_1)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD1, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD1, false);
//			key_pressed_1 = false;
//		}
//	}
//
//	// Key 2
//	if (key2 != 0
//		|| (is_keyboard_displayed && keyboard_x == 2 && keyboard_y == 2
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
////		config.scv_display = SETTING_DISPLAY_EPOCH_VAL;
////		config_changed = true;
//		if(emu)
//			emu->key_down(VK_NUMPAD2, false, key_pressed_2);
//		key_pressed_2 = true;
//		keyboard_x = 2;
//		keyboard_y = 2;
//		if(key2 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_2)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD2, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD2, false);
//			key_pressed_2 = false;
//		}
//	}
//
//	// Key 3
//	if (key3 != 0
//		|| (is_keyboard_displayed && keyboard_x == 3 && keyboard_y == 2
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
////		config.scv_display = SETTING_DISPLAY_YENO_VAL;
////		config_changed = true;
//		if(emu)
//			emu->key_down(VK_NUMPAD3, false, key_pressed_3);
//		key_pressed_3 = true;
//		keyboard_x = 3;
//		keyboard_y = 2;
//		if(key3 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_3)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD3, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD3, false);
//			key_pressed_3 = false;
//		}
//	}
//
//	// Key 4
//	if (key4 != 0
//		|| (is_keyboard_displayed && keyboard_x == 1 && keyboard_y == 1
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
////		config.scv_displayfull = SETTING_DISPLAYFULL_NO_VAL;
////		config_changed = true;
//		if(emu)
//			emu->key_down(VK_NUMPAD4, false, key_pressed_4);
//		key_pressed_4 = true;
//		keyboard_x = 1;
//		keyboard_y = 1;
//		if(key4 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_4)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD4, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD4, false);
//			key_pressed_4 = false;
//		}
//	}
//
//	// Key 5
//	if (key5 != 0
//		|| (is_keyboard_displayed && keyboard_x == 2 && keyboard_y == 1
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
////		config.scv_displayfull = SETTING_DISPLAYFULL_YES_VAL;
////		config_changed = true;
//		if(emu)
//			emu->key_down(VK_NUMPAD5, false, key_pressed_5);
//		key_pressed_5 = true;
//		keyboard_x = 2;
//		keyboard_y = 1;
//		if(key5 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_5)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD5, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD5, false);
//			key_pressed_5 = false;
//		}
//	}
//
//	// Key 6
//	if (key6 != 0
//		|| (is_keyboard_displayed && keyboard_x == 3 && keyboard_y == 1
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
//		if(emu)
//			emu->key_down(VK_NUMPAD6, false, key_pressed_6);
//		key_pressed_6 = true;
//		keyboard_x = 3;
//		keyboard_y = 1;
//		if(key6 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_6)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD6, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD6, false);
//			key_pressed_6 = false;
//		}
//	}
//
//	// Key 7
//	if (key7 != 0
//		|| (is_keyboard_displayed && keyboard_x == 1 && keyboard_y == 0
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
////		config.scv_displayfps = SETTING_DISPLAYFPS_EPOCH60_VAL;
////		config_changed = true;
//		if(emu)
//			emu->key_down(VK_NUMPAD7, false, key_pressed_7);
//		key_pressed_7 = true;
//		keyboard_x = 1;
//		keyboard_y = 0;
//		if(key7 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_7)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD7, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD7, false);
//			key_pressed_7 = false;
//		}
//	}
//
//	// Key 8
//	if (key8 != 0
//		|| (is_keyboard_displayed && keyboard_x == 2 && keyboard_y == 0
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
////		config.scv_displayfps = SETTING_DISPLAYFPS_YENO50_VAL;
////		config_changed = true;
//		if(emu)
//			emu->key_down(VK_NUMPAD8, false, key_pressed_8);
//		key_pressed_8 = true;
//		keyboard_x = 2;
//		keyboard_y = 0;
//		if(key8 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_8)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD8, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD8, false);
//			key_pressed_8 = false;
//		}
//	}
//
//	// Key 9
//	if (key9 != 0
//		|| (is_keyboard_displayed && keyboard_x == 3 && keyboard_y == 0
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
//		if(emu)
//			emu->key_down(VK_NUMPAD9, false, key_pressed_9);
//		key_pressed_9 = true;
//		keyboard_x = 3;
//		keyboard_y = 0;
//		if(key9 != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_9)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_NUMPAD9, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_NUMPAD9, false);
//			key_pressed_0 = false;
//		}
//	}
//
//	// Key CL
//	if (keyClear != 0
//		|| (is_keyboard_displayed && keyboard_x == 2 && keyboard_y == 3
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
//		if(emu)
//			emu->key_down(VK_BACK, false, key_pressed_cl);
//		key_pressed_cl = true;
//		keyboard_x = 2;
//		keyboard_y = 3;
//		if(keyClear != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_cl)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_BACK, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_BACK, false);
//			key_pressed_cl = false;
//		}
//	}
//
//	// Key EN + Controllers 1 & 2's Button START
//	if (keyEnter != 0 || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START))
//		|| (is_keyboard_displayed && keyboard_x == 3 && keyboard_y == 3
//			&& (
//					(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//					|| (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//					|| (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			)))
//	{
//		if(emu)
//			emu->key_down(VK_RETURN, false, key_pressed_en);
//		key_pressed_en = true;
//		keyboard_x = 3;
//		keyboard_y = 3;
//		if(keyEnter != 0
//		   || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//		   || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			key_pressed_keyboard_close = true;
//		else
//			key_pressed_keyboard_stay_opened = true;
//	}
//	else if(key_pressed_en)
//	{
//		if(!is_keyboard_displayed && keyboard_counter > 0)
//		{
//			if(emu)
//				emu->key_down(VK_RETURN, false, true);
//		}
//		else
//		{
//			if(emu)
//				emu->key_up(VK_RETURN, false);
//			key_pressed_en = false;
//		}
//	}
//
//	if(key_pressed_keyboard_close)
//		is_keyboard_displayed = false;
//
//	if(config_changed)
//	{
//		apply_display_config();
//		RetroSaveSettings();
//	}
//
//	// If video config change
//	if (config.window_width != window_width_old || config.window_height != window_height_old || config.window_fps != window_fps_old)
//	{
//		struct retro_system_av_info av_info;
//		retro_get_system_av_info(&av_info);
//		av_info.timing.fps = config.window_fps;
//		RetroEnvironment(RETRO_ENVIRONMENT_SET_SYSTEM_AV_INFO, &av_info);
//
//		window_width_old = config.window_width;
//		window_height_old = config.window_height;
//		window_fps_old = config.window_fps;
//
//		// Recreate SDL surfaces and renderers if the size change
//		if(frame_surface->w != config.window_width || frame_surface->h != config.window_height)
//		{
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Resolution change detected\n", CORE_NAME);
//
//			// Free SDL main frame renderer
//			if (frame_renderer != NULL)
//			{
//				SDL_DestroyRenderer(frame_renderer);
//				frame_renderer = NULL;
////				Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL main surface renderer destroyed\n", CORE_NAME);
//			}
////			else
////				Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] No SDL main surface renderer, nothing to destroy\n", CORE_NAME);
//
//			// Free SDL main frame surface
//			if (frame_surface != NULL)
//			{
//				SDL_FreeSurface(frame_surface);
//				frame_surface = NULL;
////				Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL main surface destroyed\n", CORE_NAME);
//			}
////			else
////				Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] No SDL main surface, nothing to destroy\n", CORE_NAME);
//
//			// Create SDL main frame surface
//			frame_surface = SDL_CreateRGBSurface(SDL_SWSURFACE, config.window_width, config.window_height, 8*sizeof(uint32_t), 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
//			if (frame_surface == NULL)
//			{
//				Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL main surface creation failed! %s\n", CORE_NAME, SDL_GetError());
//				return;
//			}
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL main surface created\n", CORE_NAME);
//
//			// Create SDL main frame renderer
//			frame_renderer = SDL_CreateSoftwareRenderer(frame_surface);
//			if (frame_renderer == NULL)
//			{
//				Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL main surface renderer creation failed! %s\n", CORE_NAME, SDL_GetError());
//				return;
//			}
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL main surface renderer created\n", CORE_NAME);
//		}
//
//		// Paint it black
//		SDL_SetRenderDrawColor(frame_renderer, 0, 0, 0, 255);
//		SDL_RenderClear(frame_renderer);
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL main surface cleared\n", CORE_NAME);
//	}

	// Try rendering straight into VRAM if we can
//	struct retro_framebuffer fb = {0};
//	fb.width = EMU_WIDTH;
//	fb.height = EMU_HEIGHT;
//	fb.access_flags = RETRO_MEMORY_ACCESS_WRITE;
//	if (f_retro_environment(RETRO_ENVIRONMENT_GET_CURRENT_SOFTWARE_FRAMEBUFFER, &fb) && fb.format == RETRO_PIXEL_FORMAT_XRGB8888)
//	{
//		video_buffer = (uint32_t *)fb.data;
//		stride = (unsigned)fb.pitch >> 2;
//	}
//	else
//	{
		video_width  = 320;//EMU_WIDTH;
		video_height = 240;//EMU_HEIGHT;
		video_stride = video_width;
		if(!retro_frame_buffer)
			retro_frame_buffer = (uint32_t *)calloc(video_width * video_height, sizeof(uint32_t));
		video_buffer = retro_frame_buffer;
//	}

	if(power_on && o_emu)
	{
//		f_core_retro_log_printf(RETRO_LOG_DEBUG, "[%s] emu->run()\n", CORE_NAME);
		o_emu->f_run(video_buffer, video_stride);
	}
	else
	{
		uint32_t *line = video_buffer;
		uint32_t color = f_rgb_to_color(255, 190, 10);
		for (int16_t x, y = video_height, index_y; --y >= 0; )
		{
			for (x = video_width; --x >= 0; )
				line[x] = color;
			line += video_stride;
		}
	}

////	// Clear background, changing color every 60 frames
////	if (retro_frame_counter % FRAMES_PER_SEC == 0)
////	{
////		if (retro_frame_counter > UINT64_MAX - FRAMES_PER_SEC)
////			retro_frame_counter = 0;
////		color_index++;
////		if (color_index > 16)
////			color_index = 1;
////	}
////	color = palette_pc[color_index];
////	SDL_SetRenderDrawColor(frame_renderer, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), 255);
////	SDL_RenderClear(frame_renderer);
//
////    // Draw a lonely pixel (using an other color than background)
////    // with an ellipse around
////	color = palette_pc[colorIndex < 12 ? colorIndex + 4 : colorIndex - 12 ];  // Different color of background color
////	posx = 10;
////	posy = 10;
////	boxRGBA(frame_renderer, 4*posx, 3*posy, 4*posx+3, 3*posy+2, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), 255);
////	ellipseRGBA(frame_renderer, 4*posx+1, 3*posy+1, 10, 12, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), 255);
//
//	// Logo at startup
//	if(start_up_counter_logo < EMUSCV_LOGO_DURATION)
//	{
//		start_up_counter_logo += 300/config.window_fps;
//		if(start_up_counter_logo > EMUSCV_LOGO_DURATION)
//			start_up_counter_logo = EMUSCV_LOGO_DURATION;
//	}
//	if(start_up_counter_logo < EMUSCV_LOGO_DURATION)
//	{
//
//		// Draw an embedded binary image (128x128 or 64x64 or 32x32 pixels in ARGB8888 format)
//		uint8_t alpha = (start_up_counter_logo < 1200-100 ? 255: 255*(1200-start_up_counter_logo)/100);
//		unsigned char *image;
//		if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//		{
//			if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//				image = (unsigned char *)src_res_emuscvlady128x128xrgba8888_data;
//			else
//				image = (unsigned char *)src_res_emuscv128x128xrgba8888_data;
//			sizx = 128;
//			sizy = 8;
//		}
//		else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//		{
//			if(config.scv_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//				image = (unsigned char *)src_res_emuscvlady64x64xrgba8888_data;
//			else
//				image = (unsigned char *)src_res_emuscv64x64xrgba8888_data;
//			sizx = 64;
//			sizy = 4;
//		}
//		else //if(config.window_resolution == SETTING_RESOLUTION_LOW_VAL)
//		{
//			if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//				image = (unsigned char *)src_res_emuscvlady32x32xrgba8888_data;
//			else
//				image = (unsigned char *)src_res_emuscv32x32xrgba8888_data;
//			sizx = 32;
//			sizy = 2;
//		}
//		posx = config.window_width - sizx - sizy;
//		posy = config.window_height - sizx - sizy;
//		for (int x, y = sizx; --y >= 0; )
//			for (x = sizx; --x >= 0; )
//				pixelRGBA(frame_renderer, posx+x, posy+y, image[4*(x+y*sizx)], image[4*(x+y*sizx)+1], image[4*(x+y*sizx)+2], alpha*image[4*(x+y*sizx)+3]/255);
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] Logo drawn\n", CORE_NAME);
//
//		SDL_Rect RectSrc;
//		SDL_Rect RectDst;
//
//		// Alpha for logos
//		alpha = (start_up_counter_logo < EMUSCV_LOGO_DURATION-100 ? EMUSCV_LOGO_ALPHA: EMUSCV_LOGO_ALPHA*(EMUSCV_LOGO_DURATION-start_up_counter_logo)/100);
//
//		// Draw EmuSCV logo
//		RectSrc.x = 0;
//		RectSrc.y = 0;
//		RectSrc.w = RectDst.w = logo_surface->w;
//		RectSrc.h = RectDst.h = logo_surface->h;
//		RectDst.x = config.window_width - config.window_space - RectDst.w;
//		RectDst.y = config.window_height - config.window_space - RectDst.h;
//		SDL_SetSurfaceBlendMode(logo_surface, SDL_BLENDMODE_BLEND);
//		SDL_SetSurfaceAlphaMod(logo_surface, alpha);
//		if(SDL_BlitScaled(logo_surface, &RectSrc, frame_surface, &RectDst) != 0)
//			Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL blit scaled secondary->main failed! (emuSCV logo) %s\n", CORE_NAME, SDL_GetError());
////		else
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL blit scaled secondary->main ok (emuSCV logo)\n", CORE_NAME);
//
//		// Draw Recalbox logo
//		RectSrc.x = 0;
//		RectSrc.y = 0;
//		RectSrc.w = RectDst.w = recalbox_surface->w;
//		RectSrc.h = RectDst.h = recalbox_surface->h;
//		RectDst.x = config.window_space;
//		RectDst.y = config.window_height - config.window_space - RectDst.h;
//		SDL_SetSurfaceBlendMode(recalbox_surface, SDL_BLENDMODE_BLEND);
//		SDL_SetSurfaceAlphaMod(recalbox_surface, alpha);
//		if(SDL_BlitScaled(recalbox_surface, &RectSrc, frame_surface, &RectDst) != 0)
//			Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL blit scaled secondary->main failed! (Recalbox logo) %s\n", CORE_NAME, SDL_GetError());
////		else
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL blit scaled secondary->main ok (Recalbox logo)\n", CORE_NAME);
//	}
//
////	// Draw a PNG image read from ZIP file
////	if(image_data != NULL)
////	{
////		posx = 80;
////		posy = 30;
////		sizx = image_width;
////		sizy = image_height;
////		for (uint32_t y = 0; y < sizy; y++)
////			for (uint32_t x = 0; x < sizx; x++)
////				pixelRGBA(frame_renderer, posx+x, posy+sizy-y, image_data[4*(x+y*sizx)], image_data[4*(x+y*sizx)+1], image_data[4*(x+y*sizx)+2], image_data[4*(x+y*sizx)+3]);
////	}
//
//	// Display keyboard over picture?
//	if(keyboard_counter > 0)
//	{
//		// Draw quick keyboard
//		SDL_Rect RectSrc;
//		SDL_Rect RectDst;
//
//		// Bottom
//		RectSrc.x = 0;
//		RectSrc.w = RectDst.w = keyboard_surface->w;
//		if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//			RectSrc.y = 100;
//		else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//			RectSrc.y = 50;
//		else
//			RectSrc.y = 25;
//		RectSrc.h = RectDst.h = keyboard_surface->h - RectSrc.y;
//		RectDst.x = config.window_width - RectDst.w - 8 * config.window_space;
//		RectDst.y = RectSrc.y + 4 * config.window_space;
//		SDL_SetSurfaceBlendMode(keyboard_surface, SDL_BLENDMODE_BLEND);
//		SDL_SetSurfaceAlphaMod(keyboard_surface, EMUSCV_KEYBOARD_ALPHA * keyboard_counter / EMUSCV_KEYBOARD_DELAY);
//		if(SDL_BlitScaled(keyboard_surface, &RectSrc, frame_surface, &RectDst) != 0)
//			Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL blit scaled secondary->main failed! %s\n", CORE_NAME, SDL_GetError());
////		else
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL blit scaled secondary->main ok\n", CORE_NAME);
//
//		// Left
//		RectSrc.x = 0;
//		if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//		{
//			RectSrc.y = 60;
//			RectSrc.h = RectDst.h = 40;
//			RectSrc.w = RectDst.w = 64;
//		}
//		else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//		{
//			RectSrc.y = 30;
//			RectSrc.h = RectDst.h = 20;
//			RectSrc.w = RectDst.w = 32;
//		}
//		else
//		{
//			RectSrc.y = 15;
//			RectSrc.h = RectDst.h = 10;
//			RectSrc.w = RectDst.w = 16;
//		}
//		RectDst.x = config.window_width - keyboard_surface->w - 8 * config.window_space;
//		RectDst.y = RectSrc.y + 4 * config.window_space;
//		SDL_SetSurfaceBlendMode(keyboard_surface, SDL_BLENDMODE_BLEND);
//		SDL_SetSurfaceAlphaMod(keyboard_surface, EMUSCV_KEYBOARD_ALPHA * keyboard_counter / EMUSCV_KEYBOARD_DELAY);
//		if(SDL_BlitScaled(keyboard_surface, &RectSrc, frame_surface, &RectDst) != 0)
//			Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL blit scaled secondary->main failed! %s\n", CORE_NAME, SDL_GetError());
////		else
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL blit scaled secondary->main ok\n", CORE_NAME);
//
//		// Power button
//		if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//		{
//			RectSrc.x = (is_power_on ? 52 : 76);
//			RectDst.x = config.window_width - keyboard_surface->w - 8 * config.window_space + 64;
//			RectSrc.y = 60;
//			RectSrc.h = RectDst.h = 40;
//			RectSrc.w = RectDst.w = 68;
//		}
//		else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//		{
//			RectSrc.x = (is_power_on ? 26 : 38);
//			RectDst.x = config.window_width - keyboard_surface->w - 8 * config.window_space + 32;
//			RectSrc.y = 30;
//			RectSrc.h = RectDst.h = 20;
//			RectSrc.w = RectDst.w = 34;
//		}
//		else
//		{
//			RectSrc.x = (is_power_on ? 13 : 19);
//			RectDst.x = config.window_width - keyboard_surface->w - 8 * config.window_space + 16;
//			RectSrc.y = 15;
//			RectSrc.h = RectDst.h = 10;
//			RectSrc.w = RectDst.w = 17;
//		}
//		RectDst.y = RectSrc.y + 4 * config.window_space;
//		SDL_SetSurfaceBlendMode(keyboard_surface, SDL_BLENDMODE_BLEND);
//		SDL_SetSurfaceAlphaMod(keyboard_surface, EMUSCV_KEYBOARD_ALPHA * keyboard_counter / EMUSCV_KEYBOARD_DELAY);
//		if(SDL_BlitScaled(keyboard_surface, &RectSrc, frame_surface, &RectDst) != 0)
//			Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL blit scaled secondary->main failed! %s\n", CORE_NAME, SDL_GetError());
////		else
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL blit scaled secondary->main ok\n", CORE_NAME);
//
//		// Right
//		if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//		{
//			RectSrc.x = 132;
//			RectSrc.y = 60;
//			RectSrc.h = RectDst.h = 40;
//		}
//		else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//		{
//			RectSrc.x = 66;
//			RectSrc.y = 30;
//			RectSrc.h = RectDst.h = 20;
//		}
//		else
//		{
//			RectSrc.x = 33;
//			RectSrc.y = 15;
//			RectSrc.h = RectDst.h = 10;
//		}
//		RectSrc.w = RectDst.w = keyboard_surface->w - RectSrc.x;
//		RectDst.x = config.window_width - RectSrc.w - 8 * config.window_space;
//		RectDst.y = RectSrc.y + 4 * config.window_space;
//		SDL_SetSurfaceBlendMode(keyboard_surface, SDL_BLENDMODE_BLEND);
//		SDL_SetSurfaceAlphaMod(keyboard_surface, EMUSCV_KEYBOARD_ALPHA * keyboard_counter / EMUSCV_KEYBOARD_DELAY);
//		if(SDL_BlitScaled(keyboard_surface, &RectSrc, frame_surface, &RectDst) != 0)
//			Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL blit scaled secondary->main failed! %s\n", CORE_NAME, SDL_GetError());
////		else
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL blit scaled secondary->main ok\n", CORE_NAME);
//
//		// Top
//		RectSrc.x = 0;
//		RectSrc.y = 0;
//		RectSrc.w = RectDst.w = keyboard_surface->w;
//		if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//			RectSrc.h = RectDst.h = 60;
//		else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//			RectSrc.h = RectDst.h = 30;
//		else
//			RectSrc.h = RectDst.h = 15;
//		RectDst.x = config.window_width - RectDst.w - 8 * config.window_space;
//		RectDst.y = 4 * config.window_space;
//		SDL_SetSurfaceBlendMode(keyboard_surface, SDL_BLENDMODE_BLEND);
//		SDL_SetSurfaceAlphaMod(keyboard_surface, EMUSCV_KEYBOARD_ALPHA * keyboard_counter / EMUSCV_KEYBOARD_DELAY);
//		if(SDL_BlitScaled(keyboard_surface, &RectSrc, frame_surface, &RectDst) != 0)
//			Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL blit scaled secondary->main failed! %s\n", CORE_NAME, SDL_GetError());
////		else
////			Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL blit scaled secondary->main ok\n", CORE_NAME);
//
//		// Draw current selected key
//		if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//			color = palette_pc[0xA];	// Magenta
//		else
//			color = palette_pc[0x4];	// Green
//		if(key_pressed_keyboard_stay_opened || key_pressed_keyboard_close)
//			alpha = EMUSCV_KEYBOARD_ALPHAKEYDOWN;
//		else
//			alpha = EMUSCV_KEYBOARD_ALPHAKEYUP;
//		if(keyboard_x == 0)
//		{
//			if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//			{
//				if(keyboard_y == 0)
//				{
//					posy = 60;
//					sizey = 40;
//				}
//				else
//				{
//					posy = 78;
//					sizey = 38;
//					spacey = 42;
//				}
//				posx = 47;
//				sizex = 102;
//			}
//			else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//			{
//				if(keyboard_y == 0)
//				{
//					posy = 30;
//					sizey = 20;
//				}
//				else
//				{
//					posy = 39;
//					sizey = 19;
//					spacey = 21;
//				}
//				posx = 23;
//				sizex = 51;
//			}
//			else
//			{
//				if(keyboard_y == 0)
//				{
//					posy = 15;
//					sizey = 10;
//				}
//				else
//				{
//					posy = 19;
//					sizey = 10;
//					spacey = 10;
//				}
//				posx = 11;
//				sizex = 26;
//			}
//			uint8_t keyboard_y0 = keyboard_y;
//			if(keyboard_y0 > 1)
//				keyboard_y0--;
//			boxRGBA(frame_renderer, RectDst.x + posx, RectDst.y + posy + keyboard_y0 * (sizey + spacey), RectDst.x + posx + sizex, RectDst.y + posy + keyboard_y0 * (sizey + spacey) + sizey, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//
////			switch (keyboard_y)
////			{
////				case 0:
////					break;
////				case 1:
////					break;
////				case 2:
////				default:
////					break;
////			}
//		}
//		else
//		{
//			if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//			{
//				posx = 240;
//				posy = 67;
//				sizex = 38;
//				sizey = 31;
//				spacex = 20;
//				spacey = 26;
//			}
//			else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//			{
//				posx = 120;
//				posy = 34;
//				sizex = 18;
//				sizey = 17;
//				spacex = 11;
//				spacey = 11;
//			}
//			else
//			{
//				posx = 60;
//				posy = 16;
//				sizex = 10;
//				sizey = 8;
//				spacex = 4;
//				spacey = 6;
//			}
//			boxRGBA(frame_renderer, RectDst.x + posx + (keyboard_x - 1) * (sizex + spacex), RectDst.y + posy + keyboard_y * (sizey + spacey), RectDst.x + posx + (keyboard_x - 1) * (sizex + spacex) + sizex, RectDst.y + posy + keyboard_y * (sizey + spacey) + sizey, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		}
//	}
////	// Display menu over picture?
////	else if(is_menu_displayed)
////	{
////
////	}
//
//	// Display controls over all?
//	if(config.scv_displayinputs == SETTING_DISPLAYINPUTS_YES_VAL)
//	{
//		alpha = EMUSCV_CONTROLS_ALPHA;
//
//		if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//			space = 16;
//		else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//			space = 8;
//		else //if(config.window_resolution == SETTING_RESOLUTION_LOW_VAL)
//			space = 4;
//
//
//		// FIRST CONTROLLER (LEFT SIDE, ORANGE)
//		posx = space;
//		posy = config.window_height-26-17*space;
//		// Contour
//		color = palette_pc[1];	// Black
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy,             posx+5+4*space,   posy+3+2*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// L3
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+4+2*space,   posx+5+4*space,   posy+7+4*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// L2
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+8+4*space,   posx+5+4*space,   posy+11+6*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// L1
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+12+6*space,  posx+5+4*space,   posy+15+8*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// UP
//		rectangleRGBA(frame_renderer, posx,             posy+14+8*space,  posx+3+2*space,   posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// LEFT
//		rectangleRGBA(frame_renderer, posx+4+4*space,   posy+14+8*space,  posx+7+6*space,   posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// RIGHT
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+16+10*space, posx+5+4*space,   posy+19+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// DOWN
//		rectangleRGBA(frame_renderer, posx+1+space,     posy+20+12*space, posx+6+5*space,   posy+25+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// LEFT STICK
//		rectangleRGBA(frame_renderer, posx+8+6*space,   posy+14+8*space,  posx+11+8*space,  posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// SELECT
//		rectangleRGBA(frame_renderer, posx+12+8*space,  posy+14+8*space,  posx+15+10*space, posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// START
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy,             posx+21+14*space, posy+3+2*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// R3
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+4+2*space,   posx+21+14*space, posy+7+4*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// R2
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+8+4*space,   posx+21+14*space, posy+11+6*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// R1
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+12+6*space,  posx+21+14*space, posy+15+8*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 3 (up)
//		rectangleRGBA(frame_renderer, posx+16+10*space, posy+14+8*space,  posx+19+12*space, posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 4 (left)
//		rectangleRGBA(frame_renderer, posx+20+14*space, posy+14+8*space,  posx+23+16*space, posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 2 (right)
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+16+10*space, posx+21+14*space, posy+19+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 1 (down)
//		rectangleRGBA(frame_renderer, posx+17+11*space, posy+20+12*space, posx+22+15*space, posy+25+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// RIGHT STICK
//		color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+2+space,     posy+21+12*space, posx+4+5*space,   posy+23+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// LEFT STICK
//		boxRGBA(frame_renderer, posx+18+11*space, posy+21+12*space, posx+20+15*space, posy+23+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// RIGHT STICK
//		color = palette_pc[1];	// Black
//		lineRGBA(frame_renderer, posx+3+3*space, posy+21+13*space, posx+3+3*space, posy+22+15*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);			// LEFT STICK
//		lineRGBA(frame_renderer, posx+2+2*space, posy+22+14*space, posx+3+4*space, posy+22+14*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);			// LEFT STICK
//		lineRGBA(frame_renderer, posx+18+13*space, posy+21+13*space, posx+18+13*space, posy+22+15*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// RIGHT STICK
//		lineRGBA(frame_renderer, posx+17+12*space, posy+22+14*space, posx+18+14*space, posy+22+14*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// RIGHT STICK
//		// Button L3
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L3))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[6];	// Cyan
//		boxRGBA(frame_renderer, posx+3+2*space, posy+1, posx+3+4*space, posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button L2
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L2))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[2];	// Blue Light
//		boxRGBA(frame_renderer, posx+3+2*space, posy+5+2*space, posx+3+4*space, posy+5+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button L1
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[0];	// Blue medium
//		boxRGBA(frame_renderer, posx+3+2*space, posy+9+4*space, posx+3+4*space, posy+9+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button UP
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_UP))
//			color = palette_pc[4];	// Green light
//		else if(analoglefty0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrighty0 <= EMUSCV_AXIS_NEUTRAL_MIN)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+3+2*space, posy+13+6*space, posx+3+4*space, posy+13+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button LEFT
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_LEFT))
//			color = palette_pc[4];	// Green light
//		else if(analogleftx0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrightx0 <= EMUSCV_AXIS_NEUTRAL_MIN)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1, posy+15+8*space, posx+1+2*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button RIGHT
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_RIGHT))
//			color = palette_pc[4];	// Green light
//		else if(analogleftx0 >= EMUSCV_AXIS_NEUTRAL_MAX || analogrightx0 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+5+4*space, posy+15+8*space, posx+5+6*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button DOWN
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_DOWN))
//			color = palette_pc[4];	// Green light
//		else if(analoglefty0 >= EMUSCV_AXIS_NEUTRAL_MAX || analogrighty0 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+3+2*space, posy+17+10*space, posx+3+4*space, posy+17+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Left stick
//		if(analogleftx0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogleftx0 >= EMUSCV_AXIS_NEUTRAL_MAX || analoglefty0 <= EMUSCV_AXIS_NEUTRAL_MIN || analoglefty0 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[15];	// White
//		joyposx = (analogleftx0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogleftx0 >= EMUSCV_AXIS_NEUTRAL_MAX ? analogleftx0 : 0);
//		joyposy = (analoglefty0 <= EMUSCV_AXIS_NEUTRAL_MIN || analoglefty0 >= EMUSCV_AXIS_NEUTRAL_MAX ? analoglefty0 : 0);
//		boxRGBA(frame_renderer, (int16_t)(posx+3+3*space-space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space-space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, (int16_t)(posx+3+3*space+space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space+space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button SELECT
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_SELECT))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[13];	// Green dark
//		boxRGBA(frame_renderer, posx+9+6*space, posy+15+8*space, posx+9+8*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button START
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[5];	// Green soft
//		boxRGBA(frame_renderer, posx+13+8*space, posy+15+8*space, posx+13+10*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button R3
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R3))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[11];	// Pink
//		boxRGBA(frame_renderer, posx+19+12*space, posy+1, posx+19+14*space, posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button R2
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R2))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[10];	// Fuschia
//		boxRGBA(frame_renderer, posx+19+12*space, posy+5+2*space, posx+19+14*space, posy+5+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button R1
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[3];	// Purple
//		boxRGBA(frame_renderer, posx+19+12*space, posy+9+4*space, posx+19+14*space, posy+9+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 3 (up)
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_X))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[9];	// Orange
//		boxRGBA(frame_renderer, posx+19+12*space, posy+13+6*space,  posx+19+14*space, posy+13+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 4 (left)
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_Y))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[2];	// Blue light
//		boxRGBA(frame_renderer, posx+17+10*space, posy+15+8*space, posx+17+12*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 2 (right)
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_A))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_R)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[7];	// Green medium
//		boxRGBA(frame_renderer, posx+21+14*space, posy+15+8*space,  posx+21+16*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 1 (down)
//		if(ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_B))
//			color = palette_pc[4];	// Green light
//		else if((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[8];	// Red
//		boxRGBA(frame_renderer, posx+19+12*space, posy+17+10*space, posx+19+14*space, posy+17+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Right stick
//		if(analogrightx0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrightx0 >= EMUSCV_AXIS_NEUTRAL_MAX || analogrighty0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrighty0 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[15];	// White
//		joyposx = (analogrightx0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrightx0 >= EMUSCV_AXIS_NEUTRAL_MAX ? analogrightx0 : 0);
//		joyposy = (analogrighty0 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrighty0 >= EMUSCV_AXIS_NEUTRAL_MAX ? analogrighty0 : 0);
//		boxRGBA(frame_renderer, (int16_t)(posx+18+13*space-space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space-space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, (int16_t)(posx+18+13*space+space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space+space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//
//
//		// SECOND CONTROLLER (RIGHT SIDE, BLUE)
//		posx = config.window_width-24-17*space;
//		posy = config.window_height-26-17*space;
//		// Contour
//		color = palette_pc[1];	// Black
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy,             posx+5+4*space,   posy+3+2*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// L3
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+4+2*space,   posx+5+4*space,   posy+7+4*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// L2
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+8+4*space,   posx+5+4*space,   posy+11+6*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// L1
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+12+6*space,  posx+5+4*space,   posy+15+8*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// UP
//		rectangleRGBA(frame_renderer, posx,             posy+14+8*space,  posx+3+2*space,   posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// LEFT
//		rectangleRGBA(frame_renderer, posx+4+4*space,   posy+14+8*space,  posx+7+6*space,   posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// RIGHT
//		rectangleRGBA(frame_renderer, posx+2+2*space,   posy+16+10*space, posx+5+4*space,   posy+19+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// DOWN
//		rectangleRGBA(frame_renderer, posx+1+space,     posy+20+12*space, posx+6+5*space,   posy+25+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// LEFT STICK
//		rectangleRGBA(frame_renderer, posx+8+6*space,   posy+14+8*space,  posx+11+8*space,  posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// SELECT
//		rectangleRGBA(frame_renderer, posx+12+8*space,  posy+14+8*space,  posx+15+10*space, posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// START
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy,             posx+21+14*space, posy+3+2*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// R3
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+4+2*space,   posx+21+14*space, posy+7+4*space,   R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// R2
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+8+4*space,   posx+21+14*space, posy+11+6*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// R1
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+12+6*space,  posx+21+14*space, posy+15+8*space,  R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 3 (up)
//		rectangleRGBA(frame_renderer, posx+16+10*space, posy+14+8*space,  posx+19+12*space, posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 4 (left)
//		rectangleRGBA(frame_renderer, posx+20+14*space, posy+14+8*space,  posx+23+16*space, posy+17+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 2 (right)
//		rectangleRGBA(frame_renderer, posx+18+12*space, posy+16+10*space, posx+21+14*space, posy+19+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// BUTTON 1 (down)
//		rectangleRGBA(frame_renderer, posx+17+11*space, posy+20+12*space, posx+22+15*space, posy+25+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// RIGHT STICK
//		color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+2+space,     posy+21+12*space, posx+4+5*space,   posy+23+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// LEFT STICK
//		boxRGBA(frame_renderer, posx+18+11*space, posy+21+12*space, posx+20+15*space, posy+23+16*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// RIGHT STICK
//		color = palette_pc[1];	// Black
//		lineRGBA(frame_renderer, posx+3+3*space, posy+21+13*space, posx+3+3*space, posy+22+15*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);			// LEFT STICK
//		lineRGBA(frame_renderer, posx+2+2*space, posy+22+14*space, posx+3+4*space, posy+22+14*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);			// LEFT STICK
//		lineRGBA(frame_renderer, posx+18+13*space, posy+21+13*space, posx+18+13*space, posy+22+15*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// RIGHT STICK
//		lineRGBA(frame_renderer, posx+17+12*space, posy+22+14*space, posx+18+14*space, posy+22+14*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);		// RIGHT STICK
//		// Button L3
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L3))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[6];	// Cyan
//		boxRGBA(frame_renderer, posx+3+2*space, posy+1, posx+3+4*space, posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button L2
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L2))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[2];	// Blue Light
//		boxRGBA(frame_renderer, posx+3+2*space, posy+5+2*space, posx+3+4*space, posy+5+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button L1
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[0];	// Blue medium
//		boxRGBA(frame_renderer, posx+3+2*space, posy+9+4*space, posx+3+4*space, posy+9+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button UP
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_UP))
//			color = palette_pc[4];	// Green light
//		else if(analoglefty1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrighty1 <= EMUSCV_AXIS_NEUTRAL_MIN)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+3+2*space, posy+13+6*space, posx+3+4*space, posy+13+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button LEFT
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_LEFT))
//			color = palette_pc[4];	// Green light
//		else if(analogleftx1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrightx1 <= EMUSCV_AXIS_NEUTRAL_MIN)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1, posy+15+8*space, posx+1+2*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button RIGHT
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_RIGHT))
//			color = palette_pc[4];	// Green light
//		else if(analogleftx1 >= EMUSCV_AXIS_NEUTRAL_MAX || analogrightx1 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+5+4*space, posy+15+8*space, posx+5+6*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button DOWN
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_DOWN))
//			color = palette_pc[4];	// Green light
//		else if(analoglefty1 >= EMUSCV_AXIS_NEUTRAL_MAX || analogrighty1 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+3+2*space, posy+17+10*space, posx+3+4*space, posy+17+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Left stick
//		if(analogleftx1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogleftx1 >= EMUSCV_AXIS_NEUTRAL_MAX || analoglefty1 <= EMUSCV_AXIS_NEUTRAL_MIN || analoglefty1 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[15];	// White
//		joyposx = (analogleftx1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogleftx1 >= EMUSCV_AXIS_NEUTRAL_MAX ? analogleftx1 : 0);
//		joyposy = (analoglefty1 <= EMUSCV_AXIS_NEUTRAL_MIN || analoglefty1 >= EMUSCV_AXIS_NEUTRAL_MAX ? analoglefty1 : 0);
//		boxRGBA(frame_renderer, (int16_t)(posx+3+3*space-space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space-space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, (int16_t)(posx+3+3*space+space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space+space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button SELECT
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_SELECT))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[13];	// Green dark
//		boxRGBA(frame_renderer, posx+9+6*space, posy+15+8*space, posx+9+8*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button START
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[5];	// Green soft
//		boxRGBA(frame_renderer, posx+13+8*space, posy+15+8*space, posx+13+10*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button R3
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R3))
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[11];	// Pink
//		boxRGBA(frame_renderer, posx+19+12*space, posy+1, posx+19+14*space, posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button R2
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R2))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[10];	// Fuschia
//		boxRGBA(frame_renderer, posx+19+12*space, posy+5+2*space, posx+19+14*space, posy+5+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button R1
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[3];	// Purple
//		boxRGBA(frame_renderer, posx+19+12*space, posy+9+4*space, posx+19+14*space, posy+9+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 3 (up)
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_X))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_Y)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[9];	// Orange
//		boxRGBA(frame_renderer, posx+19+12*space, posy+13+6*space,  posx+19+14*space, posy+13+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 4 (left)
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_Y))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_X)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L2)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R2)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[2];	// Blue light
//		boxRGBA(frame_renderer, posx+17+10*space, posy+15+8*space, posx+17+12*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 2 (right)
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_A))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_R)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[7];	// Green medium
//		boxRGBA(frame_renderer, posx+21+14*space, posy+15+8*space,  posx+21+16*space, posy+15+10*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Button 1 (down)
//		if(ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_B))
//			color = palette_pc[4];	// Green light
//		else if((ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_L)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[8];	// Red
//		boxRGBA(frame_renderer, posx+19+12*space, posy+17+10*space, posx+19+14*space, posy+17+12*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Right stick
//		if(analogrightx1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrightx1 >= EMUSCV_AXIS_NEUTRAL_MAX || analogrighty1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrighty1 >= EMUSCV_AXIS_NEUTRAL_MAX)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[15];	// White
//		joyposx = (analogrightx1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrightx1 >= EMUSCV_AXIS_NEUTRAL_MAX ? analogrightx1 : 0);
//		joyposy = (analogrighty1 <= EMUSCV_AXIS_NEUTRAL_MIN || analogrighty1 >= EMUSCV_AXIS_NEUTRAL_MAX ? analogrighty1 : 0);
//		boxRGBA(frame_renderer, (int16_t)(posx+18+13*space-space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space-space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, (int16_t)(posx+18+13*space+space/4)+(int16_t)(1+2*space-space/4)*joyposx/INT16_MAX, (int16_t)(posy+22+14*space+space/4)+(int16_t)(1+2*space-space/4)*joyposy/INT16_MAX, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//
//
//		// KEYBOARD
//		posx = config.window_width/2-6*space;
//		posy = config.window_height-1-12*space;
//		// Contour
//		color = palette_pc[1];	// Black
//		rectangleRGBA(frame_renderer, posx,           posy,           posx+4+4*space,  posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// POWER ON/OFF BUTTON
//		rectangleRGBA(frame_renderer, posx,           posy+3+3*space, posx+4+4*space,  posy+4+5*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// RESET BUTTON
//		rectangleRGBA(frame_renderer, posx,           posy+6+6*space, posx+4+4*space,  posy+7+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// PAUSE BUTTON
//		rectangleRGBA(frame_renderer, posx+5*space,   posy,           posx+7*space,    posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 7
//		rectangleRGBA(frame_renderer, posx+1+7*space, posy,           posx+1+9*space,  posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 8
//		rectangleRGBA(frame_renderer, posx+2+9*space, posy,           posx+2+11*space, posy+1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 9
//		rectangleRGBA(frame_renderer, posx+5*space,   posy+2+2*space, posx+7*space,    posy+3+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 4
//		rectangleRGBA(frame_renderer, posx+1+7*space, posy+2+2*space, posx+1+9*space,  posy+3+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 5
//		rectangleRGBA(frame_renderer, posx+2+9*space, posy+2+2*space, posx+2+11*space, posy+3+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 6
//		rectangleRGBA(frame_renderer, posx+5*space,   posy+4+4*space, posx+7*space,    posy+5+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 1
//		rectangleRGBA(frame_renderer, posx+1+7*space, posy+4+4*space, posx+1+9*space,  posy+5+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 2
//		rectangleRGBA(frame_renderer, posx+2+9*space, posy+4+4*space, posx+2+11*space, posy+5+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 3
//		rectangleRGBA(frame_renderer, posx+5*space,   posy+6+6*space, posx+7*space,    posy+7+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// 0
//		rectangleRGBA(frame_renderer, posx+1+7*space, posy+6+6*space, posx+1+9*space,  posy+7+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// CL
//		rectangleRGBA(frame_renderer, posx+2+9*space, posy+6+6*space, posx+2+11*space, posy+7+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);	// EN
//
//		// Key POWER ON/OFF
//		if (keyPower != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1, posy+1, posx+2+4*space, posy-1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		color = palette_pc[1];	// Black
//		if (is_power_on)
////			rectangleRGBA(frame_renderer, posx+3+3*space, posy+1, posx+3+4*space, posy+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//			rectangleRGBA(frame_renderer, posx+3+2*space, posy+1, posx+3+3*space, posy+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		else
////			rectangleRGBA(frame_renderer, posx+1, posy+1, posx+1+space, posy+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//			rectangleRGBA(frame_renderer, posx+1+space, posy+1, posx+1+2*space, posy+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key RESET
//		if (key_pressed_reset)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1, posy+4+3*space, posx+2+4*space, posy+2+5*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key PAUSE
//		if (keyPause != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1, posy+7+6*space, posx+2+4*space, posy+5+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 7
//		if (key7 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1+5*space, posy+1, posx-2+7*space, posy-1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 8
//		if (key8 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+2+7*space, posy+1, posx-1+9*space,  posy-1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 9
//		if (key9 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+3+9*space, posy+1, posx+11*space, posy-1+2*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 4
//		if (key4 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1+5*space, posy+2+2*space, posx-2+7*space, posy+1+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 5
//		if (key5 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+2+7*space, posy+3+2*space, posx-1+9*space,  posy+1+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 6
//		if (key6 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer,posx+3+9*space, posy+3+2*space, posx+11*space, posy+1+4*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 1
//		if (key1 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1+5*space, posy+5+4*space, posx-2+7*space, posy+3+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 2
//		if (key2 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+2+7*space, posy+5+4*space, posx-1+9*space, posy+3+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 3
//		if (key3 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+3+9*space, posy+5+4*space, posx+11*space, posy+3+6*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key 0
//		if (key0 != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+1+5*space, posy+7+6*space, posx-2+7*space, posy+5+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key CL
//		if (keyClear != 0)
//			color = palette_pc[4];	// Green light
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+2+7*space, posy+7+6*space, posx-1+9*space,  posy+5+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
//		// Key EN
//		if (keyEnter != 0)
//			color = palette_pc[4];	// Green light
//		else if ((ret0 & (1 << RETRO_DEVICE_ID_JOYPAD_START)) || (ret1 & (1 << RETRO_DEVICE_ID_JOYPAD_START)))
//			color = palette_pc[0xc];	// Yellow
//		else
//			color = palette_pc[14];	// Gray
//		boxRGBA(frame_renderer, posx+3+9*space, posy+7+6*space, posx+11*space, posy+5+8*space, R_OF_COLOR(color), G_OF_COLOR(color), B_OF_COLOR(color), alpha);
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] Inputs drawn\n", CORE_NAME);
//	}
//
//	// Call audio callback
//	if(emu && is_power_on)
//	{
//		int16_t *sound_ptr = emu->get_osd()->get_sound_ptr();
//		if(!sound_ptr)
//		{
//			for (uint32_t i = sound_buffer_samples+1; --i > 0; )
//				RetroAudioSample(0, 0);
//		}
//		else
//		{
//			int16_t sound_val;
//			for (uint32_t i = 0; i < sound_buffer_samples; )
//			{
//				sound_val = sound_ptr[i];
//				RetroAudioSample(sound_val, sound_val);
//				i++;
//			}
//		}
//	}
//	else
//	{
//		for (uint32_t i = sound_buffer_samples+1; --i > 0; )
//		{
//			RetroAudioSample(sound_buffer_noise[sound_buffer_noise_index], sound_buffer_noise[sound_buffer_noise_index]);
//			if(++sound_buffer_noise_index >= EMUSCV_NOISE_SAMPLES)
//				sound_buffer_noise_index = rand() % (EMUSCV_NOISE_SAMPLES - 1);
//		}
//	}
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] => RetroAudioSample()\n", CORE_NAME);
//
//	// Call video callback
//	RetroVideoRefresh(frame_surface->pixels, config.window_width, config.window_height,  config.window_width*sizeof(uint32_t));
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] RetroVideoRefresh()\n", CORE_NAME);


/*
	// Try rendering straight into VRAM if we can
	uint32_t *frame_buffer;
	unsigned stride = 0;
	struct retro_framebuffer fb{};
	fb.width = 640;
	fb.height = 480;
	fb.access_flags = RETRO_MEMORY_ACCESS_WRITE;
	if (f_retro_environment(RETRO_ENVIRONMENT_GET_CURRENT_SOFTWARE_FRAMEBUFFER, &fb) && fb.format == RETRO_PIXEL_FORMAT_XRGB8888)
	{
		frame_buffer = (uint32_t *)fb.data;
		stride = (unsigned)fb.pitch >> 2;
	}
	else
	{
		if(!retro_frame_buffer)
			retro_frame_buffer = (uint32_t *)calloc(640 * 480, sizeof(uint32_t));
		frame_buffer = retro_frame_buffer;
		stride = 640;
	}

	uint32_t color_black	= 0x00000000;
	uint32_t color_white	= 0x00ffffff;
//	uint32_t color_red		= 0x00ff0000;
//	uint32_t color_green	= 0x0000ff00;

	uint32_t *line = frame_buffer;
//	for (uint16_t x, y = 0; y < 480; y++, line += stride)
//		for (x = 0; x < 640; x++)
//			line[x] = color_black;

	for (uint16_t x, y = -1, index_y; ++y < 480; )
	{
		index_y = y & 1;
		for (x = -1; ++x < 640; )
			line[x] = (index_y ^ (x & 1)) ? color_white : color_black;
		line += stride;
	}

	f_retro_video_refresh(frame_buffer, 640, 480, stride << 2);
*/




	// Display
	f_retro_video_refresh(video_buffer, video_width, video_height, video_stride * sizeof(uint32_t));

	// Increment the frame counter
	retro_frame_counter++;
}

//
// Libretro: reset the Libretro core
//
void c_core::f_retro_reset()
{
	// Log
//	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
//	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroReset()\n", CORE_NAME);
//	if(o_emu)
//	{
//		if(save_cart)
//			emu->save_cart(0);
//		emu->reset();
//	}
}

//
// Libretro: load core settings
//
void c_core::f_retro_load_settings()
{
	// Log
//	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
//	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroLoadSettings()\n", CORE_NAME);

//	config.sound_frequency = 5;
//	config.sound_latency = 0;
//
//	struct retro_variable var;
//
//	// CONSOLE
//	config.scv_console = SETTING_CONSOLE_AUTO_VAL;
//	var.key   = SETTING_CONSOLE_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[16];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_CONSOLE_EPOCH_KEY)) == 0)
//			config.scv_console = SETTING_CONSOLE_EPOCH_VAL;
//		else if(strcmp(str, _T(SETTING_CONSOLE_YENO_KEY)) == 0)
//			config.scv_console = SETTING_CONSOLE_YENO_VAL;
//		else if(strcmp(str, _T(SETTING_CONSOLE_EPOCHLADY_KEY)) == 0)
//			config.scv_console = SETTING_CONSOLE_EPOCHLADY_VAL;
//	}
//	switch(config.scv_console)
//	{
//		case SETTING_CONSOLE_EPOCH_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Console setting: EPOCH\n", CORE_NAME);
//			break;
//		case SETTING_CONSOLE_YENO_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Console setting: YENO\n", CORE_NAME);
//			break;
//		case SETTING_CONSOLE_EPOCHLADY_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Console setting: EPOCH LADY\n", CORE_NAME);
//			break;
//		case SETTING_CONSOLE_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Console setting: automatic (EPOCH)\n", CORE_NAME);
//			break;
//	}
//
//	// DISPLAY
//	config.scv_display = SETTING_DISPLAY_AUTO_VAL;
//	var.key   = SETTING_DISPLAY_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_DISPLAY_EMUSCV_KEY)) == 0)
//			config.scv_display = SETTING_DISPLAY_EMUSCV_VAL;
//		else if(strcmp(str, _T(SETTING_DISPLAY_EPOCH_KEY)) == 0)
//			config.scv_display = SETTING_DISPLAY_EPOCH_VAL;
//		else if(strcmp(str, _T(SETTING_DISPLAY_YENO_KEY)) == 0)
//			config.scv_display = SETTING_DISPLAY_YENO_VAL;
//	}
//	switch(config.scv_display)
//	{
//		case SETTING_DISPLAY_EMUSCV_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display setting: as EmuSCV (custom)\n", CORE_NAME);
//			break;
//		case SETTING_DISPLAY_EPOCH_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display setting: as EPOCH (genuine)\n", CORE_NAME);
//			break;
//		case SETTING_DISPLAY_YENO_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display setting: as YENO (genuine)\n", CORE_NAME);
//			break;
//		case SETTING_DISPLAY_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display setting: automatic (depending of console option)\n", CORE_NAME);
//			break;
//	}
//
//	// PIXEL ASPECT
//	config.scv_pixelaspect = SETTING_PIXELASPECT_AUTO_VAL;
//	var.key   = SETTING_PIXELASPECT_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_PIXELASPECT_RECTANGULAR_KEY)) == 0)
//			config.scv_pixelaspect = SETTING_PIXELASPECT_RECTANGULAR_VAL;
//		else if(strcmp(str, _T(SETTING_PIXELASPECT_SQUARE_KEY)) == 0)
//			config.scv_pixelaspect = SETTING_PIXELASPECT_SQUARE_VAL;
//	}
//	switch(config.scv_pixelaspect)
//	{
//		case SETTING_PIXELASPECT_RECTANGULAR_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Pixel aspect setting: rectangular (genuine)\n", CORE_NAME);
//			break;
//		case SETTING_PIXELASPECT_SQUARE_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Pixel aspect setting: square (custom)\n", CORE_NAME);
//			break;
//		case SETTING_PIXELASPECT_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Pixel aspect setting: automatic (rectangular)\n", CORE_NAME);
//			break;
//	}
//
//	// RESOLUTION
//	config.scv_resolution = SETTING_RESOLUTION_AUTO_VAL;
//	var.key   = SETTING_RESOLUTION_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_RESOLUTION_LOW_KEY)) == 0)
//			config.scv_resolution = SETTING_RESOLUTION_LOW_VAL;
//		else if(strcmp(str, _T(SETTING_RESOLUTION_MEDIUM_KEY)) == 0)
//			config.scv_resolution = SETTING_RESOLUTION_MEDIUM_VAL;
//		else if(strcmp(str, _T(SETTING_RESOLUTION_HIGH_KEY)) == 0)
//			config.scv_resolution = SETTING_RESOLUTION_HIGH_VAL;
//	}
//	switch(config.scv_resolution)
//	{
//		case SETTING_RESOLUTION_LOW_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Resolution setting: low\n", CORE_NAME);
//			break;
//		case SETTING_RESOLUTION_MEDIUM_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Resolution setting: medium\n", CORE_NAME);
//			break;
//		case SETTING_RESOLUTION_HIGH_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Resolution setting: high\n", CORE_NAME);
//			break;
//		case SETTING_RESOLUTION_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Resolution setting: automatic (depending of the platform)\n", CORE_NAME);
//			break;
//	}
//
//	// PALETTE
//	config.scv_palette = SETTING_PALETTE_AUTO_VAL;
//	var.key   = SETTING_PALETTE_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_PALETTE_STANDARD_KEY)) == 0)
//			config.scv_palette = SETTING_PALETTE_STANDARD_VAL;
//		else if(strcmp(str, _T(SETTING_PALETTE_OLDNTSC_KEY)) == 0)
//			config.scv_palette = SETTING_PALETTE_OLDNTSC_VAL;
//	}
//	switch(config.scv_palette)
//	{
//		case SETTING_PALETTE_STANDARD_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Palette setting: standard PAL/NEW NTSC colors\n", CORE_NAME);
//			break;
//		case SETTING_PALETTE_OLDNTSC_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Resolution setting: old NTSC colors\n", CORE_NAME);
//			break;
//		case SETTING_PALETTE_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Resolution setting: automatic (standard PAL/NEW NTSC colors)\n", CORE_NAME);
//			break;
//	}
//
//	// FPS
//	config.scv_fps = SETTING_FPS_AUTO_VAL;
//	var.key   = SETTING_FPS_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_FPS_EPOCH60_KEY)) == 0)
//			config.scv_fps = SETTING_FPS_EPOCH60_VAL;
//		else if(strcmp(str, _T(SETTING_FPS_YENO50_KEY)) == 0)
//			config.scv_fps = SETTING_FPS_YENO50_VAL;
//	}
//	switch(config.scv_fps)
//	{
//		case SETTING_FPS_EPOCH60_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] FPS setting: 60 (EPOCH)\n", CORE_NAME);
//			break;
//		case SETTING_FPS_YENO50_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] FPS setting: 50 (YENO)\n", CORE_NAME);
//			break;
//		case SETTING_FPS_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] FPS setting: automatic (depending of the console option)\n", CORE_NAME);
//			break;
//	}
//
//	// DISPLAY FULL
//	config.scv_displayfullmemory = SETTING_DISPLAYFULLMEMORY_AUTO_VAL;
//	var.key   = SETTING_DISPLAYFULLMEMORY_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_DISPLAYFULLMEMORY_YES_KEY)) == 0)
//			config.scv_displayfullmemory = SETTING_DISPLAYFULLMEMORY_YES_VAL;
//		else if(strcmp(str, _T(SETTING_DISPLAYFULLMEMORY_NO_KEY)) == 0)
//			config.scv_displayfullmemory = SETTING_DISPLAYFULLMEMORY_NO_VAL;
//	}
//	switch(config.scv_displayfullmemory)
//	{
//		case SETTING_DISPLAYFULLMEMORY_YES_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display full memory setting: yes (developer mode)\n", CORE_NAME);
//			break;
//		case SETTING_DISPLAYFULLMEMORY_NO_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display full memory setting: no (normal mode)\n", CORE_NAME);
//			break;
//		case SETTING_DISPLAYFULLMEMORY_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display full memory setting: automatic (no)\n", CORE_NAME);
//			break;
//	}
//
//	// DISPLAY INPUTS
//	config.scv_displayinputs = SETTING_DISPLAYINPUTS_AUTO_VAL;
//	var.key   = SETTING_DISPLAYINPUTS_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_DISPLAYINPUTS_YES_KEY)) == 0)
//			config.scv_displayinputs = SETTING_DISPLAYINPUTS_YES_VAL;
//		else if(strcmp(str, _T(SETTING_DISPLAYINPUTS_NO_KEY)) == 0)
//			config.scv_displayinputs = SETTING_DISPLAYINPUTS_NO_VAL;
//	}
//	switch(config.scv_displayinputs)
//	{
//		case SETTING_DISPLAYINPUTS_YES_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display inputs setting: yes (developer mode)\n", CORE_NAME);
//			break;
//		case SETTING_DISPLAYINPUTS_NO_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display inputs setting: no (normal mode)\n", CORE_NAME);
//			break;
//		case SETTING_DISPLAYINPUTS_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Display inputs setting: automatic (no)\n", CORE_NAME);
//			break;
//	}
//
//	// LANGAGE
//	config.scv_langage	= SETTING_LANGAGE_AUTO_VAL;
//	var.key		= SETTING_LANGAGE_KEY;
//	var.value	= NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_LANGAGE_JP_KEY)) == 0)
//			config.scv_langage = SETTING_LANGAGE_JP_VAL;
//		else if(strcmp(str, _T(SETTING_LANGAGE_FR_KEY)) == 0)
//			config.scv_langage = SETTING_LANGAGE_FR_VAL;
//		else if(strcmp(str, _T(SETTING_LANGAGE_EN_KEY)) == 0)
//			config.scv_langage = SETTING_LANGAGE_EN_VAL;
//	}
//	switch(config.scv_langage)
//	{
//		case SETTING_LANGAGE_JP_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Langage setting: Japanese (genuine EPOCH)\n", CORE_NAME);
//			break;
//		case SETTING_LANGAGE_FR_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Langage setting: French (genuine YENO)\n", CORE_NAME);
//			break;
//		case SETTING_LANGAGE_EN_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Langage setting: English (custom)\n", CORE_NAME);
//			break;
//		case SETTING_LANGAGE_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Langage setting: automatic (depending of console)\n", CORE_NAME);
//			break;
//	}
//
//	// CHECK BIOS
//	config.scv_checkbios = SETTING_CHECKBIOS_AUTO_VAL;
//	var.key   = SETTING_CHECKBIOS_KEY;
//	var.value = NULL;
//	if (RetroEnvironment(RETRO_ENVIRONMENT_GET_VARIABLE, &var) && var.value)
//	{
//		_TCHAR str[sizeof(var.value)];
//		memset(str, 0, sizeof(str));
//		strncpy(str, var.value, sizeof(str));
//		if(strcmp(str, _T(SETTING_CHECKBIOS_YES_KEY)) == 0)
//			config.scv_checkbios = SETTING_CHECKBIOS_YES_VAL;
//		else if(strcmp(str, _T(SETTING_CHECKBIOS_NO_KEY)) == 0)
//			config.scv_checkbios = SETTING_CHECKBIOS_NO_VAL;
//	}
//	switch(config.scv_checkbios)
//	{
//		case SETTING_CHECKBIOS_YES_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Check BIOS setting: yes\n", CORE_NAME);
//			break;
//		case SETTING_CHECKBIOS_NO_VAL:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Check BIOS setting: no\n", CORE_NAME);
//			break;
//		case SETTING_CHECKBIOS_AUTO_VAL:
//		default:
//			Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] Check BIOS setting: automatic (yes)\n", CORE_NAME);
//			break;
//	}
//
//	apply_display_config();
//	if(config.window_palette == SETTING_PALETTE_OLDNTSC_VAL)
//		palette_pc = (scrntype_t *)palette_ntsc;
//	else
//		palette_pc = (scrntype_t *)palette_pal;
//	if(emu)
//	{
//		emu->get_osd()->reset_palette();
//		emu->get_osd()->reset_sound();
//	}
//	sound_buffer_samples = (AUDIO_SAMPLING_RATE / config.window_fps + 0.5);
//
//	// Create quick keyboard SDL surface & renderer
//	CreateKeyboardSurface();
//
//	// Create EmuSCV logo SDL surface & renderer
//	CreateLogoSurface();
//
//	// Create Recalbox logo SDL surface & renderer
//	CreateRecalboxSurface();

//	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] apply_display_config()\n", CORE_NAME);
}

//
// Libretro: save core settings
//
void c_core::f_retro_save_settings()
{
	// Log
//	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
//	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroSaveSettings()\n", CORE_NAME);

//	struct retro_variable variable[11];
//
//	variable[0].key = SETTING_CONSOLE_KEY;
//	switch(config.scv_console)
//	{
//		case SETTING_CONSOLE_EPOCH_VAL:
//			variable[0].value = "CONSOLE; EPOCH";
//			break;
//		case SETTING_CONSOLE_YENO_VAL:
//			variable[0].value = "CONSOLE; YENO";
//			break;
//		case SETTING_CONSOLE_EPOCHLADY_VAL:
//			variable[0].value = "CONSOLE; EPOCHLADY";
//			break;
//		case SETTING_CONSOLE_AUTO_VAL:
//		default:
//			variable[0].value = "CONSOLE; AUTO";
//			break;
//	}
//
//	variable[1].key = SETTING_DISPLAY_KEY;
//	switch(config.scv_display)
//	{
//		case SETTING_DISPLAY_EMUSCV_VAL:
//			variable[1].value = "DISPLAY; EMUSCV";
//			break;
//		case SETTING_DISPLAY_EPOCH_VAL:
//			variable[1].value = "DISPLAY; EPOCH";
//			break;
//		case SETTING_DISPLAY_YENO_VAL:
//			variable[1].value = "DISPLAY; YENO";
//			break;
//		case SETTING_DISPLAY_AUTO_VAL:
//		default:
//			variable[1].value = "DISPLAY; AUTO";
//			break;
//	}
//
//	variable[2].key = SETTING_PIXELASPECT_KEY;
//	switch(config.scv_pixelaspect)
//	{
//		case SETTING_PIXELASPECT_SQUARE_VAL:
//			variable[2].value = "PIXELASPECT; SQUARE";
//			break;
//		case SETTING_PIXELASPECT_RECTANGULAR_VAL:
//			variable[2].value = "PIXELASPECT; RECTANGULAR";
//			break;
//		case SETTING_PIXELASPECT_AUTO_VAL:
//		default:
//			variable[2].value = "PIXELASPECT; AUTO";
//			break;
//	}
//
//	variable[3].key = SETTING_RESOLUTION_KEY;
//	switch(config.scv_resolution)
//	{
//		case SETTING_RESOLUTION_LOW_VAL:
//			variable[3].value = "RESOLUTION; LOW";
//			break;
//		case SETTING_RESOLUTION_MEDIUM_VAL:
//			variable[3].value = "RESOLUTION; MEDIUM";
//			break;
//		case SETTING_RESOLUTION_HIGH_VAL:
//			variable[3].value = "RESOLUTION; HIGH";
//			break;
//		case SETTING_RESOLUTION_AUTO_VAL:
//		default:
//			variable[3].value = "RESOLUTION; AUTO";
//			break;
//	}
//
//	variable[4].key = SETTING_PALETTE_KEY;
//	switch(config.scv_palette)
//	{
//		case SETTING_PALETTE_STANDARD_VAL:
//			variable[4].value = "PALETTE; STANDARD";
//			break;
//		case SETTING_PALETTE_OLDNTSC_VAL:
//			variable[4].value = "PALETTE; OLDNTSC";
//			break;
//		case SETTING_PALETTE_AUTO_VAL:
//		default:
//			variable[4].value = "PALETTE; AUTO";
//			break;
//	}
//
//	variable[5].key = SETTING_FPS_KEY;
//	switch(config.scv_fps)
//	{
//		case SETTING_FPS_EPOCH60_VAL:
//			variable[5].value = "FPS; EPOCH60";
//			break;
//		case SETTING_FPS_YENO50_VAL:
//			variable[5].value = "FPS; YENO50";
//			break;
//		case SETTING_FPS_AUTO_VAL:
//		default:
//			variable[5].value = "FPS; AUTO";
//			break;
//	}
//
//	variable[6].key = SETTING_DISPLAYFULLMEMORY_KEY;
//	switch(config.scv_displayfullmemory)
//	{
//		case SETTING_DISPLAYFULLMEMORY_YES_VAL:
//			variable[6].value = "DISPLAYFULLMEMORY; YES";
//			break;
//		case SETTING_DISPLAYFULLMEMORY_NO_VAL:
//			variable[6].value = "DISPLAYFULLMEMORY; NO";
//			break;
//		case SETTING_DISPLAYFULLMEMORY_AUTO_VAL:
//		default:
//			variable[6].value = "DISPLAYFULLMEMORY; AUTO";
//			break;
//	}
//
//	variable[7].key = SETTING_DISPLAYINPUTS_KEY;
//	switch(config.scv_displayinputs)
//	{
//		case SETTING_DISPLAYINPUTS_YES_VAL:
//			variable[7].value = "DISPLAYINPUTS; YES";
//			break;
//		case SETTING_DISPLAYINPUTS_NO_VAL:
//			variable[7].value = "DISPLAYINPUTS; NO";
//			break;
//		case SETTING_DISPLAYINPUTS_AUTO_VAL:
//		default:
//			variable[7].value = "DISPLAYINPUTS; AUTO";
//			break;
//	}
//
//	variable[8].key = SETTING_LANGAGE_KEY;
//	switch(config.scv_langage)
//	{
//		case SETTING_LANGAGE_JP_VAL:
//			variable[8].value = "LANGAGE; JP";
//			break;
//		case SETTING_LANGAGE_FR_VAL:
//			variable[8].value = "LANGAGE; FR";
//			break;
//		case SETTING_LANGAGE_EN_VAL:
//			variable[8].value = "LANGAGE; EN";
//			break;
//		case SETTING_LANGAGE_AUTO_VAL:
//		default:
//			variable[8].value = "LANGAGE; AUTO";
//			break;
//	}
//
//	variable[9].key = SETTING_CHECKBIOS_KEY;
//	switch(config.scv_checkbios)
//	{
//		case SETTING_CHECKBIOS_NO_VAL:
//			variable[9].value = "CHECK BIOS; NO";
//			break;
//		case SETTING_CHECKBIOS_YES_VAL:
//			variable[9].value = "CHECK BIOS; YES";
//			break;
//		case SETTING_CHECKBIOS_AUTO_VAL:
//		default:
//			variable[9].value = "CHECK BIOS; AUTO";
//			break;
//	}
//
//	variable[10].key = NULL;
//	variable[10].value = NULL;
//
//	RetroEnvironment(RETRO_ENVIRONMENT_SET_VARIABLES, variable);
//
//	// Set possible values
//	variable[0].value = "CONSOLE; AUTO|EPOCH|YENO|EPOCHLADY";
//	variable[1].value = "DISPLAY; AUTO|EMUSCV|EPOCH|YENO";
//	variable[2].value = "PIXELASPECT; AUTO|RECTANGULAR|SQUARE";
//	variable[3].value = "RESOLUTION; AUTO|LOW|MEDIUM|HIGH";
//	variable[4].value = "PALETTE; AUTO|STANDARD|OLDNTSC";
//	variable[5].value = "FPS; AUTO|EPOCH60|YENO50";
//	variable[6].value = "DISPLAYFULLMEMORY; AUTO|YES|NO";
//	variable[7].value = "DISPLAYINPUTS; AUTO|YES|NO";
//	variable[8].value = "LANGAGE; AUTO|JP|FR|EN";
//	variable[9].value = "CHECKBIOS; AUTO|YES|NO";
//	RetroEnvironment(RETRO_ENVIRONMENT_SET_VARIABLES, variable);
//
//	apply_display_config();
//	if(config.window_palette == SETTING_PALETTE_OLDNTSC_VAL)
//		palette_pc = (scrntype_t *)palette_ntsc;
//	else
//		palette_pc = (scrntype_t *)palette_pal;
//	if(emu)
//	{
//		emu->get_osd()->reset_palette();
//		emu->get_osd()->reset_sound();
//	}
//	sound_buffer_samples = (AUDIO_SAMPLING_RATE / config.window_fps + 0.5);
}

//
// Create the SDL surface and the SDL renderer and load the picture for the quick keyboard overlay
//
//bool cCore::CreateKeyboardSurface()
//{
//	int x, y, w, h;
//	unsigned char *image;
//
//
//	DestroyKeyboardSurface();
//
//	if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//	{
//		w = 460;
//		h = 312;
//		if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//			image = (unsigned char *)src_res_keyboardlady460x312xrgba8888_data;
//		else if(config.window_console == SETTING_CONSOLE_YENO_VAL)
//			image = (unsigned char *)src_res_keyboardyeno460x312xrgba8888_data;
//		else
//			image = (unsigned char *)src_res_keyboardepoch460x312xrgba8888_data;
//	}
//	else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//	{
//		w = 230;
//		h = 156;
//		if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//			image = (unsigned char *)src_res_keyboardlady230x156xrgba8888_data;
//		else if(config.window_console == SETTING_CONSOLE_YENO_VAL)
//			image = (unsigned char *)src_res_keyboardyeno230x156xrgba8888_data;
//		else
//			image = (unsigned char *)src_res_keyboardepoch230x156xrgba8888_data;
//	}
//	else
//	{
//		w = 115;
//		h = 78;
//		if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//			image = (unsigned char *)src_res_keyboardlady115x78xrgba8888_data;
//		else if(config.window_console == SETTING_CONSOLE_YENO_VAL)
//			image = (unsigned char *)src_res_keyboardyeno115x78xrgba8888_data;
//		else
//			image = (unsigned char *)src_res_keyboardepoch115x78xrgba8888_data;
//	}
//
//	// Create SDL quick keyboard surface
//	keyboard_surface = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 8*sizeof(uint32_t), 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
//	if (!keyboard_surface)
//	{
//		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL secondary surface creation failed! (quick keyboard) %s\n", CORE_NAME, SDL_GetError());
//		return false;
//	}
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface created (quick keyboard)\n", CORE_NAME);
//
//	// Create SDL quick keyboard renderer
//	keyboard_renderer = SDL_CreateSoftwareRenderer(keyboard_surface);
//	if (!keyboard_renderer)
//	{
//		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL secondary surface renderer creation failed! (quick keyboard) %s\n", CORE_NAME, SDL_GetError());
//		return false;
//	}
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface renderer created (quick keyboard)\n", CORE_NAME);
//
//	// Draw an embedded binary image
//	for (int16_t x, y = h; --y >= 0; )
//		for (x = w; --x >= 0; )
//			pixelRGBA(keyboard_renderer, x, y, image[4*(x+y*w)], image[4*(x+y*w)+1], image[4*(x+y*w)+2], image[4*(x+y*w)+3]);
//
//	return true;
//}

//
// Destroy the SDL surface and the SDL renderer for the quick keyboard overlay
//
//bool cCore::DestroyKeyboardSurface()
//{
//	// Free SDL quick keyboard renderer
//	if (keyboard_renderer)
//	{
//		SDL_DestroyRenderer(keyboard_renderer);
//		keyboard_renderer = NULL;
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface renderer destroyed (quick keyboard)\n", CORE_NAME);
//	}
////	else
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] No SDL secondary surface renderer (quick keyboard), nothing to destroy\n", CORE_NAME);
//
//	// Free SDL quick keyboard surface
//	if (keyboard_surface)
//	{
//		SDL_FreeSurface(keyboard_surface);
//		keyboard_surface = NULL;
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface destroyed (quick keyboard)\n", CORE_NAME);
//	}
//
//	return true;
//}

//
// Create the SDL surface and the SDL renderer and load the picture for the emuSCV logo
//
//bool cCore::CreateLogoSurface()
//{
//	int x, y, w, h;
//	unsigned char *image;
//
//
//	DestroyLogoSurface();
//
//	if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//	{
//		w = 128;
//		h = 128;
//		if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//			image = (unsigned char *)src_res_emuscvlady128x128xrgba8888_data;
//		else
//			image = (unsigned char *)src_res_emuscv128x128xrgba8888_data;
//	}
//	else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//	{
//		w = 64;
//		h = 64;
//		if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//			image = (unsigned char *)src_res_emuscvlady64x64xrgba8888_data;
//		else
//			image = (unsigned char *)src_res_emuscv64x64xrgba8888_data;
//	}
//	else
//	{
//		w = 32;
//		h = 32;
//		if(config.window_console == SETTING_CONSOLE_EPOCHLADY_VAL)
//			image = (unsigned char *)src_res_emuscvlady32x32xrgba8888_data;
//		else
//			image = (unsigned char *)src_res_emuscv32x32xrgba8888_data;
//	}
//
//	// Create SDL quick keyboard surface
//	logo_surface = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 8*sizeof(uint32_t), 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
//	if (!logo_surface)
//	{
//		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL secondary surface creation failed! (emuSCV logo) %s\n", CORE_NAME, SDL_GetError());
//		return false;
//	}
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface created (emuSCV logo)\n", CORE_NAME);
//
//	// Create SDL quick keyboard renderer
//	logo_renderer = SDL_CreateSoftwareRenderer(logo_surface);
//	if (!logo_renderer)
//	{
//		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL secondary surface renderer creation failed! (emuSCV logo) %s\n", CORE_NAME, SDL_GetError());
//		return false;
//	}
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface renderer created (emuSCV logo)\n", CORE_NAME);
//
//	// Draw an embedded binary image
//	for (int16_t x, y = h; --y >= 0; )
//		for (x = w; --x >= 0; )
//			pixelRGBA(logo_renderer, x, y, image[4*(x+y*w)], image[4*(x+y*w)+1], image[4*(x+y*w)+2], image[4*(x+y*w)+3]);
//
//	return true;
//}

//
// Destroy the SDL surface and the SDL renderer for the EmuSCV logo
//
//bool cCore::DestroyLogoSurface()
//{
//	// Free SDL quick keyboard renderer
//	if (logo_renderer)
//	{
//		SDL_DestroyRenderer(logo_renderer);
//		logo_renderer = NULL;
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface renderer destroyed (emuSCV logo)\n", CORE_NAME);
//	}
////	else
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] No SDL secondary surface renderer v, nothing to destroy\n", CORE_NAME);
//
//	// Free SDL quick keyboard surface
//	if (logo_surface)
//	{
//		SDL_FreeSurface(logo_surface);
//		logo_surface = NULL;
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface destroyed (emuSCV logo)\n", CORE_NAME);
//	}
//
//	return true;
//}

//
// Create the SDL surface and the SDL renderer and load the picture for the Recalbox logo
//
//bool cCore::CreateRecalboxSurface()
//{
//	int x, y, w, h;
//	unsigned char *image;
//
//
//	DestroyRecalboxSurface();
//
//	if(config.window_resolution == SETTING_RESOLUTION_HIGH_VAL)
//	{
//		w = 624;
//		h = 160;
//		image = (unsigned char *)src_res_recalbox624x160xrgba8888_data;
//	}
//	else if(config.window_resolution == SETTING_RESOLUTION_MEDIUM_VAL)
//	{
//		w = 312;
//		h = 80;
//		image = (unsigned char *)src_res_recalbox312x80xrgba8888_data;
//	}
//	else
//	{
//		w = 156;
//		h = 40;
//		image = (unsigned char *)src_res_recalbox156x40xrgba8888_data;
//	}
//
//	// Create SDL quick keyboard surface
//	recalbox_surface = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 8*sizeof(uint32_t), 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
//	if(!recalbox_surface)
//	{
//		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL secondary surface creation failed! (Recalbox logo) %s\n", CORE_NAME, SDL_GetError());
//		return false;
//	}
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface created (Recalbox logo)\n", CORE_NAME);
//
//	// Create SDL Recalbox logo renderer
//	recalbox_renderer = SDL_CreateSoftwareRenderer(recalbox_surface);
//	if(!recalbox_renderer)
//	{
//		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] SDL secondary surface renderer creation failed! (Recalbox logo) %s\n", CORE_NAME, SDL_GetError());
//		return false;
//	}
////	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface renderer created (Recalbox logo)\n", CORE_NAME);
//
//	// Draw an embedded binary image
//	for(int16_t x, y = h; --y >= 0; )
//		for(x = w; --x >= 0; )
//			pixelRGBA(recalbox_renderer, x, y, image[4*(x+y*w)], image[4*(x+y*w)+1], image[4*(x+y*w)+2], image[4*(x+y*w)+3]);
//
//	return true;
//}

//
// Destroy the SDL surface and the SDL renderer for the quick keyboard overlay
//
//bool cCore::DestroyRecalboxSurface()
//{
//	// Free SDL quick keyboard renderer
//	if(recalbox_renderer != NULL)
//	{
//		SDL_DestroyRenderer(recalbox_renderer);
//		recalbox_renderer = NULL;
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface renderer destroyed (Recalbox logo)\n", CORE_NAME);
//	}
////	else
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] No SDL secondary surface renderer (Recalbox logo), nothing to destroy\n", CORE_NAME);
//
//	// Free SDL quick keyboard surface
//	if (recalbox_surface != NULL)
//	{
//		SDL_FreeSurface(recalbox_surface);
//		recalbox_surface = NULL;
////		Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] SDL secondary surface destroyed (Recalbox logo)\n", CORE_NAME);
//	}
//
//	return true;
//}

//
// Libretro: return save state size
//
size_t c_core::f_retro_serialize_size()
{
	// Log
//	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
//	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroSaveStateSize()\n", CORE_NAME);

	// Save current state
//	state.Init();
//	if(emu)
//		emu->save_state(&state, true);
//
//	return state.GetDataSize();

//	return STATE_MAX_DATA_SIZE;

	return 0;
}

bool c_core::f_retro_serialize(void *data, size_t size)
{
//	size_t serialize_size = 0;
//
//	// Save current state
//	state.Init();
//	if(emu)
//		emu->save_state(&state, false);
//
//	// If size is lower than serialize size, we must retrun false
//	serialize_size = state.GetDataSize();
//	if (size < serialize_size)
//		return false;
//	// Copy data
//	uint8_t *dst = (uint8_t *)data;
//	uint8_t *src = (uint8_t *)state.GetDataPtr();
//	for (int32_t i = serialize_size; --i >= 0; )
//		*dst++ = *src++;
//	return true;

	return false;
}

bool c_core::f_retro_unserialize(void *data, size_t size)
{
//	size_t serialize_size = 0;
//
//	// If size is lower than serialize size, we must return false
//	serialize_size = state.GetDataSize();
//	if (size < serialize_size)
//		return false;
//	// Copy data
//	uint8_t *dst = (uint8_t *)state.GetDataPtr();
//	uint8_t *src = (uint8_t *)data;
//	for (int32_t i = size; --i >= 0; )
//		*dst++ = *src++;
//	state.SetReadyToLoad(true);
//	return true;

	return false;
}

//
// Libretro: return save state data pointer
//
void *c_core::f_retro_get_memory_data()
{
	// Log
//	Core_RetroLogPrintf(RETRO_LOG_DEBUG, "[%s] ================================================================================\n", CORE_NAME);
//	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroSaveStateData()\n", CORE_NAME);

//	return state.GetDataPtr();

	return nullptr;
}

size_t c_core::f_retro_get_memory_size(unsigned id)
{
	return 0;
}

// void cCore::RetroResetHwContext() {
// 	// Log
// //	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroResetHwContext()\n", CORE_NAME);
// 	rglgen_resolve_symbols(retro_hw_render.get_proc_address);

// 	GLuint vertex_shader;
// 	GLuint fragment_shader;
// 	static const GLchar* vertex_shader_source[] = {
// 			"#version 120\n"
// 			"void main() {\n"
// 			"	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n"
// 			"}\n"

// 			/*"void main() {\n"
// 			"}\n"*/

// 			/*"uniform mat4 uMVP;\n"
// 			"attribute vec2 aVertex;\n"
// 			"attribute vec4 aColor;\n"
// 			"varying vec4 color;\n"
// 			"void main() {\n"
// 			"	gl_Position = uMVP * vec4(aVertex, 0.0, 1.0);\n"
// 			"	color = aColor;\n"
// 			"}\n"*/
// 	};
// 	static const GLchar* fragment_shader_source[] = {
// 			"void main() {\n"
//    			"	gl_FragColor = vec4(0.3, 1.0, 0.1, 1.0);\n"
// 			"}\n"
// 			/*"#ifdef GL_ES\n"
// 			"precision mediump float;\n"
// 			"#endif\n"
// 			"varying vec4 color;\n"
// 			"void main() {\n"
// 			"	gl_FragColor = color;\n"
// 			"}\n"*/
// 	};
// 	int  success;
// 	char info_log[1024];

// 	// Generate Vertex shader
// 	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
// 	glShaderSource(vertex_shader, 1, &vertex_shader_source[0], nullptr);
// 	glCompileShader(vertex_shader);
// 	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
// 	if(!success) {
// 		glGetShaderInfoLog(vertex_shader, 1024, nullptr, info_log);
// 		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] cCore::RetroRun() error: vertex shader compilation failed!\n%s\n", CORE_NAME, info_log);
// 	}

// 	// Generate fragment shader
// 	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
// 	glShaderSource(fragment_shader, 1, fragment_shader_source, nullptr);
// 	glCompileShader(fragment_shader);
// 	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
// 	if(!success) {
// 		glGetShaderInfoLog(fragment_shader, 1024, nullptr, info_log);
// 		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] cCore::RetroRun() error: fragment shader compilation failed!\n%s\n", CORE_NAME, info_log);
// 	}

// 	// Generate program and release vertex/fragment buffers
// 	shader_program = glCreateProgram();
// 	glAttachShader(shader_program, vertex_shader);
// 	glAttachShader(shader_program, fragment_shader);
// 	glLinkProgram(shader_program);
// 	glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
// 	if(!success) {
// 		glGetProgramInfoLog(shader_program, 1024, nullptr, info_log);
// 		Core_RetroLogPrintf(RETRO_LOG_ERROR, "[%s] cCore::RetroRun() error: shader program compilation failed!\n%s\n", CORE_NAME, info_log);
// 	}
// 	glUseProgram(shader_program);
// 	glDeleteShader(vertex_shader);
// 	glDeleteShader(fragment_shader);

// 	//
// 	glUseProgram(shader_program);
// 	// Generate Vertices
// 	glGenBuffers(1, &vertex_buffer);
// 	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
// 	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);
// 	glBindBuffer(GL_ARRAY_BUFFER, 0);
// 	// Generate colors
// /*
// 	glGenBuffers(1, &color_buffer);
// 	glBindBuffer(GL_ARRAY_BUFFER, color_buffer);
// 	glBufferData(GL_ARRAY_BUFFER, sizeof(color_buffer_data), color_buffer_data, GL_STATIC_DRAW);
// */
// 	//
// 	glUseProgram(0);
// }

// void cCore::RetroDestroyHwContext()
// {
// 	// Log
// //	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroDestroyHwContext()\n", CORE_NAME);

// //	glDeleteVertexArrays(1, &vao);
// //	vao = 0;
// //	init_multisample(0);
// //	context_alive = false;

// 	glDeleteBuffers(1, &vertex_buffer);
// 	vertex_buffer = 0;
// 	glDeleteProgram(shader_program);
// 	shader_program = 0;
// }

// bool cCore::RetroInitHwContext()
// {
// 	// Log
// 	Core_RetroLogPrintf(RETRO_LOG_INFO, "[%s] cCore::RetroInitHwContext()\n", CORE_NAME);

// #ifdef HAVE_OPENGLES
//    retro_hw_render.context_type = RETRO_HW_CONTEXT_OPENGLES2;
// #else	// #ifdef HAVE_OPENGLES
// 	retro_hw_render.context_type = RETRO_HW_CONTEXT_OPENGL;
// //	retro_hw_render.version_major = 3;
// //	retro_hw_render.version_minor = 1;
// #endif	// #ifdef HAVE_OPENGLES
// 	retro_hw_render.context_reset = cCore::RetroResetHwContext;
// 	retro_hw_render.context_destroy = RetroDestroyHwContext;
// 	retro_hw_render.depth = true;
// 	retro_hw_render.stencil = true;
// 	retro_hw_render.bottom_left_origin = true;

// 	if (!RetroEnvironment(RETRO_ENVIRONMENT_SET_HW_RENDER, &retro_hw_render))
// 		return false;

// 	return true;
// }
