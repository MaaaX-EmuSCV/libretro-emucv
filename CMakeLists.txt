cmake_minimum_required(VERSION 3.11)

set(CORE_NAME emucv_libretro)
project(${CORE_NAME})
message("CORE NAME: ${CORE_NAME}")

set(CMAKE_CXX_STANDARD 14)

macro (NOW RESULT)
    if(UNIX)
        execute_process(COMMAND "date" "+%Y%m%d%H%M%S" OUTPUT_VARIABLE ${RESULT})
        string(REGEX REPLACE "(.*)\r?\n" "\\1" ${RESULT} ${${RESULT}})
#    elseif(WIN32)
#        EXECUTE_PROCESS(COMMAND "cmd" " /C date /T" OUTPUT_VARIABLE ${RESULT})
#        string(REGEX REPLACE "(..)/(..)/..(..).*" "\\1/\\2/\\3" ${RESULT} ${${RESULT}})
    else(UNIX)
        message(SEND_ERROR "CMake \"now()\" macro not implemented for this system")
        SET(${RESULT} "00000000000000")
    endif(UNIX)
endmacro(now)

# === VERSION MAJOR =================
set(CORE_VERSION_MAJOR      "0")
# === VERSION MINOR =================
set(CORE_VERSION_MINOR      "00")
# === VERSION DATETIME ==============
set(CORE_VERSION_DATETIME "20220311200100")
#NOW(CURRENT_DATETIME)
set(CORE_VERSION_DATETIME "${CURRENT_DATETIME}")

add_definitions(-DCORE_VERSION_MAJOR="${CORE_VERSION_MAJOR}")
add_definitions(-DCORE_VERSION_MINOR="${CORE_VERSION_MINOR}")
add_definitions(-DCORE_VERSION_DATETIME="${CORE_VERSION_DATETIME}")
message("CORE VERSION: v${CORE_VERSION_MAJOR}.${CORE_VERSION_MINOR}.${CORE_VERSION_DATETIME}")

list(APPEND CMAKE_MODULE_PATH
        ${CMAKE_SOURCE_DIR}/CMake/Utils
        ${CMAKE_SOURCE_DIR}/CMake/Packages
        )

message("Adding linker script")
set(LINKER_SCRIPT "${CMAKE_SOURCE_DIR}/link.T")
set(CMAKE_EXE_LINKER_FLAGS "-T ${LINKER_SCRIPT}")
set_source_files_properties(src/libretro.cpp PROPERTIES OBJECT_DEPENDS ${LINKER_SCRIPT})

message("Removing shared library prefix")
set(CMAKE_SHARED_LIBRARY_PREFIX "")


#-------------------------------------------------------------------------------
#set up OpenGL system variable
#set(GLSystem "" CACHE STRING "The OpenGL system to be used")
#set_property(CACHE GLSystem PROPERTY STRINGS "" "OpenGL" "OpenGL ES")

#-------------------------------------------------------------------------------
#check if we're running on Raspberry Pi
MESSAGE("Looking for bcm_host.h")
if(EXISTS "/opt/vc/include/bcm_host.h")
    MESSAGE("bcm_host.h found")
    set(BCMHOST found)
    #    set(GLSystem "OpenGL ES")
else()
    MESSAGE("bcm_host.h not found")
endif()
if(${RPI_VERSION})
    add_definitions(-DRPI_VERSION=${RPI_VERSION})
endif()

#-------------------------------------------------------------------------------
#check if we're running on olinuxino
MESSAGE("Looking for libMali.so")
if(EXISTS "/usr/lib/libMali.so")
    MESSAGE("libMali.so found")
    #    set(GLSystem "OpenGL ES")
else()
    MESSAGE("libMali.so not found")
endif()

# on some drivers (the one for xu4 for example), the libmali is lowercase
MESSAGE("Looking for libmali.so")
if(EXISTS "/usr/lib/libmali.so")
    MESSAGE("libmali.so found")
    #    set(GLSystem "OpenGL ES")
else()
    MESSAGE("libmali.so not found")
endif()

#finding necessary packages
#-------------------------------------------------------------------------------
find_package(PkgConfig REQUIRED)
#find_package(Freetype REQUIRED)
#find_package(FreeImage REQUIRED)
#find_package(SDL2MIXER REQUIRED)
#find_package(SDL2 REQUIRED)
find_package(Boost REQUIRED COMPONENTS system filesystem date_time regex)
#find_package(Eigen3 REQUIRED)
#find_package(CURL REQUIRED)

#add_definitions(-DGL_GLEXT_PROTOTYPES)
#pkg_check_modules(OPENGL gl)
#pkg_check_modules(OPENGLES glesv2)
#if(OPENGL_FOUND)
#    message("OpenGL found: ${OPENGL_INCLUDE_DIR}")
#    find_package(OpenGL REQUIRED)
#    set(GLSystem "OpenGL")
#    add_definitions(-DHAVE_OPENGL)
#elseif(OPENGLES_FOUND)
#    message("OpenGLES found: ${OPENGLES_INCLUDE_DIR}")
#    find_package(OpenGLES REQUIRED)
#    set(GLSystem "OpenGL ES")
#    add_definitions(-DHAVE_OPENGLES)
#else()
#    message(FATAL_ERROR "OpenGL ES or OpenGL required!")
#endif()

#add ALSA for Linux
#if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
#    find_package(ALSA REQUIRED)
#endif()


#-------------------------------------------------------------------------------
#set up compiler flags and excutable names
if(DEFINED BCMHOST)
    add_definitions(-D_RPI_)
endif()

#-------------------------------------------------------------------------------

if(MSVC)
    set(CMAKE_DEBUG_POSTFIX "d")
    add_definitions(-D_CRT_SECURE_NO_DEPRECATE)
    add_definitions(-D_CRT_NONSTDC_NO_DEPRECATE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP") #multi-processor compilation
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /MP") #multi-processor compilation
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
    #check for G++ 4.7+
    execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE G++_VERSION)
    if (G++_VERSION VERSION_LESS 4.7)
        message(SEND_ERROR "You need at least G++ 4.7 to compile!")
    endif()

    #set up compiler flags for GCC
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wno-attributes -O0") #support C++11 for std::, optimize
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -O0")  #-s = strip binary
endif()

if(APPLE AND NOT CMAKE_COMPILER_IS_GNUCXX)
    #set up compiler flags
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wno-attributes -O0") #support C++11 for std::, optimize
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -O0")  #-s = strip binary

    #set(CMAKE_CXX_FLAGS "-std=c++11 -stdlib=libc++")
endif()

#add_definitions(-DEIGEN_DONT_ALIGN)

#-------------------------------------------------------------------------------
#add include directories
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/deps/libretro-common/include)
#include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/deps/glsym)
set(COMMON_INCLUDE_DIRS
        # OSX seems to have a conflicting platform.h somewhere and fails to find
        # getHomePath, so let use our includes paths first.
        ${CMAKE_CURRENT_SOURCE_DIR}/src/deps/libretro-common/include
        #       ${CMAKE_CURRENT_SOURCE_DIR}/es-app/src
        #       ${CMAKE_CURRENT_SOURCE_DIR}/es-core/src
        #       ${FREETYPE_INCLUDE_DIRS}
        #       ${FreeImage_INCLUDE_DIRS}
        #       ${SDL2_INCLUDE_DIR}
        ${Boost_INCLUDE_DIRS}
        #       ${EIGEN3_INCLUDE_DIR}
        #       ${CURL_INCLUDE_DIR}
        )

#add ALSA for Linux
#if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
#    LIST(APPEND COMMON_INCLUDE_DIRS
#            ${ALSA_INCLUDE_DIRS}
#            )
#endif()

if(DEFINED BCMHOST)
    LIST(APPEND COMMON_INCLUDE_DIRS
            "/opt/vc/include"
            "/opt/vc/include/interface/vcos"
            "/opt/vc/include/interface/vmcs_host/linux"
            "/opt/vc/include/interface/vcos/pthreads"
            )
#else()
#    if(${GLSystem} MATCHES "Desktop OpenGL")
#        LIST(APPEND COMMON_INCLUDE_DIRS
#                ${OPENGL_INCLUDE_DIR}
#                )
#    else()
#        LIST(APPEND COMMON_INCLUDE_DIRS
#                ${OPENGLES_INCLUDE_DIR}
#                )
#    endif()
endif()

#-------------------------------------------------------------------------------
#define libraries and directories
if(DEFINED BCMHOST)
    link_directories(
            ${Boost_LIBRARY_DIRS}
            "/opt/vc/lib"
    )
else()
    link_directories(
            ${Boost_LIBRARY_DIRS}
    )
endif()

set(COMMON_LIBRARIES
        ${Boost_LIBRARIES}
        #       ${FREETYPE_LIBRARIES}
        #       ${FreeImage_LIBRARIES}
        #       ${SDL2_LIBRARY}
        #       ${SDLMIXER_LIBRARY}
        #       ${CURL_LIBRARIES}
        #       pugixml
        #       nanosvg
        )

#add ALSA for Linux
#if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
#    LIST(APPEND COMMON_LIBRARIES
#            ${ALSA_LIBRARY}
#            )
#endif()

if(DEFINED BCMHOST)
    LIST(APPEND COMMON_LIBRARIES
            bcm_host
            #           EGL
#            ${OPENGLES_LIBRARIES}
            )
else()
    if(MSVC)
        LIST(APPEND COMMON_LIBRARIES
                winmm
                )
    endif()
#    if(${GLSystem} MATCHES "Desktop OpenGL")
#        LIST(APPEND COMMON_LIBRARIES
#                ${OPENGL_LIBRARIES}
#                )
#    else()
#        LIST(APPEND COMMON_LIBRARIES
#                #               EGL
#                ${OPENGLES_LIBRARIES}
#                )
#    endif()
endif()

#-------------------------------------------------------------------------------
# set up build directories
set(dir ${CMAKE_CURRENT_SOURCE_DIR})
set(EXECUTABLE_OUTPUT_PATH ${dir} CACHE PATH "Build directory" FORCE)
set(LIBRARY_OUTPUT_PATH ${dir} CACHE PATH "Build directory" FORCE)


#-------------------------------------------------------------------------------
# add each component

#add_subdirectory("external")
#add_subdirectory("es-core")
#add_subdirectory("es-app")


# i18n
#find_program (MSGFMT_EXECUTABLE msgfmt)
#find_program (MSGMERGE_EXECUTABLE msgmerge)
#find_program (XGETTEXT_EXECUTABLE xgettext)
#if(MSGFMT_EXECUTABLE AND MSGMERGE_EXECUTABLE AND XGETTEXT_EXECUTABLE)
#    message (STATUS "Native language support enabled.")
#    add_subdirectory (locale)
#endif()

#if(${GLSystem} MATCHES "OpenGL ES")
#    add_library(${CORE_NAME} SHARED src/libretro.cpp src/core.cpp src/core.h src/common.cpp src/common.h src/emu.cpp src/emu.h src/deps/glsym/glsym.h src/deps/glsym/rglgen.c src/deps/glsym/rglgen.h src/deps/glsym/rglgen_headers.h src/deps/glsym/glsym_es2.c src/deps/glsym/glsym_es2.h)
#    target_link_libraries(${CORE_NAME} ${OPENGLES_LIBRARIES})
#else()
#    add_library(${CORE_NAME} SHARED src/libretro.cpp src/core.cpp src/core.h src/common.cpp src/common.h src/emu.cpp src/emu.h src/deps/glsym/glsym.h src/deps/glsym/rglgen.c src/deps/glsym/rglgen.h src/deps/glsym/rglgen_headers.h src/deps/glsym/glsym_gl.c src/deps/glsym/glsym_gl.h)# src/deps/glsym/glsym_es2.c src/deps/glsym/glsym_es2.h)
#    target_link_libraries(${CORE_NAME} ${OPENGL_LIBRARIES})
#endif()

add_library(${CORE_NAME} SHARED src/libretro.cpp src/core.cpp src/core.h src/common.cpp src/common.h src/emu.cpp src/emu.h src/fileio.cpp src/fileio.h src/md5.cpp src/md5.h)
#target_link_libraries(${CORE_NAME} ${OPENGL_LIBRARIES})
