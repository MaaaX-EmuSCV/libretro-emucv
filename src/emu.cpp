// 
// emu.cpp
// 
// Emulator
// 
// Authors:
// - MARCONATO Maxime (aka MaaaX, aka EPOCH84 / maxime@maaax.com)
// 

#include "emu.h"
#include "fileio.h"
#include "md5.h"

/*
#define SET_BANK(s, e, w, r)\
{ \
	int sb = (s) >> 7, eb = (e) >> 7; \
	for(int i = sb; i <= eb; i++)\
{ \
		if((w) == wdmy)\
{ \
			wbank[i] = wdmy; \
		}\
		else\
		{ \
			wbank[i] = (w) + 0x80 * (i - sb); \
		} \
		if((r) == rdmy)\
		{ \
			rbank[i] = rdmy; \
		}\
		else\
		{ \
			rbank[i] = (r) + 0x80 * (i - sb); \
		} \
	} \
}
*/

// Constructor
c_emu::c_emu()
{
	// Precalculates addresses' values
	bool tmp = false;
	bool a01 = false;
	bool a02 = false;
	bool a03 = false;
	bool a04 = false;
	bool a05 = false;
	bool a06 = false;
	bool a07 = false;
	bool a08 = false;
	bool a09 = false;
	bool a10 = false;
	bool a11 = false;
	u_int8_t a08a11 = 0;
	// Valid addresses
	for(int16_t l = -1, p; ++l < EMU_MEMORYSIZE_PROGRAM; )
	{
		p = (a11 ? 1024 : 0) + (a10 ? 512 : 0) + (a09 ? 256 : 0) + (a08 ? 128 : 0) + (a07 ? 64 : 0) + (a06 ? 32 : 0) + (a05 ? 16 : 0) + (a04 ? 8 : 0) + (a03 ? 4 : 0) + (a02 ? 2 : 0) + (a01 ? 1 : 0);
		address_linear_to_polynomial[l] = p;
		address_polynomial_to_linear[p] = l;
		tmp = !(a06 ^ a07);
		a07 = a06;
		a06 = a05;
		a05 = a04;
		a04 = a03;
		a03 = a02;
		a02 = a01;
		a01 = tmp;
		if(!a07 && !a06 && !a05 && !a04 && !a03 && !a02 && !a01)
		{
			a08a11++;
			a08a11 &= 15;
			a08 = a08a11 & 1;
			a09 = a08a11 & 2;
			a10 = a08a11 & 4;
			a11 = a08a11 & 8;
		}
	}
	// Invalid addresses
	for(int16_t i = -1; ++i < EMU_MEMORYSIZE_INVALID; )
		address_polynomial_to_linear[i * 128 + 127] = 0xffff;

	memset(memory_program, 0, sizeof(memory_program));
	memset(memory_pattern, 0, sizeof(memory_pattern));

//	SET_BANK(0x0000, 0x07ff, wdmy, memory_program);

	// Initialize cart status
	cart_found		= false;
	cart_inserted	= false;
//	cart_ok			= false;

	// Initialize display counters and flags
	cnt_vc			= 0;
	flg_v_field 	= false;
	flg_v_blank		= false;
//	flg_v_equal 	= false;
//	flg_v_sync		= false;
	cnt_hc			= 0;
//	flg_h_4t		= false;
	flg_h_blank		= false;
	flg_h_4blank	= false;
//	flg_h_sync		= false;
//	flg_h_equal		= false;

	nb_opcodes_step = (2 * EMU_OPCODE_GRANULARITY * EMU_CLOCK_CPU) / (EMU_NTSC_FREQUENCY * EMU_NTSC_LINES * EMU_NTSC_H);

	f_reset();
}

// Destructor
c_emu::~c_emu()
{

}

// Load cart
void c_emu::f_insert_cart(const char *file_path)
{
 	c_fileio	*o_fileio_cart = new c_fileio();
	uint8_t		 cart_data[sizeof(memory_program)+sizeof(memory_pattern)];
	size_t		 cart_size;
	_TCHAR		 cart_md5[33];
	uint16_t	 memval;
	size_t		 program_size;
	size_t		 pattern_size;

 	// Close cart and initialize memory
 	f_eject_cart();

 	// Check file path
 	if(strlen(file_path) == 0)
 		goto lbl_rom_error;	// Fatal error

	// Open CART file
	if(!o_fileio_cart->f_open(file_path, FILEIO_MODE_READ_BINARY))
		goto lbl_rom_end;	// Fatal error
	
	cart_found = true;

	// File size
	cart_size = o_fileio_cart->f_size();
	// Program part have a fixed size
	program_size = sizeof(memory_program);
	// Pattern part can have some fixed size
	pattern_size = cart_size - program_size;
	if(cart_size != program_size)		// No pattern or external file
//	|| cart_size != program_size + ???	// Binary pattern, concatened 11 bits aligned, smaller size
//	|| cart_size != program_size + ???	// Hexadecimal pattern, concatened 8 bits aligned (first 3 bits of each line are ignored because alway at 0), medium size
//	|| cart_size != program_size + ???)	// Raw pattern (0x00 and 0xNN), largest size
		goto lbl_rom_error;	// Fatal error

	// Load entire file in memory
	memset(cart_data, 0, sizeof(cart_data));
	o_fileio_cart->f_read(&cart_data, cart_size, 1);

	// Program part
	memcpy(memory_program, cart_data, sizeof(memory_program));
	for(int16_t i = -1; i++ < EMU_MEMORYSIZE_PROGRAM; )
	{
		memval = memory_program[i];
		memory_program[i] = ((memval & 0x000f) << 8) ^ ((memval & 0xff00) >> 8);
	}

	// Pattern part
	static const uint8_t pattern_778c[EMU_MEMORYSIZE_PATTERN] = {
													// === NORMAL 7x7 ===
		0x78, 0x48, 0x48, 0x48, 0x78, 0x00, 0x00,	// 0x00
		0x08, 0x08, 0x08, 0x08, 0x08, 0x00, 0x00,	// 0x01
		0x78, 0x08, 0x78, 0x40, 0x78, 0x00, 0x00,	// 0x02
		0x78, 0x08, 0x38, 0x08, 0x78, 0x00, 0x00,	// 0x03
		0x50, 0x50, 0x50, 0x78, 0x10, 0x00, 0x00,	// 0x04
		0x78, 0x40, 0x78, 0x08, 0x78, 0x00, 0x00,	// 0x05
		0x78, 0x40, 0x78, 0x48, 0x78, 0x00, 0x00,	// 0x06
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x07 NOT USED
		0x78, 0x48, 0x78, 0x48, 0x78, 0x00, 0x00,	// 0x08
		0x78, 0x48, 0x78, 0x08, 0x78, 0x00, 0x00,	// 0x09
		0x78, 0x48, 0x08, 0x08, 0x08, 0x00, 0x00,	// 0x0a
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x0b
		0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00,	// 0x0c
		0x00, 0x00, 0x40, 0x40, 0x40, 0x00, 0x00,	// 0x0d
		0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x0e
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x0f NOT USED
		0x40, 0x40, 0x40, 0x40, 0x40, 0x00, 0x00,	// 0x10
		0x70, 0x70, 0x70, 0x00, 0x00, 0x00, 0x00,	// 0x11
		0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x00,	// 0x12
		0x70, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x13
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x14
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x15
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x16
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x17 NOT USED
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x18
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x19
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x1a
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x1b
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x1c
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x1d
		0x7f, 0x3e, 0x1c, 0x08, 0x00, 0x55, 0x2a,	// 0x1e
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x1f NOT USED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x20
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x21
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x22
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x23
		0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x24
		0x20, 0x70, 0x20, 0x50, 0x00, 0x00, 0x00,	// 0x25
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x26
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x27 NOTUSED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x28
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x29
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x2a
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x2b
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x2c
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x2d
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x2e
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x2f NOTUSED

													// === BENT 7x7 ===
		0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01,	// 0x30 \ 
		0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40,	// 0x31 / 
		0x40, 0x20, 0x1e, 0x00, 0x04, 0x02, 0x01,	// 0x32 \ 
		0x01, 0x02, 0x3c, 0x78, 0x10, 0x20, 0x40,	// 0x33 / 
		0x00, 0x20, 0x10, 0x08, 0x04, 0x00, 0x00,	// 0x34 \ 
		0x00, 0x08, 0x10, 0x20, 0x40, 0x00, 0x00,	// 0x35 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x36 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x37 NOT USED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x38 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x39 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x3a \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x3b / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x3c \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x3d / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x3e \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x3f NOT USED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x40 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x41 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x42 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x43 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x44 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x45 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x46 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x47 NOT USED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x48 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x49 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x4a \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x4b / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x4c \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x4d / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x4e \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x4f NOT USED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x50 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x51 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x52 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x53 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x54 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x55 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x56 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x57 NOT USED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x58 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x59 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x5a \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x5b / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x5c \ 
		0x7f, 0x00, 0x10, 0x2a, 0x55, 0x00, 0x00,	// 0x5d / 
		0x7f, 0x00, 0x10, 0x2a, 0x55, 0x00, 0x00,	// 0x5e \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x5f NOT USED
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x60 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x61 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x62 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x63 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x64 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x65 / 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x66 \ 
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x67 NOT USED

													// === Y REPEAT 7x7 ===
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x68
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x69
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x6a
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x6b
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x6c
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x6d
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x6e
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x6f NOT USED

													// === XY REPEAT 8x7 ===
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x70
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x71
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x72
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x73
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x74
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x75
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x76
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x77 NOT USED
		
													// === X REPEAT 8x7 ===
		0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x78
		0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x79
		0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x7a
		0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff,	// 0x7b
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x7c
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x7d
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	// 0x7e
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	// 0x7f NOT USED
	};
	memcpy(memory_pattern, pattern_778c, sizeof(memory_pattern));
	
	//memcpy(memory_pattern, cart_data + sizeof(memory_program), sizeof(memory_pattern));

	cart_inserted = true;

	// Check MD5
//	memset(cart_md5, 0, sizeof(cart_md5));
//	strncpy(cart_md5, get_md5(cart_data, cart_size), sizeof(cart_md5));
//	if(strcmp(cart_md5, _T("5c0a8b9e0ae3bdc62cf758c8de3c621c")) == 0)	// 
//	{
//	}
	// No check at this time
 //	cart_ok = true;

	goto lbl_rom_close;

lbl_rom_error:
 	memset(memory_program, 0, sizeof(memory_program));
 //	memset(memory_pattern, 0, sizeof(memory_pattern));

lbl_rom_close:
	o_fileio_cart->f_close();

lbl_rom_end:
	delete o_fileio_cart;
}

void c_emu::f_eject_cart()
{
	cart_found		= false;
	cart_inserted	= false;
//	cart_ok			= false;

	memset(memory_program, 0, sizeof(memory_program));
	memset(memory_pattern, 0, sizeof(memory_pattern));

	f_reset();
}

bool c_emu::f_is_cart_inserted()
{
	return cart_inserted;
}

void c_emu::f_run(uint32_t *video_buffer, uint32_t video_stride)
{
//#define TEST_OPCODE

	if(error != EMU_OK)
		return;

	//	uint32_t color_background = (flg_dsp ? emu_palette[((reg_mode & 0x30) >> 4) ^ ((reg_mode & 0x0f) << 2)]: emu_palette[0]);
		uint32_t color_background = emu_palette[((reg_mode & 0x30) >> 4) ^ ((reg_mode & 0x0f) << 2)];
		uint32_t *line_ptr = video_buffer;
		// for (int16_t x, y = EMU_HEIGHT; --y >= 0; )
		// {
		// 	for (x = EMU_WIDTH; --x >= 0; )
		// 		line_ptr[x] = color_background;
		// 	line_ptr += video_stride;
		// }

	uint16_t opcode = 0x0000;
	char hex_program_counter_polynomial[6];
	char hex_opcode[6];
	char hex_instruction[64];
	char hex_description[256];
	uint8_t	cnt_hc_init;
	uint8_t tmp_hl;
	uint8_t tmp_a1;
	uint8_t tmp_a2;
	uint8_t tmp_a3;
	uint8_t tmp_a4;
	uint16_t tmp_a;
	uint8_t tmp_h;
	uint8_t tmp_l;
	uint8_t tmp_k;

	uint8_t test;

#ifndef TEST_OPCODE
	// NTSC is 60Hz
	// 2 interlaced fieds by pictures (odd and even fields) = 30 pictures per second
//	for(uint8_t field = 0; ++field < 2; )
//	{
		// Vertical
//		flg_v_field = !flg_v_field;//(field == 1 ? false : true);
//		cnt_vc = (flg_v_field ? 1 : 0);
//		cnt_hc_init = (flg_v_field ? 48: 0);
		if(flg_v_field)
		{
			flg_v_field = false;
			cnt_vc = 0;
			cnt_hc_init = 0;
		}
		else
		{
			flg_v_field = true;
			cnt_vc = 1;
			cnt_hc_init = 48;
		}

//		flg_v_blank = true;
//		flg_v_equal = true;
//		flg_v_sync  = false;
		// Vertical clock
		for(int16_t v_tick = 0; ++v_tick <= EMU_NTSC_LINES; cnt_vc++)
		{
			// Update vertical flags
//			switch(v_tick)
//			{
//				case 7:
//					flg_v_sync = true;
//					break;
//				case 13:
//					flg_v_sync = false;
//					break;
//				case 19:
//					flg_v_equal = false;
//					break;
//				case 41:
//					flg_v_blank = false;
//					break;
//			}
			flg_v_blank = (v_tick < 41);

			// Horizontal
//			if(!flg_v_field)
//			{
//				cnt_hc = 0;
//				flg_h_blank = true;
//			}
//			else
//			{
//				cnt_hc = 48;
//				flg_h_blank = false;
//			}
//			flg_h_sync  = false;
//			flg_h_equal = false;
			// Horizontal clock
//			cnt_hc = (flg_v_field ? 48: 0);
			cnt_hc = cnt_hc_init;
			for(int16_t h_tick = 0; ++h_tick <= EMU_NTSC_H_HALF; cnt_hc++)
			{
				// Update horizontal flags
//				flg_h_4t = ((cnt_hc % 8) == 7);
//				switch(h_tick)
//				{
//					case 3:
//						flg_h_sync  = true;
//						flg_h_equal = true;
//						break;
//					case 7:
//					case 55:
//						flg_h_equal = false;
//						break;
//					case 10:
//						flg_h_sync = false;
//						break;
//					case 17:
//						flg_h_blank = false;
//						break;
//					case 51:
//						flg_h_equal  = true;
//						break;
//				}
				flg_h_blank = (h_tick < 17);
				flg_h_4blank = ((cnt_vc & 0x0003) == 0x0000) && flg_h_blank;

				// Run opcodes
				nb_opcodes_current -= nb_opcodes_step;
				while(nb_opcodes_current < 0)
				{
					nb_opcodes_current += EMU_OPCODE_GRANULARITY;
#else	// ifdef TEST_OPCODE
	program_counter_linear = 0;
	bool opcodes[0x0fff];
	for(uint16_t i = 0; i++ < 0x0fff; )
		opcodes[i] = false;

	while(program_counter_linear < EMU_MEMORYSIZE_PROGRAM)
	{
#endif	// TEST_OPCODE
					// Read current opcode
					opcode = (program_counter_linear ? memory_program[program_counter_linear] & 0x0fff : 0xf000);
//					reg_a = address_linear_to_polynomial[program_counter_linear];
#ifdef TEST_OPCODE
					if(opcode <= 0x0fff)
						opcodes[opcode] = true;
#endif	// TEST_OPCODE

					//sprintf(hex_program_counter_polynomial, "0x%03X", reg_a);
					//sprintf(hex_opcode, "0x%03X", opcode);
					//memset(hex_instruction, 0, sizeof(hex_instruction));
					//memset(hex_description, 0, sizeof(hex_description));

					// Next program address
					program_counter_linear = (program_counter_linear + 1);// & 0x07ff;
					if((program_counter_linear > 0x07ff) || (address_linear_to_polynomial[program_counter_linear] == 0xffff))
					{
						bool stop;
						stop = true;
					}
					reg_a = address_linear_to_polynomial[program_counter_linear];

					// Run one opcode
					switch(opcode)
					{
						case 0xf000: // (fake opcode)
							// Ignore the first opcode (0x0000 or 0x0fff)
							// Reset all before program start
							// sprintf(hex_instruction, "START");
							// sprintf(hex_description, "Program address 0, program init, opcode ignored");
#ifndef TEST_OPCODE
							f_reset(false);
#endif
						break;

						case 0x0000:
							// 0x000
							// 000000000000
							// sprintf(hex_instruction, "NOP");
							// sprintf(hex_description, "No operation");
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0001:
						// case 0x0002:
						// case 0x0003:
						// break;

						case 0x0004:
							// 0x004
							// 000000000100
							// GPL J
							// Skip if (Gun Port Latch) = 1
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0005:
						// case 0x0006:
						// case 0x0007:
						// break;

						case 0x0008:
							// 0x008
							// 000000001000
							// sprintf(hex_instruction, "H=>NRM");
							// sprintf(hex_description, "Move H{5:1} to NRM{5:1} (Line Buffer Register)");
#ifndef TEST_OPCODE
							if(stk_nrm_level >= EMU_STACK_NRM_SIZE)
							{
								error = EMU_ERORR_STACK_NRM_OVERFLOW;
								return;
							}
							stk_nrm[stk_nrm_level++] = ((reg_hl & 0x7c) >> 2);
#endif
						break;

						// case 0x0009:
						// case 0x000a:
						// case 0x000b:
						// case 0x000c:
						// case 0x000d:
						// case 0x000e:
						// case 0x000f:
						// case 0x0010:
						// case 0x0011:
						// case 0x0012:
						// case 0x0013:
						// case 0x0014:
						// case 0x0015:
						// case 0x0016:
						// case 0x0017:
						// break;

						case 0x0018:
							// 0x018
							// 000000011000
							// sprintf(hex_instruction, "H<=>X");
							// sprintf(hex_description, "[0{2:1},H{5:1}]{7:1} <=> X4{7:1}, 0{7:1} => X3{7:1}, 0{1} => X1'{1}, 0{1} => A1'{1}, L{2:1} <=> L'{2:1}");
#ifndef TEST_OPCODE
							tmp_hl = reg_hl;
							reg_hl = ((reg_x4 & 0x1f) << 2) ^ (reg_l_prime & 0x03);
							reg_l_prime = tmp_hl & 0x03;
							reg_x4 = tmp_hl >> 2;
							reg_x3 = 0x00;
							reg_x1_prime = 0x00;
							reg_a1_prime = 0x00;
#endif
						break;

						// case 0x0019:
						// case 0x001a:
						// case 0x001b:
						// case 0x001c:
						// case 0x001d:
						// case 0x001e:
						// case 0x001f:
						// break;

						case 0x0020:
							// 0x020
							// 000000100000
							// sprintf(hex_instruction, "SRE");
							// sprintf(hex_description, "Subroutine end, pop down return address from address stack to A{11:1} (3 levels)");
#ifndef TEST_OPCODE
							if(stk_a_level == 0)
							{
								error = EMU_ERORR_STACK_A_UNDERFLOW;
								return;
							}
							reg_a = stk_a[--stk_a_level];
							stk_a[stk_a_level] = 0;
							program_counter_linear = address_polynomial_to_linear[reg_a];
#endif
						break;

						// case 0x0021:
						// case 0x0022:
						// case 0x0023:
						// case 0x0024:
						// case 0x0025:
						// case 0x0026:
						// case 0x0027:
						// break;

						case 0x0028: case 0x029:
							// 0x028-0x029
							// 00000010100N
							// sprintf(hex_instruction, "N=>STB");
							// sprintf(hex_description, "Left shift STB{4:1}, N{1} => STB{1}");
#ifndef TEST_OPCODE
							reg_stb = (((reg_stb << 1) ^ (uint8_t)(opcode & 0x0001)) & 0x0f);
#endif
						break;

						case 0x0030:
							// 0x030
							// 000000110000
							// "PD1 J"
							// Skip if (PD1 input) = 1
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0031:
						// case 0x0032:
						// case 0x0033:
						// break;

						case 0x0034:
							// 0x034
							// 000000110100
							// "PD2 J"
							// Skip if (PD2 input) = 1
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0035:
						// case 0x0036:
						// case 0x0037:
						// break;

						case 0x0038:
							// 0x038
							// 000000111000
							// "PD3 J"
							// Skip if (PD3 input) = 1
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0039:
						// case 0x003a:
						// case 0x003b:
						// break;

						case 0x003c:
							// 0x03c
							// 000000111100
							// "PD4 J"
							// Skip if (PD4 input) = 1
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x003d:
						// case 0x003e:
						// case 0x003f:
						// case 0x0040:
						// case 0x0041:
						// case 0x0042:
						// case 0x0043:
						// case 0x0044:
						// case 0x0045:
						// case 0x0046:
						// case 0x0047:
						// case 0x0048:
						// break;

						case 0x0049:
							// 0x049
							// 000001001001
							// sprintf(hex_instruction, "4H BLK +1J");
							// sprintf(hex_description, "Skip if (4H Horizontal Blank) = 1");
#ifndef TEST_OPCODE
							if(flg_h_4blank)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							else
							{
test = 0;
							}
#endif
						break;

						case 0x004a:
							// 0x04a
							// 000001001010
							// sprintf(hex_instruction, "V BLK +1J");
							// sprintf(hex_description, "Skip if (Vertical Blank) = 1");
#ifndef TEST_OPCODE
							if(flg_v_blank)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							else
							{
test = 0;
							}
#endif
						break;

						// case 0x004b:
						// break;

						case 0x004c:
							// 0x04c
							// 000001001100
							// "GPSW/ J"
							// Skip if (GP&SW/ input) = 1
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x004d:
						// case 0x004e:
						// case 0x004f:
						// case 0x0050:
						// case 0x0051:
						// case 0x0052:
						// case 0x0053:
						// break;

						case 0x0054:
							// 0x054
							// 000001010100
							// sprintf(hex_instruction, "A=>MA");
							// sprintf(hex_description, "[A4{7:1},A3{7:1},A2{7:1},A1{7:1}]{28:1} => M[H{5:1},0{2:1}]{28:1}");
#ifndef TEST_OPCODE
							tmp_h = reg_hl & 0x7c;
							memory_ram[tmp_h]   = reg_a1;
							memory_ram[tmp_h+1] = reg_a2;
							memory_ram[tmp_h+2] = reg_a3;
							memory_ram[tmp_h+3] = reg_a4;
#endif
						break;

						// case 0x0055:
						// case 0x0056:
						// case 0x0057:
						// break;

						case 0x0058:
							// 0x058
							// 000001011000
							// sprintf(hex_instruction, "MA=>A");
							// sprintf(hex_description, "M[H{5:1},0{2:1}]{28:1} => [A4{7:1},A3{7:1},A2{7:1},A1{7:1}]{28:1}");
#ifndef TEST_OPCODE
							tmp_h = reg_hl & 0x7c;
							reg_a1 = memory_ram[tmp_h];
							reg_a2 = memory_ram[tmp_h+1];
							reg_a3 = memory_ram[tmp_h+2];
							reg_a4 = memory_ram[tmp_h+3];
#endif
						break;

						// case 0x0059:
						// case 0x005a:
						// case 0x005b:
						// break;

						case 0x005c:
							// 0x05c
							// 000001011100
							// sprintf(hex_instruction, "MA<=>A");
							// sprintf(hex_description, "[A4{7:1},A3{7:1},A2{7:1},A1{7:1}]{28:1} <=> M[H{5:1},0{2:1}]{28:1}");
#ifndef TEST_OPCODE
							tmp_h = reg_hl & 0x7c;
							tmp_a1 = reg_a1;
							tmp_a2 = reg_a2;
							tmp_a3 = reg_a3;
							tmp_a4 = reg_a4;
							reg_a1 = memory_ram[tmp_h];
							reg_a2 = memory_ram[tmp_h+1];
							reg_a3 = memory_ram[tmp_h+2];
							reg_a4 = memory_ram[tmp_h+3];
							memory_ram[tmp_h]   = tmp_a1;
							memory_ram[tmp_h+1] = tmp_a2;
							memory_ram[tmp_h+2] = tmp_a3;
							memory_ram[tmp_h+3] = tmp_a4;
#endif
						break;

						// case 0x005d:
						// case 0x005e:
						// case 0x005f:
						// break;

						case 0x0060:
							// 0x060
							// 000001100000
							// sprintf(hex_instruction, "SRE, +1");
							// sprintf(hex_description, "Subroutine end, pop down return address from address stack to A{11:1}, skip");
#ifndef TEST_OPCODE
							if(stk_a_level == 0)
							{
								error = EMU_ERORR_STACK_A_UNDERFLOW;
								return;
							}
							reg_a = stk_a[--stk_a_level];
							stk_a[stk_a_level] = 0;
							program_counter_linear = address_polynomial_to_linear[reg_a] + 1;
							reg_a = address_linear_to_polynomial[program_counter_linear];
#endif
						break;

						// case 0x0061:
						// case 0x0062:
						// case 0x0063:
						// case 0x0064:
						// case 0x0065:
						// case 0x0066:
						// case 0x0067:
						// case 0x0068:
						// case 0x0069:
						// case 0x006a:
						// case 0x006b:
						// case 0x006c:
						// case 0x006d:
						// case 0x006e:
						// case 0x006f:
						// break;

						case 0x0070:
							// 0x070
							// 000001110000
							// "PD1 J/"
							// Skip if (PD1 input) = 0
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0071:
						// case 0x0072:
						// case 0x0073:
						// break;

						case 0x0074:
							// 0x074
							// 000001110100
							// "PD2 J/"
							// Skip if (PD2 input) = 0
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0075:
						// case 0x0076:
						// case 0x0077:
						// break;

						case 0x0078:
							// 0x078
							// 000001111000
							// "PD3 J/"
							// Skip if (PD3 input) = 0
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x0079:
						// case 0x007a:
						// case 0x007b:
						// break;

						case 0x007c:
							// 0x07c
							// 000001111100
							// "PD4 J/"
							// Skip if (PD4 input) = 0
#ifndef TEST_OPCODE
test = 0;
#endif
						break;

						// case 0x007d:
						// case 0x007e:
						// case 0x007f:
						// break

						case 0x0200: case 0x0201: case 0x0202: case 0x0203: 
							// 0x0200-0x0203
							// 0010000000NN
							// sprintf(hex_instruction, "A1·A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A1{7:1} AND A1{7:1}) makes zero, N{2:1} => L{2:1}");"
#ifndef TEST_OPCODE
							if(reg_a1 == 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0204: case 0x0205: case 0x0206: case 0x0207: 
						// break;

						case 0x0208: case 0x0209: case 0x020a: case 0x020b: 
							// 0x0208-0x020b
							// 0010000010NN
							// sprintf(hex_instruction, "A1=A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A1{7:1} - A1{7:1}) makes zero, N{2:1} => L{2:1}");"
#ifndef TEST_OPCODE
							program_counter_linear++;
							reg_a = address_linear_to_polynomial[program_counter_linear];
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x020c: case 0x020d: case 0x020e: case 0x020f: 
							// 0x020c-0x020f
							// 0010000011NN
							// sprintf(hex_instruction, "A1-A1, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (A1{7:1} - A1{7:1}) makes borrow, N{2:1} => L{2:1}");"
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0210: case 0x0211: case 0x0212: case 0x0213:
							// 0x210-0x213
							// 0010000100NN
							// sprintf(hex_instruction, "A1·A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A1{7:1} AND A2{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((reg_a1 & reg_a2) == 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0214: case 0x0215: case 0x0216: case 0x0217:
						// break;

						case 0x0218: case 0x0219: case 0x021a: case 0x021b: 
							// 0x218-0x21b
							// 0010000110NN
							// sprintf(hex_instruction, "A1=A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A1{7:1} - A2{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a1 == reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x021c: case 0x021d: case 0x021e: case 0x021f: 
							// 0x21c-0x21f
							// 0010000111NN
							// sprintf(hex_instruction, "A1-A2, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (A1{7:1} - A2{7:1}) makes borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a1 < reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0220: case 0x0221: case 0x0222: case 0x0223: 
							// 0x220-0x223
							// 0010001000NN
							// sprintf(hex_instruction, "A1·A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A1{7:1} AND A1{7:1}) makes non zero, N{2:1} => L{2:1}");"
#ifndef TEST_OPCODE
							if(reg_a1 != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0224: case 0x0225: case 0x0226: case 0x0227: 
						// break;

						case 0x0228: case 0x0229: case 0x022a: case 0x022b: 
							// 0x228-0x22b
							// 0010001010NN
							// sprintf(hex_instruction, "A1=A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A1{7:1} - A1{7:1}) makes non zero, N{2:1} => L{2:1}");"
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x022c: case 0x022d: case 0x022e: case 0x022f: 
							// 0x22c-0x22f
							// 0010001011NN
							// sprintf(hex_instruction, "A1-A1, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (A1{7:1} - A1{7:1}) makes non borrow, N{2:1} => L{2:1}");"
#ifndef TEST_OPCODE
							program_counter_linear++;
							reg_a = address_linear_to_polynomial[program_counter_linear];
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0230: case 0x0231: case 0x0232: case 0x0233:
							// 0x230-0x233
							// 0010001100NN
							// sprintf(hex_instruction, "A1·A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A1{7:1} AND A2{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((reg_a1 & reg_a2) != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0234: case 0x0235: case 0x0236: case 0x0237:
						// break;

						case 0x0238: case 0x0239: case 0x023a: case 0x023b:
							// 0x238-0x23b
							// 0010001110NN
							// sprintf(hex_instruction, "A1=A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A1{7:1} - A2{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a1 != reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x023c: case 0x023d: case 0x023e: case 0x023f:
							// 0x23c0x23f
							// 0010001111NN
							// sprintf(hex_instruction, "A1-A2, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (A1{7:1} - A2{7:1}) makes non borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a1 >= reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0240: case 0x0241: case 0x0242: case 0x0243: 
							// 0x240-0x243
							// 0010010000NN
							// sprintf(hex_instruction, "A2·A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A2{7:1} AND A1{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((reg_a2 & reg_a1) == 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0244: case 0x0245: case 0x0246: case 0x0247: 
						// break;

						case 0x0248: case 0x0249: case 0x024a: case 0x024b: 
							// 0x248-0x24b
							// 0010010010NN
							// sprintf(hex_instruction, "A2=A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A2{7:1} - A1{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 == reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x024c: case 0x024d: case 0x024e: case 0x024f: 
							// 0x24c0x24f
							// 0010010011NN
							// sprintf(hex_instruction, "A2-A1, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (A2{7:1} - A1{7:1}) makes borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 < reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0250: case 0x0251: case 0x0252: case 0x0253: 
							// 0x250-0x253
							// 0010010100NN
							// sprintf(hex_instruction, "A2·A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A2{7:1} AND A2{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 == 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0254: case 0x0255: case 0x0256: case 0x0257: 
						// break;

						case 0x0258: case 0x0259: case 0x025a: case 0x025b: 
							// 0x258-0x25b
							// 0010010110NN
							// sprintf(hex_instruction, "A2=A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (A2{7:1} - A2{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							program_counter_linear++;
							reg_a = address_linear_to_polynomial[program_counter_linear];
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x025c: case 0x025d: case 0x025e: case 0x025f: 
							// 0x25c-0x25f
							// 0010010111NN
							// sprintf(hex_instruction, "A2-A2, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (A2{7:1} - A2{7:1}) makes borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0260: case 0x0261: case 0x0262: case 0x0263: 
							// 0x260-0x263
							// 0010011000NN
							// sprintf(hex_instruction, "A2·A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A2{7:1} AND A1{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((reg_a2 & reg_a1) != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0264: case 0x0265: case 0x0266: case 0x0267: 
						// break;

						case 0x0268: case 0x0269: case 0x026a: case 0x026b: 
							// 0x268-0x26b
							// 0010011010NN
							// sprintf(hex_instruction, "A2=A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A2{7:1} - A1{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 != reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x026c: case 0x026d: case 0x026e: case 0x026f: 
							// 0x26c-0x26f
							// 0010011011NN
							// sprintf(hex_instruction, "A2-A1, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (A2{7:1} - A1{7:1}) makes non borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 >= reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0270: case 0x0271: case 0x0272: case 0x0273: 
							// 0x270-0x273
							// 0010011100NN
							// sprintf(hex_instruction, "A2·A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A2{7:1} AND A2{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0274: case 0x0275: case 0x0276: case 0x0277: 
						// break;

						case 0x0278: case 0x0279: case 0x027a: case 0x027b: 
							// 0x278-0x27b
							// 0010011110NN
							// sprintf(hex_instruction, "A2=A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (A2{7:1} - A2{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x027c: case 0x027d: case 0x027e: case 0x027f: 
							// 0x27c-0x27f
							// 0010011111NN
							// sprintf(hex_instruction, "A2-A2, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (A2{7:1} - A2{7:1}) makes non borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							program_counter_linear++;
							reg_a = address_linear_to_polynomial[program_counter_linear];
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0280: case 0x0281: case 0x0282: case 0x0283: 
							// 0x280-0x283
							// 0010100000NN
							// sprintf(hex_instruction, "M·A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} AND A1{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((memory_ram[reg_hl] & reg_a1) == 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0284: case 0x0285: case 0x0286: case 0x0287: 
						// break;

						case 0x0288: case 0x0289: case 0x028a: case 0x028b: 
							// 0x288-0x28b
							// 0010100010NN
							// sprintf(hex_instruction, "M=A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A1{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] == reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x028c: case 0x028d: case 0x028e: case 0x028f:
							// 0x28c-0x28f
							// 0010100011NN
							// sprintf(hex_instruction, "M-A1, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A1{7:1}) makes borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] < reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0290: case 0x0291: case 0x0292: case 0x0293:
							// 0x290-0x293
							// 0010100100NN
							// sprintf(hex_instruction, "M·A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} AND A2{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((memory_ram[reg_hl] & reg_a2) == 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0294: case 0x0295: case 0x0296: case 0x0297:
						// break;

						case 0x0298: case 0x0299: case 0x029a: case 0x029b: 
							// 0x298-0x29b
							// 0010100110NN
							// sprintf(hex_instruction, "M=A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A2{7:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] == reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x029c: case 0x029d: case 0x029e: case 0x029f: 
							// 0x29c-0x29f
							// 0010100111NN
							// sprintf(hex_instruction, "M-A2, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A2{7:1}) makes borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] < reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;
						
						case 0x02a0: case 0x02a1: case 0x02a2: case 0x02a3:
							// 0x2a0-0x2a3
							// 0010101000NN
							// sprintf(hex_instruction, "M·A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} AND A1{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((memory_ram[reg_hl] & reg_a1) != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x02a4: case 0x02a5: case 0x02a6: case 0x02a7:
						// break;

						case 0x02a8: case 0x02a9: case 0x02aa: case 0x02ab: 
							// 0x2a8-0x2ab
							// 0010101010NN
							// sprintf(hex_instruction, "M=A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A1{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] != reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02ac: case 0x02ad: case 0x02ae: case 0x02af: 
							// 0x2ac-0x2af
							// 0010101011NN
							// sprintf(hex_instruction, "M-A1, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A1{7:1}) makes non borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] >= reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02b0: case 0x02b1: case 0x02b2: case 0x02b3:
							// 0x2b0-0x2b3
							// 0010101100NN
							// sprintf(hex_instruction, "M·A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} AND A2{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((memory_ram[reg_hl] & reg_a2) != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x02b4: case 0x02b5: case 0x02b6: case 0x02b7:
						// break;

						case 0x02b8: case 0x02b9: case 0x02ba: case 0x02bb:
							// 0x2b8-0x2bb
							// 0010101110NN
							// sprintf(hex_instruction, "M=A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A2{7:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] != reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02bc: case 0x02bd: case 0x02be: case 0x02bf: 
							// 0x2bc-0x2bf
							// 0010101111NN
							// sprintf(hex_instruction, "M-A2, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (M[H{5:1},L{2:1}]{7:1} - A2{7:1}) makes non borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] >= reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02c0: case 0x02c1: case 0x02c2: case 0x02c3: 
							// 0x2c0-0x2c3
							// 0010110000NN
							// sprintf(hex_instruction, "H·A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (H{5:1} AND A1{5:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((((reg_hl & 0x7c) >> 2) & (reg_a1 & 0x1f)) == 0)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x02c4: case 0x02c5: case 0x02c6: case 0x02c7: 
						// break;

						case 0x02c8: case 0x02c9: case 0x02ca: case 0x02cb: 
							// 0x2c8-0x2cb
							// 0010110010NN
							// sprintf(hex_instruction, "H=A1, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (H{5:1} - A1{5:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) == (reg_a1 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02cc: case 0x02cd: case 0x02ce: case 0x02cf: 
							// 0x2cc-0x2cf
							// 0010110011NN
							// sprintf(hex_instruction, "H-A1, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (H{5:1} - A1{5:1}) makes borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) < (reg_a1 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02d0: case 0x02d1: case 0x02d2: case 0x02d3: 
							// 0x2d0-0x2d3
							// 0010110100NN
							// sprintf(hex_instruction, "H·A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (H{5:1} AND A2{5:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((((reg_hl & 0x7c) >> 2) & (reg_a2 & 0x1f)) == 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x02d4: case 0x02d5: case 0x02d6: case 0x02d7: 
						// break;

						case 0x02d8: case 0x02d9: case 0x02da: case 0x02db: 
							// 0x2d8-0x2db
							// 0010110110NN
							// sprintf(hex_instruction, "H=A2, N=>L, +1 EQJ");
							// sprintf(hex_description, "Skip if (H{5:1} - A2{5:1}) makes zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) == (reg_a2 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02dc: case 0x02dd: case 0x02de: case 0x02df: 
							// 0x2dc-0x2df
							// 0010110111NN
							// sprintf(hex_instruction, "H-A2, N=>L, +1 BOJ");
							// sprintf(hex_description, "Skip if (H{5:1} - A2{5:1}) makes borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) < (reg_a2 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02e0: case 0x02e1: case 0x02e2: case 0x02e3: 
							// 0x2e0-0x2e3
							// 0010111000NN
							// sprintf(hex_instruction, "H·A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (H{5:1} AND A1{5:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((((reg_hl & 0x7c) >> 2) & (reg_a1 & 0x1f)) != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x02e4: case 0x02e5: case 0x02e6: case 0x02e7: 
						// break;

						case 0x02e8: case 0x02e9: case 0x02ea: case 0x02eb: 
							// 0x2e8-0x2eb
							// 0010111010NN
							// sprintf(hex_instruction, "H=A1, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (H{5:1} - A1{5:1}){5:1} makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) != (reg_a1 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02ec: case 0x02ed: case 0x02ee: case 0x02ef: 
							// 0x2ec-0x2ef
							// 0010111011NN
							// sprintf(hex_instruction, "H-A1, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (H{5:1} - A1{5:1}) makes non borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) >= (reg_a1 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02f0: case 0x02f1: case 0x02f2: case 0x02f3: 
							// 0x2f0-0x2f3
							// 0010111100NN
							// sprintf(hex_instruction, "H·A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (H{5:1} AND A2{5:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if((((reg_hl & 0x7c) >> 2) & (reg_a2 & 0x1f)) != 0x00)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x02f4: case 0x02f5: case 0x02f6: case 0x02f7: 
						// break;

						case 0x02f8: case 0x02f9: case 0x02fa: case 0x02fb: 
							// 0x2f8-0x2fb
							// 0010111110NN
							// sprintf(hex_instruction, "H=A2, N=>L, +1 EQJ/");
							// sprintf(hex_description, "Skip if (H{5:1} - A2{5:1}) makes non zero, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) != (reg_a2 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x02fc: case 0x02fd: case 0x02fe: case 0x02ff: 
						{
							// 0x2fc-0x2ff
							// 0010111111NN
							// sprintf(hex_instruction, "H-A2, N=>L, +1 BOJ/");
							// sprintf(hex_description, "Skip if (H{5:1} - A2{5:1}) makes non borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(((reg_hl & 0x7c) >> 2) >= (reg_a2 & 0x1f))
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0300: case 0x0301: case 0x0302: case 0x0303: 
							// 0x300-0x303
							// 0011000000NN
							// sprintf(hex_instruction, "N=>L");
							// sprintf(hex_description, "N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0304: case 0x0305: case 0x0306: case 0x0307: 
						// break;

						case 0x0308:
							// 0x308
							// 001100001000
							// sprintf(hex_instruction, "A1=>FLS, 0=>L");
							// sprintf(hex_description, "Move A1{7:1} to FLS{7:1}, 0{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_fls = reg_a1;
							reg_hl = reg_hl & 0x7c;
#endif
						break;

						case 0x0309:
							// 0x309
							// 001100001001
							// sprintf(hex_instruction, "A1=>FRS, 1=>L");
							// sprintf(hex_description, "Move A1{7:1} to FRS{7:1}, [0{1},1{1}]{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_frs = reg_a1;
							reg_hl = (reg_hl & 0x7c) & 0x01;
#endif
						break;

						case 0x030a: case 0x030b:
							// 0x30a-0x30b
							// 00110000101N
							// sprintf(hex_instruction, "A1=>MODE, 1N=>L");
							// sprintf(hex_description, "Move A1{7:1} to MODE{7:1}, [1{1},N{1}]{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_mode = reg_a1;
							reg_hl = (reg_hl & 0x7c) ^ 0x02 ^ (uint8_t)(opcode & 0x0001);
#endif
						break;

						case 0x0310: case 0x0311: case 0x0312: case 0x0313: 
							// 0x310-0x313
							// 0011000100NN
							// sprintf(hex_instruction, "A2=>A1, N=>L");
							// sprintf(hex_description, "A2{7:1} => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a1 = reg_a2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;
// 						case 0x0310: case 0x0311: case 0x0312: case 0x0313: 
// 							// 0x310-0x313
// 							// 0011000100NN
// 							// sprintf(hex_instruction, "A1·A2=>A1, N=>L");
// 							// sprintf(hex_description, "(A1{7:1} AND A2{7:1}){7:1} => A1{7:1}, N{2:1} => L{2:1}");
// #ifndef TEST_OPCODE
// 							reg_a1 = reg_a1 & reg_a2;
// 							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
// #endif
// 						break;

						case 0x0314: case 0x0315: case 0x0316: case 0x0317: 
							// 0x314-0x317
							// 0011000101NN
							// sprintf(hex_instruction, "A1+A2=>A1, N=>L");
							// sprintf(hex_description, "(A1{7:1} + A2{7:1}){7:1} A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(reg_a1) + (uint16_t)(reg_a2);
							reg_a1 = (tmp_a & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0318: case 0x0319: case 0x031a: case 0x031b: 
							// 0x318-0x31b
							// 0011000110NN
							// sprintf(hex_instruction, "A1=>RS, N=>L");
							// sprintf(hex_description, "Right shift A1{7:1}, 0{1}=>A1{7}, N{2:1} => L{2:1}");
							reg_a1 = reg_a1 >> 1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
						break;
// 						case 0x0318: case 0x0319: case 0x031a: case 0x031b: 
// 							// 0x318-0x31b
// 							// 0011000110NN
// 							// sprintf(hex_instruction, "A1vA2=>A1, N=>L");
// 							// sprintf(hex_description, "(A1{7:1} OR A2{7:1}){7:1} => A1{7:1}, N{2:1} => L{2:1}");
// #ifndef TEST_OPCODE
// 							reg_a1 = reg_a1 | reg_a2;
// 							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
// #endif
// 						break;

						case 0x031c: case 0x031d: case 0x031e: case 0x031f: 
							// 0x31c-0x31f
							// 0011000110NN
							// sprintf(hex_instruction, "A1-A2=>A1, N=>L, +1 BOJ");
							// sprintf(hex_description, "(A1{7:1} - A2{7:1}) => A1{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a1 < reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_a1 = (reg_a1 - reg_a2) & 0x7f;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0320: case 0x0321: case 0x0322: case 0x0323: 
							// 0x320-0x323
							// 0011001000NN
							// sprintf(hex_instruction, "A1·A1=>A1, N=>L");
							// sprintf(hex_description, "(A1{7:1} AND A1{7:1}){7:1} => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0324: case 0x0325: case 0x0326: case 0x0327: 
							// 0x324-0x327
							// 0011001001NN
							// sprintf(hex_instruction, "A1+A1=>A1, N=>L");
							// sprintf(hex_description, "(A1{7:1} + A1{7:1}) => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(reg_a1) << 1;
							reg_a1 = (tmp_a & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0328: case 0x0329: case 0x032a: case 0x032b: 
							// 0x328-0x32b
							// 0011001010NN
							// sprintf(hex_instruction, "A1vA1=>A1, N=>L");
							// sprintf(hex_description, "(A1{7:1} OR A1{7:1}){7:1} => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x032c: case 0x032d: case 0x032e: case 0x032f: 
							// 0x32c+0x32f
							// 0011001011NN
							// sprintf(hex_instruction, "A1-A1=>A1, N=>L, +1 BOJ");
							// sprintf(hex_description, "(A1{7:1} - A1{7:1}) => A1{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a1 = 0;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0330: case 0x0331: case 0x0332: case 0x0333: 
							// 0x330-0x333
							// 0011001100NN
							// sprintf(hex_instruction, "A1·A2=>A1, N=>L");
							// sprintf(hex_description, "(A1{7:1} AND A2{7:1}) => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a1 = reg_a1 & reg_a2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0334: case 0x0335: case 0x0336: case 0x0337: 
							// 0x334-0x337
							// 0011001101NN
							// sprintf(hex_instruction, "A1+A2=>A1, N=>L");
							// sprintf(hex_description, "(A1{7:1} + A2{7:1}) => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(reg_a1) + (uint16_t)(reg_a2);
							reg_a1 = (tmp_a1 & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0338: case 0x0339: case 0x033a: case 0x033b: 
							// 0x338-0x33b
							// 0011001110NN
							// sprintf(hex_instruction, "A1vA2=>A1, N=>L");
							// sprintf(hex_description, "(A1{7:1} OR A2{7:1}) => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a1 = reg_a1 | reg_a2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x033c: case 0x033d: case 0x033e: case 0x033f: 
							// 0x33c-0x33f
							// 0011001111NN
							// sprintf(hex_instruction, "A1-A2=>A1, N=>L, +1 BOJ");
							// sprintf(hex_description, "(A1{7:1} - A2{7:1}) => A1{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a1 < reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_a1 = (reg_a1 - reg_a2) & 0x7f;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0340: case 0x0341: case 0x0342: case 0x0343: 
							// 0x340-0x343
							// 0011010000NN
							// sprintf(hex_instruction, "A1=>A2, N=>L");
							// sprintf(hex_description, "A1{7:1} => A2{7:1}, N{2:1} => L{2:1}");
							reg_a2 = reg_a1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
						break;
// 						case 0x0340: case 0x0341: case 0x0342: case 0x0343: 
// 							// 0x340-0x343
// 							// 0011010000NN
// 							// sprintf(hex_instruction, "A2·A1=>A2, N=>L");
// 							// sprintf(hex_description, "(A2{7:1} AND A1{7:1}){7:1} => A2{7:1}, N{2:1} => L{2:1}");
// #ifndef TEST_OPCODE
// 							reg_a2 = reg_a2 & reg_a1;
// 							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
// #endif
// 						break;

						case 0x0344: case 0x0345: case 0x0346: case 0x0347: 
							// 0x344-0x347
							// 0011010001NN
							// sprintf(hex_instruction, "A2+A1=>A2, N=>L");
							// sprintf(hex_description, "(A2{7:1} + A1{7:1}) => A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(reg_a2) + (uint16_t)(reg_a1);
							reg_a2 = (tmp_a1 & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0348:
							// 0x348
							// 001101001000
							// sprintf(hex_instruction, "A2=>FLS, 0=>L");
							// sprintf(hex_description, "A2{7:1} => FLS{7:1}, 0{2:1} => L{2:1}");
							reg_fls = reg_a2;
							reg_hl = reg_hl & 0x7c;
						break;

						case 0x0349:
							// 0x349
							// 001101001001
							// sprintf(hex_instruction, "A2=>FRS, 1=>L");
							// sprintf(hex_description, "A2{7:1} => FRS{7:1}, [0{1},1{1}]{2:1} => L{2:1}");
							reg_frs = reg_a2;
							reg_hl = (reg_hl & 0x7c) & 0x01;
						break;

						case 0x034a: case 0x034b:
							// 0x34a-0x34b
							// 00110100101N
							// "A2 => MODE, 1N => L"
							// Move A2[7:1] to MODE[7:1], 1N => L[2:1]
							// sprintf(hex_instruction, "A2=>MODE, 1N=>L");
							// sprintf(hex_description, "A2{7:1} => MODE{7:1}, [1{1},N{1}]{2:1} => L{2:1}");
							reg_mode = reg_a2;
							reg_hl = (reg_hl & 0x7c) ^ 0x02 ^ (uint8_t)(opcode & 0x0001);
						break;

// 						case 0x0348: case 0x0349: case 0x034a: case 0x034b: 
// 							// 0x348-0x34b
// 							// 0011010010NN
// 							// sprintf(hex_instruction, "A2vA1=>A2, N=>L");
// 							// sprintf(hex_description, "(A2{7:1} OR A1{7:1}){7:1} => A2{7:1}, N{2:1} => L{2:1}");
// #ifndef TEST_OPCODE
// 							reg_a2 = reg_a2 | reg_a1;
// 							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
// #endif
// 						break;

						case 0x034c: case 0x034d: case 0x034e: case 0x034f: 
							// 0x34c-0x34f
							// 0011010011NN
							// sprintf(hex_instruction, "A2-A1=>A2, N=>L, +1 BOJ");
							// sprintf(hex_description, "(A2{7:1} - A1{7:1}) => A2{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 < reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_a2 = (reg_a2 - reg_a1) & 0x7f;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x0350: case 0x0351: case 0x0352: case 0x0353: 
						// case 0x0354: case 0x0355: case 0x0356: case 0x0357: 
						// break;

						case 0x0358: case 0x0359: case 0x035a: case 0x035b: 
							// 0x358-0x35b
							// 0011010110NN
							// sprintf(hex_instruction, "A2=>RS, N=>L");
							// sprintf(hex_description, "Right shift A2{7:1}, 0{1} => A1{7}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a2 = reg_a2 >> 1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0360: case 0x0361: case 0x0362: case 0x0363: 
							// 0x360-0x363
							// 0011011000NN
							// sprintf(hex_instruction, "A2·A1=>A2, N=>L");
							// sprintf(hex_description, "(A2{7:1} AND A1{7:1}) => A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a2 = reg_a2 & reg_a1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0364: case 0x0365: case 0x0366: case 0x0367: 
							// 0x364-0x367
							// 0011011001NN
							// sprintf(hex_instruction, "A2+A1=>A2, N=>L");
							// sprintf(hex_description, "(A2{7:1} + A1{7:1}) => A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(reg_a2) + (uint16_t)(reg_a1);
							reg_a2 = (tmp_a & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0368: case 0x0369: case 0x036a: case 0x036b: 
							// 0x368-0x36b
							// 0011011010NN
							// sprintf(hex_instruction, "A2vA1=>A2, N=>L");
							// sprintf(hex_description, "(A2{7:1} OR A1{7:1}) => A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a2 = reg_a2 | reg_a1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x036c: case 0x036d: case 0x036e: case 0x036f: 
							// 0x36c-0x36f
							// 0011011011NN
							// sprintf(hex_instruction, "A2-A1=>A2, N=>L, +1 BOJ");
							// sprintf(hex_description, "(A2{7:1} - A1{7:1}) => A2{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(reg_a2 < reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_a2 = (reg_a2 - reg_a1) & 0x7f;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0370: case 0x0371: case 0x0372: case 0x0373: 
							// 0x370-0x373
							// 0011011100NN
							// sprintf(hex_instruction, "A2·A2=>A2, N=>L");
							// sprintf(hex_description, "(A2{7:1} AND A2{7:1}) => A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0374: case 0x0375: case 0x0376: case 0x0377: 
							// 0x374-0x377
							// 0011011101NN
							// sprintf(hex_instruction, "A2+A2=>A2, N=>L");
							// sprintf(hex_description, "(A2{7:1} + A2{7:1}) => A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(reg_a2) << 1;
							reg_a2 = (tmp_a & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0378: case 0x0379: case 0x037a: case 0x037b: 
							// 0x378-0x37b
							// 0011011110NN
							// sprintf(hex_instruction, "A2vA2=>A2, N=>L");
							// sprintf(hex_description, "(A2{7:1} OR A2{7:1}) => A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x037c: case 0x037d: case 0x037e: case 0x037f: 
							// 0x37c-0x37f
							// 0011011011NN
							// sprintf(hex_instruction, "A2-A2=>A2, N=>L, +1 BOJ");
							// sprintf(hex_description, "(A2{7:1} - A2{7:1}) => A2{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a2 = 0;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0380: case 0x0381: case 0x0382: case 0x0383: 
							// 0x380-0x383
							// 0011100000NN
							// sprintf(hex_instruction, "A1=>M, N=>L");
							// sprintf(hex_description, "A1{7:1} => M[H{5:1},L{2:1}]{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							memory_ram[reg_hl] = reg_a1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0384: case 0x0385: case 0x0386: case 0x0387: 
							// 0x384-0x387
							// 0011100001NN
							// sprintf(hex_instruction, "M<=>A1, N=>L");
							// sprintf(hex_description, "M[H{5:1},L{2:1}]{7:1} <=> A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a1 = memory_ram[reg_hl];
							memory_ram[reg_hl] = reg_a1;
							reg_a1 = tmp_a1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0388:
							// 0x388
							// 001110001000
							// sprintf(hex_instruction, "M=>FLS, 0=>L");
							// sprintf(hex_description, "M[H{5:1},L{2:1}]{7:1} => FLS{7:1}, 0{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_fls = memory_ram[reg_hl];
							reg_hl &= 0x7c;
#endif
						break;

						case 0x0389:
							// 0x389
							// 001110001001
							// sprintf(hex_instruction, "M=>FRS, 1=>L");
							// sprintf(hex_description, "M[H{5:1},L{2:1}]{7:1} => FRS{7:1}, 1{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_frs = memory_ram[reg_hl];
							reg_hl = (reg_hl & 0x7c) ^ 0x01;
#endif
						break;

						case 0x038a: case 0x038b:
							// 0x38a-0x38b
							// 00111000101N
							// sprintf(hex_instruction, "M=>MODE, 1N=>L");
							// sprintf(hex_description, "M[H{5:1},L{2:1}]{7:1} => MODE{7:1}, [1{1},N{1}]{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_mode = memory_ram[reg_hl];
							reg_hl = (reg_hl & 0x7c) ^ 0x02 ^ (uint8_t)(opcode & 0x0001);
#endif
						break;

						case 0x038c: case 0x038d: case 0x038e: case 0x038f: 
							// 0x38c-0x38f
							// 0011100011NN
							// sprintf(hex_instruction, "M=>A1, N=>L");
							// sprintf(hex_description, "M[H{5:1},L{2:1}]{7:1} => A1{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a1 = memory_ram[reg_hl];
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0390: case 0x0391: case 0x0392: case 0x0393: 
							// 0x390-0x393
							// 0011100100NN
							// sprintf(hex_instruction, "A2=>M, N=>L");
							// sprintf(hex_description, "A2{7:1} => M[H{5:1},L{2:1}]{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							memory_ram[reg_hl] = reg_a2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0394: case 0x0395: case 0x0396: case 0x0397: 
							// 0x394-0x397
							// 0011100101NN
							// sprintf(hex_instruction, "M<=>A2, N=>L");
							// sprintf(hex_description, "M[H{5:1},L{2:1}]{7:1} <=> A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a2 = memory_ram[reg_hl];
							memory_ram[reg_hl] = reg_a2;
							reg_a2 = tmp_a2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x0398: case 0x0399: case 0x039a: case 0x039b: 
							// 0x398-0x39b
							// 0011100110NN
							// sprintf(hex_instruction, "M=>RS, N=>L");
							// sprintf(hex_description, "right shift M[H{5:1},L{2:1}]{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							memory_ram[reg_hl] = memory_ram[reg_hl] >> 1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x039c: case 0x039d: case 0x039e: case 0x039f: 
							// 0x39c-0x39f
							// 0011100111NN
							// sprintf(hex_instruction, "M=>A2, N=>L");
							// sprintf(hex_description, "Move M[H{5:1},L{2:1}]{7:1} to A2{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a2 = memory_ram[reg_hl];
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03a0: case 0x03a1: case 0x03a2: case 0x03a3: 
							// 0x3a0-0x3a3
							// 0011101000NN
							// sprintf(hex_instruction, "M·A1 => M, N => L");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} AND A1{7:1}){7:1} => M[H{5:1},L{2:1}]{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							memory_ram[reg_hl] = memory_ram[reg_hl] & reg_a1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03a4: case 0x03a5: case 0x03a6: case 0x03a7: 
							// 0x3a4-0x3a7
							// 0011101001NN
							// sprintf(hex_instruction, "M+A1 => M, N => L, +1 CAJ");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} + A1{7:1}){7:1} => M[H{5:1},L{2:1}]{7:1}, skip if carry, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(memory_ram[reg_hl]) + (uint16_t)(reg_a1);
							if(tmp_a > 0x007f)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							memory_ram[reg_hl] = (tmp_a & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03a8: case 0x03a9: case 0x03aa: case 0x03ab: 
							// 0x3a8-0x3ab
							// 0011101010NN
							// sprintf(hex_instruction, "MvA1 => M, N => L");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} OR A1{7:1}){7:1} => M[H{5:1},L{2:1}]{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							memory_ram[reg_hl] = memory_ram[reg_hl] | reg_a1;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03ac: case 0x03ad: case 0x03ae: case 0x03af: 
							// 0x3ac+0x3af
							// 0011101011NN
							// sprintf(hex_instruction, "M-A1=>M, N=>L, +1 BOJ");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} - A1{7:1}) => M[H{5:1},L{2:1}]{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] < reg_a1)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							memory_ram[reg_hl] = (memory_ram[reg_hl] - reg_a1) & 0x7f;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03b0: case 0x03b1: case 0x03b2: case 0x03b3: 
							// 0x3b0-0x3b3
							// 0011101100NN
							// sprintf(hex_instruction, "M·A2=>M, N=>L");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} AND A2{7:1}) => M[H{5:1},L{2:1}]{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							memory_ram[reg_hl] = memory_ram[reg_hl] & reg_a2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03b4: case 0x03b5: case 0x03b6: case 0x03b7: 
							// 0x3b4-0x3b7
							// 0011101101NN
							// sprintf(hex_instruction, "M+A2 => M, N => L, +1 CAJ");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} + A2{7:1}){7:1} => M[H{5:1},L{2:1}]{7:1}, skip if carry, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							tmp_a = (uint16_t)(memory_ram[reg_hl]) + (uint16_t)(reg_a2);
							if(tmp_a > 0x007f)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							memory_ram[reg_hl] = (tmp_a & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03b8: case 0x03b9: case 0x03ba: case 0x03bb: 
							// 0x3b8-0x3bb
							// 0011101110NN
							// sprintf(hex_instruction, "MvA2 => M, N => L");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} OR A2{7:1}){7:1} => M[H{5:1},L{2:1}]{7:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							memory_ram[reg_hl] = memory_ram[reg_hl] | reg_a2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03bc: case 0x03bd: case 0x03be: case 0x03bf: 
							// 0x3bc-0x3bf
							// 0011101111NN
							// sprintf(hex_instruction, "M-A2=>M, N=>L, +1 BOJ");
							// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} - A2{7:1}) => M[H{5:1},L{2:1}]{7:1}, skip if borrow, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							if(memory_ram[reg_hl] < reg_a2)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							memory_ram[reg_hl] = (memory_ram[reg_hl] - reg_a2) & 0x7f;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03c0: case 0x03c1: case 0x03c2: case 0x03c3: 
							// 0x3c0-0x3c3
							// 0011110000NN
							// sprintf(hex_instruction, "A1=>H, N=>L");
							// sprintf(hex_description, "A1{5:1} => H{5:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_a1 << 2) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x03c4: case 0x03c5: case 0x03c6: case 0x03c7: 
						// case 0x03c8: case 0x03c9: case 0x03ca: case 0x03cb: 
						// break;

						case 0x03cc: case 0x03cd: case 0x03ce: case 0x03cf: 
							// 0x3cc-0x3cf
							// 0011110011NN
							// sprintf(hex_instruction, "H=>A1, N=>L");
							// sprintf(hex_description, "0{2:1} => A1{7:1}, H{5:1} => A1{5:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a1 = reg_hl >> 2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03d0: case 0x03d1: case 0x03d2: case 0x03d3: 
							// 0x3d0-0x3d3
							// 0011110100NN
							// sprintf(hex_instruction, "A2=>H, N=>L");
							// sprintf(hex_description, "A2{5:1} => H{5:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (reg_a2 << 2) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x03d4: case 0x03d5: case 0x03d6: case 0x03d7: 
						// case 0x03d8: case 0x03d9: case 0x03da: case 0x03db: 
						// break;

						case 0x03dc: case 0x03dd: case 0x03de: case 0x03df: 
							// 0x3dc-0x3df
							// 0011110111NN
							// sprintf(hex_instruction, "H=>A2, N=>L");
							// sprintf(hex_description, "0{2:1} => A2{7:1}, H{5:1} => A2{5:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_a2 = reg_hl >> 2;
							reg_hl = (reg_hl & 0x7c) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03e0: case 0x03e1: case 0x03e2: case 0x03e3:
							// 0x3e0-0x3e3
							// 0011111000NN
							// sprintf(hex_instruction, "H·A1=>>H, N=>L");
							// sprintf(hex_description, "H{5:1} AND A1{5:1} => H{5:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (((reg_hl >> 2) & reg_a1) << 2) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						case 0x03e4: case 0x03e5: case 0x03e6: case 0x03e7:
							// 0x3e4-0x3e7
							// 0011111001NN
							// sprintf(hex_instruction, "H+A1-->H, 2-->L");
							// sprintf(hex_description, "H{5:1} + A1{5:1} => H{5:1}, N{2:1} => L{2:1}");
#ifndef TEST_OPCODE
							reg_hl = (((reg_hl >> 2) + (reg_a1 & 0x1f)) << 2) ^ (uint8_t)(opcode & 0x0003);
#endif
						break;

						// case 0x03e8: case 0x03e9: case 0x03ea: case 0x03eb: 
						
						case 0x03ec: case 0x03ed: case 0x03ee: case 0x03ef:
							// 0x3ec-0x3ef
							// 0011111011NN
							// sprintf(hex_instruction, "H-A1=>>H, N=>L");
							// sprintf(hex_description, "H{5:1} - A1{5:1} => H{5:1}, skip if borrow, N{2:1} => L{2:1}");
							reg_hl = ((((reg_hl >> 2) - (reg_a1 & 0x1f)) & 0x7f) << 2) ^ (uint8_t)(opcode & 0x0003);
						break;

						// case 0x3f0:
						// case 0x3f1:
						// case 0x3f2:
						// case 0x3f3:
						// case 0x3f4:
						// case 0x3f5:
						// case 0x3f6:
						// case 0x3f7:
						// case 0x3f8:
						// case 0x3f9:
						// case 0x3fa:
						// case 0x3fb:
						// case 0x3fc:
						// case 0x3fd:
						// case 0x3fe:
						// case 0x3ff:
						// break;

						case 0x0400: case 0x0401:
							// 0x400-0x401
							// 01000000000N
							// sprintf(hex_instruction, "N=>A11");
							// sprintf(hex_description, "N{1} => A{11}");
#ifndef TEST_OPCODE
							reg_a = (reg_a & 0x03ff) ^ (opcode & 0x0001);
#endif
						break;

						case 0x0402: case 0x0403:
							// 0x402-0x403
							// 01000000001N
							// sprintf(hex_instruction, "JPM, 0=>L, N=>A11");
							// sprintf(hex_description, "N => A[11], 0{3:1} => A{10:8}, M[H{5:1},L{2:1}]{5:1} => A{7:3}, 1{1} => A{2}, N{1} => A{1}, JP A, 0 => L [2:1]");
#ifndef TEST_OPCODE
							tmp_a = opcode & 0x0001;
							reg_a = (tmp_a << 10) ^ ((uint16_t)(memory_ram[reg_hl] & 0x1f) << 2) ^ 0x0002 ^ tmp_a;
							program_counter_linear = address_polynomial_to_linear[reg_a];
							reg_hl = reg_hl & 0x7c;
#endif
						break;

						// case 0x404: case 0x405: case 0x406: case 0x407:
						// case 0x408: case 0x409: case 0x40a: case 0x40b: case 0x40c: case 0x40d: case 0x40e: case 0x40f:
						// case 0x410: case 0x411: case 0x412: case 0x413: case 0x414: case 0x415: case 0x416: case 0x417:
						// case 0x418: case 0x419: case 0x41a: case 0x41b: case 0x41c: case 0x41d: case 0x41e: case 0x41f:
						// case 0x420: case 0x421: case 0x422: case 0x423: case 0x424: case 0x425: case 0x426: case 0x427:
						// case 0x428: case 0x429: case 0x42a: case 0x42b: case 0x42c: case 0x42d: case 0x42e: case 0x42f:
						// case 0x430: case 0x431: case 0x432: case 0x433: case 0x434: case 0x435: case 0x436: case 0x437:
						// case 0x438: case 0x439: case 0x43a: case 0x43b: case 0x43c: case 0x43d: case 0x43e: case 0x43f:
						// break;

						case 0x0440: case 0x0441:
						// case 0x0442: case 0x0443:
						case 0x0444: case 0x0445:
						// case 0x0446: case 0x0447:
						case 0x0448: case 0x0449:
						// case 0x044a: case 0x044b:
						case 0x044c: case 0x044d:
						// case 0x044e: case 0x044f:
						case 0x0450: case 0x0451:
						// case 0x0452: case 0x0453:
						case 0x0454: case 0x0455:
						// case 0x0456: case 0x0457:
						case 0x0458: case 0x0459:
						// case 0x045a: case 0x045b:
						case 0x045c: case 0x045d:
						// case 0x045e: case 0x045f:
						case 0x0460: case 0x0461:
						// case 0x0461: case 0x0462:
						case 0x0464: case 0x0465:
						// case 0x0466: case 0x0467:
						case 0x0468: case 0x0469:
						// case 0x046a: case 0x046b:
						case 0x046c: case 0x046d:
						// case 0x046e: case 0x046f:
						case 0x0470: case 0x0471:
						// case 0x0472: case 0x0473:
						case 0x0474: case 0x0475:
						// case 0x0476: case 0x0477:
						case 0x0478: case 0x0479:
						// case 0x0487a: case 0x047b:
						case 0x047c: case 0x047d:
						// case 0x0487e: case 0x047f:
							// 0x440-0x441, 0x444-0x445, 0x448-0x449, 0x44c-0x44d, 0x450-0x451, 0x454-0x455, 0x458-0x459, 0x45c-0x45d, 0x460-0x461, 0x464-0x465, 0x468-0x469, 0x46c-0x46d, 0x470-0x471, 0x474-0x475, 0x478-0x479, 0x47c-0x47d
							// 010001DGKS0N
							// sprintf(hex_instruction, "D=>DSP, G=>GPE, K=>KIE, S=>SME, N=>A11");
							// sprintf(hex_description, "D{1} => display enable, G{1} => gunport enable, K{1} => key input enable, S{1} => shot memory enable, N{1} => A{11}");
#ifndef TEST_OPCODE
							flg_dsp = ((opcode & 0x0020) > 0);
if(flg_dsp)
	bool disp = true;
							flg_gpe  = ((opcode & 0x0010) > 0);
if(flg_gpe)
	bool disp = true;
							flg_kie  = ((opcode & 0x0008) > 0);
if(flg_kie)
	bool disp = true;
							flg_sme  = ((opcode & 0x0004) > 0);
if(flg_sme)
	bool disp = true;
							reg_a = (reg_a & 0x03ff) ^ ((opcode & 0x0001) << 10);
							program_counter_linear = address_polynomial_to_linear[reg_a];
#endif
						break;

						case 0x0480: case 0x0481: case 0x0482: case 0x0483: case 0x0484: case 0x0485: case 0x0486: case 0x0487:
						case 0x0488: case 0x0489: case 0x048a: case 0x048b: case 0x048c: case 0x048d: case 0x048e: case 0x048f: 
						case 0x0490: case 0x0491: case 0x0492: case 0x0493: case 0x0494: case 0x0495: case 0x0496: case 0x0497:
						case 0x0498: case 0x0499: case 0x049a: case 0x049b: case 0x049c: case 0x049d: case 0x049e: case 0x049f: 
							// 0x480-0x49f
							// 0100100KKKKK
							// sprintf(hex_instruction, "H-K=>H, +1 BOJ");
							// sprintf(hex_description, "(H{5:1} - K{5:1}){5:1} => H{5:1}, Skip if borrow");
#ifndef TEST_OPCODE
							tmp_h = ((reg_hl & 0x7c) >> 2);
							tmp_l = reg_hl & 0x03;
							tmp_k = (uint8_t)(opcode & 0x001f);
							if(tmp_h < tmp_k)
							{
								program_counter_linear++;
								reg_a = address_linear_to_polynomial[program_counter_linear];
							}
							reg_hl = (((tmp_h - tmp_k) & 0x1f) << 2) ^ tmp_l;
#endif
						break;

						// case 0x04a0: case 0x04a1: case 0x04a2: case 0x04a3: case 0x04a4: case 0x04a5: case 0x04a6: case 0x04a7:
						// case 0x04a8: case 0x04a9: case 0x04aa: case 0x04ab: case 0x04ac: case 0x04ad: case 0x04ae: case 0x04af: 
						// case 0x04b0: case 0x04b1: case 0x04b2: case 0x04b3: case 0x04b4: case 0x04b5: case 0x04b6: case 0x04b7:
						// case 0x04b8: case 0x04b9: case 0x04ba: case 0x04bb: case 0x04bc: case 0x04bd: case 0x04be: case 0x04bf: 
						// break;

						case 0x04c0: case 0x04c1: case 0x04c2: case 0x04c3: case 0x04c4: case 0x04c5: case 0x04c6: case 0x04c7:
						case 0x04c8: case 0x04c9: case 0x04ca: case 0x04cb: case 0x04cc: case 0x04cd: case 0x04ce: case 0x04cf: 
						case 0x04d0: case 0x04d1: case 0x04d2: case 0x04d3: case 0x04d4: case 0x04d5: case 0x04d6: case 0x04d7:
						case 0x04d8: case 0x04d9: case 0x04da: case 0x04db: case 0x04dc: case 0x04dd: case 0x04de: case 0x04df: 
							// 0x4c0-0x4df
							// 0100110KKKKK
							// sprintf(hex_instruction, "H+K=>H, +1 CAJ");
							// sprintf(hex_description, "(H{5:1} + K{5:1}){5:1} => H{5:1}, Skip if carry");
#ifndef TEST_OPCODE
							tmp_a = (reg_hl >> 2) + (opcode & 0x1f);
							reg_hl = ((uint8_t)(tmp_a & 0x001f) << 2) ^ (reg_hl & 0x03);
#endif
						break;

						// case 0x04e0: case 0x04e1: case 0x04e2: case 0x04e3: case 0x04e4: case 0x04e5: case 0x04e6: case 0x04e7:
						// case 0x04e8: case 0x04e9: case 0x04ea: case 0x04eb: case 0x04ec: case 0x04ed: case 0x04ee: case 0x04ef: 
						// case 0x04f0: case 0x04f1: case 0x04f2: case 0x04f3: case 0x04f4: case 0x04f5: case 0x04f6: case 0x04f7:
						// case 0x04f8: case 0x04f9: case 0x04fa: case 0x04fb: case 0x04fc: case 0x04fd: case 0x04fe: case 0x04ff: 
						// break;

						default:
							switch(opcode & 0xfc00)
							{
								case 0x0800:
									// 0x800->0xbff
									// 10KKKKKKKKKK
									// sprintf(hex_instruction, "K=>A10, JP A");
									// sprintf(hex_description, "K{10:1} => A{10:1},  Jump to A{11:1}");
#ifndef TEST_OPCODE
									reg_a = (reg_a & 0x0400) ^ (opcode & 0x03ff);
									program_counter_linear = address_polynomial_to_linear[reg_a];
#endif
								break;

								case 0x0c00:
									// 0xc00->0xfff
									// 11KKKKKKKKKK
									// sprintf(hex_instruction, "0=>A11, K=>A10, JS A");
									// sprintf(hex_description, "0{1} => A{11}, K{10:1} => A{10:1}, Push up next program address to address stack (3 levels), Jump to A{11:1}");
#ifndef TEST_OPCODE
									if(stk_a_level >= EMU_STACK_A_SIZE)
									{
										error = EMU_ERORR_STACK_A_OVERFLOW;
										return;
									}
									stk_a[stk_a_level++] = reg_a;
									reg_a = opcode & 0x03ff;
									program_counter_linear = address_polynomial_to_linear[reg_a];
#endif
								break;

								default:
									switch(opcode & 0x0f80)
									{
										case 0x0080:
											// 0x080-0xff
											// 00001KKKKKKK
											// sprintf(hex_instruction, "M-K, +1 BOJ");
											// sprintf(hex_description, "Skip if ([M[H{5:1},L{2:1}]{7:1} - K{7:1}) makes borrow");
#ifndef TEST_OPCODE
											if(memory_ram[reg_hl] < (uint8_t)(opcode & 0x007f))
											{
												program_counter_linear++;
												reg_a = address_linear_to_polynomial[program_counter_linear];
											}
#endif
										break;

										case 0x0100:
											// 0x100-0x17f
											// 00010NNKKKKK
											// sprintf(hex_instruction, "M+K => M, N => L, +1 CAJ");
											// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} + [N{2:1},K{5:1}]{7:1}) => M[H{5:1},L{2:1}]{7:1}, Skip if carry, N{2:1} => L {2:1}");
#ifndef TEST_OPCODE
											tmp_a = (uint16_t)(memory_ram[reg_hl]) + (uint16_t)(opcode & 0x007f);
											if(tmp_a > 0x007f)
											{
												program_counter_linear++;
												reg_a = address_linear_to_polynomial[program_counter_linear];
											}
											memory_ram[reg_hl] = (tmp_a & 0x007f) + ((tmp_a & 0x3f80) >> 7) - (tmp_a > 0x007f ? 1 : 0);
											reg_hl = (reg_hl & 0x7c) ^ ((uint8_t)(opcode & 0x0060) >> 5);
#endif
										break;

										case 0x0180:
											// 0x180-0x1ff
											// 00011NNKKKKK
											// sprintf(hex_instruction, "M-K => M, N => L, +1 BOJ");
											// sprintf(hex_description, "(M[H{5:1},L{2:1}]{7:1} - [N{2:1},K{5:1}]{7:1}) => M[H{5:1},L{2:1}]{7:1}, Skip if borrow, N{2:1} => L {2:1}");
#ifndef TEST_OPCODE
											if(memory_ram[reg_hl] < (uint8_t)(opcode & 0x007f))
											{
												program_counter_linear++;
												reg_a = address_linear_to_polynomial[program_counter_linear];
											}
											memory_ram[reg_hl] = (memory_ram[reg_hl] - (uint8_t)(opcode & 0x007f)) & 0x7f;
											reg_hl = (reg_hl & 0x7c) ^ ((uint8_t)(opcode & 0x0060) >> 5);
#endif
										break;

										case 0x0500:
											// 0x500-0x57f
											// 01010KKKKKKK
											// sprintf(hex_instruction, "K=>M");
											// sprintf(hex_description, "K{7:1} => M[H{5:1},L{2:1}]{7:1}");
#ifndef TEST_OPCODE
											memory_ram[reg_hl] = (uint8_t)(opcode & 0x007f);
#endif
										break;


										case 0x0580:
											// 0x580-0x5ff
											// 01011KKKKKKK
											// sprintf(hex_instruction, "K=>L,H");
											// sprintf(hex_description, "K{7:1} => [L{2:1},H{5:1}]{7:1}");
#ifndef TEST_OPCODE
											reg_hl = ((uint8_t)(opcode & 0x001f) << 2) ^ ((uint8_t)(opcode & 0x0060) >> 5);
#endif
										break;

										case 0x600:
											// 0x600-0x67f
											// 01100KKKKKKK
											// sprintf(hex_instruction, "K=>A1");
											// sprintf(hex_description, "K{7:1} => A1{7:1}");
#ifndef TEST_OPCODE
											reg_a1 = (uint8_t)(opcode & 0x007f);
#endif
										break;

										case 0x680:
											// 0x680-0x6ff
											// 01101KKKKKKK
											// sprintf(hex_instruction, "K=>A2");
											// sprintf(hex_description, "K{7:1} => A2{7:1}");
#ifndef TEST_OPCODE
											reg_a2 = (uint8_t)(opcode & 0x007f);
#endif
										break;

										case 0x700:
											// 0x700-0x77f
											// 01110KKKKKKK
											// sprintf(hex_instruction, "K=>A3");
											// sprintf(hex_description, "K{7:1} => A3{7:1}");
#ifndef TEST_OPCODE
											reg_a3 = (uint8_t)(opcode & 0x007f);
#endif
										break;

										case 0x780:
											// 0x780-0x7ff
											// 01111KKKKKKK
											// sprintf(hex_instruction, "K=>A4");
											// sprintf(hex_description, "K{7:1} => A4{7:1}");
#ifndef TEST_OPCODE
											reg_a4 = (uint8_t)(opcode & 0x007f);
#endif
										break;

										default:
											// UNKNOW OPCODE
											error = EMU_ERROR_UNKNOWN_OPCODE;
										break;
									}
								break;
							}
						break;
					}
					}
#ifdef TEST_OPCODE
	}
	bool stop = true;
#else	// ifndef TEST_OPCODE


				}
			}













	// Draw screen

//	if(flg_dsp)
//	{



		uint8_t pattern_color_index;
		uint32_t pattern_color;
		uint8_t pattern_index;
		uint8_t pattern_x;
		uint8_t pattern_y;
		uint8_t pattern_w;
		uint8_t pattern_h = 7;
		uint8_t *pattern_ptr;
		for(uint8_t hl = 0x00; hl <= 0x64; hl += 0x04)
		{
//			uint8_t hl = 0x0a << 2;//stk_nrm[--stk_nrm_level];
//		stk_nrm[stk_nrm_level] = 0x00;
			pattern_index = (memory_ram[hl+0x02] & 0x7f);
			if((pattern_index & 0x03) != 0x07)
			{
				pattern_ptr = memory_pattern + pattern_index * pattern_h;
				pattern_x = memory_ram[hl+0x01] & 0x7f;
				pattern_y = (memory_ram[hl] & 0x7e) >> 1;
				pattern_w = (pattern_index < 0x70 ? 7 : 8);
				pattern_color_index = ((memory_ram[hl] & 0x01) << 5) ^ ((memory_ram[hl+0x03] & 0x0e) << 1);
				pattern_color = emu_palette[pattern_color_index];
				line_ptr = video_buffer + pattern_x + pattern_y * video_stride;
				for (int16_t x, y = 0; y < pattern_h; y++)
				{
					for (x = 0; x < pattern_w; x++)
					{
						if(pattern_ptr[0] & (0x01 << x))
							line_ptr[x] = pattern_color;
//						else
//							line_ptr[x] = color_background;
					}
					pattern_ptr++;
					line_ptr += video_stride;
				}
			}
		}
//	}


			if(stk_nrm_level > 0)
			{

memset(stk_nrm, 0, sizeof(stk_nrm));
stk_nrm_level = 0;
			}



		}
//	}
#endif	// TEST_OPCODE
}


void c_emu::f_reset(bool reset_reg_a)
{
	memset(memory_ram, 0, sizeof(memory_ram));

	if(reset_reg_a)
	{
		program_counter_linear = 0;
		reg_a = address_linear_to_polynomial[program_counter_linear];
	}
	
	reg_hl       = 0;
	reg_l_prime  = 0;
	reg_a1       = 0;
	reg_a1_prime = 0;
	reg_a2       = 0;
	reg_a3       = 0;
	reg_a4       = 0;
	reg_fls      = 0;
	reg_frs      = 0;
	reg_mode     = 0;
	reg_x1_prime = 0;
	reg_x3       = 0;
	reg_x4       = 0;
	reg_stb      = 0;

	memset(stk_a, 0, sizeof(stk_a));
	stk_a_level = 0;

	memset(stk_nrm, 0, sizeof(stk_nrm));
	stk_nrm_level = 0;

	flg_dsp    = false;
	flg_gpe    = false;
	flg_kie    = false;
	flg_sme    = false;

	error = EMU_OK;

	nb_opcodes_current = 0;
}
